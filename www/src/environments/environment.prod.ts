export const environment = {
  production: true,
  hmr: false,
  preloadingStrategy: 'PreloadAllModules'
};

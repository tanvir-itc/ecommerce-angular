export const environment = {
  production: false,
  hmr: true,
  preloadingStrategy: 'NoPreloading',
};

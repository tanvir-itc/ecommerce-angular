import { Injectable, isDevMode } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { LOCAL_STORAGE_TOKEN, LOCAL_STORAGE_REDIRECT } from 'app/storage.constants';

@Injectable({ providedIn: 'root' })
export class LoginCheckRouteGuard implements CanActivate {
  constructor(
    private router: Router,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> {
    return this.checkLogin(state.url);
  }

  checkLogin(url: string) {
    const token = window.localStorage[LOCAL_STORAGE_TOKEN];

    if (!!token) {
      return true;
    }

    window.localStorage[LOCAL_STORAGE_REDIRECT] = url;

    if (url && url.indexOf('login') === -1) { //dont redirect to login page after login
      this.router.navigate(['/login']);
      return false;
    }
  }
}

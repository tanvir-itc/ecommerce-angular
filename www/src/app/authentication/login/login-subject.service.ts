import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class LoginSubjectService {

  loggedIn: Subject<any> = new Subject();
  loggedIn$: Observable<any> = this.loggedIn.asObservable();

}

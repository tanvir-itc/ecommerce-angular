import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


import * as _ from 'lodash';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { ApiService } from 'app/modules-core/api/api.service';
import { AuthService } from '../auth.service';
import { LoginSubjectService } from './login-subject.service';
import { LOCAL_STORAGE_USER, LOCAL_STORAGE_TOKEN, LOCAL_STORAGE_REDIRECT } from 'app/storage.constants';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: fuseAnimations
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  loginFormErrors: any;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private _formBuilder: FormBuilder,
    private router: Router,
    private apiService: ApiService,
    private authService: AuthService,
    private _loginSubjectService: LoginSubjectService,
    private route: ActivatedRoute,
  ) {
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        }
      }
    };

    // Set the defaults
    this.loginFormErrors = {
      email: {},
      password: {}
    };

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    let urlQuaryParams = _.merge({}, _.get(this.route, 'snapshot.queryParams'));
    this.loginForm = this._formBuilder.group({
      email: [_.get(urlQuaryParams, 'email', null), [Validators.required]],
      password: ['', Validators.required]
    });

    this.loginForm.valueChanges
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(() => {
        this.onLoginFormValuesChanged();
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * On form values changed
   */
  onLoginFormValuesChanged(): void {
    for (const field in this.loginFormErrors) {
      if (!this.loginFormErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.loginFormErrors[field] = {};

      // Get the control
      const control = this.loginForm.get(field);

      if (control && control.dirty && !control.valid) {
        this.loginFormErrors[field] = control.errors;
      }
    }
  }

  onSubmit() {
    this.apiService.post('users/login', this.loginForm.value)
      .subscribe(
        data => {
          window.localStorage[LOCAL_STORAGE_USER] = JSON.stringify(data);
          window.localStorage[LOCAL_STORAGE_TOKEN] = data.accessToken;

          this.authService.getCurrentUser(); //calling so that service class updates user data

          //const redirect = window.localStorage[LOCAL_STORAGE_REDIRECT];
          const redirect = false;
          if (redirect) {
            window.localStorage.removeItem(LOCAL_STORAGE_REDIRECT);
            this.router.navigateByUrl(redirect);
          } else {
            let userRole = _.chain(this.authService.getCurrentUser()).get('roles').head().get('name').value();
            if (userRole == 'ROLE_WAREHOUSE') {
              this.router.navigate(['/shipping/pending']);
            } else if (userRole == 'ROLE_SUPPLIER') {
              this.router.navigate(['/factory-shipments']);
            } else {
              this.router.navigate(['/catalogue']);
            }

          }
          this._loginSubjectService.loggedIn.next("loggedIn");
        },
        err => {


          console.log(err);
        }
      );
  }


}

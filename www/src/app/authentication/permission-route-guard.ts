import { Injectable, isDevMode } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class PermissionCheckRouteGuard implements CanActivate {
  constructor(
    private router: Router,
    public _auth: AuthService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> {
    return this.checkPermission(route);
  }

  checkPermission(route) {
    if (this._auth.hasPermission(route.data.permission)) {
      return true;
    }

    else {
      this.router.navigate(['/catalogue']);
      return false;
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { ApiService } from 'app/modules-core/api/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { APP_URL } from 'app/app.constants';

@Component({
  selector: 'password-reset-form',
  templateUrl: './password-reset-form.component.html',
  styleUrls: ['./password-reset-form.component.scss'],
  animations: fuseAnimations
})
export class PasswordResetFormComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  forgotPasswordFormErrors: any;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {FormBuilder} _formBuilder
   */

  data = <any>{
    password: null,
    confirmPassword: null
  }
  queryParams = <any>{}

  constructor(
    private _fuseConfigService: FuseConfigService,
    private _formBuilder: FormBuilder,
    private apiService: ApiService,
    private snackBar: SnackbarService,
    private _router: Router,
    private route: ActivatedRoute,
  ) {
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        }
      }
    };

    // Set the defaults
    this.forgotPasswordFormErrors = {
      email: {}
    };

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.queryParams = _.merge({}, _.get(this.route, 'snapshot.queryParams'));
  }



  resetPassword() {
    if (!this.data.password) {
      this.snackBar.open(`Please! Enter password!`, 'Close');
      return;
    }
    if (!this.data.confirmpassword) {
      this.snackBar.open(`Please! Enter confirm password!`, 'Close');
      return;
    }

    if (this.data.password != this.data.confirmpassword) {
      this.snackBar.open(`Password and confirm password does not match!`, 'Close');
      return;
    }
    let payload = {
      token: _.get(this.queryParams, 'token'),
      password: this.data.password,
      confirmPassword: this.data.password,
      redirectUrl: `${APP_URL}login`,
    };
    this.apiService.putApi('users/reset-password/v1', payload)
      .subscribe(data => {
        this.data = <any>{
          password: null
        }
        this.snackBar.open(`Password change successfully!`, 'Close');
        this._router.navigate(['login'], { queryParams: { email: _.get(data, 'data.email', null) } });
      });
  }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'app/modules-core/common/user/user.service';

@Component({
  selector: 'email-change-verify',
  templateUrl: './email-change-verify.component.html',
  styleUrls: ['./email-change-verify.component.scss'],
  animations: fuseAnimations
})
export class EmailChangeVerifyComponent implements OnInit, OnDestroy {
  message: any = null
  private _unsubscribeAll: Subject<any>;
  constructor(
    private _fuseConfigService: FuseConfigService,
    private _userService: UserService,
    private snackBar: SnackbarService,
    private route: ActivatedRoute,
  ) {
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        }
      }
    };

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }


  ngOnInit(): void {
    let urlParams = _.merge({}, _.get(this.route, 'snapshot.queryParams'));
    let payload = {
      verificationHash: _.get(urlParams, 'token'),
      id: _.get(urlParams, 'userid'),
      email: _.get(urlParams, 'email'),
    };
    this._userService.verifyEmail(payload).subscribe(res => {
      this.message = 'Email verification completed. Please re-login with your updated email address and existing password.';
    })

  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { EmailChangeVerifyComponent } from './email-change-verify.component';

const routes = [
  {
    path: 'email-verify',
    component: EmailChangeVerifyComponent
  },
];

@NgModule({
  declarations: [
    EmailChangeVerifyComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    ButtonModule,
    FuseSharedModule
  ]
})
export class EmailChangeVerifyModule {
}

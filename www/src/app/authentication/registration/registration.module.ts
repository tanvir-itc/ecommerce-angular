import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { RegistrationComponent } from './registration.component';
import { MaterialModule } from 'app/modules-core/utility/material.module';

const routes = [
  {
    path: 'registration',
    component: RegistrationComponent
  }
];

@NgModule({
  declarations: [
    RegistrationComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    MaterialModule,

    FuseSharedModule
  ]
})
export class RegistrationModule {
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { CustomerService } from 'app/modules-core/merchandising/customers/customer.service';
import { ProvinceService } from 'app/modules-core/common/province/province.service';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { APP_URL } from 'app/app.constants';

@Component({
  selector: 'customer-registration',
  templateUrl: './customer-registration.component.html',
  styleUrls: ['./customer-registration.component.scss'],
  animations: fuseAnimations
})
export class CustomerRegistrationComponent implements OnInit {
  provinceList: any[] = [];
  businessYears: any[] = [];

  newCustomer: any = {};

  constructor(
    private _fuseConfigService: FuseConfigService,
    private router: Router,
    private _provinceService: ProvinceService,
    private _customerService: CustomerService,
    private route: ActivatedRoute,
    private snackBar: SnackbarService,
  ) {
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        }
      }
    };


  }

  ngOnInit(): void {
    this.getAllProvinces();
    for (let i = 0; i < 201; i++) {
      this.businessYears.push(_.add(0, i));
    }
    this.clearCustomerObject();
  }



  onSubmit() {
    let payload = _.merge({}, this.newCustomer);
    if (!payload.contactFirstName) {
      this.snackBar.open(`Please enter first name`, "Close");
      return;
    }
    if (!payload.contactLastName) {
      this.snackBar.open(`Please enter last name`, "Close");
      return;
    }
    if (!payload.email) {
      this.snackBar.open(`Please enter email`, "Close");
      return;
    }
    if (!payload.name) {
      this.snackBar.open(`Please enter company`, "Close");
      return;
    }
    if (!payload.phoneNo) {
      this.snackBar.open(`Please enter phone number`, "Close");
      return;
    }

    payload['requested'] = true;
    let urlParams = _.merge({}, _.get(this.route, 'snapshot.queryParams'));
    if (_.get(urlParams, 'userid')) {
      payload.saleRepCustomers = [{
        userId: _.get(urlParams, 'userid')
      }]
    }
    payload.user = {
      roleName: "ROLE_CUSTOMER",
      firstName: payload.contactFirstName,
      lastName: payload.contactLastName,
      email: payload.email,
      company: payload.name,
      phoneNo: payload.phoneNo,
    }

    payload['appUrl'] = `${APP_URL}login`;

    this._customerService.create(payload).subscribe(resp => {
      this.clearCustomerObject();
      this.snackBar.open(`Registration successfully done. Please, check your email inbox for email verification and password set.`, "Close");
    });
  }


  clearCustomerObject() {
    this.newCustomer = {
      contactFirstName: null,
      contactLastName: null,
      email: null,
      name: null,
      phoneNo: null,
      user: {},
    }
  }

  getAllProvinces() {
    this._provinceService.getAll().subscribe((data) => {
      this.provinceList = data;
    });
  }


}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { CustomerRegistrationComponent } from './customer-registration.component';

const routes = [
  {
    path: 'customer-registration',
    component: CustomerRegistrationComponent
  }
];

@NgModule({
  declarations: [
    CustomerRegistrationComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    MaterialModule,

    FuseSharedModule
  ]
})
export class CustomerRegistrationModule {
}

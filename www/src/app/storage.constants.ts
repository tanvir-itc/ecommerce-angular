export const LOCAL_STORAGE_TOKEN = 'ecomCanadaAccessToken';
export const LOCAL_STORAGE_USER = 'ecomCanadaUser';
export const LOCAL_STORAGE_REDIRECT = 'ecomCanadaRedirectUrl';
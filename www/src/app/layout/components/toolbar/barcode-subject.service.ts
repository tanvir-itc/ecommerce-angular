import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class BarcodeSubjectService {
  routeWithBarcode: Subject<any> = new Subject();
  routeWithBarcode$: Observable<any> = this.routeWithBarcode.asObservable();
}

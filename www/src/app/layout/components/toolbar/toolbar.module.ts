import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatIconModule, MatMenuModule, MatToolbarModule, MatBadgeModule } from '@angular/material';

import { FuseSearchBarModule, FuseShortcutsModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

import { ToolbarComponent } from 'app/layout/components/toolbar/toolbar.component';

import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DiaryModule } from 'app/modules-core/diary/diary.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { CustomerDetailsModule } from 'app/modules-core/merchandising/customers/components/customer-details-update-modal/customer-details.module';
import { UserProfileUpdateModalModule } from 'app/modules-core/common/user/components/user-profile-update-modal/user-profile-update-modal.module';

@NgModule({
  declarations: [
    ToolbarComponent
  ],
  imports: [
    RouterModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule,
    MatBadgeModule,
    FuseSharedModule,
    FuseSearchBarModule,
    FuseShortcutsModule,
    DiaryModule,
    NgProgressModule.forRoot(),
    NgProgressHttpModule,
    ButtonModule,
    GeneralModule,
    DateTimeModule,
    MaterialModule,
    CustomerDetailsModule,
    UserProfileUpdateModalModule
  ],
  exports: [
    ToolbarComponent
  ]
})
export class ToolbarModule {
}

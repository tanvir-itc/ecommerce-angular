import { Component, ViewEncapsulation } from '@angular/core';
import { QuickPanelService } from './quick-panel.service';
import { MatSnackBar } from '@angular/material';
import { Message } from '@stomp/stompjs';
import * as _ from 'lodash';
import { joinArrayObjectValues } from "app/modules-core/utility/helpers";
import { AuthService } from 'app/authentication/auth.service';
import { QuickPanelSubjectService } from './quick-panel-subject.service';
import { LoginSubjectService } from 'app/authentication/login/login-subject.service';
import { LOCAL_STORAGE_TOKEN } from 'app/storage.constants';
@Component({
  selector: 'quick-panel',
  templateUrl: './quick-panel.component.html',
  styleUrls: ['./quick-panel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class QuickPanelComponent {
  date: Date;
  events: any[];
  notes: any[];
  settings: any;
  notificationList = null;
  Notification$;
  notificationSubscription;
  countNotification = 0;
  userId = null;
  notificationsFromWebSocket: any[] = [];
  /**
   * Constructor
   */
  constructor(
    private _quickPanelService: QuickPanelService,
    private snackBar: MatSnackBar,
    public _authService: AuthService,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _loginSubjectService: LoginSubjectService,
  ) {
    // Set the defaults
    this.date = new Date();
    this.settings = {
      notify: true,
      cloud: false,
      retro: true
    };
  }

  ngOnInit() {


  }


}

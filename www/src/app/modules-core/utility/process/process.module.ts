import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { ProcessStepsComponent } from './process-steps.component';

@NgModule({
  declarations: [
    ProcessStepsComponent,
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FuseSharedModule,
  ],
  exports: [
    ProcessStepsComponent,
  ]
})

export class ProcessModule {
}

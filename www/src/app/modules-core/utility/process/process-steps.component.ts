import { Component, EventEmitter, Output, Input, OnInit } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'process-steps',
  template: `
    <div fxLayout
      class="process">
      <div fxFlex
        class="process-item"
        *ngFor="let step of steps; let i = index"
        [ngClass]="{'process-current-state': step.flag}">
        <div class="process-pointer">
        </div>
        <div class="process-line"></div>
        <div class="process-title">{{step?.label}}</div>
      </div>
    </div>
  `,
})

export class ProcessStepsComponent implements OnInit {

  @Input()
  list: any[] = [];

  @Input()
  type: string = null;

  @Input()
  steps: any[] = [];

  ngOnInit(): void {
    if (this.type == "sample-type") {
      this.sampleTypeTransformer();
    }
  }

  sampleTypeTransformer() {
    this.steps = _.map(this.list, v => {
      let s = {
        label: null,
        flag: false
      };
      s.label = _.get(v, "sampleType.label");
      s.flag = _.get(v, 'status.name') == "approved" || false;

      return s;
    });
  }

}

import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material";

@Injectable({
  providedIn: "root",
})
export class SnackbarService {
  defaultConfig = {
    duration: 6000,
  }
  constructor(private snackBar: MatSnackBar) { }

  open(message: string, action: string, disabledAutoHide: boolean = false) {
    disabledAutoHide ? this.defaultConfig.duration = null : null;
    this.snackBar.open(message, action, this.defaultConfig);
  }

}
import { Component, EventEmitter, Output, Input, SimpleChanges } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'autocomplete-base',
  template: ``,
})

export class AutocompleteBase {
  items = [];
  preSelectId = null;

  @Input()
  hidePlaceholder = false;

  @Input()
  selectedId = null;

  @Input()
  postLoad = false;

  //list will be loaded (e.g., API call) when dropdown clicked
  @Input()
  lazyload = false;

  @Input()
  preSelectIfOnlyOne = false;

  @Output()
  change = new EventEmitter();

  ngAfterContentInit() {
    this.selectedId && this.onSelect(_.find(this.items, { 'id': this.selectedId }) || { id: this.selectedId });
  }

  onSelect($event) {
    this.change.emit(($event) || null);
  }

  preSelect() {
    let preSelect = _.find(this.items, { 'id': this.selectedId }) || _.find(this.items, ['isDefault', true]);
    let preSelectId = preSelect && _.get(preSelect, 'id');

    if (!preSelectId && (!this.lazyload && this.preSelectIfOnlyOne)) {
      if (this.items.length === 1) {
        preSelectId = _.get(this.items, '0.id');
        preSelect = _.get(this.items, '0');
      }
    }

    this.preSelectId = preSelectId;

    this.onSelect(preSelect);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.get(changes, 'selectedId.currentValue') && !_.get(changes, 'selectedId.firstChange')) {
      this.preSelect();
      return;
    }
    this.preSelectId = null;
  }

  reset() {
    this.preSelectId = null;
  }

  loadItems() {
    //override in child
  }

  //list is loaded (e.g., API call) when dropdown clicked
  onOpen($event) {
    if (this.lazyload) {
      this.loadItems();
    }
  }

}

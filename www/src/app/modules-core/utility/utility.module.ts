import { NgModule } from "@angular/core";
import { DateTimeModule } from "./date-time-picker/date-time.module";
import { NiceDatePipe } from "./nice-date.pipe";
import { AutocompleteBase } from "./autocomplete-base";
import { PageTitleService } from "./page-title.service";


@NgModule({
  declarations: [
    NiceDatePipe,
    AutocompleteBase,
  ],
  imports: [
    DateTimeModule,
  ],
  exports: [
    DateTimeModule,
    NiceDatePipe,
  ],
  providers: [
    PageTitleService,
  ]
})
export class UtilityModule {
  constructor(titleService: PageTitleService) {
    titleService.init();
  }
}
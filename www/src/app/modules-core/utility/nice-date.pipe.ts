import { Pipe, PipeTransform } from '@angular/core';
import { niceDateTime } from './helpers';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({ name: 'niceDate' })
export class NiceDatePipe implements PipeTransform {
  transform(value: number) {
    return niceDateTime(value);
  }
}
import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { toDateString } from 'app/modules-core/utility/helpers';
import * as _ from 'lodash';

@Component({
  selector: 'date-picker',
  template: `
    <div class="dx-field" style="min-width: 150px;">
        <div *ngIf="!hideLabel" class="dx-field-label"> {{labelName}} </div>
        <div [ngClass]="{'dx-field-label': !hideLabel}">
            <dx-date-box
            [value]="dateValue"
            [disabled]="disabled"
            [min]="minValue"
            [max]="maxValue"
            type="date"
            [displayFormat]="displayFormat"
            [placeholder]="labelName"
            [showClearButton]="true"
            [focusStateEnabled]="false"
            (onValueChanged)="onChange($event)">
            </dx-date-box>
        </div>
    </div>`
})

export class DateComponent implements OnInit {

  @Input()
  dateValue = null;

  @Input()
  labelName = null;

  @Input()
  disabled = false;

  @Input()
  minValue = null;

  @Input()
  maxValue = null;

  //https://github.com/globalizejs/globalize/blob/master/doc/api/date/date-formatter.md#using-open-ended-skeletons
  @Input()
  displayFormat = 'MMM d, y';

  @Input()
  hideLabel: boolean;

  @Output()
  change = new EventEmitter();

  constructor(
  ) { }

  ngOnInit() {
    if (this.dateValue) {
      this.onChange({
        value: toDateString(this.dateValue),
        init: true
      });
    }
  }

  onChange($event) {
    this.change.emit({
      date: $event.value,
      dateFormatted: $event.value && toDateString($event.value),
      init: _.get($event, 'init', false),
    });
  }

}

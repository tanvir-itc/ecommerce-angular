import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import * as _ from 'lodash';
import { getDays } from 'app/modules-core/utility/helpers';


@Component({
  selector: 'day-autocomplete',
  template: `
    <ng-select [items]="days"
              [clearable]="false"
              bindLabel="label"
              bindValue="value"
              placeholder="{{hidePlaceholder ? '' : 'Day'}}"
              [(ngModel)]="selectedId"
              (change)="onSelect($event)">
    </ng-select>
  `,
})

export class DayAutocomplete implements OnInit {
  days = [];

  @Input()
  hidePlaceholder = false;

  @Input()
  selectedId;

  @Input()
  fabricTypeSubject = null;

  @Output()
  change = new EventEmitter();

  constructor(
  ) { }

  ngOnInit() {

    this.days = getDays();


  }

  onSelect($event) {
    this.change.emit(($event && $event.value) || null);
  }

}

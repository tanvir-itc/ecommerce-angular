import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { toTimeString, toDateTimeString, toDateString } from 'app/modules-core/utility/helpers';
import * as moment from 'moment';

@Component({
  selector: 'time-picker',
  template: `
    <div class="dx-field">
        <div *ngIf="!hideLabel" class="dx-field-label"> {{labelName}} </div>
        <div [ngClass]="{'dx-field-label': !hideLabel}">
            <dx-date-box
            [value]="time"
            type="datetime"
            [showClearButton]="true"
            [focusStateEnabled]="false"
            [displayFormat]="displayFormat"
            (onOpened)="opened($event)"
            (onValueChanged)="onChange($event)">
            </dx-date-box>
        </div>
    </div>`
})

export class TimeComponent implements OnInit {

  @Input()
  dateValue;

  time;

  @Input()
  labelName;

  @Input()
  hideLabel: boolean;

  @Input()
  displayFormat = 'HH:mm:ss';

  @Output()
  change = new EventEmitter();

  constructor(
  ) {

  }

  opened(e) {
    e.component
      .content()
      .getElementsByClassName("dx-box-item")[0].style.display =
      "none";
  }

  ngOnInit() {
    if (this.dateValue) {
      this.time = toDateString(moment()) + ' ' + this.dateValue;
    } else {
      this.time = toDateTimeString(moment());
    }

    this.onChange({
      value: toDateTimeString(this.time)
    });
  }

  onChange($event) {
    this.change.emit({
      date: $event.value,
      dateFormatted: toTimeString($event.value)
    });
  }

}

import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { toDateTimeString } from 'app/modules-core/utility/helpers';

@Component({
  selector: 'date-time-picker',
  template: `
    <div class="dx-field">
        <div *ngIf="!hideLabel" class="dx-field-label"> {{labelName}} </div>
        <div [ngClass]="{'dx-field-label': !hideLabel}">
            <dx-date-box
            [value]="dateValue"
            type="datetime"
            [showClearButton]="true"
            [focusStateEnabled]="false"
            (onValueChanged)="onChange($event)">
            </dx-date-box>
        </div>
    </div>`
})

export class DateTimeComponent implements OnInit {

  @Input()
  dateValue;

  @Input()
  labelName;

  @Input()
  hideLabel: boolean;

  @Output()
  change = new EventEmitter();

  constructor(
  ) { }

  ngOnInit() {
    this.onChange({
      value: toDateTimeString(this.dateValue)
    });
  }

  onChange($event) {
    this.change.emit({
      date: $event.value,
      dateFormatted: toDateTimeString($event.value)
    });
  }

}

import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import * as _ from 'lodash';
import { extend } from 'webdriver-js-extender';
import { BtnBaseComponent } from './btn-base.component';

@Component({
  selector: 'btn-icon',
  template: `
    <button [disabled]="disabled" mat-icon-button class="w-20 h-20" [ngClass]="active.iconclass" style="line-height: 20px !important;"
      matTooltip="{{active?.text}}"
      (click)="handleClick($event)">
      <mat-icon class="s-16" *ngIf="!faicon">{{active.icon}}</mat-icon>
      <span *ngIf="faicon" [ngClass]="faicon"></span>
    </button>
  `,
  styleUrls: ['./btn.component.scss']
})

export class BtnIconComponent extends BtnBaseComponent implements OnInit {

  ngOnInit(): void {
    this.init();
  }

};

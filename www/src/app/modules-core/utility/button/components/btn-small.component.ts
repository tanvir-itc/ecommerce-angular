import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import * as _ from 'lodash';
import { extend } from 'webdriver-js-extender';
import { BtnBaseComponent } from './btn-base.component';

@Component({
  selector: 'btn-small',
  template: `
    <ng-container *ngIf="!modalClose">
      <button [disabled]="disabled" mat-stroked-button class="btn-small" [ngClass]="active.smallclass"
        (click)="handleClick($event)">
        <span fxFlex fxFlexAlign="center">{{active.text}}</span>
      </button>
    </ng-container>
    <ng-container  *ngIf="modalClose">
      <button [disabled]="disabled" mat-stroked-button  class="btn-small" [ngClass]="active.smallclass"
        mat-dialog-close
        (click)="handleClick($event)">
        <span fxFlex fxFlexAlign="center">{{active.text}}</span>
      </button>
    </ng-container>
  `,
  styleUrls: ['./btn.component.scss']
})

export class BtnSmallComponent extends BtnBaseComponent implements OnInit {

  ngOnInit(): void {
    this.init();
  }

};

export class ListPageBaseClass {
  pageSizeOptions = [20, 40, 50, 100];
  pageSize = 20;
  pageIndex = 0
}
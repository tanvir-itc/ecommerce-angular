import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "app/modules-core/utility/material.module";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { FuseSharedModule } from "@fuse/shared.module";
import { NgSelectModule } from "@ng-select/ng-select";
import { DateTimeModule } from "app/modules-core/utility/date-time-picker/date-time.module";
import { GeneralModule } from "app/modules-core/general/general.module";
import { LoginCheckRouteGuard } from "app/authentication/check-login-route-guard";
import { ButtonModule } from "app/modules-core/utility/button/button.module";

import { AttachmentModule } from "app/modules-core/common/attachment/attachment.module";
import { ProfileComponent } from "./profile.component";

const routes = [
  {
    path: "profile",
    component: ProfileComponent,
    canActivate: [LoginCheckRouteGuard],
  }
];

@NgModule({

  declarations: [
    ProfileComponent,
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    NgSelectModule,
    DateTimeModule,
    GeneralModule,
    ButtonModule,
    AttachmentModule,
  ],
  exports: []
})
export class ProfileModule { }

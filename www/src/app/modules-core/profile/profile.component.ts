import { Component, OnInit, TemplateRef } from '@angular/core';
import * as _ from 'lodash';
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'app/modules-core/common/user/user.service';
import { MatDialog } from '@angular/material';
import { AuthService } from 'app/authentication/auth.service';
import { fuseAnimations } from '@fuse/animations';
import { SnackbarService } from '../utility/snackbar.service';
import { APP_URL } from 'app/app.constants';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  animations: fuseAnimations,
})
export class ProfileComponent implements OnInit {

  id: any = null;

  user: any = null;

  newUser: any = null;
  newPass: any = null;
  newEmail: any = null;
  modalRef: any = null;


  constructor(
    private route: ActivatedRoute,
    private _userService: UserService,
    private _router: Router,
    private snackBar: SnackbarService,
    private auth: AuthService,
    private dialog: MatDialog,

    private _quickPanelSubjectService: QuickPanelSubjectService
  ) {
    this._quickPanelSubjectService.heading.next('My Profile');
  }

  ngOnInit() {
    this.id = this.auth.currentUser.id;
    this.getUserDetails();
  }

  getUserDetails() {
    this.getUser();
  }

  getUser() {
    this._userService.getUser(this.id)
      .subscribe(user => this.user = user);
  }


  openEditModal(templateRef) {
    this.newUser = {
      firstName: _.get(this.user, 'firstName'),
      lastName: _.get(this.user, 'lastName'),
      phoneNo: _.get(this.user, 'phoneNo'),
      company: _.get(this.user, 'company'),
      address: _.get(this.user, 'address'),
      id: this.id,
    }
    this.modalRef = this.dialog.open(templateRef, {
      width: "600px",
    });
  }

  userUpdate() {
    if (!_.get(this.newUser, "firstName")) {
      this.snackBar.open(`Please! Insert first name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "lastName")) {
      this.snackBar.open(`Please! Insert last name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "phoneNo")) {
      this.snackBar.open(`Please! Insert phone no!`, "Close");
      return;
    }


    this._userService.updateFields(this.id, this.newUser).subscribe(resp => {
      this.snackBar.open(`User updated!`, 'Close');
      this.modalRef.close();
      this.getUser();
    });
  }

  openPassChangeModal(templateRef: TemplateRef<any>) {
    this.newPass = {
      confirmPassword: null,
      password: null,
      oldPassword: null,
    }
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "35%"
    });
  }
  openEmailChangeModal(templateRef: TemplateRef<any>) {
    this.newEmail = {
      email: null,
    }
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "400px"
    });
  }

  savePassword() {
    if (this.newPass.password !== this.newPass.confirmPassword) {
      this.snackBar.open(`New password and confirm password must be same!`, 'Close');
      return
    }
    if (!this.newPass.oldPassword) {
      this.snackBar.open(`Please! Enter current password!`, 'Close');
      return
    }
    var newPasswordData = {
      password: this.newPass.password,
      confirmPassword: this.newPass.confirmPassword,
      oldPassword: this.newPass.oldPassword,
      userId: _.get(this.auth.getCurrentUser(), 'id'),
    }
    this._userService.updatePassword(newPasswordData).subscribe(res => {
      this.snackBar.open(`Password  updated!`, 'Close');
      this.getUser();
      this.modalRef.close(close);
    })
  }

  saveEmail() {
    if (!this.newEmail.email) {
      this.snackBar.open(`Enter your new email address`, 'Close');
      return
    }
    let payload = {
      email: _.get(this.newEmail, 'email'),
      userId: this.id,
      redirectUrl: `${APP_URL}email-verify`,
    }
    this._userService.updateEmail(payload).subscribe(res => {
      this.snackBar.open(`A verification link has been sent to your new email address.`, 'Close');
      this.getUser();
      this.modalRef.close(close);
    })
  }

}

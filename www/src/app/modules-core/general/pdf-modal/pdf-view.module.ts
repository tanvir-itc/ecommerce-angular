import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { FuseSharedModule } from "@fuse/shared.module";
import { MaterialModule } from "app/modules-core/utility/material.module";
import { ButtonModule } from "app/modules-core/utility/button/button.module";
import { PdfModalComponent } from "./pdf-modal.component";
import { NgxSummernoteModule } from "ngx-summernote";

@NgModule({
  declarations: [
    PdfModalComponent
  ],
  entryComponents: [
    PdfModalComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    ButtonModule,
    NgxSummernoteModule
  ],
  exports: [
    PdfModalComponent
  ],
  providers: [

  ]
})
export class PdfViewModule {

}
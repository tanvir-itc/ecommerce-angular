import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from "@angular/material";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { AuthService } from "app/authentication/auth.service";
import { EmailService } from "app/modules-core/common/email/email.service";
import { UserService } from "app/modules-core/common/user/user.service";
import { DigitalCatalogueService } from "app/modules-core/merchandising/digital-catalogue/digital-catalogue.service";
import { ShipmentService } from "app/modules-core/merchandising/shipping/shipment.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import * as _ from 'lodash';

@Component({
  selector: "pdf-modal",
  templateUrl: "pdf-modal.component.html"
})
export class PdfModalComponent {
  title: any = "PDF"
  resource: SafeResourceUrl = null;
  emailButton = false;
  emailObject = {};
  email: any = {};

  userList: any[] = [];

  sendInvitationModal: any = null;
  contactModal: any = null;
  config: any = {
    placeholder: '',
    lineheight: 1,
    tabsize: 1,
    height: 450,
    uploadImagePath: '',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo']],
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table']],
    ],
    fontNames: ['Helvetica', 'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Roboto', 'Times']
  }

  btnDisabled: any = false;



  constructor(
    public dialogRef: MatDialogRef<PdfModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    public sanitizer: DomSanitizer,
    public _shipmentService: ShipmentService,
    public _digitalCatalogueService: DigitalCatalogueService,
    public _emailService: EmailService,
    private snackBar: SnackbarService,
    private _userService: UserService,
    private auth: AuthService,
    private dialog: MatDialog,
  ) {
    this.title = data.title ? data.title : "PDF Viewer";
    this.emailButton = data.emailButton ? data.emailButton : false;
    this.resource = this.sanitizer.bypassSecurityTrustResourceUrl(data.resource);

    if (this.emailButton) {
      this.emailObject = data.emailObject
    }
  }

  openEmailSentModal(templateRef) {
    this.btnDisabled = false;
    if (_.get(this.emailObject, 'type') == 'invoice') {
      this._shipmentService.getInvoiceEmailBody(_.get(this.emailObject, 'shipmentId')).subscribe(resp => {
        this.email = {
          emailBody: resp.body,
          attachments: resp.attachments,
          ccList: resp.ccList ? _.join(resp.ccList, ',') : null,
          subject: resp.subject,
          to: resp.to,
          from: resp.from
        }
        this.sendInvitationModal = this.dialog.open(templateRef, {
          minWidth: "1200px"
        });
      })
    }
    if (_.get(this.emailObject, 'type') == 'packing') {
      this._shipmentService.getPackingEmailBody(_.get(this.emailObject, 'shipmentId')).subscribe(resp => {
        this.email = {
          emailBody: resp.body,
          attachments: resp.attachments,
          ccList: resp.ccList ? _.join(resp.ccList, ',') : null,
          subject: resp.subject,
          to: resp.to,
          from: resp.from
        }
        this.sendInvitationModal = this.dialog.open(templateRef, {
          minWidth: "1200px"
        });
      })
    }

    if (_.get(this.emailObject, 'type') == 'digital-catalogue') {
      this._digitalCatalogueService.getEmailContent(_.get(this.emailObject, 'digitalCatalogueId')).subscribe(resp => {
        let user = this.auth.currentUser
        this.email = {
          emailBody: resp.body,
          attachments: resp.attachments,
          ccList: resp.ccList ? _.join(resp.ccList, ',') : null,
          subject: resp.subject,
          to: "",
          from: user.email,
          fromName: `${user.firstName} ${user.lastName}`
        }

        this.sendInvitationModal = this.dialog.open(templateRef, {
          minWidth: "1200px"
        });
      })
    }
  }

  closeModal() {
    this.sendInvitationModal.close();
  }

  sendEmail() {
    this.btnDisabled = true;
    let payload = {
      body: this.email.emailBody,
      attachments: this.email.attachments,
      ccList: _.split(this.email.ccList, ','),
      subject: this.email.subject,
      to: this.email.to,
      from: this.email.from
    }
    if (_.get(this.emailObject, 'type') == 'invoice') {
      payload['shipmentId'] = _.get(this.emailObject, 'shipmentId')
    }
    if (_.get(this.emailObject, 'type') == 'digital-catalogue') {
      this._digitalCatalogueService.sendMail(_.get(this.emailObject, 'digitalCatalogueId'), payload).subscribe(resp => {
        this.btnDisabled = false;
        this.closeModal();
        this.snackBar.open(`Email has been sent successfully.`, 'Close');
      }, error => {
        this.btnDisabled = false;
      })
    } else {
      this._emailService.create(payload).subscribe(resp => {
        this.btnDisabled = false;
        this.closeModal();
        this.snackBar.open(`Email has been sent successfully.`, 'Close');
      }, error => {
        this.btnDisabled = false;
      })
    }

  }
  openContactModal(templateRef) {
    let queryParams = {
      rolename: 'ROLE_CUSTOMER'
    }

    this._userService.getAll(queryParams).subscribe(data => {
      this.userList = data
      this.contactModal = this.dialog.open(templateRef, {
        maxWidth: "1200px"
      });
    });

  }

  closeContactModal() {
    this.contactModal.close();
  }

  addToEmailList() {
    let emails = _.chain(this.userList).filter(['select', true]).map('email').join(',').value()
    this.email.to = emails
    this.closeContactModal();
  }

  selectFlag: any = false;
  selectAll() {
    this.userList = _.map(this.userList, item => {
      item.select = this.selectFlag
      return item;
    })
  }
}
import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: 'input[numberonly]'
})
export class NumberOnlyDirective {
  @Input('decimals') decimals: any = 2;

  private regex: RegExp = null;
  private specialKeys: Array<string> = ['Backspace', 'Tab'];

  constructor(private el: ElementRef) { }

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    this.decimals = parseInt(this.decimals);
    if (this.decimals) {
      this.regex = new RegExp("^\\s*((\\d+(\\.\\d{0," +
        this.decimals +
        "})?)|((\\d*(\\.\\d{1," +
        this.decimals +
        "}))))\\s*$");
    } else {
      this.regex = new RegExp(/^[0-9]*$/g);
    }
    const current: string = this.el.nativeElement.value;
    const next: string = current.concat(event.key);
    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }
  }

  @HostListener('paste', ['$event'])
  onPaste(event: KeyboardEvent) {
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    this.decimals = parseInt(this.decimals);
    if (this.decimals) {
      this.regex = new RegExp("^\\s*((\\d+(\\.\\d{0," +
        this.decimals +
        "})?)|((\\d*(\\.\\d{1," +
        this.decimals +
        "}))))\\s*$");
    } else {
      this.regex = new RegExp(/^[0-9]*$/g);
    }
    const current: string = this.el.nativeElement.value;
    const next: string = current.concat(event.key);
    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }
  }




}
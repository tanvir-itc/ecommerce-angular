import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';


@Component({
  selector: 'sorter-icon',
  template: `
    <span *ngIf="flag" >
      <span *ngIf="!sortType" class="fas fa-sort">
      </span>
      <span *ngIf="sortType=='asc'" class="fas fa-sort-up">
      </span>
      <span *ngIf="sortType=='desc'" class="fas fa-sort-down">
      </span>
    </span>
    <span *ngIf="!flag" class="fas fa-sort" style="color: #ccc">
    </span>
  `,
})
export class SorterIconComponent implements OnInit, OnChanges {
  @Input()
  flag: boolean = false;

  @Input()
  sortType: any = false;


  constructor(
  ) {
  }
  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

}

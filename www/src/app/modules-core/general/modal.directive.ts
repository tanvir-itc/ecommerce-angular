import { Directive, Input, TemplateRef, Output, EventEmitter, HostListener } from "@angular/core";
import { MatDialogConfig, MatDialog } from "@angular/material";
import * as _ from 'lodash';

@Directive({ selector: '[modal]' })
export class ModalShowOnClickDirective {
  _modalConf: MatDialogConfig = { //default
    disableClose: false,
  }

  _modalRef;

  @Input('modal')
  templateRef: TemplateRef<any>;

  @Input('modal-conf')
  modalConf: MatDialogConfig;

  @Output('modal-ref')
  modalRef: EventEmitter<any> = new EventEmitter();

  @Output('modal-on-close')
  modalOnClose: EventEmitter<any> = new EventEmitter();

  constructor(
    private dialog: MatDialog,
  ) { }

  @HostListener('click', ['$event']) onClick($event) {
    if (!this.templateRef) {
      throw new Error('Modal Directive - TemplateRef not found or not provided');
    }

    this._modalRef = this.dialog.open(
      this.templateRef,
      _.merge(this._modalConf, this.modalConf)
    );

    this.modalRef.emit(this._modalRef);

    this._modalRef.afterClosed().subscribe(result => {
      this.modalOnClose.emit(result);
    });
  }
} //class
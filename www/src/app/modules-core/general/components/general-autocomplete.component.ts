import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import * as _ from 'lodash';
import { AutocompleteBase } from 'app/modules-core/utility/autocomplete-base';

@Component({
  selector: 'general-autocomplete',
  template: `
    <ng-select [items]="items"
              [clearable]="false"
              bindLabel="{{bindLabel}}"
              bindValue="id"
              placeholder="{{hidePlaceholder ? '' : name}}"
              [(ngModel)]="preSelectId"
              (change)="onSelect($event)">
    </ng-select>
  `,
})

export class GeneralAutocomplete extends AutocompleteBase implements OnInit {

  @Input()
  items = [];

  @Input()
  bindLabel = "label";

  @Input()
  name: string = 'Please Select';

  constructor(
  ) { super(); }

  ngOnInit() {

  }

  ngAfterContentInit() {
    this.preSelect();
  }

}

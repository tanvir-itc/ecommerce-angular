import { Component, OnInit, TemplateRef } from '@angular/core';
import { SettingsChildBase } from '../settings-child-base';
import { MatDialog } from '@angular/material';
import * as _ from 'lodash';
import { AuthService } from 'app/authentication/auth.service';
import { StyleTypeService } from 'app/modules-core/common/style-type/style-type.service';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
@Component({
  selector: 'app-settings-style-type',
  templateUrl: './settings-style-type.component.html',
})
export class SettingsStyleTypeComponent extends SettingsChildBase implements OnInit {
  styleType = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: 'detail'
  };
  modalRef = null;

  constructor(
    private styleTypeService: StyleTypeService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }


  init() {
    this.styleTypeService.getAll().subscribe((data) => {
      this.styleType.items = data;
    });
  }

  typeOnDetailClick(typeId) {
    this.styleType.createOrDetail = 'detail'
    this.setActiveRight('settings2right');
    this.styleTypeService.get(typeId).subscribe(res => {
      this.styleType.detail = res;
    })
  }

  typeOnDeleteClick(styleTypeId) {
    if (confirm("Type Delete confirm?")) {
      this.styleTypeService.delete(styleTypeId).subscribe(res => {
        this.setActiveRight(false);
        this.init();
        this.snackbar.open('Style Type deleted!', "close");
      })
    }
  }

  typeCreateForm() {
    this.styleType.createOrDetail = 'create';
  }

  openTypeEditModal(templateRef: TemplateRef<any>) {
    this.styleType.editData = _.merge({}, this.styleType.detail)
    this.modalRef = this.dialog.open(templateRef, {
      // width: '25%'
    });
  }


  typeOnUpdate() {
    this.styleTypeService.update(this.styleType.editData.id, this.styleType.editData).subscribe(data => {
      this.snackbar.open('Type updated', "close");
      this.typeOnDetailClick(this.styleType.detail.id);
      this.init();
      close && this.modalRef.close(close);
    })
  }

  typeOnSubmit() {
    if (this.styleType.item.name) {
      this.styleTypeService.create(this.styleType.item).subscribe(data => {
        this.typeOnDetailClick(data.id);
        this.snackbar.open('Type created', "close");
        this.styleType.item = {};
        this.init();
      })
    }
    else {
      this.snackbar.open('Name must be provided', "close");
      return;
    }
  }

}

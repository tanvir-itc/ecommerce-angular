import { Component, OnInit, TemplateRef } from "@angular/core";
import { SettingsChildBase } from "../settings-child-base";
import { MatDialog } from "@angular/material";
import * as _ from "lodash";
import { AuthService } from "app/authentication/auth.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { AdjustmentTypeService } from "app/modules-core/common/adjustment-type/adjustment-type.service";
@Component({
  selector: "app-settings-adjustment-type",
  templateUrl: "./settings-adjustment-type.component.html",
})
export class SettingsAdjustmentTypeComponent extends SettingsChildBase
  implements OnInit {
  adjustmentType = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: "detail",
  };
  modalRef = null;

  constructor(
    private adjustmentTypeService: AdjustmentTypeService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.adjustmentTypeService.getAll().subscribe((data) => {
      this.adjustmentType.items = data;
    });
  }

  adjustmentTypeOnDetailClick(adjustmentTypeId) {
    this.adjustmentType.createOrDetail = "detail";
    this.setActiveRight("settings2right");
    this.adjustmentTypeService.get(adjustmentTypeId).subscribe((res) => {
      this.adjustmentType.detail = res;
    });
  }

  adjustmentTypeOnDeleteClick(adjustmentTypeId) {
    if (confirm("Adjustment type Delete confirm?")) {
      this.adjustmentTypeService.delete(adjustmentTypeId).subscribe((res) => {
        this.setActiveRight(false);
        this.init();
        this.snackbar.open("Adjustment type deleted!", "close");
      });
    }
  }

  adjustmentTypeCreateForm() {
    this.adjustmentType.createOrDetail = "create";
  }

  openAdjustmentTypeEditModal(templateRef: TemplateRef<any>) {
    this.adjustmentType.editData = _.merge({}, this.adjustmentType.detail);
    this.modalRef = this.dialog.open(templateRef, {
      // width: '25%'
    });
  }

  adjustmentTypeOnUpdate() {
    this.adjustmentTypeService
      .update(this.adjustmentType.editData.id, this.adjustmentType.editData)
      .subscribe((data) => {
        this.snackbar.open("adjustment type updated", "close");
        this.adjustmentTypeOnDetailClick(this.adjustmentType.detail.id);
        this.init();
        close && this.modalRef.close(close);
      });
  }

  adjustmentTypeOnSubmit() {
    if (this.adjustmentType.item.label) {
      this.adjustmentTypeService.create(this.adjustmentType.item).subscribe((data) => {
        this.adjustmentTypeOnDetailClick(data.id);
        this.snackbar.open("Adjustment type created", "close");
        this.adjustmentType.item = {};
        this.init();
      });
    } else {
      this.snackbar.open("label must be provided", "close");
      return;
    }
  }
}

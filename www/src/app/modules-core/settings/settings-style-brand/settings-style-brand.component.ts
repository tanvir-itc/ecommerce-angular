import { Component, OnInit, TemplateRef } from '@angular/core';
import { SettingsChildBase } from '../settings-child-base';
import { MatDialog } from '@angular/material';
import * as _ from 'lodash';
import { AuthService } from 'app/authentication/auth.service';
import { StyleBrandService } from 'app/modules-core/common/style-brand/style-brand.service';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
@Component({
  selector: 'app-settings-style-brand',
  templateUrl: './settings-style-brand.component.html',
})
export class SettingsStyleBrandComponent extends SettingsChildBase implements OnInit {
  styleBrand = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: 'detail'
  };
  modalRef = null;

  constructor(
    private styleBrandService: StyleBrandService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }


  init() {
    this.styleBrandService.getAll().subscribe((data) => {
      this.styleBrand.items = data;
    });
  }

  sizeOnDetailClick(sizeId) {
    this.styleBrand.createOrDetail = 'detail'
    this.setActiveRight('settings2right');
    this.styleBrandService.get(sizeId).subscribe(res => {
      this.styleBrand.detail = res;
    })
  }

  sizeOnDeleteClick(styleBrandId) {
    if (confirm("Brand Delete confirm?")) {
      this.styleBrandService.delete(styleBrandId).subscribe(res => {
        this.setActiveRight(false);
        this.init();
        this.snackbar.open('Style Brand deleted!', "close");
      })
    }
  }

  sizeCreateForm() {
    this.styleBrand.createOrDetail = 'create';
  }

  openBrandEditModal(templateRef: TemplateRef<any>) {
    this.styleBrand.editData = _.merge({}, this.styleBrand.detail)
    this.modalRef = this.dialog.open(templateRef, {
      // width: '25%'
    });
  }


  sizeOnUpdate() {
    this.styleBrandService.update(this.styleBrand.editData.id, this.styleBrand.editData).subscribe(data => {
      this.snackbar.open('Brand updated', "close");
      this.sizeOnDetailClick(this.styleBrand.detail.id);
      this.init();
      close && this.modalRef.close(close);
    })
  }

  sizeOnSubmit() {
    if (this.styleBrand.item.label) {
      this.styleBrandService.create(this.styleBrand.item).subscribe(data => {
        this.sizeOnDetailClick(data.id);
        this.snackbar.open('Brand created', "close");
        this.styleBrand.item = {};
        this.init();
      })
    }
    else {
      this.snackbar.open('Name must be provided', "close");
      return;
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { RolePermissionService } from 'app/modules-core/common/role-permission/role-permission.service';
import * as _ from 'lodash';
import { SettingsChildBase } from '../settings-child-base';
import { ActivatedRoute, Router } from '@angular/router';
import { removeEmptyFields } from 'app/modules-core/utility/helpers';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';

@Component({
  selector: 'app-settings-role-permission',
  templateUrl: './settings-role-permission.component.html',
})
export class SettingsRolePermissionComponent extends SettingsChildBase implements OnInit {
  filter: any = {};
  role = {
    item: {

    },
    detail: [],
    items: [],
    createOrDetail: 'detail',
  };
  filterCounter = 0;
  rolePermission = {
    items: [],
    EditOrUpdate: null,
    check: null
  }

  constructor(
    private _rolePermissionService: RolePermissionService,
    private snackbar: SnackbarService,
    private route: ActivatedRoute,
    private _router: Router,
  ) {
    super();
  }

  ngOnInit() {
    this.init();
    var queryParams = _.merge({}, _.get(this.route, 'snapshot.queryParams'));
    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    // this.applyFilters(this.filter, 1);
  }

  init() {
    this._rolePermissionService.getAllRoles().subscribe((data) => {
      this.role.items = data;
    });
  }

  RoleCreateForm() {
    this.role.createOrDetail = 'create';
  }
  selectedRoleId: null;
  RoleOnSubmit() {
    this._rolePermissionService.create(this.role.item).subscribe(data => {
      // this.StoreLocationOnDetailClick(data.id);
      this.snackbar.open('Role created', "close");
      this.role.item = {};
      this.init();
    })
  }
  rolePermissionDetailClick(id) {
    this.selectedRoleId = id;
    this.role.createOrDetail = 'detail';
    this.rolePermission.EditOrUpdate = 'edit';
    this.viewMode = false;
    this.filter = <any>{};
    this.setActiveRight('settings2right');
    this.applyFilters(this.filter, this.selectedRoleId, false);

  }
  deleteRole(roleId) {
    if (!confirm('Are you sure to do this?')) {
      return
    }
    this._rolePermissionService.delete(roleId).subscribe(res => {
      this.snackbar.open(`Role deleted!`, `Close`);
      this._rolePermissionService.getAllRoles().subscribe((data) => {
        this.role.items = data;
      });
    })
  }
  viewMode: boolean
  editRolePermission() {
    this.rolePermission.EditOrUpdate = 'update'
    this.viewMode = true
  }

  updateRolePermission() {
    this._rolePermissionService.update(this.rolePermission.items, this.selectedRoleId).subscribe(data => {
      this.snackbar.open('Role permission updated', "close");
      // this.rolePermission.items = data;
      // this.rolePermissionDetailClick(this.selectedRoleId);
      this.role.createOrDetail = 'detail';
      this.rolePermission.EditOrUpdate = 'edit';
      this.viewMode = false;
      _.each(this.rolePermission.items, v => {
        v.show = true;
        v['exist'] = false;
      })
      this.filter = <any>{};
    }, error => {
      var errorIdList = error.error.data;
      _.each(this.rolePermission.items, v => {
        v.show = true;
        var isExists = _.includes(errorIdList, v.id);
        v['exist'] = isExists
      })
      this.filter = <any>{};
      console.log(this.rolePermission.items)
    })
  }
  cancelUpdateing() {
    this.rolePermission.EditOrUpdate = 'edit';
    this.viewMode = false
  }

  onFilter() {
    this.applyFilters(this.filter, this.selectedRoleId, true);
  }

  filterParamsGenerator(params) {
    return params
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);

    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this.route
    });
  }

  applyFilters(filterParams, id, flag) {
    var params = _.merge({}, filterParams);
    if (!_.get(params, 'label')) {
      delete params.label

    }
    if (!_.get(params, 'module')) {
      delete params.module

    }
    _.each(this.rolePermission.items, r => {
      r.show = false;
    })

    this._rolePermissionService.getByIdWithFilter(params, id).subscribe(res => {

      if (flag) {
        _.each(this.rolePermission.items, i => {
          _.each(res, r => {
            if (i.id === r.id) {
              i.show = true;
            }
          })
        })
      }

      else if (flag === null) {
        _.each(this.rolePermission.items, r => {
          r.show = true;
        })
      }

      else {
        _.each(res, r => {
          r.show = true;
        })
        this.rolePermission.items = res;
      }

      this.setFilterToURL(filterParams);
      // console.log(this.rolePermission.items)
    })
  }
  clearAllFilter() {
    this.filter = {
      label: null,
      module: null
    }
  }
  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter, this.selectedRoleId, null);
  }

}

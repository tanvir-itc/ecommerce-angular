import { Component, OnInit, TemplateRef } from '@angular/core';
import { SettingsChildBase } from '../settings-child-base';
import { MatDialog } from '@angular/material';
import * as _ from 'lodash';
import { AuthService } from 'app/authentication/auth.service';
import { InventoryLocationService } from 'app/modules-core/common/inventory-location/inventory-location.service';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { ProvinceService } from 'app/modules-core/common/province/province.service';
@Component({
  selector: 'app-settings-inventory-location',
  templateUrl: './settings-inventory-location.component.html',
})
export class SettingsInventoryLocationComponent extends SettingsChildBase implements OnInit {
  inventoryLocation = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: 'detail'
  };
  modalRef = null;

  provinceList: any = [];

  constructor(
    private inventoryLocationService: InventoryLocationService,
    private _provinceService: ProvinceService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }


  init() {
    this.inventoryLocationService.getAll().subscribe((data) => {
      this.inventoryLocation.items = data;
    });
  }

  inventoryLocationOnDetailClick(inventoryLocationId) {
    this.inventoryLocation.createOrDetail = 'detail'
    this.setActiveRight('settings2right');
    this.inventoryLocationService.get(inventoryLocationId).subscribe(res => {
      this.inventoryLocation.detail = res;
    })
  }

  inventoryLocationOnDeleteClick(inventoryLocationId) {
    if (confirm("Inventory Location Delete confirm?")) {
      this.inventoryLocationService.delete(inventoryLocationId).subscribe(res => {
        this.setActiveRight(false);
        this.init();
        this.snackbar.open('Inventory Location deleted!', "close");
      })
    }
  }

  inventoryLocationCreateForm() {
    this.inventoryLocation.createOrDetail = 'create';
    this.getAllProvinces();
  }

  openinventoryLocationEditModal(templateRef: TemplateRef<any>) {
    this.getAllProvinces();
    this.inventoryLocation.editData = _.merge({}, this.inventoryLocation.detail)
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: '500px',
    });
  }


  inventoryLocationOnUpdate() {
    if (!this.inventoryLocation.editData.name) {
      this.snackbar.open('Name must be provided', "close");
      return;
    }
    if (!this.inventoryLocation.editData.street) {
      this.snackbar.open('Street must be provided', "close");
      return;
    }
    if (!this.inventoryLocation.editData.city) {
      this.snackbar.open('City must be provided', "close");
      return;
    }
    if (!this.inventoryLocation.editData.provinceId) {
      this.snackbar.open('Province must be selected', "close");
      return;
    }
    if (!this.inventoryLocation.editData.postalCode) {
      this.snackbar.open('Postal code must be provided', "close");
      return;
    }
    // if (!this.inventoryLocation.editData.poBox) {
    //   this.snackbar.open('Po box no must be provided', "close");
    //   return;
    // }

    this.inventoryLocationService.update(this.inventoryLocation.editData.id, this.inventoryLocation.editData).subscribe(data => {
      this.snackbar.open('Inventory Location updated', "close");
      this.inventoryLocationOnDetailClick(this.inventoryLocation.detail.id);
      this.init();
      close && this.modalRef.close(close);
    })
  }

  inventoryLocationOnSubmit() {
    if (!this.inventoryLocation.item.name) {
      this.snackbar.open('Name must be provided', "close");
      return;
    }
    if (!this.inventoryLocation.item.street) {
      this.snackbar.open('Street must be provided', "close");
      return;
    }
    if (!this.inventoryLocation.item.city) {
      this.snackbar.open('City must be provided', "close");
      return;
    }
    if (!this.inventoryLocation.item.provinceId) {
      this.snackbar.open('Province must be selected', "close");
      return;
    }
    if (!this.inventoryLocation.item.postalCode) {
      this.snackbar.open('Postal code must be provided', "close");
      return;
    }
    // if (!this.inventoryLocation.item.poBox) {
    //   this.snackbar.open('Po box no must be provided', "close");
    //   return;
    // }

    this.inventoryLocationService.create(this.inventoryLocation.item).subscribe(data => {
      this.inventoryLocationOnDetailClick(data.id);
      this.snackbar.open('Inventory Location created', "close");
      this.inventoryLocation.item = {};
      this.init();
    })
  }

  getAllProvinces() {
    this._provinceService.getAll().subscribe((data) => {
      this.provinceList = data;
    });
  }

}

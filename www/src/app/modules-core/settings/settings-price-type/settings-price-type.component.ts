import { Component, OnInit, TemplateRef } from '@angular/core';
import { SettingsChildBase } from '../settings-child-base';
import { MatDialog } from '@angular/material';
import * as _ from 'lodash';
import { AuthService } from 'app/authentication/auth.service';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { PriceTypeService } from 'app/modules-core/common/price-type/price-type.service';
@Component({
  selector: 'app-settings-price-type',
  templateUrl: './settings-price-type.component.html',
})
export class SettingsPriceTypeComponent extends SettingsChildBase implements OnInit {
  priceType = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: 'detail'
  };
  modalRef = null;

  constructor(
    private priceTypeService: PriceTypeService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }


  init() {
    this.priceTypeService.getAll().subscribe((data) => {
      this.priceType.items = data;
    });
  }

  priceTypeOnDetailClick(priceTypeId) {
    this.priceType.createOrDetail = 'detail'
    this.setActiveRight('settings2right');
    this.priceTypeService.get(priceTypeId).subscribe(res => {
      this.priceType.detail = res;
    })
  }

  priceTypeOnDeleteClick(priceTypeId) {
    if (confirm("Price Type Delete confirm?")) {
      this.priceTypeService.delete(priceTypeId).subscribe(res => {
        this.setActiveRight(false);
        this.init();
        this.snackbar.open('Style Type deleted!', "close");
      })
    }
  }

  priceTypeCreateForm() {
    this.priceType.createOrDetail = 'create';
  }

  openPriceTypeEditModal(templateRef: TemplateRef<any>) {
    this.priceType.editData = _.merge({}, this.priceType.detail)
    this.modalRef = this.dialog.open(templateRef, {
      // width: '25%'
    });
  }


  priceTypeOnUpdate() {
    this.priceTypeService.update(this.priceType.editData).subscribe(data => {
      this.snackbar.open('Price Type updated', "close");
      this.priceTypeOnDetailClick(this.priceType.detail.id);
      this.init();
      close && this.modalRef.close(close);
    })
  }

  priceTypeOnSubmit() {
    if (this.priceType.item.label) {
      this.priceTypeService.create(this.priceType.item).subscribe(data => {
        this.priceTypeOnDetailClick(data.id);
        this.snackbar.open('Price Type created', "close");
        this.priceType.item = {};
        this.init();
      })
    }
    else {
      this.snackbar.open('Label must be provided', "close");
      return;
    }
  }

}

import { Component, OnInit, TemplateRef } from '@angular/core';
import { SettingsChildBase } from '../settings-child-base';
import { MatDialog } from '@angular/material';
import * as _ from 'lodash';
import { AuthService } from 'app/authentication/auth.service';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { CostTypeService } from 'app/modules-core/common/cost-type/cost-type.service';
@Component({
  selector: 'app-settings-cost-type',
  templateUrl: './settings-cost-type.component.html',
})
export class SettingsCostTypeComponent extends SettingsChildBase implements OnInit {
  costType = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: 'detail'
  };
  modalRef = null;

  constructor(
    private costTypeService: CostTypeService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }


  init() {
    this.costTypeService.getAll().subscribe((data) => {
      this.costType.items = data;
    });
  }

  costTypeOnDetailClick(costTypeId) {
    this.costType.createOrDetail = 'detail'
    this.setActiveRight('settings2right');
    this.costTypeService.get(costTypeId).subscribe(res => {
      this.costType.detail = res;
    })
  }

  costTypeOnDeleteClick(costTypeId) {
    if (confirm("Cost Type Delete confirm?")) {
      this.costTypeService.delete(costTypeId).subscribe(res => {
        this.setActiveRight(false);
        this.init();
        this.snackbar.open('Style Type deleted!', "close");
      })
    }
  }

  costTypeCreateForm() {
    this.costType.createOrDetail = 'create';
  }

  openCostTypeEditModal(templateRef: TemplateRef<any>) {
    this.costType.editData = _.merge({}, this.costType.detail)
    this.modalRef = this.dialog.open(templateRef, {
      // width: '25%'
    });
  }


  costTypeOnUpdate() {
    this.costTypeService.update(this.costType.editData).subscribe(data => {
      this.snackbar.open('Cost Type updated', "close");
      this.costTypeOnDetailClick(this.costType.detail.id);
      this.init();
      close && this.modalRef.close(close);
    })
  }

  costTypeOnSubmit() {
    if (this.costType.item.label) {
      this.costTypeService.create(this.costType.item).subscribe(data => {
        this.costTypeOnDetailClick(data.id);
        this.snackbar.open('Cost Type created', "close");
        this.costType.item = {};
        this.init();
      })
    }
    else {
      this.snackbar.open('Label must be provided', "close");
      return;
    }
  }

}

import { Component, OnInit, TemplateRef } from '@angular/core';
import { SettingsChildBase } from '../settings-child-base';
import { MatDialog } from '@angular/material';
import { SizeService } from 'app/modules-core/common/size/size.service';
import * as _ from 'lodash';
import { AuthService } from 'app/authentication/auth.service';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { SizeGroupService } from 'app/modules-core/common/size/size-group.service';
@Component({
  selector: 'app-settings-size',
  templateUrl: './settings-size.component.html',
})
export class SettingsSizeComponent extends SettingsChildBase implements OnInit {
  sizeGroup = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: 'detail',
    groupId: null,
  };

  size = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: 'detail',
    groupId: null,
  };

  modalRef = null;

  constructor(
    private sizeGroupService: SizeGroupService,
    private sizeService: SizeService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }


  init() {
    this.sizeGroupService.getAll().subscribe((data) => {
      this.sizeGroup.items = data;
    });
  }

  sizeGroupOnDetailClick(sizeGroupId) {
    this.sizeGroup.createOrDetail = 'detail';
    this.sizeGroup.groupId = sizeGroupId;
    this.setActiveRight('settings2right');
    this.sizeGroupService.get(sizeGroupId).subscribe(res => {
      this.sizeGroup.detail = res;
    })
    this.getSizes();
  }

  sizeGroupOnDeleteClick(sizeGroupId) {
    if (confirm("sizeGroup Delete confirm?")) {
      this.sizeGroupService.delete(sizeGroupId).subscribe(res => {
        this.setActiveRight(false);
        this.init();
        this.snackbar.open('sizeGroup deleted!', "close");
      })
    }
  }

  sizeGroupCreateForm() {
    this.sizeGroup.createOrDetail = 'create';
  }

  openSizeGroupEditModal(templateRef: TemplateRef<any>) {
    this.sizeGroup.editData = _.merge({}, this.sizeGroup.detail)
    this.modalRef = this.dialog.open(templateRef, {
    });
  }


  sizeGroupOnUpdate() {
    this.sizeGroupService.update(this.sizeGroup.editData.id, this.sizeGroup.editData).subscribe(data => {
      this.snackbar.open('Size updated', "close");
      this.sizeGroupOnDetailClick(this.sizeGroup.detail.id);
      this.init();
      close && this.modalRef.close(close);
    })
  }

  sizeGroupOnSubmit() {
    if (this.sizeGroup.item.name) {
      this.sizeGroupService.create(this.sizeGroup.item).subscribe(data => {
        this.sizeGroupOnDetailClick(data.id);
        this.snackbar.open('Size created', "close");
        this.sizeGroup.item = {};
        this.init();
      })
    }
    else {
      this.snackbar.open('Name must be provided', "close");
      return;
    }
  }

  getSizes() {
    this.sizeService.getAll({ sizegroupid: this.sizeGroup.groupId }).subscribe((data) => {
      this.size.items = data;
    });
  }

  openSizeEditModal(templateRef: TemplateRef<any>, size) {
    this.size.editData = _.merge({}, size)
    this.modalRef = this.dialog.open(templateRef, {
    });
  }

  openSizeAddModal(templateRef: TemplateRef<any>) {
    this.size.item = {};
    this.modalRef = this.dialog.open(templateRef, {
    });
  }


  sizeOnUpdate() {
    if (this.size.editData.name) {
      let payload = {
        sequence: _.get(this.size.editData, 'sequence'),
        sizeGroupId: _.get(this.sizeGroup, 'groupId'),
        sizeName: _.get(this.size.editData, 'name'),
        sizeId: _.get(this.size.editData, 'id'),
      }
      this.sizeGroupService.updateSizeFields(_.get(this.sizeGroup, 'groupId'), this.size.editData).subscribe(data => {
        this.snackbar.open('Size updated', "close");
        this.getSizes();
        this.modalRef.close();
      })
    }
  }


  sizeOnSubmit() {
    if (this.size.item.name) {
      let payload = {
        sequence: _.get(this.size.item, 'sequence'),
        sizeGroupId: _.get(this.sizeGroup, 'groupId'),
        sizeName: _.get(this.size.item, 'name')
      }
      this.sizeGroupService.addSize(_.get(this.sizeGroup, 'groupId'), payload).subscribe(data => {
        this.getSizes();
        this.snackbar.open('Size created', "close");
        this.size.item = {};
        this.modalRef.close();
      })
    }
    else {
      this.snackbar.open('Name must be provided', "close");
      return;
    }
  }

  sizeOnDeleteClick(sizeId) {
    if (confirm("sizeGroup Delete confirm?")) {
      this.sizeGroupService.deleteSize(_.get(this.sizeGroup, 'groupId'), sizeId).subscribe(res => {
        this.getSizes();
        this.snackbar.open('sizeGroup deleted!', "close");
      })
    }
  }

}

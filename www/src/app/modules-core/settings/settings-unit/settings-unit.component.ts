import { Component, OnInit, TemplateRef } from '@angular/core';
import { SettingsChildBase } from '../settings-child-base';
import { MatDialog } from '@angular/material';
import * as _ from 'lodash';
import { AuthService } from 'app/authentication/auth.service';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { UnitService } from 'app/modules-core/common/unit/unit.service';
@Component({
  selector: 'app-settings-unit',
  templateUrl: './settings-unit.component.html',
})
export class SettingsUnitComponent extends SettingsChildBase implements OnInit {
  unit = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: 'detail'
  };
  modalRef = null;

  constructor(
    private unitService: UnitService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }


  init() {
    this.unitService.getAll().subscribe((data) => {
      this.unit.items = data;
    });
  }

  unitOnDetailClick(unitId) {
    this.unit.createOrDetail = 'detail'
    this.setActiveRight('settings2right');
    this.unitService.get(unitId).subscribe(res => {
      this.unit.detail = res;
    })
  }

  unitOnDeleteClick(unitId) {
    if (confirm("Unit Type Delete confirm?")) {
      this.unitService.delete(unitId).subscribe(res => {
        this.setActiveRight(false);
        this.init();
        this.snackbar.open('Unit deleted!', "close");
      })
    }
  }

  unitCreateForm() {
    this.unit.createOrDetail = 'create';
  }

  openUnitEditModal(templateRef: TemplateRef<any>) {
    this.unit.editData = _.merge({}, this.unit.detail)
    this.modalRef = this.dialog.open(templateRef, {
      // width: '25%'
    });
  }


  unitOnUpdate() {
    this.unitService.update(this.unit.editData.id, this.unit.editData).subscribe(data => {
      this.snackbar.open('Unit updated', "close");
      this.unitOnDetailClick(this.unit.detail.id);
      this.init();
      close && this.modalRef.close(close);
    })
  }

  unitOnSubmit() {
    if (this.unit.item.name) {
      this.unitService.create(this.unit.item).subscribe(data => {
        this.unitOnDetailClick(data.id);
        this.snackbar.open('Unit created', "close");
        this.unit.item = {};
        this.init();
      })
    }
    else {
      this.snackbar.open('Name must be provided', "close");
      return;
    }
  }

}

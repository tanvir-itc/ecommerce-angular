import { Component, OnInit, TemplateRef } from "@angular/core";
import { SettingsChildBase } from "../settings-child-base";
import { MatDialog } from "@angular/material";
import * as _ from "lodash";
import { AuthService } from "app/authentication/auth.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { SaleDiscountService } from "app/modules-core/common/sale-discounts/sale-discount.service";
@Component({
  selector: "app-settings-sale-discount.component",
  templateUrl: "./settings-sale-discount.component.html",
})
export class SettingsSaleDiscountComponent extends SettingsChildBase
  implements OnInit {
  saleDiscount = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: "detail",
  };
  modalRef = null;

  constructor(
    private saleDiscountService: SaleDiscountService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.saleDiscountService.getAll().subscribe((data) => {
      this.saleDiscount.items = data;
    });
  }

  saleDiscountOnDetailClick(saleDiscountId) {
    this.saleDiscount.createOrDetail = "detail";
    this.setActiveRight("settings2right");
    this.saleDiscountService.get(saleDiscountId).subscribe((res) => {
      this.saleDiscount.detail = res;
    });
  }

  saleDiscountOnDeleteClick(saleDiscountId) {
    if (confirm("Promotion Delete confirm?")) {
      this.saleDiscountService.delete(saleDiscountId).subscribe((res) => {
        this.setActiveRight(false);
        this.init();
        this.snackbar.open("Promotion deleted.", "close");
      });
    }
  }

  saleDiscountCreateForm() {
    this.saleDiscount.createOrDetail = "create";
  }

  openSaleDiscountEditModal(templateRef: TemplateRef<any>) {
    this.saleDiscount.editData = _.merge({}, this.saleDiscount.detail);
    this.modalRef = this.dialog.open(templateRef, {
      // width: '25%'
    });
  }

  saleDiscountOnUpdate() {
    this.saleDiscountService
      .updateFields(this.saleDiscount.editData.id, this.saleDiscount.editData)
      .subscribe((data) => {
        this.snackbar.open("Promotion updated", "close");
        this.saleDiscountOnDetailClick(this.saleDiscount.detail.id);
        this.init();
        close && this.modalRef.close(close);
      });
  }

  saleDiscountOnSubmit() {
    if (this.saleDiscount.item.reason && this.saleDiscount.item.discountPercentage) {
      this.saleDiscountService.create(this.saleDiscount.item).subscribe((data) => {
        this.saleDiscountOnDetailClick(data.id);
        this.snackbar.open("Promotion created", "close");
        this.saleDiscount.item = {};
        this.init();
      });
    } else {
      this.snackbar.open("label, discount, must be provided", "close");
      return;
    }
  }
}

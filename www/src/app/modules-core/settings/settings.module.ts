import { NgModule, ViewEncapsulation } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SettingsPageComponent } from "./settings.component";
import { LoginCheckRouteGuard } from "app/authentication/check-login-route-guard";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "@fuse/shared.module";
import { MaterialModule } from "../utility/material.module";
import { FuseSidebarModule } from "@fuse/components/sidebar/sidebar.module";
import { settingsChildRoutes } from "./settings.child-routes";
import { SettingsRolePermissionComponent } from "./settings-role-permission/settings-role-permission.component";
import { ButtonModule } from "../utility/button/button.module";
import { SettingsSizeComponent } from "./settings-size/settings-size.component";
import { SettingsStyleTypeComponent } from "./settings-style-type/settings-style-type.component";
import { SettingsStyleBrandComponent } from "./settings-style-brand/settings-style-brand.component";
import { SettingsSeasonComponent } from "./settings-season/settings-season.component";
import { SettingsInventoryLocationComponent } from "./settings-inventory-location/settings-inventory-location.component";
import { SettingsProvinceComponent } from "./settings-province/settings-province.component";
import { SettingsCostTypeComponent } from "./settings-cost-type/settings-cost-type.component";
import { SettingsUnitComponent } from "./settings-unit/settings-unit.component";
import { SettingsPriceTypeComponent } from "./settings-price-type/settings-price-type.component";
import { SettingsAdjustmentTypeComponent } from "./settings-adjustment-type/settings-adjustment-type.component";
import { SettingsGlobalComponent } from "./settings-global/settings-global.component";
import { SettingsSaleDiscountComponent } from "./settings-sale-discount/settings-sale-discount.component";
import { DateTimeModule } from "../utility/date-time-picker/date-time.module";
import { NgxSummernoteModule } from "ngx-summernote";

const routes = [
  {
    path: "settings",
    component: SettingsPageComponent,
    canActivate: [LoginCheckRouteGuard],
    encapsulation: ViewEncapsulation.None,
    children: settingsChildRoutes,
  },
];

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FuseSharedModule,
    FuseSidebarModule,
    RouterModule.forChild(routes),
    ButtonModule,
    DateTimeModule,
    NgxSummernoteModule
  ],
  declarations: [
    SettingsPageComponent,
    SettingsRolePermissionComponent,
    SettingsSizeComponent,
    SettingsStyleTypeComponent,
    SettingsStyleBrandComponent,
    SettingsSeasonComponent,
    SettingsInventoryLocationComponent,
    SettingsProvinceComponent,
    SettingsCostTypeComponent,
    SettingsPriceTypeComponent,
    SettingsUnitComponent,
    SettingsAdjustmentTypeComponent,
    SettingsGlobalComponent,
    SettingsSaleDiscountComponent,
  ],
})
export class SettingsModule { }

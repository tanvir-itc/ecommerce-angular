import { Component, OnInit, TemplateRef } from "@angular/core";
import { SettingsChildBase } from "../settings-child-base";
import { MatDialog } from "@angular/material";
import { SeasonService } from "app/modules-core/common/season/season.service";
import * as _ from "lodash";
import { AuthService } from "app/authentication/auth.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
@Component({
  selector: "app-settings-season",
  templateUrl: "./settings-season.component.html",
})
export class SettingsSeasonComponent extends SettingsChildBase
  implements OnInit {
  season = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: "detail",
  };
  modalRef = null;

  constructor(
    private seasonService: SeasonService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.seasonService.getAll().subscribe((data) => {
      this.season.items = data;
    });
  }

  sizeOnDetailClick(seasonId) {
    this.season.createOrDetail = "detail";
    this.setActiveRight("settings2right");
    this.seasonService.get(seasonId).subscribe((res) => {
      this.season.detail = res;
    });
  }

  sizeOnDeleteClick(seasonId) {
    if (confirm("Season Delete confirm?")) {
      this.seasonService.delete(seasonId).subscribe((res) => {
        this.setActiveRight(false);
        this.init();
        this.snackbar.open("Season deleted!", "close");
      });
    }
  }

  sizeCreateForm() {
    this.season.createOrDetail = "create";
  }

  openSizeEditModal(templateRef: TemplateRef<any>) {
    this.season.editData = _.merge({}, this.season.detail);
    this.modalRef = this.dialog.open(templateRef, {
      // width: '25%'
    });
  }

  sizeOnUpdate() {
    this.seasonService
      .update(this.season.editData.id, this.season.editData)
      .subscribe((data) => {
        this.snackbar.open("Season updated", "close");
        this.sizeOnDetailClick(this.season.detail.id);
        this.init();
        close && this.modalRef.close(close);
      });
  }

  sizeOnSubmit() {
    if (this.season.item.label) {
      this.seasonService.create(this.season.item).subscribe((data) => {
        this.sizeOnDetailClick(data.id);
        this.snackbar.open("Season created", "close");
        this.season.item = {};
        this.init();
      });
    } else {
      this.snackbar.open("Name must be provided", "close");
      return;
    }
  }
}

import { Component, OnInit, ViewEncapsulation, TemplateRef } from '@angular/core';
import { MatSnackBar, MatDialog } from '@angular/material';
import * as _ from 'lodash';
import { settingsChildRoutes } from './settings.child-routes';
import { Router } from '@angular/router';
import { AuthService } from 'app/authentication/auth.service';
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
@Component({
  selector: 'settings-page',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SettingsPageComponent implements OnInit {
  currentItemLeft = '';
  currentItemRight = '';
  currentMail;

  urlCurrent;
  settings;

  tnaStep = {
    item: {
      tnaType: {}
    },
    detail: {},
    items: [],
    createOrDetail: 'detail'
  };

  modalRef = null;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private _auth: AuthService,
    private _quickPanelSubjectService: QuickPanelSubjectService
  ) {
    //this.settings = settingsChildRoutes;
    this._quickPanelSubjectService.heading.next('Settings');
    this.settings = _.sortBy(settingsChildRoutes, 'data.label')

    this.router.events.subscribe((res) => {
      this.urlCurrent = this.router.url;
    })
  }

  ngOnInit() {
  }

  isCurrentSetting(settingPath) {
    return _.endsWith(this.urlCurrent, settingPath);
  }

  setActiveLeft(item) {
    this.currentItemLeft = item.key;
    this.setActiveRight(false);

    if (item.initHandler && typeof item.initHandler === 'function') {
      item.initHandler.call(this);
    }
  }

  setActiveRight(item) {
    this.currentItemRight = item;
  }

}

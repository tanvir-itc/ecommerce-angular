import { LoginCheckRouteGuard } from "app/authentication/check-login-route-guard";
import { SettingsRolePermissionComponent } from "./settings-role-permission/settings-role-permission.component";
import { SettingsSizeComponent } from "./settings-size/settings-size.component";
import { SettingsStyleTypeComponent } from "./settings-style-type/settings-style-type.component";
import { SettingsSeasonComponent } from "./settings-season/settings-season.component";
import { SettingsInventoryLocationComponent } from "./settings-inventory-location/settings-inventory-location.component";
import { SettingsProvinceComponent } from "./settings-province/settings-province.component";
import { SettingsCostTypeComponent } from "./settings-cost-type/settings-cost-type.component";
import { SettingsUnitComponent } from "./settings-unit/settings-unit.component";
import { SettingsPriceTypeComponent } from "./settings-price-type/settings-price-type.component";
import { SettingsStyleBrandComponent } from "./settings-style-brand/settings-style-brand.component";
import { SettingsAdjustmentTypeComponent } from "./settings-adjustment-type/settings-adjustment-type.component";
import { SettingsGlobalComponent } from "./settings-global/settings-global.component";
import { SettingsSaleDiscountComponent } from "./settings-sale-discount/settings-sale-discount.component";

export const settingsChildRoutes = [
  {
    path: "size",
    component: SettingsSizeComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Size",
      matIco: "settings",
      permission: "PERMISSION_SIZE_VIEW",
    },
  },
  {
    path: "global",
    component: SettingsGlobalComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Global",
      matIco: "settings",
      permission: "PERMISSION_GLOBAL_SETTING_EDIT",
    },
  },
  {
    path: "unit",
    component: SettingsUnitComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Unit",
      matIco: "settings",
      permission: "PERMISSION_SIZE_VIEW",
    },
  },
  {
    path: "season",
    component: SettingsSeasonComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Season",
      matIco: "settings",
      permission: "PERMISSION_SEASON_VIEW",
    },
  },
  {
    path: "adjustment-type",
    component: SettingsAdjustmentTypeComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Adjustment Type",
      matIco: "settings",
      permission: "PERMISSION_ADJUSTMENT_TYPE_VIEW",
    },
  },
  {
    path: "style-type",
    component: SettingsStyleTypeComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Style Type",
      matIco: "settings",
      permission: "PERMISSION_STYLE_TYPE_VIEW",
    },
  },
  {
    path: "style-brand",
    component: SettingsStyleBrandComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Style Brand",
      matIco: "settings",
      permission: "PERMISSION_STYLE_TYPE_VIEW",
    },
  },
  {
    path: "cost-type",
    component: SettingsCostTypeComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Cost Type",
      matIco: "settings",
      permission: "PERMISSION_COST_TYPE_VIEW",
    },
  },
  {
    path: "price-type",
    component: SettingsPriceTypeComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Price Type",
      matIco: "settings",
    },
  },
  {
    path: "promotions",
    component: SettingsSaleDiscountComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Promotions",
      matIco: "settings",
    },
  },
  {
    path: "province",
    component: SettingsProvinceComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Province",
      matIco: "settings",
      permission: "PERMISSION_PROVINCE_VIEW",
    },
  },
  {
    path: "inventory-location",
    component: SettingsInventoryLocationComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Inventory Location",
      matIco: "settings",
      permission: "PERMISSION_INVENTORY_LOCATION_VIEW",
    },
  },
  {
    path: "role-permission",
    component: SettingsRolePermissionComponent,
    canActivate: [LoginCheckRouteGuard],
    data: {
      label: "Role Permission",
      matIco: "settings",
      permission: "PERMISSION_MANAGE_ROLE_PERMISSION",
    },
  }
];

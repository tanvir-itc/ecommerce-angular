import { Component, OnInit, TemplateRef } from "@angular/core";
import { SettingsChildBase } from "../settings-child-base";
import { MatDialog } from "@angular/material";
import * as _ from "lodash";
import { AuthService } from "app/authentication/auth.service";
import { ProvinceService } from "app/modules-core/common/province/province.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
@Component({
  selector: "app-settings-province",
  templateUrl: "./settings-province.component.html",
})
export class SettingsProvinceComponent extends SettingsChildBase
  implements OnInit {
  styleType = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: "detail",
  };
  modalRef = null;

  constructor(
    private styleTypeService: ProvinceService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.styleTypeService.getAll().subscribe((data) => {
      this.styleType.items = data;
    });
  }

  sizeOnDetailClick(sizeId) {
    this.styleType.createOrDetail = "detail";
    this.setActiveRight("settings2right");
    this.styleTypeService.get(sizeId).subscribe((res) => {
      this.styleType.detail = res;
    });
  }

  sizeOnDeleteClick(styleTypeId) {
    if (confirm("Province Delete confirm?")) {
      this.styleTypeService.delete(styleTypeId).subscribe((res) => {
        this.setActiveRight(false);
        this.init();
        this.snackbar.open("Province deleted!", "close");
      });
    }
  }

  sizeCreateForm() {
    this.styleType.createOrDetail = "create";
  }

  openSizeEditModal(templateRef: TemplateRef<any>) {
    this.styleType.editData = _.merge({}, this.styleType.detail);
    this.modalRef = this.dialog.open(templateRef, {
      // width: '25%'
    });
  }

  sizeOnUpdate() {
    this.styleTypeService
      .update(this.styleType.editData.id, this.styleType.editData)
      .subscribe((data) => {
        this.snackbar.open("Province updated", "close");
        this.sizeOnDetailClick(this.styleType.detail.id);
        this.init();
        close && this.modalRef.close(close);
      });
  }

  sizeOnSubmit() {
    if (this.styleType.item.name && this.styleType.item.label && this.styleType.item.taxRate) {
      this.styleTypeService.create(this.styleType.item).subscribe((data) => {
        this.sizeOnDetailClick(data.id);
        this.snackbar.open("Province created", "close");
        this.styleType.item = {};
        this.init();
      });
    } else {
      this.snackbar.open("Name, label and tax rate must be provided", "close");
      return;
    }
  }
}

import { Component, OnInit, TemplateRef } from "@angular/core";
import { SettingsChildBase } from "../settings-child-base";
import { MatDialog } from "@angular/material";
import * as _ from "lodash";
import { AuthService } from "app/authentication/auth.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { GlobalService } from "app/modules-core/common/global/global.service";
import { FileUploadModalComponent } from "app/modules-core/common/attachment/components/file-upload-modal/file-upload-modal.component";
import { AttachmentService } from "app/modules-core/common/attachment/attachment.service";
import { getSummarnoteConfig } from "app/modules-core/utility/helpers";
@Component({
  selector: "app-settings-global",
  templateUrl: "./settings-global.component.html",
})
export class SettingsGlobalComponent extends SettingsChildBase
  implements OnInit {
  global = <any>{
    item: {},
    detail: {},
    items: [],
    editData: {},
    createOrDetail: "detail",
  };
  logo = null
  modalRef = null;
  config = getSummarnoteConfig();

  constructor(
    private globalService: GlobalService,
    private snackbar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
    public _attachmentService: AttachmentService
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.globalService.getAll().subscribe((data) => {
      this.global.items = data;
      if (data.length) {
        this.globalOnDetailClick(this.global.items[0].id)
        this.getLogo(this.global.items[0].id);

      }
    });
  }

  globalOnDetailClick(globalId) {
    this.global.createOrDetail = "detail";
    this.setActiveRight("settings2right");
    this.globalService.get(globalId).subscribe((res) => {
      this.global.detail = res;
    });
  }

  globalOnDeleteClick(globalId) {
    if (confirm("Global Delete confirm?")) {
      this.globalService.delete(globalId).subscribe((res) => {
        this.setActiveRight(false);
        this.init();
        this.snackbar.open("Global deleted!", "close");
      });
    }
  }

  globalCreateForm() {
    this.global.createOrDetail = "create";
  }

  openGlobalEditModal(templateRef: TemplateRef<any>) {
    this.global.editData = _.merge({}, this.global.detail);
    this.modalRef = this.dialog.open(templateRef, {
      width: '40%'
    });
  }

  globalOnUpdate() {
    this.globalService
      .updateFields(this.global.editData.id, this.global.editData)
      .subscribe((data) => {
        this.snackbar.open("Global updated", "close");
        this.globalOnDetailClick(this.global.detail.id);
        this.init();
        close && this.modalRef.close(close);
      });
  }

  globalOnSubmit() {
    this.globalService.create(this.global.item).subscribe((data) => {
      this.globalOnDetailClick(data.id);
      this.snackbar.open("Global created", "close");
      this.global.item = {};
      this.init();
    });
  }

  openAttachementsModal() {
    const dialogRef = this.dialog.open(FileUploadModalComponent, {
      width: '40%',
      data: {
        entityType: 'companyLogo',
        entityId: this.global.items[0].id,
        config: { maxFiles: 1 }
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getLogo(this.global.items[0].id);
    });
  }

  getLogo(globalId) {
    this._attachmentService
      .getAll({ entitytype: "companyLogo", entityid: globalId })
      .subscribe((data) => {
        this.logo = _.chain(data)
          .last()
          .get("fullpath")
          .value();
      });
  }
}

import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import * as _ from 'lodash';
import { AutocompleteBase } from 'app/modules-core/utility/autocomplete-base';
import { UserManualService } from '../user-manual.service';

@Component({
  selector: 'user-manual-autocomplete',
  template: `
    <ng-select [items]="items"
              [clearable]="false"
              bindLabel="topicName"
              bindValue="id"
              placeholder="{{hidePlaceholder ? '' : 'User Manual'}}"
              [(ngModel)]="preSelectId"
              (change)="onSelect($event)">
    </ng-select>
  `,
})

export class UserManualAutocomplete extends AutocompleteBase implements OnInit {
  constructor(
    private _userManualService: UserManualService
  ) {
    super();
  }

  ngOnInit() {
    this._userManualService.get()
      .subscribe(data => {
        this.items = data;
      });
  }
}

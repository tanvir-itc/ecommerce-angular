import { NgModule } from '@angular/core';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { UserManualAutocomplete } from './user-manual-autocomplete.component';


@NgModule({
  declarations: [
    UserManualAutocomplete
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    MaterialModule,
    TranslateModule,

    FuseSharedModule,

    MaterialModule,

    NgSelectModule,
  ],
  exports: [
    UserManualAutocomplete
  ]
})

export class UserManualAutocompleteModule {
}

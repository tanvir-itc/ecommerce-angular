import { NgModule } from '@angular/core';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { UserManualListComponent } from './pages/user-manual-list/user-manual-list.component';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { RouterModule } from '@angular/router';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { NgxSummernoteModule } from 'ngx-summernote';
import { DxTreeListModule } from 'devextreme-angular';
import { EditorModule } from 'primeng/editor';

const routes = [
  {
    path: 'user-manuals',
    component: UserManualListComponent,
    canActivate: [LoginCheckRouteGuard],
  }];
@NgModule({
  declarations: [
    UserManualListComponent,
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(routes),
    TranslateModule,

    FuseSharedModule,

    MaterialModule,

    NgSelectModule,

    ButtonModule,

    NgxSummernoteModule,

    DxTreeListModule,

    EditorModule,
  ],
  exports: [
  ]
})

export class UserManualModule {
}

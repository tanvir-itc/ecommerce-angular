import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { ListPageBaseClass } from 'app/modules-core/utility/list-page-base-class';
import { UserManualService } from '../../user-manual.service';
import * as _ from "lodash";
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
import { getSummarnoteConfig } from 'app/modules-core/utility/helpers';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';

@Component({
  selector: 'app-user-manual-list',
  templateUrl: './user-manual-list.component.html',
  animations: fuseAnimations,
})
export class UserManualListComponent extends ListPageBaseClass implements OnInit {

  constructor(

    private _userManualService: UserManualService,
    private snackBar: SnackbarService,
    public dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService
  ) {
    super();
    this._quickPanelSubjectService.heading.next('User Manual');
  }
  config = getSummarnoteConfig()
  edit = false;
  modalRef;
  showDetails = false;
  root = true;
  showParentList = false;
  userManual = <any>{
    parentTopic: {
      id: null
    }
  }
  details = <any>{};
  userManualList = [];

  selectTopic = <any>{};

  ngOnInit() {
    this.getUserManual()
  }

  getUserManual() {
    this._userManualService.get().subscribe(res => {
      this.userManualList = res;
    })
  }


  openUserManualAddModal(templateRef: TemplateRef<any>) {
    this.userManual = <any>{
      parentTopic: {
        id: null
      }
    };
    this.showParentList = false;
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: '90%'

    });
  }

  addUserManual() {
    if (!this.userManual.parentTopic.id) {
      this.userManual.parentTopic = null;
    }
    this._userManualService.create(this.userManual).subscribe(res => {
      this.snackBar.open(`User Manual added!`, "Close");
      this.getUserManual();
      close && this.modalRef.close(close);
      this.userManual = <any>{
        parentTopic: {
          id: null
        }
      };
      this.showParentList = false;
    })
  }

  onRowClick(e) {
    this.userManual.parentTopic = e.data;
    console.log(this.userManual.parentTopic)

  }

  showOptions(event) {
    if (event.checked) {
      this.userManual.parentTopic.id = null;
      this.showParentList = false;
    }
    else {
      this.showParentList = true;
    }
  }

  onTopicClick(event) {
    this._userManualService.getById(event.data.id).subscribe(res => {
      this.selectTopic = res;
      this.showDetails = true;
      this.edit = false;
    })
  }

  editUserManual() {
    this.edit = true;
    this.details = _.merge({}, this.selectTopic);
  }

  updateUserManual() {
    this._userManualService.update(this.details.id, this.details).subscribe(res => {
      this.selectTopic = res;
      this.edit = false;
      this.getUserManual();
    })
  }

  closeEdit() {
    this.edit = false;
  }

}



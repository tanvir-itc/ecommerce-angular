import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { EmailModalComponent } from './components/email-modal/email-modal.component';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { NgxSummernoteModule } from 'ngx-summernote';
import { AttachmentModule } from '../attachment/attachment.module';
import { EditorModule } from 'primeng/editor';


@NgModule({
  declarations: [
    EmailModalComponent
  ],
  entryComponents: [
    EmailModalComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    // RouterModule.forChild(routes),
    TranslateModule,

    FuseSharedModule,

    MaterialModule,

    NgSelectModule,

    NgxSummernoteModule,

    AttachmentModule,

    ButtonModule,
    EditorModule
  ],
  exports: [
    EmailModalComponent
  ]
})

export class EmailModule {
}

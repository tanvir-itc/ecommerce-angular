import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from "@angular/material";
import * as _ from "lodash";
import { getSummarnoteConfig } from "app/modules-core/utility/helpers";
import { EmailService } from "../../email.service";
import { FileUploadModalComponent } from "app/modules-core/common/attachment/components/file-upload-modal/file-upload-modal.component";
import { AttachmentService } from "app/modules-core/common/attachment/attachment.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";

@Component({
  selector: "email-modal",
  templateUrl: "email-modal.component.html"
})
export class EmailModalComponent {
  id: any = null;

  emailInfo: any = {};
  fileName = null;
  modalRef = null;
  config = getSummarnoteConfig();

  constructor(
    public dialogRef: MatDialogRef<EmailModalComponent>,
    private snackBar: SnackbarService,
    public dialog: MatDialog,
    private _emailService: EmailService,
    private _attachmentService: AttachmentService,

    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.id = data.id;
    this.emailInfo = data.emailInfo;
    if (_.get(data, 'type')) {
      if (data.type === 'fabric') {
        this.fileName = "fabric_po_" + this.id + "_print.pdf";
      }
      else if (data.type === 'trim') {
        this.fileName = "trim_po_" + this.id + "_print.pdf";
      }
    }
  }
  ngOnInit() {
    this.config['height'] = 450;
  }

  sendEmail() {
    this.emailInfo.entityId = this.id;
    this._emailService.create(this.emailInfo).subscribe(res => {
      close && this.dialogRef.close(close);
      this.snackBar.open(`Email sent successfully!`, "Close");
    })
  }

  openAttachmentModal() {
    let payload = <any>{};
    if (_.get(this.data, 'type') === 'fabric') {
      payload = {
        name: "Attachment Upload",
        entityType: 'fabric_po_extra_file',
        entityId: this.id,
        isThumbnailRequired: false,
      }
    }
    else if (_.get(this.data, 'type') === 'trim') {
      payload = {
        name: "Attachment Upload",
        entityType: 'trim_po_extra_file',
        entityId: this.id,
        isThumbnailRequired: false,
      }
    }

    else {
      return;
    }

    const dialogRef = this.dialog.open(FileUploadModalComponent, {
      width: '40%',
      data: payload
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getAttachmentFiles();
    });

  }


  getAttachmentFiles() {
    _.each(this.emailInfo.attachments, a => {
      if (_.get(a, 'denable')) {
        _.chain(this.emailInfo.attachments)
          .remove({ 'id': a.id })
          .value();
      }
    })

    if (_.get(this.data, 'type') === 'fabric') {
      this._attachmentService.getAll({ entitytype: 'fabric_po_extra_file', entityid: this.id })
        .subscribe(data => {
          _.each(data, d => {
            d.denable = true;
          })
          this.emailInfo.attachments = _.concat(this.emailInfo.attachments, data);
        });
    }

    else if (_.get(this.data, 'type') === 'trim') {
      this._attachmentService.getAll({ entitytype: 'trim_po_extra_file', entityid: this.id })
        .subscribe(data => {
          _.each(data, d => {
            d.denable = true;
          })
          this.emailInfo.attachments = _.concat(this.emailInfo.attachments, data);
        });
    }

  }

  removeAttachment(index, attachment) {
    if (!confirm("Are you want to remove it?")) {
      return
    }

    if (_.get(attachment, 'denable')) {
      this._attachmentService.delete(attachment.id).subscribe(res => {
      });
    }

    this.emailInfo.attachments.splice(index, 1);

  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from 'rxjs/internal/operators/map';
import { Observable } from 'rxjs/Observable';
import { makeParams } from "app/modules-core/utility/helpers";
import * as _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class FactoryShipmentService {

  urlBase = 'factory-shipments';
  urlBaseForPicking = 'factory-shipments/v2';
  httpOptions = {
    params: new HttpParams(),
  };

  constructor(
    private http: HttpClient,
  ) { }

  getAll(opts = {}) {
    return this.http
      .get(`${this.urlBase}`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          // const nameSorter = (item) => item.name.toLowerCase();
          const sequenceSorter = (item) => item.sequence;
          // resp.data = _.sortBy(resp.data, [sequenceSorter, nameSorter], ["asc"]);
          return resp.data;
        })
      );
  }

  get() {
    return this.http.get(this.urlBase)
      .pipe(map((resp: any) => resp.data));
  }

  getById(id) {
    return this.http.get(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  delete(id) {
    return this.http.delete(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  create(data) {
    return this.http.post(
      `${this.urlBase}`,
      data
    ).pipe(map((resp: any) => resp));
  }
  detailCreate(data, id) {
    return this.http.post(
      `${this.urlBase}/${id}/details`,
      data
    ).pipe(map((resp: any) => resp.data));
  }
  detailUpdate(data, id) {
    return this.http.patch(
      `${this.urlBase}/${id}/details`,
      data
    ).pipe(map((resp: any) => resp.data));
  }

  update(data) {
    return this.http.put(
      `${this.urlBase}`,
      data
    ).pipe(map((resp: any) => resp.data));
  }

  updateFields(id, payload) {
    return this.http
      .patch(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  deleteBox(detailid, shipmentid) {
    return this.http.delete(`${this.urlBase}/${shipmentid}/details/${detailid}`)
      .pipe(map((resp: any) => resp.data));
  }

  completePO(data) {
    return this.http.patch(
      `${this.urlBase}`,
      data
    ).pipe(map((resp: any) => resp.data));
  }

  getWearHouseBoxList(opts = {}) {
    return this.http
      .get(`${this.urlBaseForPicking}`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          // const nameSorter = (item) => item.name.toLowerCase();
          const sequenceSorter = (item) => item.sequence;
          // resp.data = _.sortBy(resp.data, [sequenceSorter, nameSorter], ["asc"]);
          return resp.data;
        })
      );
  }

}

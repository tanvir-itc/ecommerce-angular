import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { UserAutocomplete } from './user-autocomplete.component';

@NgModule({
  declarations: [
    UserAutocomplete,
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    MaterialModule,
    TranslateModule,
    FuseSharedModule,
    NgSelectModule
  ],
  exports: [
    UserAutocomplete,
  ]
})

export class UserAutocompleteModule {
}

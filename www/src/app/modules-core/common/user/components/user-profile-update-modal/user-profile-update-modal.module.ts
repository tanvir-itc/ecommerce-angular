import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { RouterModule } from '@angular/router';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { NgxSummernoteModule } from 'ngx-summernote';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { UserProfileUpdateModalComponent } from './user-profile-update-modal.component';
import { UserBasicComponent } from './user-basic/user-basic.component';

@NgModule({
  declarations: [
    UserProfileUpdateModalComponent,
    UserBasicComponent
  ],
  entryComponents: [
    UserProfileUpdateModalComponent,
    UserBasicComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FuseSharedModule,
    RouterModule,
    ButtonModule,
    DateTimeModule,
    GeneralModule,
    NgxSummernoteModule,
    AttachmentModule,
  ],
  exports: [
    UserProfileUpdateModalComponent,
  ]
})

export class UserProfileUpdateModalModule {
}
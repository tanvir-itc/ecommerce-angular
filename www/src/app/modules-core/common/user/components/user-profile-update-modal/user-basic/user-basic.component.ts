import { Component, OnInit, Input, Inject } from '@angular/core';
import { AuthService } from 'app/authentication/auth.service';
import { PriceTypeService } from 'app/modules-core/common/price-type/price-type.service';
import { UserService } from 'app/modules-core/common/user/user.service';
import * as _ from 'lodash';
import { APP_URL } from 'app/app.constants';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';

@Component({
  selector: 'user-basic',
  templateUrl: './user-basic.component.html',
})
export class UserBasicComponent implements OnInit {
  @Input()
  userId = null;

  @Input()
  typeFlag = null;

  user: any = null;
  newUser: any = null;
  newEmail: any = {
    editMode: false,
    email: null,
  };

  newPass: any = {
    editMode: false,
    oldPassword: null,
    password: null,
    confirmPassword: null,
  };

  salesRepList: any[] = null;
  constructor(
    public _auth: AuthService,
    public _userService: UserService,
    public _priceTypeService: PriceTypeService,
    private snackBar: SnackbarService,
  ) {
  }
  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this._userService.get(this.userId).subscribe((user) => {
      this.newUser = {
        editMode: false,
        firstName: _.get(user, 'firstName'),
        lastName: _.get(user, 'lastName'),
        phoneNo: _.get(user, 'phoneNo'),
        company: _.get(user, 'company'),
        address: _.get(user, 'address'),
        id: this.userId,
      };

    });
  }


  updateUser() {

    if (!_.get(this.newUser, "firstName")) {
      this.snackBar.open(`Please! Insert first name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "lastName")) {
      this.snackBar.open(`Please! Insert last name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "phoneNo")) {
      this.snackBar.open(`Please! Insert phone no!`, "Close");
      return;
    }
    this._userService.updateFields(this.userId, this.newUser).subscribe(resp => {
      this.snackBar.open(`User updated!`, 'Close');
      this.getUser();
    });
  }

  updatePassword() {
    if (this.newPass.password !== this.newPass.confirmPassword) {
      this.snackBar.open(`New password and confirm password must be same!`, 'Close');
      return
    }
    if (!this.newPass.oldPassword) {
      this.snackBar.open(`Please! Enter current password!`, 'Close');
      return
    }
    var newPasswordData = {
      password: this.newPass.password,
      confirmPassword: this.newPass.confirmPassword,
      oldPassword: this.newPass.oldPassword,
      userId: this.userId
    }
    this._userService.updatePassword(newPasswordData).subscribe(res => {
      this.snackBar.open(`Password  updated!`, 'Close');
      this.clearPassword()
    })
  }
  clearPassword() {
    this.newPass = {
      editMode: false,
      oldPassword: null,
      password: null,
      confirmPassword: null,
    };
  }

  updateEmail() {
    if (!this.newEmail.email) {
      this.snackBar.open(`Enter your new email address`, 'Close');
      return
    }
    let payload = {
      email: _.get(this.newEmail, 'email'),
      userId: this.userId,
      redirectUrl: `${APP_URL}email-verify`,
    }
    this._userService.updateEmail(payload).subscribe(res => {
      this.snackBar.open(`A verification link has been sent to your new email address.`, 'Close');
      this.clearEmail();
    })
  }
  clearEmail() {
    this.newEmail = {
      editMode: false,
      email: null,
    };
  }

}
import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import * as _ from 'lodash';
import { AuthService } from "app/authentication/auth.service";


@Component({
  selector: 'user-profile-update-modal',
  templateUrl: 'user-profile-update-modal.component.html',
})
export class UserProfileUpdateModalComponent {
  userId = null;
  constructor(
    public dialogRef: MatDialogRef<UserProfileUpdateModalComponent>,
    public _auth: AuthService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.userId = data.userId;
  }

}
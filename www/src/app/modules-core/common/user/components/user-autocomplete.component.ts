import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { AutocompleteBase } from 'app/modules-core/utility/autocomplete-base';
import * as _ from 'lodash';
import { UserService } from '../user.service';


@Component({
  selector: 'user-autocomplete',
  template: `
    <ng-select [items]="items"
              [clearable]="false"
              bindLabel="fullname"
              bindValue="id"
              placeholder="{{hidePlaceholder ? '' : 'User'}}"
              [(ngModel)]="preSelectId"
              (change)="onSelect($event)"
              (open)="onOpen($event)">
    </ng-select>
  `,
})

export class UserAutocomplete extends AutocompleteBase implements OnInit {

  @Input()
  type = null;

  @Input()
  permissionName = null;

  constructor(
    private _userService: UserService
  ) {
    super();
  }

  loadItems() {
    let params = null;

    if (this.type) {
      params = {
        rolename: this._userService.ROLES[this.type]
      }
    }
    if (this.permissionName) {
      params = {
        permissionname: this.permissionName
      }
    }

    this._userService.getAll(params).subscribe(data => {
      this.items = data;
      this.preSelect();
    });
  }

  ngOnInit() {
    if (!this.lazyload) {
      this.loadItems();
    }
  }

}

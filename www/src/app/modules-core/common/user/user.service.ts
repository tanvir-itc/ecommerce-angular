import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import { makeParams } from 'app/modules-core/utility/helpers';
import * as _ from 'lodash';
import { APP_URL } from 'app/app.constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  ROLES = {
    ADMIN: 'ROLE_ADMIN',
    MERCHANDISER: 'ROLE_MERCHANDISER',
    ROLE_MERCHANDISER_MANAGER: 'ROLE_MERCHANDISER_MANAGER',
  }

  urlBase = 'users';

  httpOptions = {
    params: new HttpParams()
  }

  constructor(
    private http: HttpClient,
  ) {
  }

  getAll(opts = {}) {
    return this.http.get(this.urlBase, {
      params: makeParams(_.merge({
        httpParams: this.httpOptions.params
      }, { queryParams: opts }))
    }).pipe(map((resp: any) => {
      resp.data = _.map(resp.data, (v, k) => {
        v.fullname = _.get(v, 'firstName') + " " + _.get(v, 'lastName');
        return v;
      });
      return resp.data;
    }));
  }


  get(id) {
    return this.http.get(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => {
        resp.data['fullname'] = _.get(resp.data, 'firstName') + " " + _.get(resp.data, 'lastName');
        return resp.data
      }));
  }

  create(payload) {
    return this.http.post(this.urlBase, payload)
      .pipe(map((resp: any) => resp.data));
  }

  update(id, payload) {
    return this.http.put(`${this.urlBase}/${id}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFields(id, payload) {
    return this.http.patch(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  userRoleChange(id, payload) {
    return this.http.patch(`${this.urlBase}/role-change`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  delete(id) {
    return this.http.delete(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  createUser(data) {
    let payload = {
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      password: data.password,
      roles: [{
        id: 1 //todo, remove hardcode
      }],
      factoryId: "2" //todo - remove
    }

    return this.http.post(
      this.urlBase,
      payload
    ).pipe(map((resp: any) => resp.data));
  }

  getUser(id) {
    return this.http.get(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => {
        resp.data['fullname'] = _.get(resp.data, 'firstName') + " " + _.get(resp.data, 'lastName');
        return resp.data
      }));
  }


  createEmployeeUser(data) {
    return this.http.post(
      this.urlBase,
      data
    ).pipe(map((resp: any) => resp.data));
  }
  updateEmployeeUser(data, id) {
    return this.http.patch(
      `${this.urlBase}/${id}`,
      data
    ).pipe(map((resp: any) => resp.data));
  }

  logout() {
    return this.http.post(
      `${this.urlBase}/logout`,
      {}
    ).pipe(map((resp: any) => resp.data));
  }

  updatePassword(payload) {
    return this.http.put(`${this.urlBase}/reset-password`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateEmail(payload) {
    return this.http.post(`${this.urlBase}/email-change`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  verifyEmail(payload) {
    return this.http.patch(`${this.urlBase}/email-change`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  adminPasswordReset(user) {
    let payload = {
      email: _.get(user, 'email'),
      redirectUrl: `${APP_URL}reset-password`,
    }
    return this.http.post(`${this.urlBase}/${_.get(user, 'id')}/reset-password/email`, payload)
      .pipe(map((resp: any) => resp.data));
  }


  getAllLoginHistory(opts = {}) {
    return this.http.get(`login-histories`, {
      params: makeParams(_.merge({
        httpParams: this.httpOptions.params
      }, { queryParams: opts }))
    }).pipe(map((resp: any) => {
      resp.data = _.map(resp.data, (v, k) => {
        v.fullname = _.get(v, 'firstName') + " " + _.get(v, 'lastName');
        return v;
      });
      return resp.data;
    }));
  }

  sendInvitaion(id) {
    return this.http.get(`${this.urlBase}/${id}/email?appUrl=${APP_URL}`)
      .pipe(map((resp: any) => resp.data));
  }

} //class



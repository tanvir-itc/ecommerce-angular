import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { UserComponent } from './pages/user-list/user-list.component';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { AttachmentModule } from '../attachment/attachment.module';
import { UserDetailsComponent } from './pages/user-details/user-details.component';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';


const routes = [
  {
    path: 'users',
    component: UserComponent,
    data: { permission: 'PERMISSION_MENU_USER_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  },
  {
    path: 'users/:id',
    component: UserDetailsComponent,
    data: { permission: 'PERMISSION_MENU_USER_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  }
];
@NgModule({
  declarations: [
    UserComponent,
    UserDetailsComponent,
  ],
  entryComponents: [
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    DateTimeModule,
    AttachmentModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
  ],
  exports: [
    UserComponent
  ]
})

export class UserModule {
}

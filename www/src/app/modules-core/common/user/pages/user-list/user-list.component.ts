import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import { fuseAnimations } from '@fuse/animations';
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/authentication/auth.service';
import { integerStringToInteger, removeEmptyFields } from 'app/modules-core/utility/helpers';
import * as _ from 'lodash';
import { RolePermissionService } from 'app/modules-core/common/role-permission/role-permission.service';
import { APP_URL } from 'app/app.constants';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { ApiService } from 'app/modules-core/api/api.service';


@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  animations: fuseAnimations,
})
export class UserComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  users: any[] = [];
  roleList: any[] = [];
  modalRef = null;
  newUser: any = null;

  constructor(
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _userService: UserService,
    private _roleService: RolePermissionService,
    private apiService: ApiService,
  ) {
    this._quickPanelSubjectService.heading.next('Users');
  }

  ngOnInit() {
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, 'snapshot.queryParams'));

    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
    this.getAllRoles();
  }

  clearAllFilter() {
    this.filter = {
      firstname: null,
      lastname: null,
      email: null,
      phoneno: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
    }
    return params;
  }



  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {

    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    this._userService.getAll(params).subscribe(data => {
      this.users = data;
      this.setFilterToURL(filterParams);
    });
  }


  openCreateModal(templateRef) {
    this.clearCreateObject();
    this.modalRef = this.dialog.open(templateRef, {
      width: "600px",
      disableClose: true,
    });
  }

  clearCreateObject() {
    this.newUser = {
      email: null,
      firstName: null,
      lastName: null,
      phoneNo: null,
      address: null,
      company: null,
      roleName: null,
    }
  }

  createUser() {
    var payload = _.merge({}, this.newUser);
    if (!_.get(this.newUser, "roleName")) {
      this.snackBar.open(`Please! Select role!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "firstName")) {
      this.snackBar.open(`Please! Insert first name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "lastName")) {
      this.snackBar.open(`Please! Insert last name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "email")) {
      this.snackBar.open(`Please! Insert email!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "phoneNo")) {
      this.snackBar.open(`Please! Insert phone no!`, "Close");
      return;
    }


    payload['appUrl'] = `${APP_URL}login`;
    this._userService.create(payload).subscribe(resp => {
      this._router.navigateByUrl(`users/${resp.id}`);
      this.snackBar.open(`User created!`, 'Close');
      this.modalRef.close();
    });
  }

  goLink(userId) {
    this._router.navigateByUrl(`users/${userId}`);
  }

  deleteUser(id) {
    if (!confirm("User delete confirm?")) {
      return;
    }
    this._userService.delete(id).subscribe(res => {
      this.snackBar.open(`User Deleted!`, "Close");
      this.applyFilters(this.filter)
    });
  }

  getAllRoles() {
    this._roleService.getAllRoles().subscribe((data) => {
      this.roleList = data;
    });
  }

  onEmailSend(user) {
    if (!confirm("Change password link send confirmation?")) {
      return;
    }
    this._userService.adminPasswordReset(user).subscribe(resp => {
      this.snackBar.open(`A change password link send in this user email!`, 'Close');
    });
  }

  selectedRole: any = null
  openRoleChangeModal(templateRef, role, user) {
    this.selectedRole = {
      user: user,
      role: role
    }
    this.modalRef = this.dialog.open(templateRef, {
      width: "600px",
    });
  }
  changeUserRole() {
    let payload = {
      id: this.selectedRole.user.id,
    }
    if (this.selectedRole.role == 'ROLE_PARTNER') {
      payload['roleName'] = 'ROLE_SALES'
    }
    if (this.selectedRole.role == 'ROLE_SALES') {
      payload['roleName'] = 'ROLE_PARTNER'
    }
    this._userService.updateFields(this.selectedRole.user.id, payload).subscribe(resp => {
      this.applyFilters(this.filter)
      this.modalRef.close();
    });;
  }


} //class


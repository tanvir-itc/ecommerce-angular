import { Component, OnInit, Input } from "@angular/core";
import * as _ from "lodash";
import { fuseAnimations } from "@fuse/animations";
import { AuthService } from "app/authentication/auth.service";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "app/modules-core/common/user/user.service";
import { MatDialog } from "@angular/material";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";

@Component({
  selector: "user-details",
  templateUrl: "./user-details.component.html",
  animations: fuseAnimations,
})
export class UserDetailsComponent implements OnInit {
  id: any = null;

  user: any = null;

  newUser: any = null;
  modalRef: any = null;

  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _userService: UserService,
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
  ) {
    this._quickPanelSubjectService.heading.next("User Details");
  }

  ngOnInit() {
    this.id = +this._route.snapshot.paramMap.get("id");
    this.getUserDetails();
  }

  getUserDetails() {
    this.getUser();
  }

  getUser() {
    this._userService.getUser(this.id)
      .subscribe(user => this.user = user);
  }


  openEditModal(templateRef) {
    this.newUser = {
      firstName: _.get(this.user, 'firstName'),
      lastName: _.get(this.user, 'lastName'),
      phoneNo: _.get(this.user, 'phoneNo'),
      company: _.get(this.user, 'company'),
      address: _.get(this.user, 'address'),
      id: this.id,
    }
    this.modalRef = this.dialog.open(templateRef, {
      width: "600px",
    });
  }

  userUpdate() {
    if (!_.get(this.newUser, "firstName")) {
      this.snackBar.open(`Please! Insert first name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "lastName")) {
      this.snackBar.open(`Please! Insert last name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "phoneNo")) {
      this.snackBar.open(`Please! Insert phone no!`, "Close");
      return;
    }
    this._userService.updateFields(this.id, this.newUser).subscribe(resp => {
      this.snackBar.open(`User updated!`, 'Close');
      this.modalRef.close();
      this.getUser();
    });
  }

  userEnableDisable(flag) {
    let payload = { id: this.id, enable: flag };
    this._userService.updateFields(this.id, payload).subscribe(resp => {
      this.snackBar.open(`User updated!`, 'Close')
      this.getUser();
    });
  }
}

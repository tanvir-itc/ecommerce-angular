import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class BankService {

  urlBase = 'banks';


  constructor(
    private http: HttpClient,
  ) { }

  getSectionDetails(id) {
    return this.http.get(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  get() {
    return this.http.get(this.urlBase)
      .pipe(map((resp: any) => resp.data));
  }

  getById(id) {
    return this.http.get(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  delete(id) {
    return this.http.delete(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  create(data) {
    return this.http.post(
      `${this.urlBase}`,
      data
    ).pipe(map((resp: any) => resp.data));
  }

  update(data) {
    return this.http.put(
      `${this.urlBase}`,
      data
    ).pipe(map((resp: any) => resp.data));
  }

}

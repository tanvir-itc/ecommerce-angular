import { NgModule } from '@angular/core';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { BankAutocomplete } from './bank-autocomplete.component';


@NgModule({
  declarations: [
    BankAutocomplete
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    MaterialModule,
    TranslateModule,

    FuseSharedModule,

    MaterialModule,

    NgSelectModule,
  ],
  exports: [
    BankAutocomplete
  ]
})

export class BankAutocompleteModule {
}

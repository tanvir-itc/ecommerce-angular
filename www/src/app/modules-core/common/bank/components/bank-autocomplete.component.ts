import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import * as _ from 'lodash';
import { AutocompleteBase } from 'app/modules-core/utility/autocomplete-base';
import { BankService } from '../bank.service';

@Component({
  selector: 'bank-autocomplete',
  template: `
    <ng-select [items]="items"
              [clearable]="false"
              bindLabel="name"
              bindValue="id"
              placeholder="{{hidePlaceholder ? '' : 'Bank'}}"
              [(ngModel)]="preSelectId"
              (change)="onSelect($event)"
              (open)="onOpen($event)">
    </ng-select>
  `,
})

export class BankAutocomplete extends AutocompleteBase implements OnInit {
  constructor(
    private _bankService: BankService
  ) {
    super();
  }

  loadItems() {
    this._bankService.get().subscribe(data => {
      this.items = data;
      this.preSelect();
    });
  }

  ngOnInit() {
    if (!this.lazyload) {
      this.loadItems();
    }
  }

}

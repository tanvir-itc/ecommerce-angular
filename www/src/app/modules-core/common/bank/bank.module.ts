import { NgModule } from '@angular/core';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  declarations: [
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    MaterialModule,
    // RouterModule.forChild(routes),
    TranslateModule,

    FuseSharedModule,

    MaterialModule,

    NgSelectModule,
  ],
  exports: [
  ]
})

export class BankModule {
}

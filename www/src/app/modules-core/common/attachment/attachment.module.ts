import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { FileUploadModalComponent } from './components/file-upload-modal/file-upload-modal.component';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { PhotoGalleryComponent } from './components/photo-gallery/photo-gallery.component';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { SinglePhotoCardComponent } from './components/single-photo-card/single-photo-card.component';
import { PhotoGalleryHorizontalComponent } from './components/photo-gallery-horizontal/photo-gallery-horizontal.component';
import { HeatmapModule } from 'app/modules-core/merchandising/style/components/heatmap/heatmap.module';
import { DragulaModule } from 'ng2-dragula';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    FileUploadModalComponent,
    PhotoGalleryComponent,
    PhotoGalleryHorizontalComponent,
    SinglePhotoCardComponent,
  ],
  entryComponents: [
    FileUploadModalComponent,
    PhotoGalleryComponent,
    SinglePhotoCardComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    MaterialModule,
    DropzoneModule,
    ButtonModule,
    HeatmapModule,
    DragulaModule.forRoot(),
    FormsModule,
  ],
  exports: [
    FileUploadModalComponent,
    PhotoGalleryComponent,
    PhotoGalleryHorizontalComponent,
    SinglePhotoCardComponent,
  ]
})

export class AttachmentModule {
}

import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material';
import * as _ from 'lodash';
import { AttachmentService } from 'app/modules-core/common/attachment/attachment.service';
import { FileUploadModalComponent } from 'app/modules-core/common/attachment/components/file-upload-modal/file-upload-modal.component';
import { AuthService } from 'app/authentication/auth.service';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { DragulaService } from 'ng2-dragula';
import { Subscription } from 'rxjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'photo-gallery',
  template: `
    <div style="display: flex">
      <div style="flex: 0 0 125px;">
        <div style="padding: 5px; margin: 3px;" class="text-center">
          <button mat-raised-button style="width: 100%!important;" *ngIf="enableEdit && _auth.hasPermission('PERMISSION_ADMIN_USER')"
            (click)="openImageModal()">
            <mat-icon>add_a_photo</mat-icon>
          </button>
        </div>
        <div dragula="IMAGE_ITEMS"
          id="images"
          [(dragulaModel)]="images">
          <div class="drag-cursor" style="padding: 5px; margin: 3px; cursor: pointer; position: relative;" *ngFor="let atta of images"
            (click)="bannerImage = atta.fullpath">
            <img [src]="atta.thumbFullpath">
            <btn-icon style="position: absolute; top: 0px; left: 5px; z-index: 99999;" *ngIf="enableEdit && _auth.hasPermission('PERMISSION_ADMIN_USER')"
                [text]="'delete'"
                (click)="deleteAttachmentImage(atta.id)"></btn-icon>
            <mat-checkbox style="position: absolute; top: 5px; right: 0px; z-index: 99999;" *ngIf="enableEdit && _auth.hasPermission('PERMISSION_ADMIN_USER')"
               matTooltip="Is Default" (change)="makeDefault(atta.id,$event)" [(ngModel)]="atta.isDefault"></mat-checkbox>
          </div>        
        </div>

      </div>
      <div>
        <div style="padding: 5px; margin: 8px;" *ngIf="bannerImage">
          <img [src]="bannerImage">
        </div>
      </div>
    </div>
  `,

})

export class PhotoGalleryComponent implements OnInit {
  @Input()
  entityType: string = null;

  @Input()
  entityId: number = null;

  @Input()
  enableEdit: boolean = true;

  @Input()
  titleHide: boolean = false;

  images = [];
  bannerImage = null;
  atta = { 'isDefault': false }
  BAG = 'IMAGE_ITEMS';
  subs = new Subscription();
  constructor(
    private _attachmentService: AttachmentService,
    private snackBar: SnackbarService,
    public _auth: AuthService,
    private _dragulaService: DragulaService,
    public dialog: MatDialog,
  ) {
    _dragulaService.createGroup(this.BAG, {
      revertOnSpill: true
    });
    this.subs.add(_dragulaService.drop(this.BAG)
      .subscribe(v => {
        this.updateSequence();
      })
    );
  }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges) {
    this.getImages();
  }

  getImages() {
    this._attachmentService.getAll({ entitytype: this.entityType, entityid: this.entityId })
      .subscribe(data => {
        this.images = data;
        if (_.find(this.images, ['sequence', null])) {
          this.updateSequence();
        }

        this.bannerImage = null;
        this.bannerImage = _.chain(this.images)
          .head()
          .get('fullpath', this.bannerImage)
          .value();
      });
  }

  openImageModal() {
    const dialogRef = this.dialog.open(FileUploadModalComponent, {
      width: '40%',
      data: {
        name: "Image Upload",
        entityType: this.entityType,
        entityId: this.entityId,
        isThumbnailRequired: true,
        config: {
          acceptedFiles: 'image/*',
        }
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getImages();
    });
  }

  updateSequence() {
    let payload = [];
    _.each(this.images, (v, i) => {
      payload.push({ id: v.id, sequence: i + 1 });
    });
    this._attachmentService.updateList(payload).subscribe(res => {
      this.getImages();
    });
  }


  deleteAttachmentImage(id) {
    if (!confirm("Are you want to remove it?")) {
      return
    }
    this._attachmentService.delete(id).subscribe(res => {
      this.getImages();
      this.snackBar.open(`Image deleted!`, 'Close');
    });
  }

  makeDefault(id, e) {
    let payload = [{ "id": id, "isDefault": _.get(e, 'checked') }];
    this._attachmentService.updateList(payload).subscribe(res => {
      this.getImages();
    });
  }

  ngOnDestroy(): void {
    this._dragulaService.destroy(this.BAG);
    this.subs.unsubscribe();
  }
}
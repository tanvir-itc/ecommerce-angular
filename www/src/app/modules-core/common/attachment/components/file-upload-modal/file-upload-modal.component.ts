import { Component, Inject, ViewEncapsulation, Input } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import * as _ from 'lodash';
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import { SERVER_API_URL } from "app/app.constants";
import { LOCAL_STORAGE_TOKEN } from "app/storage.constants";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";

@Component({
  selector: 'file-upload-modal',
  templateUrl: 'file-upload-modal.component.html',
  styleUrls: ['./file-upload-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FileUploadModalComponent {

  @Input()
  name = 'Upload';

  @Input()
  subtitle = '';

  items: any[] = [];

  public config: DropzoneConfigInterface = {
    url: SERVER_API_URL + 'attachments',
    maxFiles: 10,
    clickable: true,
    acceptedFiles: 'image/*, .pdf, .xlsx, .xls, .doc, .docx, .psd',
    createImageThumbnails: true,
    headers: {
      accessToken: window.localStorage.getItem(LOCAL_STORAGE_TOKEN)
    },
    params: {}
  };

  constructor(
    public dialogRef: MatDialogRef<FileUploadModalComponent>,
    private snackBar: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.config = _.merge(this.config, data.config);
    this.config.params['entityType'] = data.entityType;

    this.config.params['entityId'] = _.get(data, 'entityId', 0);

    this.config.params['isThumbnailRequired'] = data.isThumbnailRequired || false;
    this.name = data.name || this.name;
    this.subtitle = data.subtitle || this.subtitle;
  }

  ngOnInit() {
    this.items = [];
  }

  onError(event) {
    //this.snackBar.open(`File upload failed!`, 'Close');
  }

  onSuccess(event) {
    event[1].data['thumbFullpath'] = `${SERVER_API_URL}${event[1].data.path}${event[1].data.thumnailFileName}`;
    event[1].data['mediumFullpath'] = `${SERVER_API_URL}${event[1].data.path}${event[1].data.mediumFileName}`;
    event[1].data['fullpath'] = `${SERVER_API_URL}${event[1].data.path}${event[1].data.fileName}`;
    this.items.push(event[1].data);
    this.config.maxFiles == 1 && this.modalClose();
  }

  modalClose() {
    this.dialogRef.close(this.items || null);
  }

}
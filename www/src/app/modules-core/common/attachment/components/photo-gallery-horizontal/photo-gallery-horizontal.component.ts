import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material';
import * as _ from 'lodash';
import { AttachmentService } from 'app/modules-core/common/attachment/attachment.service';

@Component({
  selector: 'photo-gallery-horizontal',
  template: `
    <div style="display: flex;">
      <div style="width: 250px; margin: 5px;" *ngIf="images?.length">
        <div style="padding: 5px; margin: 3px; cursor: pointer;" *ngFor="let atta of images"
          (click)="bannerImage = atta.fullpath">
          <img [src]="atta.thumbFullpath">
        </div>
      </div>
      <div>
        <div style="padding: 5px; margin: 8px;" *ngIf="bannerImage">
          <img [src]="bannerImage">
        </div>
        <div *ngIf="heatmap" style="margin: -12px 0; padding: 0 10px;">
          <heatmap [value]="heatmap"></heatmap>
        </div>
      </div>
    </div>
  `,
})

export class PhotoGalleryHorizontalComponent implements OnInit {
  @Input()
  entityType: string = null;

  @Input()
  entityId: number = null;

  @Input()
  heatmap: number = null;

  images = [];
  bannerImage = null;

  constructor(
    private _attachmentService: AttachmentService,
    public dialog: MatDialog,
  ) {
  }
  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.getImages();
  }

  getImages() {
    this._attachmentService.getAll({ entitytype: this.entityType, entityid: this.entityId })
      .subscribe(data => {
        this.images = data;
        this.bannerImage = null;
        this.bannerImage = _.chain(this.images)
          .head()
          .get('fullpath', this.bannerImage)
          .value();
      });
  }

}
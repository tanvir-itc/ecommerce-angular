import { Component, Input, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';
import * as _ from 'lodash';
import { AttachmentService } from 'app/modules-core/common/attachment/attachment.service';
import { FileUploadModalComponent } from 'app/modules-core/common/attachment/components/file-upload-modal/file-upload-modal.component';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';

@Component({
  selector: 'single-photo-card',
  template: `
    <div class="single-photo-card" [ngClass]="{'single-photo-card-placeholder': !bannerImage}" [ngClass]="{'withborder': withborder}">
      <ng-container *ngIf="!bannerImage" >
        <btn-icon *ngIf="enableEdit" [text]="'add'"
            (click)="openImageModal()"></btn-icon>
      </ng-container>
      <div class="thumb" *ngIf="bannerImage">
        <btn-icon class="thumb-btn" *ngIf="enableEdit"
          [text]="'delete'"
          (click)="deleteAttachmentImage()"></btn-icon>
        <img
          [src]="bannerImage">
      </div>
    </div>
  `,
  styleUrls: ['./single-photo-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class SinglePhotoCardComponent implements OnInit {
  @Input()
  name: string = "Image Upload";
  @Input()
  subtitle: string = null;

  @Input()
  entityType: string = null;

  @Input()
  entityId: number = null;


  @Input()
  enableEdit: boolean = true;

  @Input()
  withborder: boolean = true;

  images = [];
  bannerImage = null;

  constructor(
    private _attachmentService: AttachmentService,
    private snackBar: SnackbarService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges) {
    this.getImages();
  }

  getImages() {
    this._attachmentService.getAll({ entitytype: this.entityType, entityid: this.entityId })
      .subscribe(data => {
        this.images = data;
        this.bannerImage = null;
        this.bannerImage = _.chain(this.images)
          .head()
          .get('fullpath', null)
          .value();
      });
  }

  openImageModal() {
    const dialogRef = this.dialog.open(FileUploadModalComponent, {
      width: '40%',
      data: {
        name: this.name,
        subtitle: this.subtitle,
        entityType: this.entityType,
        entityId: this.entityId,
        isThumbnailRequired: true,
        config: {
          acceptedFiles: 'image/*',
          maxFiles: 1
        }
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getImages();
    });
  }


  deleteAttachmentImage() {
    if (!confirm("Are you want to remove it?")) {
      return
    }
    let id = _.chain(this.images)
      .head()
      .get('id')
      .value();
    this._attachmentService.delete(id).subscribe(res => {
      this.getImages();
      this.snackBar.open(`Image deleted!`, 'Close');
    });
  }

}
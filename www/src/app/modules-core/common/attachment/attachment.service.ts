import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import { makeParams } from 'app/modules-core/utility/helpers';
import * as _ from 'lodash';
import { SERVER_API_URL } from 'app/app.constants';

@Injectable({
  providedIn: 'root'
})
export class AttachmentService {
  urlBase = 'attachments';

  httpOptions = {
    params: new HttpParams()
  }

  constructor(
    private http: HttpClient,
  ) {
  }

  getAll(opts = {}) {
    return this.http.get(this.urlBase, {
      params: makeParams(_.merge({
        httpParams: this.httpOptions.params
      }, { queryParams: opts }))
    }).pipe(map((resp: any) => {
      resp.data = _.map(resp.data, v => {
        v.fullpath = `${SERVER_API_URL}${v.path}${v.fileName}`;
        v.mediumFullpath = `${SERVER_API_URL}${v.path}${v.mediumFileName}`;
        v.thumbFullpath = `${SERVER_API_URL}${v.path}${v.thumnailFileName}`;
        return v;
      });
      return resp.data;
    }));
  }

  updateList(payload) {
    return this.http
      .patch(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  delete(id) {
    return this.http.delete(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

} //class



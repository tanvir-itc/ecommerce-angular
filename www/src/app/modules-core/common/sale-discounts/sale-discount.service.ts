import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from "rxjs/internal/operators/map";
import { makeParams } from "app/modules-core/utility/helpers";
import * as _ from "lodash";

@Injectable({
  providedIn: "root",
})
export class SaleDiscountService {
  urlBase = "sale-discounts";

  httpOptions = {
    params: new HttpParams(),
  };
  constructor(private http: HttpClient) { }

  getAll(opts = {}) {
    return this.http
      .get(`${this.urlBase}`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          return resp.data;
        })
      );
  }

  get(id) {
    return this.http
      .get(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  create(payload) {
    return this.http
      .post(this.urlBase, payload)
      .pipe(map((resp: any) => resp.data));
  }

  update(id, payload) {
    return this.http
      .put(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }
  updateAll(payload) {
    return this.http
      .put(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFields(id, payload) {
    return this.http
      .patch(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  delete(id, payload = {}) {
    return this.http
      .delete(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }
}

import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from "rxjs/internal/operators/map";
import { makeParams } from "app/modules-core/utility/helpers";
import * as _ from "lodash";

@Injectable({
  providedIn: "root",
})
export class PatnerSalesRepService {
  urlBase = "users/multiple";

  httpOptions = {
    params: new HttpParams(),
  };
  constructor(private http: HttpClient) { }


  get(id) {
    return this.http
      .get(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  addSalesRep(payload) {
    return this.http
      .patch(this.urlBase, payload)
      .pipe(map((resp: any) => resp.data));
  }

  update(payload) {
    return this.http
      .put(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }
  updateAll(payload) {
    return this.http
      .put(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFields(id, payload) {
    return this.http
      .patch(`${this.urlBase}/${id}`, payload)
      .pipe(map((resp: any) => resp.data));
  }


}

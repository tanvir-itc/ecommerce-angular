import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { PartnerListComponent } from './pages/partner-list/partner-list.component';
import { PartnerDetailsModule } from './components/partner-details-modal/sales-rep-details.module';


const routes = [
  {
    path: 'sales-managers',
    component: PartnerListComponent,
    data: { permission: 'PERMISSION_MENU_PARTNER' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  }
];
@NgModule({
  declarations: [
    PartnerListComponent,
  ],
  entryComponents: [
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    DateTimeModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
    AttachmentModule,
    PartnerDetailsModule
  ],
  exports: [
  ]
})

export class PartnerModule {
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { RouterModule } from '@angular/router';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { NgxSummernoteModule } from 'ngx-summernote';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { PartnerDetailsModalComponent } from './partner-details-modal.component';
import { PartnerCommissionComponent } from './partner-commission/partner-commission.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { PartnerBasicComponent } from './partner-basic/partner-basic.component';
import { PartnerSalesRepListComponent } from './partner-sales-rep-list/partner-sales-rep-list.component';

@NgModule({
  declarations: [
    PartnerDetailsModalComponent,
    PartnerBasicComponent,
    PartnerSalesRepListComponent,
    PartnerCommissionComponent,
  ],
  entryComponents: [
    PartnerDetailsModalComponent,
    PartnerBasicComponent,
    PartnerSalesRepListComponent,
    PartnerCommissionComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FuseSharedModule,
    RouterModule,
    ButtonModule,
    DateTimeModule,
    GeneralModule,
    NgxSummernoteModule,
    AttachmentModule,
    NgSelectModule,
  ],
  exports: [
    PartnerDetailsModalComponent
  ]
})

export class PartnerDetailsModule {
}
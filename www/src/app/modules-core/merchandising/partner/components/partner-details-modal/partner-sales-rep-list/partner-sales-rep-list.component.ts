import { Component, OnInit, Input, Inject } from '@angular/core';
import { AuthService } from 'app/authentication/auth.service';
import { UserService } from 'app/modules-core/common/user/user.service';
import * as _ from 'lodash';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { MatDialog } from '@angular/material';
import { CustomerService } from 'app/modules-core/merchandising/customers/customer.service';
import { PatnerSalesRepService } from 'app/modules-core/common/partner-sales-rep/partner-sales-rep.service';

@Component({
  selector: 'partner-sales-rep-list',
  templateUrl: './partner-sales-rep-list.component.html',
})
export class PartnerSalesRepListComponent implements OnInit {
  @Input()
  partnerId = null;


  userSalesRepList: any[] = [];
  salesRepList: any[] = [];
  customerList: any[] = [];
  modalAddRep: any = null;
  constructor(
    public _auth: AuthService,
    public _userService: UserService,
    private _partnerSalesRepService: PatnerSalesRepService,
    public _customerService: CustomerService,
    private snackBar: SnackbarService,
    private dialog: MatDialog,
  ) {
  }
  ngOnInit() {
    this.getPartnerSaleReps();
    this.getCustomers();
  }


  getPartnerSaleReps() {
    this._userService.getAll({ partnerid: this.partnerId }).subscribe(resp => {
      this.userSalesRepList = resp;
    });
  }

  getCustomers() {
    this._customerService.getAll({ partnerid: this.partnerId, requested: false }).subscribe(resp => {
      this.customerList = resp;
    });
  }

  getSalesReps() {
    this._userService.getAll({ rolename: 'ROLE_SALES' }).subscribe((data) => {
      let userSalesRepListIds = _.map(this.userSalesRepList, 'id');
      this.salesRepList = [];
      _.each(data, item => {
        if (!userSalesRepListIds.includes(item.id)) {
          this.salesRepList.push(item);
        }
      });
    });
  }




  openAddSalesRepModal(templateRef) {
    this.getSalesReps();
    this.modalAddRep = this.dialog.open(templateRef, {
      width: "90%",
      disableClose: true,
    });
  }

  closeModal() {
    this.modalAddRep.close();
  }

  addSalesRep() {
    let payload = [];
    _.each(this.salesRepList, item => {
      if (_.get(item, 'select')) {
        payload.push({
          id: item.id,
          partnerUser: { id: this.partnerId }
        });
      }
    });

    if (!payload.length) {
      this.snackBar.open(`Please, select minimum one partner.!`, "Close");
      return;
    }
    this._partnerSalesRepService.addSalesRep(payload).subscribe(resp => {
      this.getPartnerSaleReps();
      this.getCustomers();
      this.closeModal()
    });
  }


  deleteSalesRep(id, index) {
    if (!confirm("Are you sure?")) {
      return;
    }
    let payload = [{
      id: id,
      partnerUser: null,
    }]
    this._partnerSalesRepService.addSalesRep(payload).subscribe(resp => {
      this.userSalesRepList.splice(index, 1);
      this.getSalesReps();
      this.getCustomers();
    });
  }


}
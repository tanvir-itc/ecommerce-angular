import { Component, OnInit, Input, Inject } from '@angular/core';
import { AuthService } from 'app/authentication/auth.service';
import { UserService } from 'app/modules-core/common/user/user.service';
import * as _ from 'lodash';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';

@Component({
  selector: 'partner-basic',
  templateUrl: './partner-basic.component.html',
})
export class PartnerBasicComponent implements OnInit {
  @Input()
  partnerId = null;
  user: any = null;
  newUser: any = null;
  constructor(
    public _auth: AuthService,
    public _userService: UserService,
    private snackBar: SnackbarService,
  ) {
  }
  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this._userService.getUser(this.partnerId)
      .subscribe(user => {
        this.newUser = null;
        this.newUser = {
          editMode: false,
          firstName: _.get(user, 'firstName'),
          lastName: _.get(user, 'lastName'),
          phoneNo: _.get(user, 'phoneNo'),
          company: _.get(user, 'company'),
          address: _.get(user, 'address'),
          id: this.partnerId,
        }

      });
  }

  updateUser() {
    if (!_.get(this.newUser, "firstName")) {
      this.snackBar.open(`Please! Insert first name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "lastName")) {
      this.snackBar.open(`Please! Insert last name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "phoneNo")) {
      this.snackBar.open(`Please! Insert phone no!`, "Close");
      return;
    }


    this._userService.updateFields(this.partnerId, this.newUser).subscribe(resp => {
      this.snackBar.open(`User updated!`, 'Close');
      this.getUser();
    });
  }





}
import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import * as _ from 'lodash';
import { AuthService } from "app/authentication/auth.service";


@Component({
  selector: 'partner-details-modal',
  templateUrl: 'partner-details-modal.component.html',
})
export class PartnerDetailsModalComponent {
  partnerId = null;
  constructor(
    public dialogRef: MatDialogRef<PartnerDetailsModalComponent>,
    public _auth: AuthService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.partnerId = data.partnerId;
  }

}
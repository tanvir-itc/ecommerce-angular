import { Component, OnInit, Input } from "@angular/core";
import * as _ from "lodash";
import { fuseAnimations } from "@fuse/animations";
import { AuthService } from "app/authentication/auth.service";
import { MatDialog } from "@angular/material";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "app/modules-core/common/user/user.service";
import { InventoryLocationService } from "app/modules-core/common/inventory-location/inventory-location.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";

@Component({
  selector: "factory-user-details",
  templateUrl: "./factory-user-details.component.html",
  animations: fuseAnimations,
})
export class FactoryUserDetailsComponent implements OnInit {
  id: any = null;

  user: any = null;
  userLocationList: any[] = [];
  locationList: any[] = [];
  modalRef: any = null;
  newLocation: any = null;

  newUser: any = null;

  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _userService: UserService,
    private _locationService: InventoryLocationService,
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
  ) {
    this._quickPanelSubjectService.heading.next("Factory User Details");
  }

  ngOnInit() {
    this.id = +this._route.snapshot.paramMap.get("id");
    this.getUserDetails();
  }


  getUserDetails() {
    this.getUser();
    this.getUserLocations();
  }

  getUser() {
    this._userService.getUser(this.id)
      .subscribe(user => this.user = user);
  }

  getUserLocations() {
    this._locationService.getUserLocations({ "user.id": this.id }).subscribe(resp => {
      this.userLocationList = resp;
    });
  }

  getLocations() {
    this._locationService.getAll().subscribe((data) => {
      this.locationList = data;
    });
  }


  clearLocationAddObject() {
    this.newLocation = null;
    this.newLocation = {
      inventoryLocation: {
        id: null
      },
      userId: this.id,
    }
  }

  openAddLocationModal(templateRef) {
    this.clearLocationAddObject();
    this.getLocations();
    this.modalRef = this.dialog.open(templateRef, {
      width: "700px",
      disableClose: true,
    });
    this.modalRef.afterClosed().subscribe((resp) => {
      this.getLocations();
    });
  }

  addLocation() {
    if (!_.get(this.newLocation, "inventoryLocation.id")) {
      this.snackBar.open(`Please, select location!`, "Close");
      return;
    }
    this._locationService.addUserLocation(this.newLocation).subscribe(resp => {
      this.getUserLocations();
      this.clearLocationAddObject();
      this.snackBar.open(`Location added!`, "Close");
    });
  }

  deleteLocation(id) {
    if (!confirm("Are you sure?")) {
      return;
    }
    this._locationService.deleteUserLocation(id).subscribe(resp => {
      this.getUserLocations();
      this.snackBar.open(`Location deleted!`, "Close");
    });
  }


  openEditModal(templateRef) {
    this.newUser = {
      firstName: _.get(this.user, 'firstName'),
      lastName: _.get(this.user, 'lastName'),
      phoneNo: _.get(this.user, 'phoneNo'),
      company: _.get(this.user, 'company'),
      address: _.get(this.user, 'address'),
      id: this.id,
    }
    this.modalRef = this.dialog.open(templateRef, {
      width: "600px",
    });
  }

  userUpdate() {
    if (!_.get(this.newUser, "firstName")) {
      this.snackBar.open(`Please! Insert first name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "lastName")) {
      this.snackBar.open(`Please! Insert last name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "phoneNo")) {
      this.snackBar.open(`Please! Insert phone no!`, "Close");
      return;
    }


    this._userService.updateFields(this.id, this.newUser).subscribe(resp => {
      this.snackBar.open(`User updated!`, 'Close');
      this.modalRef.close();
      this.getUser();
    });
  }

  userEnableDisable(flag) {
    let payload = { id: this.id, enable: flag };
    this._userService.updateFields(this.id, payload).subscribe(resp => {
      this.snackBar.open(`User updated!`, 'Close')
      this.getUser();
    });
  }

}

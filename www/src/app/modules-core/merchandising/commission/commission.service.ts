import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from "rxjs/internal/operators/map";
import { makeParams } from "app/modules-core/utility/helpers";
import * as _ from "lodash";

@Injectable({
  providedIn: "root",
})
export class CommissionService {
  urlCommission = "commissions";
  urlCommissionSetup = "commission-setups";
  urlCommissionSetupStyle = "commission-setup-styles";

  httpOptions = {
    params: new HttpParams(),
  };
  constructor(private http: HttpClient) { }

  getAllCommissionReport(opts = {}) {
    return this.http
      .get(`${this.urlCommission}/report`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          let balance = 0;
          resp.data = _.map(resp.data, item => {
            if (item.transactionType == 'Invoice') {
              balance = _.add(balance, item.amount)
            } else {
              balance = _.subtract(balance, item.amount)
            }
            if (item.commissionType == 'SALE_REP') {
              item.comType = "Standard"
            } else if (item.commissionType == 'INVESTOR') {
              item.comType = "Investor"
            }
            item.balance = _.add(balance, 0)
            return item;
          });
          return resp.data;
        })
      );
  }

  getAllSalesCommission(opts = {}) {
    return this.http
      .get(`${this.urlCommissionSetup}`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          return resp.data;
        })
      );
  }

  createSalesCommission(payload) {
    return this.http
      .post(this.urlCommissionSetup, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateSalesCommission(id, payload) {
    return this.http
      .put(`${this.urlCommissionSetup}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFieldsSalesCommission(payload) {
    return this.http
      .patch(`${this.urlCommissionSetup}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  getAllSalesCommissionStyle(opts = {}) {
    return this.http
      .get(`${this.urlCommissionSetupStyle}`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          return resp.data;
        })
      );
  }

  createSalesCommissionStyle(payload) {
    return this.http
      .post(this.urlCommissionSetupStyle, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateSalesCommissionStyle(id, payload) {
    return this.http
      .put(`${this.urlCommissionSetupStyle}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFieldsSalesCommissionStyle(payload) {
    return this.http
      .patch(`${this.urlCommissionSetupStyle}`, payload)
      .pipe(map((resp: any) => resp.data));
  }


  deleteSalesCommissionStyle(id) {
    return this.http.delete(`${this.urlCommissionSetupStyle}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  createSalesCommissionStyleItem(id, payload) {
    return this.http
      .post(`${this.urlCommissionSetupStyle}/${id}/details`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFieldsSalesCommissionStyleItem(id, payload) {
    return this.http
      .patch(`${this.urlCommissionSetupStyle}/${id}/details`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  deleteSalesCommissionStyleItem(id, detailsId) {
    return this.http
      .delete(`${this.urlCommissionSetupStyle}/${id}/details/${detailsId}`)
      .pipe(map((resp: any) => resp.data));
  }
}

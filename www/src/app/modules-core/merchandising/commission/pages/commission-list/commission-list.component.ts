import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  stringToIntegerArray,
  removeEmptyFields,
  integerArrayToString,
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { CustomerService } from "app/modules-core/merchandising/customers/customer.service";

import { UserService } from "app/modules-core/common/user/user.service";
import { CommissionService } from "../../commission.service";
import { MatDialog } from "@angular/material";
import { CommissionPaymentModalComponent } from "../../components/commission-payment-modal/commission-payment-modal.component";
import { CommissionDiscountModalComponent } from "../../components/commission-discount-modal/commission-discount-modal.component";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { CommissionDiscountUpdateModalComponent } from "../../components/commission-discount-update-modal/commission-discount-update-modal.component";
import { CommissionPaymentUpdateModalComponent } from "../../components/commission-payment-update-modal/commission-payment-update-modal.component";
import { CommissionDetailsModalComponent } from "../../components/commission-details-modal/commission-details-modal.component";

@Component({
  selector: "commission-list",
  templateUrl: "./commission-list.component.html",
})
export class CommissionListComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  commissionList: any[] = [];
  total: any = {};
  customerList: any[] = [];
  userList: any[] = [];

  sorter: any = {
    field: null,
    sortType: 'asc',
    fieldType: 'text',
  }

  userRole: any = null;

  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _customerService: CustomerService,
    private _commissionService: CommissionService,
    private _userService: UserService,
    private snackBar: SnackbarService,
    private dialog: MatDialog,
  ) {
    this._quickPanelSubjectService.heading.next("Earnings");
  }
  ngOnInit() {
    this.userRole = _.chain(this._auth.getCurrentUser()).get('roles').head().get('name').value();
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));
    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter)
    this.getAllCustomers();
    this.getAllUsers();
  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null,
      commissionuserid: null,
      customerid: null
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "statusname") {
        params[key] = stringToIntegerArray(params[key]);
      }
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "statusname") {
        params[key] = integerArrayToString(params[key]);
      }
      if (key == "customerid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    this.commissionList = [];
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    this._commissionService.getAllCommissionReport(params).subscribe((data) => {
      this.commissionList = data;
      this.total = {
        shipQty: _.sumBy(this.commissionList, 'shipQty'),
        shipValue: _.sumBy(this.commissionList, 'shipValue'),
        commission: _.subtract(
          _.chain(this.commissionList).filter(['transactionType', 'Invoice']).sumBy('amount').value(),
          _.chain(this.commissionList).filter(['transactionType', 'Discount']).sumBy('amount').value()
        ),
        payment: _.chain(this.commissionList).filter(['transactionType', 'Payment']).sumBy('amount').value(),
      }
      this.setFilterToURL(filterParams);
    });

  }

  getAllUsers() {
    var params = {};
    params['rolename'] = 'ROLE_PARTNER,ROLE_SALES,ROLE_INVESTOR'
    if (this.userRole == 'ROLE_PARTNER') {
      params['partnerid'] = this._auth.getCurrentUser().id
    }
    this._userService.getAll(params).subscribe(data => {
      this.userList = data;
    });
  }

  getAllCustomers() {
    this._customerService.getAll({ requested: false }).subscribe((data) => {
      this.customerList = data;
    });
  }

  openPaymentModal() {
    let modalRef = this.dialog.open(CommissionPaymentModalComponent, {
      minWidth: "500px",
      maxWidth: "90%",
      disableClose: true,
      data: {}
    });
    modalRef.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
    });
  }

  openDiscountModal() {
    let modalRef = this.dialog.open(CommissionDiscountModalComponent, {
      minWidth: "500px",
      maxWidth: "90%",
      disableClose: true,
      data: {}
    });
    modalRef.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
    });
  }

  openDiscountDetails(id) {
    let modalRef = this.dialog.open(CommissionDiscountUpdateModalComponent, {
      minWidth: "500px",
      maxWidth: "90%",
      disableClose: true,
      data: {
        discountId: id
      }
    });
    modalRef.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
    });
  }

  openPaymentDetails(id) {
    let modalRef = this.dialog.open(CommissionPaymentUpdateModalComponent, {
      minWidth: "600px",
      maxWidth: "90%",
      disableClose: true,
      data: {
        paymentId: id
      }
    });
    modalRef.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
    });
  }
  openCommissionDetails(item) {
    let modalRef = this.dialog.open(CommissionDetailsModalComponent, {
      minWidth: "600px",
      maxWidth: "90%",
      disableClose: true,
      data: {
        commission: item
      }
    });
  }



} //class

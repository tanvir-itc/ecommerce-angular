import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from '../../utility/date-time-picker/date-time.module';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { CommissionListComponent } from './pages/commission-list/commission-list.component';
import { CommissionPaymentModalComponent } from './components/commission-payment-modal/commission-payment-modal.component';
import { CommissionDiscountModalComponent } from './components/commission-discount-modal/commission-discount-modal.component';
import { CommissionDiscountUpdateModalComponent } from './components/commission-discount-update-modal/commission-discount-update-modal.component';
import { CommissionPaymentUpdateModalComponent } from './components/commission-payment-update-modal/commission-payment-update-modal.component';
import { CommissionDetailsModalComponent } from './components/commission-details-modal/commission-details-modal.component';

const routes = [
  {
    path: 'commission-list',
    component: CommissionListComponent,
    data: { permission: 'PERMISSION_MENU_COMMISSION_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  },

];
@NgModule({
  declarations: [
    CommissionListComponent,
    CommissionPaymentModalComponent,
    CommissionDiscountModalComponent,
    CommissionDiscountUpdateModalComponent,
    CommissionPaymentUpdateModalComponent,
    CommissionDetailsModalComponent,
  ],
  entryComponents: [
    CommissionPaymentModalComponent,
    CommissionDiscountModalComponent,
    CommissionDiscountUpdateModalComponent,
    CommissionPaymentUpdateModalComponent,
    CommissionDetailsModalComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    AttachmentModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
  ],
  exports: [
  ]
})
export class CommissionModule {
}
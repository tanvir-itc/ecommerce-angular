import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import * as _ from "lodash";

@Component({
  selector: 'commission-details-modal',
  templateUrl: 'commission-details-modal.component.html',
})
export class CommissionDetailsModalComponent implements OnInit {
  commission = null;

  constructor(
    public dialogRef: MatDialogRef<CommissionDetailsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.commission = this.data.commission
  }
  ngOnInit() {


  }

}
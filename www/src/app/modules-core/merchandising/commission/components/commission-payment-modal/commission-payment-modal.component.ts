import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import { CommissionPaymentService } from "app/modules-core/common/commission-payment/commission-payment.service";
import { UserService } from "app/modules-core/common/user/user.service";
import { toDateString } from "app/modules-core/utility/helpers";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import * as _ from "lodash";

@Component({
  selector: 'commission-payment-modal',
  templateUrl: 'commission-payment-modal.component.html',
})
export class CommissionPaymentModalComponent implements OnInit {

  payment: any = {};
  userList: any[] = [];
  constructor(
    public dialogRef: MatDialogRef<CommissionPaymentModalComponent>,
    public _auth: AuthService,
    @Inject(MAT_DIALOG_DATA) public data,
    private snackBar: SnackbarService,
    private _commissionPaymentService: CommissionPaymentService,
    private _userService: UserService,
    private _router: Router,
  ) {

  }
  ngOnInit() {
    this.clearPaymentObject();
    this.getUsers()
  }

  getUsers() {
    this._userService.getAll({ rolename: 'ROLE_PARTNER,ROLE_SALES,ROLE_INVESTOR' }).subscribe((data) => {
      this.userList = data;
    });
  }

  clearPaymentObject() {
    this.payment = {
      amount: null,
      date: toDateString(new Date),
      details: null,
      commissionUserId: null
    }
  }

  createCommissionPayment() {
    if (!_.get(this.payment, "date")) {
      this.snackBar.open(`Please select date.`, "Close");
      return;
    }
    if (!_.get(this.payment, "commissionUserId")) {
      this.snackBar.open(`Please select commission.`, "Close");
      return;
    }
    if (!_.get(this.payment, "amount")) {
      this.snackBar.open(`Enter amount.`, "Close");
      return;
    }
    this._commissionPaymentService.create(this.payment).subscribe((data) => {
      this.snackBar.open(`Payment created!`, "Close");
      this.dialogRef.close(true);
    });
  }
}
import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import { CommissionDiscountService } from "app/modules-core/common/commission-discount/commission-discount.service";
import { UserService } from "app/modules-core/common/user/user.service";
import { ShipmentService } from "app/modules-core/merchandising/shipping/shipment.service";
import { toDateString } from "app/modules-core/utility/helpers";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import * as _ from "lodash";

@Component({
  selector: 'commission-discount-update-modal',
  templateUrl: 'commission-discount-update-modal.component.html',
})
export class CommissionDiscountUpdateModalComponent implements OnInit {
  discountId = null;

  editMode = false;
  discount: any = {};
  discountDisplay: any = {};
  userList: any[] = [];
  shipmentList: any[] = [];
  constructor(
    public dialogRef: MatDialogRef<CommissionDiscountUpdateModalComponent>,
    public _auth: AuthService,
    @Inject(MAT_DIALOG_DATA) public data,
    private snackBar: SnackbarService,
    private _commissionDiscountService: CommissionDiscountService,
    private _userService: UserService,
    private _shipmentService: ShipmentService,
    private _router: Router,
  ) {
    this.discountId = parseInt(this.data.discountId)
  }
  ngOnInit() {
    this.clearDiscountObject();
    this.getUsers();
    this.getDetails();

  }

  getDetails() {
    this._commissionDiscountService.get(this.discountId).subscribe((data) => {
      this.discount = data;
      this.discountDisplay = data;
      this.editMode = false;
      this.onUserChange();
    });
  }

  getUsers() {
    this._userService.getAll({ rolename: 'ROLE_PARTNER,ROLE_SALES,ROLE_INVESTOR' }).subscribe((data) => {
      this.userList = data;

    });
  }

  getAllShipments() {
    let params = {
      invoice: true,
      commissionuserid: this.discount.commissionUserId,
      sort: 's.invoice_date,desc'
    }
    this._shipmentService.getAll(params).subscribe((data) => {
      this.shipmentList = _.map(data, item => {
        item.invoiceNumber = `${item.rimsPoNo}-${item.invoiceId}`
        return item;
      });
    });
  }

  clearDiscountObject() {
    this.discount = {
      amount: null,
      date: toDateString(new Date),
      details: null,
      commissionUserId: null,
      shipmentId: null,
    }
  }

  onUserChange() {
    this.getAllShipments()
  }

  createCommissionDiscount() {
    if (!_.get(this.discount, "date")) {
      this.snackBar.open(`Please select date.`, "Close");
      return;
    }
    if (!_.get(this.discount, "commissionUserId")) {
      this.snackBar.open(`Please select commission.`, "Close");
      return;
    }
    if (!_.get(this.discount, "amount")) {
      this.snackBar.open(`Enter amount.`, "Close");
      return;
    }
    this._commissionDiscountService.updateFields(this.discount).subscribe((data) => {
      this.snackBar.open(`Discount updated!`, "Close");
      this.dialogRef.close(true);
    });
  }
  onDelete() {
    if (!confirm(`Confirm delete?`)) {
      return;
    }
    this._commissionDiscountService.delete(this.discountId).subscribe((data) => {
      this.snackBar.open(`Discount deleted!`, "Close");
      this.dialogRef.close(true);
    });
  }
}
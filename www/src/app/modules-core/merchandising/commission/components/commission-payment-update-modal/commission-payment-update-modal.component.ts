import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import { CommissionPaymentService } from "app/modules-core/common/commission-payment/commission-payment.service";
import { UserService } from "app/modules-core/common/user/user.service";
import { toDateString } from "app/modules-core/utility/helpers";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import * as _ from "lodash";

@Component({
  selector: 'commission-payment-update-modal',
  templateUrl: 'commission-payment-update-modal.component.html',
})
export class CommissionPaymentUpdateModalComponent implements OnInit {
  paymentId = null;
  editMode = false;
  payment: any = {};
  paymentDisplay: any = {};
  userList: any[] = [];
  constructor(
    public dialogRef: MatDialogRef<CommissionPaymentUpdateModalComponent>,
    public _auth: AuthService,
    @Inject(MAT_DIALOG_DATA) public data,
    private snackBar: SnackbarService,
    private _commissionPaymentService: CommissionPaymentService,
    private _userService: UserService,
    private _router: Router,
  ) {
    this.paymentId = parseInt(this.data.paymentId)
  }
  ngOnInit() {
    this.clearPaymentObject();
    this.getUsers()
    this.getDetails()
  }

  getDetails() {
    this._commissionPaymentService.get(this.paymentId).subscribe((data) => {
      this.payment = data;
      this.paymentDisplay = data;
      this.editMode = false;
    });
  }

  getUsers() {
    this._userService.getAll({ rolename: 'ROLE_PARTNER,ROLE_SALES,ROLE_INVESTOR' }).subscribe((data) => {
      this.userList = data;
    });
  }

  clearPaymentObject() {
    this.payment = {
      amount: null,
      date: toDateString(new Date),
      details: null,
      commissionUserId: null
    }
  }

  createCommissionPayment() {
    if (!_.get(this.payment, "date")) {
      this.snackBar.open(`Please select date.`, "Close");
      return;
    }
    if (!_.get(this.payment, "commissionUserId")) {
      this.snackBar.open(`Please select commission.`, "Close");
      return;
    }
    if (!_.get(this.payment, "amount")) {
      this.snackBar.open(`Enter amount.`, "Close");
      return;
    }
    this._commissionPaymentService.updateFields(this.payment).subscribe((data) => {
      this.snackBar.open(`Payment updated!`, "Close");
      this.dialogRef.close(true);
    });
  }

  onDelete() {
    if (!confirm(`Confirm delete?`)) {
      return;
    }
    this._commissionPaymentService.delete(this.paymentId).subscribe((data) => {
      this.snackBar.open(`Discount deleted!`, "Close");
      this.dialogRef.close(true);
    });
  }
}
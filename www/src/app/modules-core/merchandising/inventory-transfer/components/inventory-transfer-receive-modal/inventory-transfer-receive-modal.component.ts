import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { AuthService } from "app/authentication/auth.service";
import * as _ from "lodash";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { InventoryTransferService } from "../../inventory-transfer.service";
import { toDateString } from "app/modules-core/utility/helpers";

@Component({
  selector: 'inventory-transfer-receive-modal',
  templateUrl: 'inventory-transfer-receive-modal.component.html',
})
export class InventoryTransferReceiveModalComponent implements OnInit {
  id = null;

  inventories: any[] = [];
  transferObject: any = null;

  constructor(
    private _inventoryTransferService: InventoryTransferService,
    public _auth: AuthService,
    private snackBar: SnackbarService,
    public dialogRef: MatDialogRef<InventoryTransferReceiveModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.id = data.id;
  }

  ngOnInit() {
    this.getTransferDetails();
    this.transferObject = {
      receiveComment: null,
    }
  }




  getTransferDetails() {
    this.inventories = [];
    this._inventoryTransferService.get(this.id).subscribe(resp => {
      this.inventories = resp;
    });
  }

  receive() {
    let payload = {
      id: this.id,
      receive: true,
      receiveDate: toDateString(new Date),
      receiveComment: _.get(this.transferObject, 'receiveComment'),
      inventoryTransferDetails: [],
    }


    let error = [];
    payload.inventoryTransferDetails = [];

    _.each(this.inventories, group => {
      _.each(group.inventories, style => {
        for (var property in style.breakdown) {
          if (style.breakdown[property].newReceiveQty) {
            // bq = balance quantity, nrq = new receive qty,
            let bq = null;
            let nrq = null;
            bq = style.breakdown[property].quantity;
            nrq = parseInt(style.breakdown[property].newReceiveQty);
            if (nrq > bq) {
              error.push(`${style.styleName}-${style.breakdown[property].size},${style.breakdown[property].combo} can not be more than inventory quantity!`);
            }
            let qtyObject = _.get(style.breakdown, property);
            debugger
            payload.inventoryTransferDetails.push({
              id: _.get(_.head(qtyObject.details), 'id'),
              receiveQty: nrq,
            });

          }
        }
      });
    });













    this._inventoryTransferService.updateFields(payload).subscribe(resp => {
      this.snackBar.open(`Transfer received!`, "Close");
      this.dialogRef.close(true);
    });
  }

}
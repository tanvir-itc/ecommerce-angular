import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { AuthService } from "app/authentication/auth.service";
import { InventoryLocationService } from "app/modules-core/common/inventory-location/inventory-location.service";
import { toDateString } from "app/modules-core/utility/helpers";
import * as _ from "lodash";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { InventoryTransferService } from "../../inventory-transfer.service";
import { InventoryService } from "app/modules-core/merchandising/inventory/inventory.service";

@Component({
  selector: 'inventory-transfer-create-modal',
  templateUrl: 'inventory-transfer-create-modal.component.html',
})
export class InventoryTransferCreateModalComponent implements OnInit {
  transferObject: any = null;

  inventoryLocationList: any[] = [];
  inventoryLocationAllListTemp: any[] = [];
  inventoryLocationAllList: any[] = [];

  inventories: any[] = [];

  constructor(
    private _inventoryLocationService: InventoryLocationService,
    private _inventoryTransferService: InventoryTransferService,
    private _inventoryService: InventoryService,
    public _auth: AuthService,
    private snackBar: SnackbarService,
    public dialogRef: MatDialogRef<InventoryTransferCreateModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {

  }

  ngOnInit() {
    this.clearObject();
    this.getAllInventoryLocation();
    this.getInventoryLocations();
  }




  createTransfer() {
    let error = [];
    let payload = _.merge({}, this.transferObject);
    payload.inventoryTransferDetails = [];

    _.each(this.inventories, group => {
      _.each(group.inventories, style => {
        for (var property in style.breakdown) {
          if (style.breakdown[property].transferQty) {
            // bq = balance quantity, oq = order, sq = shipped, ntq = new transfer qty,
            property
            let bq = null;
            let ntq = null;
            bq = style.breakdown[property].quantity;
            ntq = parseInt(style.breakdown[property].transferQty);
            if (ntq > bq) {
              error.push(`${style.styleName}-${style.breakdown[property].size},${style.breakdown[property].combo} can not be more than inventory quantity!`);
            }
            let qtyObject = _.get(style.breakdown, property);
            payload.inventoryTransferDetails.push({
              styleId: _.get(style, 'styleId'),
              sizeId: _.get(qtyObject, 'sizeId'),
              styleComboId: _.get(qtyObject, 'styleComboId'),
              transferQty: ntq,
            });

          }
        }
      });
    });

    if (!_.get(payload, 'fromId')) {
      this.snackBar.open(`Please select inventory location.`, "Close");
      return;
    }
    if (!_.get(payload, 'toId')) {
      this.snackBar.open(`Please select laction where will be transfer.`, "Close");
      return;
    }
    if (!payload.inventoryTransferDetails.length) {
      this.snackBar.open(`Please! add minimum quantity!`, "Close");
      return;
    }
    if (error.length) {
      this.snackBar.open(_.join(error, ', '), "Close", true);
      return;
    }

    this._inventoryTransferService.create(payload).subscribe(resp => {
      this.snackBar.open(`Inventory transfer submited!`, "Close");
      this.dialogRef.close(true);
    });
  }

  onLocationChange() {
    this.getInventorys();

    this.transferObject.toId = null;

    this.inventoryLocationAllList = [];
    _.each(this.inventoryLocationAllListTemp, item => {
      if (item.id == this.transferObject.fromId) {
      } else {
        this.inventoryLocationAllList.push(item);
      }
    });

  }


  getInventorys() {
    this.inventories = [];
    let params = {
      inventorylocationid: _.get(this.transferObject, 'fromId'),
    };
    this._inventoryService.getAllBySizeGroup(params).subscribe(resp => {
      this.inventories = resp;
    });
  }

  clearObject() {
    this.transferObject = {
      date: toDateString(new Date),
      fromId: null,
      toId: null,
      inventoryTransferDetails: [],
      comment: null,
    }
  }

  getInventoryLocations() {
    this._inventoryLocationService.getAll().subscribe(data => {
      this.inventoryLocationList = data;
      if (_.size(this.inventoryLocationList) == 1) {
        this.transferObject.fromId = _.get(this.inventoryLocationList[0], 'id');
        this.onLocationChange();
      }
    });
  }
  getAllInventoryLocation() {
    this._inventoryLocationService.getAll({ all: true }).subscribe(data => {
      this.inventoryLocationAllList = data;
      this.inventoryLocationAllListTemp = data;
    });
  }




}
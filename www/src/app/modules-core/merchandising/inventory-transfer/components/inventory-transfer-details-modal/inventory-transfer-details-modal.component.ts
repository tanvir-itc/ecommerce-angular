import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { AuthService } from "app/authentication/auth.service";
import * as _ from "lodash";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { InventoryTransferService } from "../../inventory-transfer.service";
import { toDateString } from "app/modules-core/utility/helpers";

@Component({
  selector: 'inventory-transfer-details-modal',
  templateUrl: 'inventory-transfer-details-modal.component.html',
})
export class InventoryTransferDetailsModalComponent implements OnInit {
  id = null;

  approvalFlag: any = null;
  receiveFlag: any = null;

  inventories: any[] = [];

  constructor(
    private _inventoryTransferService: InventoryTransferService,
    public _auth: AuthService,
    private snackBar: SnackbarService,
    public dialogRef: MatDialogRef<InventoryTransferDetailsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.id = data.id;
    this.approvalFlag = _.get(data, 'approvalFlag', null);
    this.receiveFlag = _.get(data, 'receiveFlag', null);
  }

  ngOnInit() {
    this.getTransferDetails();
  }




  getTransferDetails() {
    this.inventories = [];
    this._inventoryTransferService.get(this.id).subscribe(resp => {
      this.inventories = resp;
    });
  }

  updateTransfer() {
    let payload = {
      id: this.id,
      approve: true,
      approveDate: toDateString(new Date)
    }

    this._inventoryTransferService.updateFields(payload).subscribe(resp => {
      this.snackBar.open(`Transfer approved!`, "Close");
      this.dialogRef.close(true);
    });
  }

}
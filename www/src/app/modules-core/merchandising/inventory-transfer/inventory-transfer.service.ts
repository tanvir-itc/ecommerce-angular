import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from "rxjs/internal/operators/map";
import * as _ from "lodash";
import { makeParams } from "app/modules-core/utility/helpers";

@Injectable({
  providedIn: "root",
})
export class InventoryTransferService {
  urlBase = "inventory-transfer";

  httpOptions = {
    params: new HttpParams(),
  };
  constructor(private http: HttpClient) { }

  getAll(opts = {}) {
    return this.http
      .get(`${this.urlBase}`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          resp.data = _.map(resp.data, item => {
            item['totalQuantity'] = _.sumBy(item.inventoryTransferDetails, 'transferQty');
            item['totalRecievedQuantity'] = _.sumBy(item.inventoryTransferDetails, 'receiveQty');
            item['balance'] = _.subtract(item.totalQuantity, item.totalRecievedQuantity);
            return item;
          });
          return resp.data;
        })
      );
  }

  get(id) {
    return this.http.get(`${this.urlBase}/${id}/v2`).pipe(
      map((resp: any) => {
        _.each(resp.data, group => {
          _.each(group.inventories, style => {

            let uniqueCombos = _.uniqBy(style.inventories, 'styleComboId');
            style['colorList'] = _.map(uniqueCombos, item => {
              return {
                id: _.get(item, 'styleComboId'),
                name: _.get(item, 'styleCombo'),
              }
            });

            style['breakdown'] = {};
            style['totalQuantity'] = _.sumBy(style.inventories, 'quantity');
            style['totalReceivedQuantity'] = 0;

            _.each(style.inventories, qtyBreakdown => {
              qtyBreakdown['newReceiveQty'] = _.get(qtyBreakdown, 'quantity');
              qtyBreakdown['receiveQty'] = _.get(_.head(qtyBreakdown.details), 'receiveQty');
              style.totalReceivedQuantity = _.add(style.totalReceivedQuantity, qtyBreakdown.receiveQty);
              let key = null;
              key = "_" + qtyBreakdown.styleComboId + "_" + qtyBreakdown.sizeId;
              style.breakdown[key] = qtyBreakdown;
            });

          });
        });


        return resp.data;
      })
    );
  }

  create(payload) {
    return this.http
      .post(this.urlBase, payload)
      .pipe(map((resp: any) => resp.data));
  }

  update(id, payload) {
    return this.http
      .put(`${this.urlBase}/${id}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFields(payload) {
    return this.http
      .patch(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  delete(id) {
    return this.http
      .delete(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }




}

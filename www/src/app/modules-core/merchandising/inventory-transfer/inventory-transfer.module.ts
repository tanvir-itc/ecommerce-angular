import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from '../../utility/date-time-picker/date-time.module';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { StyleBasicModule } from '../style/components/style-basic/style-basic.module';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { InventoryTransferCreateModalComponent } from './components/inventory-transfer-create-modal/inventory-transfer-create-modal.component';
import { InventoryTransferListComponent } from './pages/inventory-transfer-list/inventory-transfer-list.component';
import { TransferRequestedListComponent } from './pages/inventory-transfer-list/transfer-requested-list/transfer-requested-list.component';
import { InventoryTransferDetailsModalComponent } from './components/inventory-transfer-details-modal/inventory-transfer-details-modal.component';
import { TransferReceivePendingListComponent } from './pages/inventory-transfer-list/transfer-receive-pending-list/transfer-receive-pending-list.component';
import { TransferHistoryListComponent } from './pages/inventory-transfer-list/transfer-history-list/transfer-history-list.component';
import { InventoryTransferReceiveModalComponent } from './components/inventory-transfer-receive-modal/inventory-transfer-receive-modal.component';

const routes = [

  {
    path: "inventory-transfer",
    component: InventoryTransferListComponent,
    data: { permission: 'PERMISSION_MENU_STOCK_TRANSFER' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
    children: [
      {
        path: "",
        redirectTo: "transfer-request",
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_STOCK_TRANSFER_REQUEST' },
      },
      {
        path: "transfer-request",
        component: TransferRequestedListComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_STOCK_TRANSFER_REQUEST' },
      },
      {
        path: "transfer-receive-pending",
        component: TransferReceivePendingListComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_STOCK_TRANSFER_RECEIVE' },
      },
      {
        path: "transfer-history",
        component: TransferHistoryListComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_STOCK_TRANSFER_HISTORY' },
      },
    ],
  },



];
@NgModule({
  declarations: [
    InventoryTransferListComponent,
    TransferRequestedListComponent,
    TransferReceivePendingListComponent,
    TransferHistoryListComponent,
    InventoryTransferCreateModalComponent,
    InventoryTransferDetailsModalComponent,
    InventoryTransferReceiveModalComponent,
  ],
  entryComponents: [
    InventoryTransferCreateModalComponent,
    InventoryTransferDetailsModalComponent,
    InventoryTransferReceiveModalComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    AttachmentModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
  ],
  exports: [
  ]
})
export class InventoryTransferModule {
}
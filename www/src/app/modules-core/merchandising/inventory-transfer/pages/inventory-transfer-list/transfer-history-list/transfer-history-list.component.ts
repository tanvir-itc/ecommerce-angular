import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  stringToIntegerArray,
  removeEmptyFields,
  integerArrayToString,
  customSortBy
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { InventoryTransferService } from "../../../inventory-transfer.service";
import { MatDialog } from "@angular/material";
import { InventoryTransferDetailsModalComponent } from "../../../components/inventory-transfer-details-modal/inventory-transfer-details-modal.component";

@Component({
  selector: "transfer-history-list",
  templateUrl: "./transfer-history-list.component.html",
})
export class TransferHistoryListComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};
  sorter: any = {
    field: null,
    sortType: 'asc',
    fieldType: 'text',
  }
  userRole: any = null;



  reportList: any[] = [];

  modalRef = null;



  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _inventoryTransferService: InventoryTransferService,

  ) {
    this._quickPanelSubjectService.heading.next("Transfer");
  }
  ngOnInit() {
    this.userRole = _.chain(this._auth.getCurrentUser()).get('roles').head().get('name').value();
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));

    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);




  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null,
      styleid: null,
      stylename: null,
      statusname: [],
      customerid: null,
      salerepid: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "statusname") {
        params[key] = stringToIntegerArray(params[key]);
      }
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "statusname") {
        params[key] = integerArrayToString(params[key]);
      }
      if (key == "customerid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    this.reportList = [];
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    params['approve'] = 'true';
    params['receive'] = 'true';
    this._inventoryTransferService.getAll(params).subscribe((data) => {
      this.reportList = data;
      this.setFilterToURL(filterParams);
    });

  }



  goLink(id) {
    this._router.navigateByUrl(`orders/${id}`);
  }
  onSort(field, fieldType = 'text') {
    this.sorter = {
      field: field,
      fieldType: fieldType,
      sortType: this.sorter.sortType == 'asc' ? 'desc' : 'asc',
    }
    this.reportList = customSortBy(this.reportList, this.sorter);
  }



  openDetailsModal(id) {
    const transferDetailsModal = this.dialog.open(InventoryTransferDetailsModalComponent, {
      minWidth: '50%',
      maxWidth: '95%',
      disableClose: true,
      data: {
        id: id,
        receiveFlag: true,
      }
    });
    transferDetailsModal.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
    });
  }







} //class

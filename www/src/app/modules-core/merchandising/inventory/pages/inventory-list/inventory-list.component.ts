import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material';
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/authentication/auth.service';
import { toDateString, integerStringToInteger, stringToIntegerArray, removeEmptyFields, integerArrayToString } from 'app/modules-core/utility/helpers';
import * as moment from 'moment';
import { StyleTypeService } from 'app/modules-core/common/style-type/style-type.service';
import { InventoryService } from '../../inventory.service';
import { StyleService } from 'app/modules-core/merchandising/style/style.service';
import { SeasonService } from 'app/modules-core/common/season/season.service';
import { InventoryLocationService } from 'app/modules-core/common/inventory-location/inventory-location.service';
import { SizeGroupService } from 'app/modules-core/common/size/size-group.service';
import { InventoryAddModalComponent } from '../../components/inventory-add-edit-modal/inventory-add-modal.component';
import { InventoryEditModalComponent } from '../../components/inventory-add-edit-modal/inventory-edit-modal.component';
import { InventoryAdjustmentModalComponent } from '../../components/inventory-adjustment-modal/inventory-adjustment-modal.component';

@Component({
  selector: 'inventory-list',
  templateUrl: './inventory-list.component.html',
  animations: fuseAnimations,
})
export class InventoryListComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  inventories: any[] = [];

  styleTypeList: any[] = [];
  seasonList: any[] = [];
  inventoryLocationList: any[] = [];
  modalRef = null;

  filterStyle: any = {};
  filterCounterStyle = 0;
  filterMoreStyle = false;

  styleList: any[] = [];
  sizeGroupList: any[] = [];

  sizes: any[] = [];

  styleId = null;
  sizeGroupId = null;

  total: any = {
    styleTotal: null,
  }
  displayFlag = {
    actual: false,
    available: true,
  }
  stockTotal: any = {
    availableTotalQuantity: null,
    totalQuantity: null,
    totalUpcomingQuantity: null
  }

  constructor(
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _inventoryService: InventoryService,
    private _styleTypeService: StyleTypeService,
    private _seasonService: SeasonService,
    private _styleService: StyleService,
    private _sizeGroupService: SizeGroupService,
    private _inventoryLocationService: InventoryLocationService,
  ) {
    this._quickPanelSubjectService.heading.next('Inventory');
  }
  ngOnInit() {
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, 'snapshot.queryParams'));

    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
    this.getAllStyleTypes();
    this.getAllSeasons();
    this.getAllInventoryLocation();
    this.getAllSizeGroup();
    this.applyFilters(this.filter);
  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null,
      styleid: null,
      stylename: null,
      styletypeid: [],
      seasonid: [],
      inventorylocationid: null,
      sizegroupid: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "styletypeid") {
        params[key] = stringToIntegerArray(params[key]);
      }
      if (key == "seasonid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(moment(this.filter.todate || new Date).add(1, 'months').endOf('month'));
      this.filter.fromdate = toDateString(moment(this.filter.todate || new Date).startOf('month'));
    } else {
      this.filter.fromdate = toDateString(moment(this.filter.fromdate || new Date).subtract(1, 'months').startOf('month'));
      this.filter.todate = toDateString(moment(this.filter.fromdate || new Date).endOf('month'));
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == 'styletypeid') {
        params[key] = integerArrayToString(params[key]);
      }
      if (key == 'seasonid') {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    this.inventories = [];
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    this._inventoryService.getAllBySizeGroup(params).subscribe(resp => {
      this.inventories = resp;
      this.stockTotal = {
        availableTotalQuantity: _.sumBy(this.inventories, 'availableTotalQuantity') || null,
        totalQuantity: _.sumBy(this.inventories, 'totalQuantity') || null,
        totalUpcomingQuantity: _.sumBy(this.inventories, 'totalUpcomingQuantity') || null,
      }
      this.setFilterToURL(filterParams);
    });
  }

  getAllStyleTypes() {
    this._styleTypeService.getAll().subscribe(data => {
      this.styleTypeList = data;
    });
  }
  getAllSizeGroup() {
    this._sizeGroupService.getAll().subscribe(data => {
      this.sizeGroupList = data;
    });
  }

  getAllSeasons() {
    if (this._auth.hasPermission('PERMISSION_SEASON_VIEW')) {
      this._seasonService.getAll().subscribe(data => {
        this.seasonList = data;
      });
    }

  }

  getAllInventoryLocation() {
    this._inventoryLocationService.getAll().subscribe(data => {
      this.inventoryLocationList = data;
    });
  }

  openAddModal(templateRef) {

    this.clearAddObject();
    this.clearAllFilterStyle();
    this.applyStyleFilters(this.filterStyle);
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "90%",
      disableClose: true,
    });
  }

  clearAddObject() {
    this.styleId = null;
  }

  clearAllFilterStyle() {
    this.filterStyle = {
      //fromdate: toDateString(moment(this.filterStyle.todate || new Date).subtract(3, 'months').startOf('month')),
      //todate: toDateString(moment(this.filterStyle.todate || new Date).endOf('month')),
      formdate: null,
      todate: null,
      styleid: null,
      stylename: null,
      styletypeid: [],
      seasonid: [],
    };
  }

  onStyleDateNextPrivious(priv) {
    if (priv) {
      this.filterStyle.todate = toDateString(moment(this.filterStyle.todate || new Date).add(1, 'months').endOf('month'));
      this.filterStyle.fromdate = toDateString(moment(this.filterStyle.todate || new Date).startOf('month'));
    } else {
      this.filterStyle.fromdate = toDateString(moment(this.filterStyle.fromdate || new Date).subtract(1, 'months').startOf('month'));
      this.filterStyle.todate = toDateString(moment(this.filterStyle.fromdate || new Date).endOf('month'));
    }
    this.applyStyleFilters(this.filter);
  }

  onStyleFilter() {
    this.applyStyleFilters(this.filterStyle);
  }


  onStyleClear() {
    this.clearAllFilterStyle();
    this.applyStyleFilters(this.filterStyle);
  }

  applyStyleFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    this._styleService.getAll(params).subscribe(data => {
      this.styleList = data;
    });
  }



  inventoryAdded(event) {
    this.modalRef.close();
    this.applyFilters(this.filter);
  }


  openAddStyleInventoryModal(styleId, sizeGroupId) {
    const addModal = this.dialog.open(InventoryAddModalComponent, {
      minWidth: '400px',
      maxWidth: '95%',
      disableClose: true,
      data: {
        styleId: styleId,
        groupId: sizeGroupId,
        inventoryLocationId: _.get(this.filter, 'inventorylocationid', null),
      }
    });
    addModal.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
    });
  }

  openEditStyleInventoryEditModal(styleId, sizeGroupId) {
    const editModal = this.dialog.open(InventoryEditModalComponent, {
      minWidth: '400px',
      maxWidth: '95%',
      disableClose: true,
      data: {
        styleId: styleId,
        groupId: sizeGroupId,
        inventoryLocationId: _.get(this.filter, 'inventorylocationid', null),
      }
    });
    editModal.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
    });
  }

  openAdjustmentModal() {
    const editModal = this.dialog.open(InventoryAdjustmentModalComponent, {
      minWidth: '800px',
      maxWidth: '95%',
      disableClose: true,
      data: {}
    });
    editModal.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
    });
  }


  goLink(id) {
    this._router.navigateByUrl(`inventories/${id}`);
  }

} //class
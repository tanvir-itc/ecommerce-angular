import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material';
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/authentication/auth.service';
import { toDateString, integerStringToInteger, stringToIntegerArray, removeEmptyFields, integerArrayToString, getSummarnoteConfig } from 'app/modules-core/utility/helpers';
import * as moment from 'moment';
import { StyleTypeService } from 'app/modules-core/common/style-type/style-type.service';
import { InventoryService } from '../../inventory.service';
import { StyleService } from 'app/modules-core/merchandising/style/style.service';
import { SeasonService } from 'app/modules-core/common/season/season.service';
import { InventoryLocationService } from 'app/modules-core/common/inventory-location/inventory-location.service';
import { SizeGroupService } from 'app/modules-core/common/size/size-group.service';
import { InventoryAddModalComponent } from '../../components/inventory-add-edit-modal/inventory-add-modal.component';
import { InventoryEditModalComponent } from '../../components/inventory-add-edit-modal/inventory-edit-modal.component';
import { InventoryAdjustmentModalComponent } from '../../components/inventory-adjustment-modal/inventory-adjustment-modal.component';
import { SizeService } from 'app/modules-core/common/size/size.service';
import { flatMap } from 'lodash';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { BoxHistoryModalComponent } from '../../components/box-history-modal/box-history-modal.component';

@Component({
  selector: 'inventory-edit',
  templateUrl: './inventory-edit.component.html',
  animations: fuseAnimations,
})
export class InventoryEditNewComponent implements OnInit {
  filter: any = {};
  filterInMergeModal: any = {};
  wearhouseBoxList: any[] = [];
  inventoryLocationList: any[] = [];
  sizeGroupList: any[] = [];
  sizeList: any[] = [];
  uniqSizeList: any[] = [];
  styleList: any[] = [];
  wearhouseBoxListFormerging: any[] = [];
  locaionId = null;
  modalRef: any = null;
  targetBox: any = {};
  locaionIdOnMarging = null;
  displayActionButtons = false;
  selectedBoxesToMerge = []
  selectedBoxesToMergeChunked = [];
  detailModalRef = null;
  selectedBoxId = null;
  detailModalTemplateRef = null;
  mergeCommetn = null;
  commentBox = null;
  commentModalRef = null;
  boxNumberForSearching = null;
  boxNumberForSearchingOnmarging = null;
  styelNameForSearching = null;
  shipNameForSearching = null;
  styelNameForSearchingOnMarging = null;
  shipNameForSearchingOnMerging = null;
  sizeIdForSearching = null;
  colorNameForSearching = null;
  totalQuantity = 0;
  totalNumberOfBoxes = 0;
  config = getSummarnoteConfig();

  constructor(
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _inventoryService: InventoryService,
    private _styleTypeService: StyleTypeService,
    private _seasonService: SeasonService,
    private _styleService: StyleService,
    private _sizeGroupService: SizeGroupService,
    private _inventoryLocationService: InventoryLocationService,
    private _sizeService: SizeService,
    private snackBar: SnackbarService,
  ) {
    this._quickPanelSubjectService.heading.next('Inventory Edit');
  }
  ngOnInit() {
    this.onClear();
    this.getAllSizeGroup();
    // this.getAllBoxList(false);
    this.getAllInventoryLocation();
    var queryParams = _.merge({}, _.get(this._route, 'snapshot.queryParams'));


  }


  onLocationChange(mergeState) {
    this.getAllBoxList(mergeState);
  }

  getAllBoxList(mergeState) {
    // this.wearhouseBoxList = [];
    var params = _.merge({}, mergeState ? this.filterInMergeModal : this.filter);
    params = removeEmptyFields(params);
    this._inventoryService.getAllBoxList(params).subscribe(data => {
      this.wearhouseBoxListFormerging = _.flatMap(data, 'boxList');
      this.filterBoxListWhenMarging();
      if (this.selectedBoxesToMergeChunked.length) {
        _.each(this.wearhouseBoxListFormerging, obj => {                 //to keep selected the boxes which were selected before location dropdown change
          if (_.find(_.flatten(this.selectedBoxesToMergeChunked), ['box.id', obj.box.id])) {
            obj.selectedToMerge = true
          }
        })
      }
      // console.log(_.flatten(this.selectedBoxesToMerge))

      if (mergeState) {
        return;
      }
      this.uniqSizeList = _.chain(data)
        .flatMap('boxList')
        .flatMap('boxDetail')
        .flatMap('details')
        .uniqBy('sizeId')
        .map(value => {
          return {
            'sizeId': value.sizeId,
            'sizeName': value.sizeName
          }
        })
        .orderBy('sizeId')
        .value()

      _.each(data, obj => {
        _.each(obj.boxList, box => {
          box.box['availavelSizes'] = box.boxDetail.length > 1 ? 'multiple' : box.boxDetail[0].details
          _.each(box.styleList, st => {
            st['shipProcessingQty'] = _.get(box, 'shipProcessingQty')
            st['locationName'] = _.get(box, 'box.locationName')
            _.each(st.colorList, c => {
              c['sizeList'] = _.map(this.uniqSizeList, size => {
                let existingSize = _.find(c.details, ['sizeId', size.sizeId]);
                let key = null;
                key = "_" + c.styleComboId + "_" + size.sizeId;
                if (existingSize) {
                  return existingSize
                } else {
                  return {
                    'quantity': 0, 'sizeId': size.sizeId, 'sizeName': size.sizeName
                  }

                }

              })
            })
          })
        })
      })
      this.wearhouseBoxList = data;
      console.log('this.wearhouseBoxList', this.wearhouseBoxList)

      this.totalQuantity = _.sumBy(_.flatMap(this.wearhouseBoxList, 'boxList'), 'box.qty') + _.sumBy(_.flatMap(this.wearhouseBoxList, 'boxList'), 'shipProcessingQty')
      this.styleList = this.styleList.length ? _.merge([], _.get(_.find(_.flatMap(this.wearhouseBoxList, 'boxList'), ['box.id', this.styleList[0].id]), 'styleList')) : []
      this.targetBox = _.find(_.flatMap(this.wearhouseBoxList, 'boxList'), ['box.id', this.selectedBoxId])
      this.totalNumberOfBoxes = _.flatMap(this.wearhouseBoxList, 'boxList').length
      this.applyselectedBox(this.selectedBoxId);
    });
  }

  onClear() {
    this.filter = {
      locationid: null,
      box: null,
      styelname: null,
      shipmentname: null,
      colorname: null,
      sizeid: null,
      emptyBox: false
    }

    this.getAllBoxList(false)
  }
  onClearInMergeState() {
    this.filterInMergeModal = {
      locationid: null,
      box: null,
      styelname: null,
      shipmentname: null,
      colorname: null,
      sizeid: null,
      emptyBox: false
    }
    this.getAllBoxList(true)
  }

  getAllInventoryLocation() {
    this._inventoryLocationService.getAll().subscribe(data => {
      this.inventoryLocationList = data;
    });
  }

  getAllSizeGroup() {
    this._sizeGroupService.getAll().subscribe(data => {
      this.sizeGroupList = data;
    });
  }

  // BOX CLICK/

  onBoxClick(data, id, templateRef) {

    this.selectedBoxId = id
    this.detailModalTemplateRef = templateRef;
    this.applyselectedBox(id);
    this.styleList = _.merge([], data);
    this.detailModalRef = this.dialog.open(templateRef, {
      minWidth: "50%",
      disableClose: true,
    });
    console.log('this.styleList ', this.styleList)
    this.displayActionButtons = true
  }

  applyselectedBox(id) {
    _.each(_.flatMap(this.wearhouseBoxList, 'boxList'), box => {
      if (box.box.id == id) {
        box.box['selected'] = true
      } else {
        box.box['selected'] = false
      }
    })
  }

  openMergeBoxModal(templateRef) {
    this.filterInMergeModal = {
      locationid: null,
      box: null,
      styelname: null,
      shipmentname: null,
      colorname: null,
      sizeid: null,
      emptyBox: false
    }
    this.selectedBoxesToMerge = [];
    this.wearhouseBoxListFormerging = [];
    this.selectedBoxesToMergeChunked = [];
    // this.getAllBoxList(true);
    this.targetBox = _.find(_.flatMap(this.wearhouseBoxList, 'boxList'), ['box.id', this.selectedBoxId])
    console.log('target', this.targetBox)
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "75%",
      disableClose: true,
    });
  }

  filterBoxListWhenMarging() {
    this.wearhouseBoxListFormerging = _.filter(this.wearhouseBoxListFormerging, v => {
      return v.box.id !== _.get(this.targetBox, 'box.id')
    })

  }

  onSelectBoxToMerge(box, flag) {
    if (flag) {
      this.selectedBoxesToMerge.push(box)
    } else {
      _.remove(this.selectedBoxesToMerge, ['box.id', box.box.id])
    }
    this.selectedBoxesToMergeChunked = _.chunk(this.selectedBoxesToMerge, 5);
    console.log(this.selectedBoxesToMerge)
  }

  mergeBoxes() {
    if (!this.selectedBoxesToMerge.length) {
      this.snackBar.open(`Select Boxes to  Merge!`, 'Close');
      return;
    }
    let payload = {};
    payload = {
      'id': _.get(this.targetBox, 'box.id'),
      'mergePackingDetails': _.map(_.flatMap(_.flatMap(this.selectedBoxesToMerge, 'boxDetail'), 'details'), boxDetail => { return { 'id': boxDetail.id, 'quantity': boxDetail.quantity } }),
      'comment': this.mergeCommetn,
    };
    this._inventoryService.mergeBoxes(payload).subscribe(resp => {
      // this.locaionIdOnMarging = null;
      this.boxNumberForSearchingOnmarging = null;
      this.selectedBoxesToMerge = [];
      // this.wearhouseBoxListFormerging = [];
      this.selectedBoxesToMergeChunked = [];
      // this._router.navigateByUrl(`factory-user/${resp.id}`);
      this.getAllBoxList(false);
      this.snackBar.open(`Boxes Merged!`, 'Close');
      // this.modalRef.close();
    });
  }

  openCommentModal(templateRef) {
    // this.commentBox = boxData
    // console.log('on add modal', _.flatMap(this.wearhouseBoxList, 'boxList'))
    this.mergeCommetn = null;
    // this.mergeCommetn = _.get(boxData, 'comment');
    this.commentModalRef = this.dialog.open(templateRef, {
      width: "50%",
      disableClose: true,
    });

  }

  saveComment() {
    _.each(this.selectedBoxesToMerge, box => {
      if (box.box.id == this.commentBox.id) {
        box.box.comment = this.mergeCommetn;
      }
    })
    this.commentModalRef.close();
  }

  // style Add

  openAddStyleInventoryModal() {
    // console.log('on add modal', _.flatMap(this.wearhouseBoxList, 'boxList'))
    const addModal = this.dialog.open(InventoryAddModalComponent, {
      width: '50%',
      maxWidth: '55%',
      disableClose: true,
      data: {
        data: _.find(_.flatMap(this.wearhouseBoxList, 'boxList'), 'box.selected'),
        sizes: this.uniqSizeList
      }
    });
    addModal.afterClosed().subscribe(result => {
      if (result) {
        this.getAllBoxList(false);
      }
    });
  }

  openEditModal() {
    const editModal = this.dialog.open(InventoryEditModalComponent, {
      width: '50vw',
      disableClose: true,
      data: {
        data: this.styleList,
        sizes: this.uniqSizeList
      }
    });
    editModal.afterClosed().subscribe(result => {
      if (result) {
        this.getAllBoxList(false);
      }
    });
  }
  openHistoryModal(boxData) {
    const historyModal = this.dialog.open(BoxHistoryModalComponent, {
      width: '50vw',
      disableClose: true,
      data: {
        data: boxData
      }
    });
    historyModal.afterClosed().subscribe(result => {
      if (result) {
        this.getAllBoxList(false);
      }
    });
  }


} //class
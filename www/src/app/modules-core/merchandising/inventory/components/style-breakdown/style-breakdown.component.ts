import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
} from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { StyleService } from "app/modules-core/merchandising/style/style.service";
import * as _ from "lodash";
import { AuthService } from "app/authentication/auth.service";
import { forkJoin } from "rxjs";
import { SizeService } from "app/modules-core/common/size/size.service";
import { InventoryLocationService } from "app/modules-core/common/inventory-location/inventory-location.service";
import { InventoryService } from "../../inventory.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";

@Component({
  selector: "style-breakdown",
  templateUrl: "./style-breakdown.component.html",
})
export class StyleBreakdownComponent implements OnInit {
  @Input()
  styleId = null;

  @Input()
  groupId = null;

  @Input()
  inventoryLocationId = null;

  @Output()
  added = new EventEmitter();

  colors: any[] = [];
  sizes: any[] = [];

  inventoryLocationList: any[] = [];

  constructor(
    private _styleService: StyleService,
    private _sizeService: SizeService,
    private _inventoryService: InventoryService,
    private snackBar: SnackbarService,
    private _inventoryLocationService: InventoryLocationService,
    public _auth: AuthService
  ) { }
  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges): void {
    this.getSizeColors();
    this.getAllInventoryLocation();
  }

  getSizeColors() {
    forkJoin([
      this._styleService.getColors({ styleid: this.styleId }),
      this._sizeService.getAll({ sizegroupid: this.groupId }),
    ]).subscribe((results) => {
      this.sizes = _.map(results[1], (item) => {
        item["quantity"] = null;
        return item;
      });
      var colors = _.map(results[0], (item) => {
        item["sizes"] = JSON.parse(JSON.stringify(this.sizes));
        return item;
      });
      this.colors = _.sortBy(colors, "id");
    });
  }

  addInventory() {
    if (!this.inventoryLocationId) {
      this.snackBar.open(`Please! Select inventory location first!`, "Close");
      return;
    }
    var payload = [];

    _.each(this.colors, (color) => {
      _.each(color.sizes, (size) => {
        if (size.quantity) {
          payload.push({
            quantity: _.get(size, "quantity"),
            sizeId: _.get(size, "id"),
            styleComboId: _.get(color, "id"),
            // styleId: this.styleId,
            inventoryLocation: {
              id: this.inventoryLocationId,
            },
          });
        }
      });
    });

    if (!payload.length) {
      this.snackBar.open(`Please! Insert minimum single quantity!`, "Close");
      return;
    }
    this._inventoryService.create(payload).subscribe((resp) => {
      this.added.emit(resp || null);
      this.snackBar.open(`Inventory added!`, "Close");
    });
  }

  getAllInventoryLocation() {
    this._inventoryLocationService.getAll().subscribe((data) => {
      this.inventoryLocationList = data;
    });
  }
}

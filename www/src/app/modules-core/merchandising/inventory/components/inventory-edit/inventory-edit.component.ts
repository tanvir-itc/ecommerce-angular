import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
} from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { StyleService } from "app/modules-core/merchandising/style/style.service";
import * as _ from "lodash";
import { AuthService } from "app/authentication/auth.service";
import { forkJoin } from "rxjs";
import { SizeService } from "app/modules-core/common/size/size.service";
import { InventoryLocationService } from "app/modules-core/common/inventory-location/inventory-location.service";
import { InventoryService } from "../../inventory.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";

@Component({
  selector: "inventory-edit",
  templateUrl: "./inventory-edit.component.html",
})
export class InventoryEditComponent implements OnInit {
  @Input()
  styleId = null;

  @Input()
  groupId = null;

  @Input()
  inventoryLocationId = null;

  @Output()
  added = new EventEmitter();

  colors: any[] = [];
  sizes: any[] = [];

  inventoryLocationList: any[] = [];

  constructor(
    private _styleService: StyleService,
    private _sizeService: SizeService,
    private _inventoryService: InventoryService,
    private snackBar: SnackbarService,
    private _inventoryLocationService: InventoryLocationService,
    public _auth: AuthService
  ) { }
  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges): void {
    this.getSizeColorBreakdown();
    this.getAllInventoryLocation();
  }

  getSizeColorBreakdown() {
    if (!this.inventoryLocationId) {
      return;
    }
    let params = {
      styleid: this.styleId,
      inventorylocationid: this.inventoryLocationId,
    };
    forkJoin([
      this._styleService.getColors({ styleid: this.styleId }),
      this._sizeService.getAll({ sizegroupid: this.groupId }),
      this._inventoryService.getBreakdown(params),
    ]).subscribe((results) => {
      this.sizes = [];
      this.colors = [];
      var inventories = [];
      this.sizes = results[1];

      inventories = _.chain(results[2]).head().get("inventories").value();

      this.colors = _.map(results[0], (color) => {
        let sizes = JSON.parse(JSON.stringify(this.sizes));
        sizes = _.map(sizes, (size) => {
          let inventoryObj = _.find(inventories, {
            styleComboId: color.id,
            sizeId: size.id,
          });
          if (inventoryObj) {
            size["quantityObject"] = inventoryObj;
          } else {
            size["quantityObject"] = { id: null, quantity: null };
          }
          return size;
        });
        color["sizes"] = sizes;
        return color;
      });
    });
  }

  updateInventory() {
    var payload = [];

    _.each(this.colors, (color) => {
      _.each(color.sizes, (size) => {
        let item = {
          quantity: _.get(size, "quantityObject.quantity", 0),
          sizeId: _.get(size, "id"),
          styleComboId: _.get(color, "id"),
          inventoryLocation: {
            id: this.inventoryLocationId,
          },
          edit: true,
        };
        if (_.get(size, "quantityObject.id")) {
          item["id"] = _.get(size, "quantityObject.id");
        }
        payload.push(item);
      });
    });

    if (!payload.length) {
      this.snackBar.open(`Please! Insert minimum single quantity!`, "Close");
      return;
    }
    this._inventoryService.create(payload).subscribe((resp) => {
      this.added.emit(resp || null);
      this.snackBar.open(`Inventory added!`, "Close");
    });
  }

  getAllInventoryLocation() {
    this._inventoryLocationService.getAll().subscribe((data) => {
      this.inventoryLocationList = data;
    });
  }

  onLocationChange(event) {
    this.getSizeColorBreakdown();
  }
}

import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  Inject
} from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { StyleService } from "app/modules-core/merchandising/style/style.service";
import * as _ from "lodash";
import { AuthService } from "app/authentication/auth.service";
import { forkJoin } from "rxjs";
import { SizeService } from "app/modules-core/common/size/size.service";
import { InventoryLocationService } from "app/modules-core/common/inventory-location/inventory-location.service";
import { InventoryService } from "../../inventory.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";

@Component({
  selector: "box-history-modal",
  templateUrl: "./box-history-modal.component.html",
})
export class BoxHistoryModalComponent implements OnInit {
  @Input()
  boxData: any = {};
  factoryShipmentId = null;
  boxId = null

  // @Output()
  // added = new EventEmitter();

  colors: any[] = [];
  sizes: any[] = [];

  boxHistoryList: any[] = [];

  constructor(
    private _styleService: StyleService,
    private _sizeService: SizeService,
    private _inventoryService: InventoryService,
    private snackBar: SnackbarService,
    private _inventoryLocationService: InventoryLocationService,
    public _auth: AuthService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.boxData = _.merge({}, _.get(data, 'data'));

  }
  ngOnInit() {
    console.log('boxData', this.boxData)
    this.factoryShipmentId = _.get(this.boxData, 'factoryShipmentId')
    this.boxId = _.get(this.boxData, 'id')
    this.getBoxHistory();
  }

  getBoxHistory() {
    this._inventoryService.getBoxHistory(this.boxId, this.factoryShipmentId).subscribe((data) => {
      this.boxHistoryList = data;
      console.log('this.boxHistoryList', this.boxHistoryList)
    });
  }
}

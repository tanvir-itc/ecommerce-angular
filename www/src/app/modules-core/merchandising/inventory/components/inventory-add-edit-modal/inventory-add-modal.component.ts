import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from "@angular/material";
import * as _ from 'lodash';
import { AuthService } from "app/authentication/auth.service";
import { forkJoin } from "rxjs";
import { StyleService } from "app/modules-core/merchandising/style/style.service";
import { SizeService } from "app/modules-core/common/size/size.service";
import { InventoryService } from "../../inventory.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { InventoryLocationService } from "app/modules-core/common/inventory-location/inventory-location.service";
import { CustomerService } from "app/modules-core/merchandising/customers/customer.service";
import { BoxHistoryModalComponent } from "../box-history-modal/box-history-modal.component";
import { getSummarnoteConfig } from "app/modules-core/utility/helpers";


@Component({
  selector: 'inventory-add-modal',
  templateUrl: 'inventory-add-modal.component.html',
})
export class InventoryAddModalComponent implements OnInit {
  styleId = null;
  groupId = null;
  inventoryLocationId = null;
  boxData = null;

  colors: any[] = [];
  sizes: any[] = [];

  inventoryLocationList: any[] = [];
  styleList: any[] = [];
  uniqSizeList: any[] = [];
  addItemList: any[] = [];
  allStyleList: any[] = [];
  sizeList: any[] = [];
  colorList: any[] = [];
  customerList: any[] = [];
  customerId = null;
  addComment = "";
  config = getSummarnoteConfig();
  displayCommentForm = false;
  constructor(
    public dialogRef: MatDialogRef<InventoryAddModalComponent>,
    public _auth: AuthService,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data,
    private _styleService: StyleService,
    private _sizeService: SizeService,
    private _inventoryService: InventoryService,
    private snackBar: SnackbarService,
    private _inventoryLocationService: InventoryLocationService,
    private _customerService: CustomerService,
  ) {
    this.styleId = _.get(data, 'styleId');
    this.groupId = _.get(data, 'groupId');
    this.boxData = _.get(data, 'data');
    this.uniqSizeList = _.get(data, 'sizes');
    this.inventoryLocationId = _.get(data, 'inventoryLocationId');
  }




  ngOnInit() {
    // this.getSizeColors();
    // this.getAllInventoryLocation();
    console.log('this.boxData', this.boxData)
    this.getAllCustomers()
    this.getAllStyleList()
    this.getSizes()
    this.onAddRow()

  }

  getStyleList() {
    this.styleList = _.get(this.boxData, 'styleList')
    console.log(' this.styleList ', this.styleList)
  }

  getAllStyleList() {
    this._styleService.getAll().subscribe((data) => {
      this.allStyleList = data;
      console.log(this.styleList)
    });
  }

  getSizes() {
    this._sizeService.getAll().subscribe((data) => {
      this.sizeList = data;
      console.log(this.sizeList)
    });
  }

  onChangeStyle(data, item) {
    this.colorList = [];
    item.styleComboId = null;
    item.colorList = _.merge([], _.get(data, 'combos'))
    console.log('item.colorList', item.colorList)
  }

  onAddRow() {
    let item = {
      'styleId': null,
      'comboId': null,
      'sizeId': null,
      'quantity': null,
      'comment': this.addComment
    }
    this.addItemList.push(item)
    // console.log(' this.addItemList ', this.addItemList)
  }

  addStyleToInventory() {
    // if (!this.customerId) {
    //   this.snackBar.open(`Please! Select customer!`, "Close");
    //   return;
    // }
    var payload = [];
    payload = _.map(this.addItemList, style => {
      return {
        // "po": style.po,
        // "shipment": style.shipment,
        "quantity": style.quantity,
        "factoryShipmentPackingId": _.get(this.boxData.box, 'id'),
        "styleComboId": style.styleComboId,
        // "customer": this.customerId,
        "sizeId": style.sizeId,
        "comment": this.addComment,
      }
    })

    if (!payload.length) {
      this.snackBar.open(`Please! Insert minimum single quantity!`, "Close");
      return;
    }
    this._inventoryService.addStyleToInventory(payload).subscribe((resp) => {
      this.dialogRef.close(true);
      this.snackBar.open(`Style  added!`, "Close");
    });
  }









  getSizeColors() {
    forkJoin([
      this._styleService.getColors({ styleid: this.styleId }),
      this._sizeService.getAll({ sizegroupid: this.groupId }),
    ]).subscribe((results) => {
      this.sizes = _.map(results[1], (item) => {
        item["quantity"] = null;
        return item;
      });
      var colors = _.map(results[0], (item) => {
        item["sizes"] = JSON.parse(JSON.stringify(this.sizes));
        return item;
      });
      this.colors = _.sortBy(colors, "id");
    });
  }

  addInventory() {
    if (!this.inventoryLocationId) {
      this.snackBar.open(`Please! Select inventory location first!`, "Close");
      return;
    }
    var payload = [];

    _.each(this.colors, (color) => {
      _.each(color.sizes, (size) => {
        if (size.quantity) {
          payload.push({
            quantity: _.get(size, "quantity"),
            sizeId: _.get(size, "id"),
            styleComboId: _.get(color, "id"),
            inventoryLocation: {
              id: this.inventoryLocationId,
            },
          });
        }
      });
    });

    if (!payload.length) {
      this.snackBar.open(`Please! Insert minimum single quantity!`, "Close");
      return;
    }
    this._inventoryService.create(payload).subscribe((resp) => {
      this.dialogRef.close(true);
      this.snackBar.open(`Inventory added!`, "Close");
    });
  }

  getAllInventoryLocation() {
    this._inventoryLocationService.getAll().subscribe((data) => {
      this.inventoryLocationList = data;
    });
  }
  getAllCustomers() {
    this._customerService.getAll().subscribe((data) => {
      this.customerList = data;
    });
  }

  openHistoryModal(boxData) {
    const historyModal = this.dialog.open(BoxHistoryModalComponent, {
      width: '90vw',
      disableClose: true,
      data: {
        data: boxData
      }
    });
    historyModal.afterClosed().subscribe(result => {
      if (result) {
        // this.getAllBoxList(false);
      }
    });
  }

  rowRemove(index) {
    this.addItemList.splice(index, 1);
  }


}
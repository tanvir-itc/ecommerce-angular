import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from "@angular/material";
import * as _ from 'lodash';
import { AuthService } from "app/authentication/auth.service";
import { forkJoin } from "rxjs";
import { StyleService } from "app/modules-core/merchandising/style/style.service";
import { SizeService } from "app/modules-core/common/size/size.service";
import { InventoryService } from "../../inventory.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { InventoryLocationService } from "app/modules-core/common/inventory-location/inventory-location.service";
import { CustomerService } from "app/modules-core/merchandising/customers/customer.service";
import { BoxHistoryModalComponent } from "../box-history-modal/box-history-modal.component";
import { getSummarnoteConfig } from "app/modules-core/utility/helpers";
import { NgxSummernoteModule } from 'ngx-summernote';


@Component({
  selector: 'inventory-edit-modal',
  templateUrl: 'inventory-edit-modal.component.html',
})
export class InventoryEditModalComponent implements OnInit {
  styleId = null;
  groupId = null;
  inventoryLocationId = null;


  colors: any[] = [];
  sizes: any[] = [];
  styleListForEdit: any[] = [];
  uniqSizeList: any[] = [];

  inventoryLocationList: any[] = [];
  customerList: any[] = [];
  customerId = null;
  displayCommentForm = false;
  editComment = " ";
  config = getSummarnoteConfig();
  locaionId = null;

  constructor(
    public dialogRef: MatDialogRef<InventoryEditModalComponent>,
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
    private _styleService: StyleService,
    private _sizeService: SizeService,
    private _inventoryService: InventoryService,
    private _inventoryLocationService: InventoryLocationService,
    private _customerService: CustomerService,

    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.styleId = _.get(data, 'styleId');
    this.groupId = _.get(data, 'groupId');
    this.inventoryLocationId = _.get(data, 'inventoryLocationId');
    this.styleListForEdit = _.merge([], _.get(data, 'data'));
    this.uniqSizeList = _.get(data, 'sizes');
  }




  ngOnInit() {
    this.getAllCustomers();
    this.getSizeColorBreakdown();
    this.getAllInventoryLocation();
    console.log('this.boxData ', this.styleListForEdit)
  }

  getAllCustomers() {
    this._customerService.getAll().subscribe((data) => {
      this.customerList = data;
    });
  }
  getSizeColorBreakdown() {
    if (!this.inventoryLocationId) {
      return;
    }
    let params = {
      styleid: this.styleId,
      inventorylocationid: this.inventoryLocationId,
    };
    forkJoin([
      this._styleService.getColors({ styleid: this.styleId }),
      this._sizeService.getAll({ sizegroupid: this.groupId }),
      this._inventoryService.getBreakdown(params),
    ]).subscribe((results) => {
      this.sizes = [];
      this.colors = [];
      var inventories = [];
      this.sizes = results[1];

      inventories = _.chain(results[2]).head().get("inventories").value();

      this.colors = _.map(results[0], (color) => {
        let sizes = JSON.parse(JSON.stringify(this.sizes));
        sizes = _.map(sizes, (size) => {
          let inventoryObj = _.find(inventories, {
            styleComboId: color.id,
            sizeId: size.id,
          });
          if (inventoryObj) {
            size["quantityObject"] = inventoryObj;
          } else {
            size["quantityObject"] = { id: null, quantity: null };
          }
          return size;
        });
        color["sizes"] = sizes;
        return color;
      });
    });
  }

  updateInventory() {
    // if (!this.customerId) {
    //   this.snackBar.open(`Please! Select customer!`, "Close");
    //   return;
    // }
    var payload = [];

    _.each(this.styleListForEdit, styleObject => {
      _.each(styleObject.colorList, color => {
        _.each(color.sizeList, size => {
          let item = {
            "quantity": size.quantity,
            "factoryShipmentPackingId": _.get(styleObject, 'id'),
            "styleComboId": color.styleComboId,
            // "customer": this.customerId,
            "sizeId": _.get(size, 'sizeId'),
            "id": _.get(size, 'id'),
            "comment": this.editComment,
            'locationId': this.locaionId
          }
          payload.push(item)
        })
      })

    })
    if (!payload.length) {
      this.snackBar.open(`Please! Insert minimum single quantity!`, "Close");
      return;
    }
    this._inventoryService.updateInventory(payload).subscribe((resp) => {
      this.dialogRef.close(true);
      this.snackBar.open(`Inventory updated!`, "Close");
    });
  }

  getAllInventoryLocation() {
    this._inventoryLocationService.getAll().subscribe((data) => {
      this.inventoryLocationList = data;
    });
  }

  onLocationChange(event) {
    this.getSizeColorBreakdown();
  }
  openHistoryModal(boxData) {
    const historyModal = this.dialog.open(BoxHistoryModalComponent, {
      width: '90vw',
      disableClose: true,
      data: {
        data: boxData
      }
    });
    historyModal.afterClosed().subscribe(result => {
      if (result) {
        // this.getAllBoxList(false);
      }
    });
  }

  deleteItem(styleObject) {
    if (!confirm("Delete confirm?")) {
      return;
    }
    this._inventoryService.deleteStyle(styleObject.id).subscribe(res => {
      this.snackBar.open(`Item Deleted!`, "Close");
    });
  }


}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { RouterModule } from '@angular/router';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { InventoryAddModalComponent } from './inventory-add-modal.component';
import { StyleBasicModule } from 'app/modules-core/merchandising/style/components/style-basic/style-basic.module';
import { InventoryEditModalComponent } from './inventory-edit-modal.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxSummernoteModule } from 'ngx-summernote';
@NgModule({
  declarations: [
    InventoryAddModalComponent,
    InventoryEditModalComponent,
  ],
  entryComponents: [
    InventoryAddModalComponent,
    InventoryEditModalComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FuseSharedModule,
    RouterModule,
    ButtonModule,
    DateTimeModule,
    GeneralModule,
    StyleBasicModule,
    NgSelectModule,
    NgxSummernoteModule,
  ],
  exports: [
    InventoryAddModalComponent,
    InventoryEditModalComponent,
  ]
})

export class InventoryAddEditModalModule {
}
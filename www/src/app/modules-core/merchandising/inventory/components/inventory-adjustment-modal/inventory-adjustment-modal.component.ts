import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import * as _ from 'lodash';
import { AuthService } from "app/authentication/auth.service";
import { InventoryLocationService } from "app/modules-core/common/inventory-location/inventory-location.service";
import { AdjustmentTypeService } from "app/modules-core/common/adjustment-type/adjustment-type.service";
import { toDateString } from "app/modules-core/utility/helpers";
import { InventoryService } from "../../inventory.service";
import { StyleService } from "app/modules-core/merchandising/style/style.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";



@Component({
  selector: 'inventory-adjustment-modal',
  templateUrl: 'inventory-adjustment-modal.component.html',
})
export class InventoryAdjustmentModalComponent implements OnInit {
  adjustmentObject: any = {};

  inventories: any[] = [];
  styleList: any[] = [];
  inventoryLocationList: any[] = [];
  adjustmentTypeList: any[] = [];

  selectedStyleList = [];

  constructor(
    public dialogRef: MatDialogRef<InventoryAdjustmentModalComponent>,
    public _auth: AuthService,
    public _inventoryLocationService: InventoryLocationService,
    public _adjustmentTypeService: AdjustmentTypeService,
    public _inventoryService: InventoryService,
    public _styleService: StyleService,
    private snackBar: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    //this.styleId = _.get(data, 'styleId');
  }




  ngOnInit() {
    this.clearObject();
    this.getAllInventoryLocation();
    this.getAllAdjustmentTypes();
    this.getStyleList();
  }




  getInventorys() {
    this.inventories = [];
    let params = {
      inventorylocationid: _.get(this.adjustmentObject, 'fromId'),
    };
    this._inventoryService.getAllBySizeGroupV4(params).subscribe(resp => {
      this.inventories = resp;
    });
  }

  onLocationChange() {
    this.getInventorys();
  }
  getAllInventoryLocation() {
    this._inventoryLocationService.getAll({ all: true }).subscribe(data => {
      this.inventoryLocationList = data;
    });
  }
  getAllAdjustmentTypes() {
    this._adjustmentTypeService.getAll().subscribe(data => {
      this.adjustmentTypeList = _.filter(data, item => {
        return item.name != 'transfer';
      });
    });
  }

  getStyleList() {
    this._styleService.getAllLight().subscribe(data => {
      this.styleList = data;
    });
  }

  clearObject() {
    this.adjustmentObject = {
      date: toDateString(new Date),
      fromId: null,
      type: null,
      actionType: 'SUBSTRACT',
      inventoryTransferDetails: [],
      comment: null,
      styleList: []
    }
  }

  onStyleSelect(event) {
    _.each(this.inventories, group => {
      _.each(group.inventories, style => {
        if (this.adjustmentObject.styleList.includes(style.styleId)) {
          style.display = true;
        } else {
          style.display = false;
        }
      })
    })
  }

  update() {
    let payload = [];
    let error = [];

    if (!_.get(this.adjustmentObject, 'fromId')) {
      this.snackBar.open(`Please select inventory location.`, "Close");
      return;
    }
    if (!_.get(this.adjustmentObject, 'date')) {
      this.snackBar.open(`Please select a date.`, "Close");
      return;
    }
    if (!_.get(this.adjustmentObject, 'type.id')) {
      this.snackBar.open(`Please select adjustment type.`, "Close");
      return;
    }
    if (!_.get(this.adjustmentObject, 'actionType')) {
      this.snackBar.open(`Please select type.`, "Close");
      return;
    }
    if (!this.adjustmentObject.styleList.length) {
      this.snackBar.open(`Please select minimum one style.`, "Close");
      return;
    }


    _.each(this.inventories, group => {
      _.each(group.inventories, style => {
        if (style.display) {
          for (var property in style.breakdown) {
            let qtyObj = style.breakdown[property]
            let item = {
              edit: true,
              inventoryAdjustmentTypeId: this.adjustmentObject.type.id,
              inventoryLocation: {
                id: this.adjustmentObject.fromId
              },
              styleId: _.get(style, 'styleId'),
              sizeId: _.get(qtyObj, 'sizeId'),
              styleComboId: _.get(qtyObj, 'styleComboId'),
              quantity: null,
              reason: this.adjustmentObject.comment
            }
            qtyObj.editQuantity = parseInt(qtyObj.editQuantity || 0)
            qtyObj.adjustmentQuantity = parseInt(qtyObj.adjustmentQuantity || 0)
            if (this.adjustmentObject.type.name == 'new') {
              item.quantity = _.get(qtyObj, 'actualStockQuantity', 0) + _.get(qtyObj, 'adjustmentQuantity', 0)
            } else if (this.adjustmentObject.type.name == 'edit') {
              item.quantity = _.get(qtyObj, 'editQuantity')
              if (item.quantity < 0) {
                error.push(`${qtyObj.styleName}-(${qtyObj.sizeName}-${qtyObj.sizeName}) can not less then 0.`);
              }
            } else {
              if (this.adjustmentObject.actionType == 'SUBSTRACT') {
                let qty = _.subtract(_.get(qtyObj, 'actualStockQuantity', 0), _.get(qtyObj, 'adjustmentQuantity', 0))
                item.quantity = qty || 0
                if (item.quantity < 0) {
                  error.push(`${qtyObj.styleName}-(${qtyObj.sizeName}-${qtyObj.sizeName}) can not remove over then stock quantity.`);
                }
              } else if (this.adjustmentObject.actionType == 'ADD') {
                let qty = _.get(qtyObj, 'actualStockQuantity', 0) + _.get(qtyObj, 'adjustmentQuantity', 0)
                item.quantity = qty || 0
              }
            }

            payload.push({ ...item })

          }
        }
      })
    })
    if (error.length) {
      this.snackBar.open(_.join(error, ', '), "Close", true);
      return;
    }
    this._inventoryService.create(payload).subscribe((resp) => {
      this.dialogRef.close(true);
      this.snackBar.open(`Inventory updated!`, "Close");
    });
  }


}
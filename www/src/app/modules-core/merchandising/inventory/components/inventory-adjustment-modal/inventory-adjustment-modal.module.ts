import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { RouterModule } from '@angular/router';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { InventoryAdjustmentModalComponent } from './inventory-adjustment-modal.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    InventoryAdjustmentModalComponent,
  ],
  entryComponents: [
    InventoryAdjustmentModalComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FuseSharedModule,
    TranslateModule,
    NgSelectModule,
    RouterModule,
    ButtonModule,
    DateTimeModule,
    GeneralModule,


  ],
  exports: [
    InventoryAdjustmentModalComponent,
  ]
})
export class InventoryAdjustmentModalModule { }
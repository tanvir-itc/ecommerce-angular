import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from "rxjs/internal/operators/map";
import * as _ from "lodash";
import { makeParams } from "app/modules-core/utility/helpers";

@Injectable({
  providedIn: "root",
})
export class InventoryService {
  urlBase = "inventory-quantities";
  urlBaseHistory = "inventory-histories";
  urlForStyleAddToBox = "factory-shipment-packing-details";
  urlForMerging = "factory-shipments/move";

  httpOptions = {
    params: new HttpParams(),
  };
  constructor(private http: HttpClient) { }

  getAll(opts = {}) {
    return this.http
      .get(`${this.urlBase}/v1`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = JSON.parse(resp.data);
            resp.data = _.map(resp.data, (item) => {
              item["totalQuantity"] = _.sumBy(item.inventories, "quantity");
              return item;
            });
            return resp.data;
          }
          return resp.data;
        })
      );
  }

  getAllBySizeGroup(opts = {}) {
    return this.http
      .get(`${this.urlBase}/v3`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = JSON.parse(resp.data);

            _.each(resp.data, group => {
              _.each(group.inventories, style => {
                let uniqueCombos = _.uniqBy(_.filter(style.inventories, item => item.quantity > 0), 'styleCombo');
                style['colorList'] = _.map(uniqueCombos, item => {
                  if (item.styleCombo.includes("In Stock")) {
                    let status = 'In Stock'
                    item['displayName'] = item.styleCombo.slice(0, item.styleCombo.indexOf('In Stock'))
                    item['colorStatus'] = item.styleCombo.slice(item.styleCombo.indexOf('In Stock'), item.styleCombo.indexOf('In Stock') + (status.length + 1))
                    item['stockDate'] = item.styleCombo.slice((item.styleCombo.indexOf('In Stock') + status.length + 1))
                  } else if (item.styleCombo.includes("Available")) {
                    item['status'] = 'available'
                    item['displayName'] = item.styleCombo.slice(0, item.styleCombo.indexOf('Available'))
                    item['colorStatus'] = item.styleCombo.slice(item.styleCombo.indexOf('Available'), item.styleCombo.length)
                  } else {
                    item['displayName'] = item.styleCombo
                  }

                  return {
                    id: _.get(item, 'styleComboId'),
                    name: _.get(item, 'styleCombo'),
                    comboNameForDisplay: item.displayName,
                    comboStatusForDisplay: item.colorStatus,
                    comboStockDateForDisplay: item.stockDate,
                    colortoalQty: _.sumBy(_.filter(style.inventories, ['styleCombo', _.get(item, 'styleCombo')]), 'quantity'),
                    colortoalAvaialableQty: _.sumBy(_.filter(style.inventories, ['styleCombo', _.get(item, 'styleCombo')]), 'availableOrderQty')
                  }
                });

                style['breakdown'] = {};
                style['colorImages'] = _.filter(_.flatMap(style.comboList, 'images'), 'isDefault')
                
                style['totalQuantity'] = _.sumBy(style.inventories.flatMap(obj => obj.details).filter(inventory => inventory.poId ===null), 'quantity');
                style['totalUpcomingQuantity'] = _.sumBy(style.inventories.flatMap(obj => obj.details).filter(inventory => inventory.poId !==null), 'quantity');
                _.each(style.inventories, qtyBreakdown => {
                  qtyBreakdown['transferQty'] = null;
                  qtyBreakdown['orderQty'] = _.sumBy(qtyBreakdown.details, 'orderQty')
                  qtyBreakdown['availableOrderQty'] = _.subtract(qtyBreakdown.quantityForOrderBalance || 0, qtyBreakdown.orderQty || 0)
                  qtyBreakdown['shipProcessingQty'] = _.sumBy(qtyBreakdown.details, 'shipProcessing')
                  let key = null;
                  key = "_" + qtyBreakdown.styleComboId + "_" + qtyBreakdown.sizeId + "_" + qtyBreakdown.styleCombo;
                  style.breakdown[key] = qtyBreakdown;
                });
                style['availableTotalQuantity'] = _.sumBy(style.inventories, 'availableOrderQty');
                _.each(style.colorList, c => {
                  c.colortoalAvaialableQty = _.sumBy(_.filter(style.inventories, ['styleCombo', _.get(c, 'name')]), 'availableOrderQty')
                })

              });
              group['totalQuantity'] = _.sumBy(group.inventories, 'totalQuantity');
              group['totalUpcomingQuantity'] = _.sumBy(group.inventories, 'totalUpcomingQuantity');
              group['availableTotalQuantity'] = _.sumBy(group.inventories, 'availableTotalQuantity');
            });


            return resp.data;
          }
          return resp.data;
        })
      );
  }

  getAllBySizeGroupV2(opts = {}) {
    return this.http
      .get(`${this.urlBase}/v3`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = JSON.parse(resp.data);

            _.each(resp.data, group => {
              _.each(group.inventories, style => {

                let uniqueCombos = _.uniqBy(style.inventories, 'styleComboId');
                style['colorList'] = _.map(uniqueCombos, item => {
                  return {
                    id: _.get(item, 'styleComboId'),
                    name: _.get(item, 'styleCombo'),
                  }
                });

                style['breakdown'] = {};
                style['totalQuantity'] = _.sumBy(style.inventories, 'quantity');
                _.each(style.inventories, qtyBreakdown => {
                  qtyBreakdown['transferQty'] = null;
                  let key = null;
                  key = "_" + qtyBreakdown.styleComboId + "_" + qtyBreakdown.sizeId;
                  style.breakdown[key] = qtyBreakdown;
                });

              });
            });


            return resp.data;
          }
          return resp.data;
        })
      );
  }
  getAllBySizeGroupV4(opts = {}) {
    return this.http
      .get(`${this.urlBase}/v4`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = JSON.parse(resp.data);
            _.each(resp.data, group => {
              _.each(group.inventories, style => {
                style['display'] = false;
                style['breakdown'] = {};
                _.each(style.comboList, color => {
                  _.each(group.sizeList, size => {
                    let breakdown = {
                      styleComboId: color.id,
                      sizeId: size.id,
                      comboName: color.name,
                      sizeName: size.name,
                      styleName: style.name,
                      actualStockQuantity: _.chain(style.inventories).find({ styleComboId: color.id, sizeId: size.id }).get('quantity', 0).value(),
                    }
                    breakdown['adjustmentQuantity'] = null;
                    breakdown['editQuantity'] = breakdown.actualStockQuantity || 0;
                    let key = null;
                    key = "_" + color.id + "_" + size.id;
                    style.breakdown[key] = breakdown;

                  })
                })
              });
            });
            return resp.data;
          }
          return resp.data;
        })
      );
  }

  getBreakdown(opts = {}) {
    return this.http
      .get(`${this.urlBase}/v5`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = JSON.parse(resp.data);
            var styleBreakdown = [];
            _.each(resp.data, (styleItem) => {
              var uniqueCombos = [];
              uniqueCombos = _.chain(styleItem)
                .get("inventories")
                .uniqBy("styleComboId")
                .value();
              uniqueCombos = _.map(uniqueCombos, (combo) => {
                return {
                  comboId: combo.styleComboId,
                  comboName: combo.styleCombo,
                };
              });

              styleItem["combos"] = _.sortBy(uniqueCombos, "comboId");
              styleItem["breakdown"] = {};
              _.each(styleItem.inventories, (item) => {
                var key = null;
                key = "_" + item.styleComboId + "_" + item.sizeId;
                styleItem["breakdown"][key] = item;
              });

              _.each(styleItem.combos, combo => {
                combo['totalQty'] = _.chain(styleItem.inventories).filter({ styleComboId: combo.comboId }).sumBy('quantity').value();
              });

              styleItem["total"] = _.sumBy(styleItem.inventories, "quantity");
              styleBreakdown.push(styleItem);
            });
            return styleBreakdown;
          }
          return [];
        })
      );
  }

  get(id) {
    return this.http.get(`${this.urlBase}/${id}/v5`).pipe(
      map((resp: any) => {
        if (resp.data) {
          resp.data = JSON.parse(resp.data);
          return _.head(resp.data);
        }
        return resp.data;
      })
    );
  }

  create(payload) {
    return this.http
      .post(this.urlBase, payload)
      .pipe(map((resp: any) => resp.data));
  }

  update(id, payload) {
    return this.http
      .put(`${this.urlBase}/${id}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFields(id, payload) {
    return this.http
      .patch(`${this.urlBase}/${id}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  delete(id, payload) {
    return this.http
      .delete(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }




  getAllStyleHistory(opts = {}) {
    return this.http
      .get(`${this.urlBase}/histories`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = JSON.parse(resp.data);
            return resp.data;
          }
          return resp.data;
        })
      );
  }

  getAllHistory(opts = {}) {
    return this.http
      .get(`${this.urlBase}/histories`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = JSON.parse(resp.data);
            return resp.data;
          }
          return resp.data;
        })
      );
  }

  getAllBoxList(opts = {}) {
    return this.http
      .get(`factory-shipment-packing-details`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            // let sizeList = _.chain(resp.data)
            //   .flatMap('boxList')
            //   .flatMap('boxDetail')
            //   .flatMap('details')
            //   .uniqBy('sizeId')
            //   .map(value => {
            //     return {
            //       'sizeId': value.sizeId,
            //       'sizeName': value.sizeName
            //     }
            //   })
            //   .value()
            _.each(resp.data, obj => {
              _.each(obj.boxList, box => {
                box.styleList = []
                box['styleGroupList'] = _.groupBy(box.boxDetail, 'styleName')
                box['shipProcessingQty'] = _.sumBy(_.flatMap(box.boxDetail, 'details'), 'shipProcessingQty')
                box['styleList'] = _.map(box.styleGroupList, (v, k) => {
                  return {
                    'shipment': box.box.shipmentName,
                    'po': box.box.poNo,
                    'quantity': box.box.qty,
                    'id': box.box.id,
                    'boxNumber': box.box.boxNumber,
                    'factoryShipmentId': box.box.factoryShipmentId,
                    'styleName': k,
                    'colorList': v,
                  }
                })
                _.each(box.styleList, s => {
                  _.each(s.colorList, c => {
                    c['sizeList'] = []
                  })
                })
              })

            })
            console.log(resp.data)
            resp.data = resp.data;

            return resp.data;
          }
          return resp.data;
        })
      );
  }
  addStyleToInventory(payload) {
    return this.http
      .post(this.urlForStyleAddToBox, payload)
      .pipe(map((resp: any) => resp.data));
  }
  updateInventory(payload) {
    return this.http
      .patch(`${this.urlForStyleAddToBox}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  mergeBoxes(payload) {
    return this.http
      .post(this.urlForMerging, payload)
      .pipe(map((resp: any) => resp.data));
  }


  getBoxHistory(boxid, factoryShipId) {
    return this.http.get(`factory-shipments/${factoryShipId}/details/${boxid}/histories`).pipe(
      map((resp: any) => {
        if (resp.data) {
          resp.data = resp.data;
          return resp.data;
        }
        return resp.data;
      })
    );
  }


  deleteStyle(id) {
    return this.http
      .delete(`${this.urlForStyleAddToBox}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

}

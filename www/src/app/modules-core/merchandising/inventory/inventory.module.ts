import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from '../../utility/date-time-picker/date-time.module';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { InventoryListComponent } from './pages/inventory-list/inventory-list.component';
import { StyleBasicModule } from '../style/components/style-basic/style-basic.module';
import { StyleBreakdownComponent } from './components/style-breakdown/style-breakdown.component';
import { InventoryEditComponent } from './components/inventory-edit/inventory-edit.component';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { InventoryAddEditModalModule } from './components/inventory-add-edit-modal/inventory-add-edit-modal.module';
import { InventoryAdjustmentModalModule } from './components/inventory-adjustment-modal/inventory-adjustment-modal.module';
import { InventoryEditNewComponent } from './pages/inventory-edit/inventory-edit.component';
import { NgxSummernoteModule } from 'ngx-summernote';
import { BoxHistoryModalComponent } from './components/box-history-modal/box-history-modal.component';

const routes = [
  {
    path: 'inventories',
    component: InventoryListComponent,
    data: { permission: 'PERMISSION_MENU_INVENTORY_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  },
  {
    path: 'inventory-edit',
    component: InventoryEditNewComponent,
    data: { permission: 'PERMISSION_MENU_INVENTORY_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  },
];
@NgModule({
  declarations: [
    InventoryListComponent,
    StyleBreakdownComponent,
    InventoryEditComponent,
    InventoryEditNewComponent,
    BoxHistoryModalComponent
  ],
  entryComponents: [
    BoxHistoryModalComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    DateTimeModule,
    AttachmentModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
    StyleBasicModule,
    InventoryAddEditModalModule,
    InventoryAdjustmentModalModule,
    NgxSummernoteModule
  ],
  exports: [
    StyleBreakdownComponent,
    InventoryEditComponent,
    InventoryEditNewComponent,
    BoxHistoryModalComponent
  ]
})
export class InventoryModule {
}
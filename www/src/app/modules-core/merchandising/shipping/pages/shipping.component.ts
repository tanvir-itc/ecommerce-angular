import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { fuseAnimations } from "@fuse/animations";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-shipping",
  templateUrl: "./shipping.component.html",
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ShippingComponent implements OnInit {
  show = false;
  tabs;
  tabFromUrl;

  basePath;

  constructor(
    private _route: ActivatedRoute,
  ) {
    // this.id = +this._route.snapshot.paramMap.get("id");
    this.tabFromUrl = this._route.snapshot.firstChild.routeConfig.path; //find a better way to get child param
    this.basePath = this._route.snapshot.url[0].path; //find a better way to get child param
  }

  ngOnInit() {
    this._route.paramMap.subscribe(params => {
      this.show = false;
      this.tabs = [
        {
          path: `${this.basePath}/pending`,
          label: "Pending Orders",
          color: 'tab-red',
        },
        {
          path: `${this.basePath}/packing-in-progress`,
          label: "Packing in Progress",
          color: 'tab-yellow',
        },
        {
          path: `${this.basePath}/shipped`,
          label: "Shipped",
          color: 'tab-green',
        }
      ];
      this.show = true;
    });
  }

}

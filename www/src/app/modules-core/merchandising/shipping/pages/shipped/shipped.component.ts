import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  stringToIntegerArray,
  removeEmptyFields,
  integerArrayToString,
  customSortBy,
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { CustomerService } from "app/modules-core/merchandising/customers/customer.service";
import { ShipmentService } from "app/modules-core/merchandising/shipping/shipment.service";
import { PdfModalComponent } from "app/modules-core/general/pdf-modal/pdf-modal.component";
import { MatDialog } from "@angular/material";

@Component({
  selector: "app-shippped",
  templateUrl: "./shipped.component.html",
})
export class ShippedComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  list: any[] = [];

  customerList: any[] = [];

  modalRef: any = null;

  sorter: any = {
    field: null,
    sortType: 'asc',
    fieldType: 'text',
  }

  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _customerService: CustomerService,
    private _shipmentService: ShipmentService,
    private dialog: MatDialog,
  ) {
    this._quickPanelSubjectService.heading.next("Shipping");
  }
  ngOnInit() {
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));
    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.filter.fromdate = toDateString(
      moment(this.filter.todate || new Date()).startOf("month")
    );
    this.filter.todate = toDateString(
      moment(this.filter.fromdate || new Date()).endOf("month")
    );
    this.applyFilters(this.filter);
    this.getAllCustomers();
  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null,
      styleid: null,
      orderid: null,
      customerid: [],
      shipmentid: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "customerid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    params['packingprocessing'] = false;
    params['sort'] = 's.shipment_date,desc';
    this._shipmentService.getAll(params).subscribe((data) => {
      this.list = data;
      this.setFilterToURL(filterParams);
    });
  }

  getAllCustomers() {
    this._customerService.getAll({ requested: false }).subscribe((data) => {
      this.customerList = data;
    });
  }


  onSort(field, fieldType = 'text') {
    this.sorter = {
      field: field,
      fieldType: fieldType,
      sortType: this.sorter.sortType == 'asc' ? 'desc' : 'asc',
    }
    this.list = customSortBy(this.list, this.sorter);
  }



  goLink(id) {
    this._router.navigateByUrl(`orders/${id}`);
  }


  openNewTab(id) {
    const url = this._router.serializeUrl(
      this._router.createUrlTree([`/shipment/${id}/shipment-print`])
    );
    window.open(url, '_blank');
  }

  openPackingPDFModal(shipmentId) {
    this._shipmentService.getPackingPDF(shipmentId).subscribe(resp => {
      this.modalRef = this.dialog.open(PdfModalComponent, {
        // minWidth: "700px",
        width: "90%",
        // disableClose: true,
        data: {
          title: "Packing",
          resource: _.get(resp, 'path'),
          emailButton: true,
          emailObject: {
            type: 'packing',
            shipmentId: shipmentId,
          }
        }
      });
    })
  }

  openPackingBoxPDFModal(shipmentId) {
    this._shipmentService.getPackingBoxPDF(shipmentId).subscribe(resp => {
      this.modalRef = this.dialog.open(PdfModalComponent, {
        // minWidth: "700px",
        width: "90%",
        // disableClose: true,
        data: {
          title: "Packing box",
          resource: _.get(resp, 'path'),
          emailButton: true,
          emailObject: {
            type: 'packing',
            shipmentId: shipmentId,
          }
        }
      });
    })
  }


} //class

import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { MatDialog } from "@angular/material";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  stringToIntegerArray,
  removeEmptyFields,
  integerArrayToString,
  customSortBy,
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { CustomerService } from "app/modules-core/merchandising/customers/customer.service";
import { OrderService } from "app/modules-core/merchandising/order/order.service";
import { ShipmentCreateModalComponent } from "../../components/shipment-create/shipment-create-modal.component";
import { PdfModalComponent } from "app/modules-core/general/pdf-modal/pdf-modal.component";
import { ShipmentService } from "../../shipment.service";

@Component({
  selector: "shipping-pending",
  templateUrl: "./shipping-pending.component.html",
})
export class ShippingPendingComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  list: any[] = [];

  customerList: any[] = [];
  statusList: any[] = [];

  modalRef: any = null;

  sorter: any = {
    field: null,
    sortType: 'asc',
    fieldType: 'text',
  }
  user: any = null
  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _customerService: CustomerService,
    private _orderService: OrderService,
    private _shipmentService: ShipmentService,
    private dialog: MatDialog,
  ) {
    this._quickPanelSubjectService.heading.next("Shipping");
    this.user = this._auth.getCurrentUser()
  }
  ngOnInit() {
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));
    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
    this.getAllCustomers();

  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null,
      styleid: null,
      orderid: null,
      customerid: [],
      shipmentid: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "customerid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    params['statusname'] = 'new,partial_shipped';
    params['approved'] = true;
    params['sort'] = 'order_date,desc';
    this._orderService.getAll(params).subscribe((data) => {
      this.list = data;
      this.setFilterToURL(filterParams);
    });
  }

  getAllCustomers() {
    this._customerService.getAll({ requested: false }).subscribe((data) => {
      this.customerList = data;
    });
  }


  onSort(field, fieldType = 'text') {
    this.sorter = {
      field: field,
      fieldType: fieldType,
      sortType: this.sorter.sortType == 'asc' ? 'desc' : 'asc',
    }
    this.list = customSortBy(this.list, this.sorter);
  }


  openShipmentCreateModal(orderId) {
    const modal = this.dialog.open(ShipmentCreateModalComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      width: '95%',
      height: '98%',
      maxHeight: '98%',
      minHeight: '98%',
      data: {
        orderId: orderId,
        editMode: 'create',
      },
      disableClose: true,
    });
    modal.afterClosed().subscribe(result => {
      this.applyFilters(this.filter);
    });
  }

  openShipmentEditModal(orderId, shipmentId, comment) {
    const modal = this.dialog.open(ShipmentCreateModalComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      width: '95%',
      height: '98%',
      maxHeight: '98%',
      minHeight: '98%',
      disableClose: true,
      data: {
        shipmentId: shipmentId,
        orderId: orderId,
        editMode: 'edit',
        comment: comment,
      }
    });
    modal.afterClosed().subscribe(result => {
      this.applyFilters(this.filter);
    });
  }



  openNewTab(id) {
    const url = this._router.serializeUrl(
      this._router.createUrlTree([`/shipment/${id}/shipment-print`])
    );
    window.open(url, '_blank');
  }
  openPackingPDFModal(shipmentId) {
    this._shipmentService.getPackingPDF(shipmentId).subscribe(resp => {
      this.modalRef = this.dialog.open(PdfModalComponent, {
        // minWidth: "700px",
        width: "90%",
        // disableClose: true,
        data: {
          title: "Packing",
          resource: _.get(resp, 'path'),
          emailButton: true,
          emailObject: {
            type: 'packing',
            shipmentId: shipmentId,
          }
        }
      });
    })
  }



} //class

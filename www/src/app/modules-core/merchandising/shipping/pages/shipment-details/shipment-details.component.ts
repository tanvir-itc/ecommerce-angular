import { Component, OnInit, Input } from '@angular/core';
import * as _ from 'lodash';
import { AuthService } from 'app/authentication/auth.service';
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
import { ActivatedRoute } from '@angular/router';
import { SizeService } from 'app/modules-core/common/size/size.service';
import { forkJoin } from 'rxjs';
import { ShipmentService } from '../../shipment.service';


@Component({
  selector: 'shipment-details',
  templateUrl: './shipment-details.component.html',
})
export class ShipmentDetailsComponent implements OnInit {
  id;

  shipment: any = null;

  sizes: any[] = [];

  styleBreakdown: any[] = [];

  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    public _auth: AuthService,
    private _shipmentService: ShipmentService,
    private _sizeService: SizeService,
  ) {
    this._quickPanelSubjectService.heading.next('Shipment Details');
  }

  ngOnInit() {
    this.id = + this._route.snapshot.paramMap.get('id');
    this.getDetails();
  }

  getDetails() {
    forkJoin([
      this._sizeService.getAll(),
      this._shipmentService.getBreakdown(this.id)
    ]).subscribe(results => {
      this.sizes = results[0];
      this.shipment = _.get(results[1], 'shipment');
      this.styleBreakdown = _.get(results[1], 'breakdown');
    });

  }


}

import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { MatDialog } from "@angular/material";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  stringToIntegerArray,
  removeEmptyFields,
  integerArrayToString,
  customSortBy,
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { CustomerService } from "app/modules-core/merchandising/customers/customer.service";
import { ShipmentCreateModalComponent } from "../../components/shipment-create/shipment-create-modal.component";
import { ShipmentService } from "../../shipment.service";


@Component({
  selector: "packing-processing",
  templateUrl: "./packing-processing.component.html",
})
export class PackingProcessingComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  list: any[] = [];

  customerList: any[] = [];

  modalRef: any = null;

  sorter: any = {
    field: null,
    sortType: 'asc',
    fieldType: 'text',
  }

  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _customerService: CustomerService,
    private _shipmentService: ShipmentService,
    private dialog: MatDialog,
  ) {
    this._quickPanelSubjectService.heading.next("Shipping");
  }
  ngOnInit() {
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));
    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
    this.getAllCustomers();
  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null,
      styleid: null,
      orderid: null,
      customerid: [],
      shipmentid: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "customerid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    params['packingprocessing'] = true;
    params['sort'] = 's.shipment_date,desc';
    this._shipmentService.getAll(params).subscribe((data) => {
      this.list = data;
      this.setFilterToURL(filterParams);
    });
  }

  getAllCustomers() {
    this._customerService.getAll({ requested: false }).subscribe((data) => {
      this.customerList = data;
    });
  }


  onSort(field, fieldType = 'text') {
    this.sorter = {
      field: field,
      fieldType: fieldType,
      sortType: this.sorter.sortType == 'asc' ? 'desc' : 'asc',
    }
    this.list = customSortBy(this.list, this.sorter);
  }


  deleteShipment(shipmentId) {
    if (!confirm("Confirm! shipment delete?")) {
      return;
    }
    this._shipmentService.delete(shipmentId).subscribe(resp => {
      this.applyFilters(this.filter);
    });
  }




  openShipmentEditModal(shipmentId, orderId, comment) {
    const modal = this.dialog.open(ShipmentCreateModalComponent, {
      maxWidth: '98%',
      minWidth: '98%',
      height: '98%',
      data: {
        shipmentId: shipmentId,
        orderId: orderId,
        editMode: 'packing',
        comment: comment,
      }
    });
    modal.afterClosed().subscribe(result => {
      this.applyFilters(this.filter);
    });
  }


} //class

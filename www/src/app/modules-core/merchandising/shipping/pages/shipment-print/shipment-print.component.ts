import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/authentication/auth.service';
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
import * as _ from "lodash";
import { ShipmentService } from '../../shipment.service';

@Component({
  selector: 'app-shipment-print',
  templateUrl: './shipment-print.component.html',
  styleUrls: ['./shipment-print.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ShipmentPrintComponent implements OnInit {
  id: any = null;
  shipment: any = null;
  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _shipmentService: ShipmentService,
    public _auth: AuthService,
    private _quickPanelSubjectService: QuickPanelSubjectService
  ) {
    this.id = + this.route.snapshot.paramMap.get('id');
    this._quickPanelSubjectService.heading.next('Shipment Packing');
  }

  ngOnInit() {
    this.getShipment();
  }

  getShipment() {
    let params = {
      shipmentid: this.id
    }
    this._shipmentService.getGroupBreakdown(params).subscribe(resp => {
      this.shipment = resp;
    });
  }


  goLink(id) {
    this._router.navigateByUrl(`styles/${id}/`);
  }
}

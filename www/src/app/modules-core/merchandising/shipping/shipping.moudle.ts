import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { ShippingComponent } from './pages/shipping.component';
import { ShippingPendingComponent } from './pages/shipping-pending/shipping-pending.component';
import { ShipmentCreateModalComponent } from './components/shipment-create/shipment-create-modal.component';
import { PackingProcessingComponent } from './pages/packing-processing/packing-processing.component';
import { ShippedComponent } from './pages/shipped/shipped.component';
import { ShipmentDetailsComponent } from './pages/shipment-details/shipment-details.component';
import { ShipmentPrintComponent } from './pages/shipment-print/shipment-print.component';
import { OrderBasicModule } from '../order/components/order-basic/order-basic.module';


const routes = [
  {
    path: "shipping",
    component: ShippingComponent,
    data: { permission: 'PERMISSION_MENU_SHIPMENT_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
    children: [
      {
        path: "",
        redirectTo: "pending",
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_SHIPMENT_VIEW' },
      },
      {
        path: "pending",
        component: ShippingPendingComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_SHIPMENT_VIEW' },
      },
      {
        path: "packing-in-progress",
        component: PackingProcessingComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_SHIPMENT_VIEW' },
      },
      {
        path: "shipped",
        component: ShippedComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_SHIPMENT_VIEW' },
      },
    ],
  },
  {
    path: 'invoice/:id/shipment',
    component: ShipmentDetailsComponent,
    data: { permission: 'PERMISSION_MENU_SHIPMENT_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  },
  {
    path: 'shipment/:id/shipment-print',
    component: ShipmentPrintComponent,
    canActivate: [LoginCheckRouteGuard],
  },

];

@NgModule({
  declarations: [
    ShippingComponent,
    ShippingPendingComponent,
    PackingProcessingComponent,
    ShippedComponent,
    ShipmentCreateModalComponent,
    ShipmentDetailsComponent,
    ShipmentPrintComponent,
  ],
  entryComponents: [
    ShipmentCreateModalComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    ButtonModule,
    DateTimeModule,
    GeneralModule,
    OrderBasicModule,
  ],
  exports: [
  ]
})

export class ShippingModule {
}

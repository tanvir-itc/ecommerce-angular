import { Component, Inject, ViewChild } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatStepper } from "@angular/material";
import { Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import { FactoryShipmentService } from "app/modules-core/common/factory-shipment/factory-shipment.service";
import { PdfModalComponent } from "app/modules-core/general/pdf-modal/pdf-modal.component";
import { OrderService } from "app/modules-core/merchandising/order/order.service";
import { toDateString } from "app/modules-core/utility/helpers";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { debug } from "console";
import * as _ from 'lodash';
import { debounce } from "lodash";
import { ShipmentService } from "../../shipment.service";

@Component({
  selector: 'shipment-create-modal',
  templateUrl: 'shipment-create-modal.component.html',
  styleUrls: ['shipment-create-modal.component.scss']
})
export class ShipmentCreateModalComponent {

  @ViewChild('stepper') private cartStepper: MatStepper;
  btnDisable = false;
  orderId = null;
  orderDetailsId = null;
  order: any = null;
  userRole: any = null;

  shipmentId: any = null;
  comment: any = null;
  shipment: any = null;

  editMode: any = false;
  displayAcceptBtn: any = false;

  modalRef: any = null;
  wearHouseBoxListView: any = false;
  wearHouseBoxList: any = [];
  cart: any = [];
  pickedSelectedBoxCartList: any = [];
  pickedSelectedBoxCartListForTamplate: any = [];
  selectedQuantity = 0;
  selectedLocation: any = {
    id: null,
  }
  selectedOrderedProductForPicking = {};

  shipmentProcessingUpdate: any = null;
  availableQty: any = 0;
  totalOrderedNumber: any = 0;
  disableAcceptBtn = true
  disablePickBtn = false
  errorMsgDisplay = false
  receivedData = []
  totalOrderedQty = 0;
  totalPickedQty = 0;
  totalShippedQty = 0;
  instantPicked = 0;
  maximumPickableQty: any = 0
  wasPickedFromCurrentLocation = false;
  wearHouseBoxListForView: any = [];
  cartForView: any = [];
  allBoxexInWearHouseOfAnItem: any = [];
  qtyInCart = 0;
  previousCartQuantity = 0;
  constructor(
    public dialogRef: MatDialogRef<ShipmentCreateModalComponent>,
    private _orderService: OrderService,
    private _shipmentService: ShipmentService,
    private _factoryShipmentService: FactoryShipmentService,
    private snackBar: SnackbarService,
    private _router: Router,
    public _auth: AuthService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.orderId = data.orderId;
    this.editMode = data.editMode;
    this.shipmentId = data.shipmentId;
    this.comment = data.comment;

  }

  ngOnInit() {
    this.clearShipmentCreateObjcet();
    this.getOrder();

    this.userRole = _.chain(this._auth.getCurrentUser()).get('roles').head().get('name').value();

  }

  onLocationChange() {
    // this.clearShipmentCreateObjcet();
    this.emptyCarts();
    _.each(this.shipment.order.groupBreakdown, group => {
      if (group.styleBreakdown.length) {
        _.each(group.styleBreakdown, style => {

          _.each(style.breakdown, (innerValue, innerKey) => {
            if (_.find(innerValue.details[0].locations, ['id', this.selectedLocation.id])) {
              innerValue['fromSelectedLocation'] = true
            } else {
              innerValue['fromSelectedLocation'] = false;
            }

          });
        });
      } else if (!group.styleBreakdown.length && this.selectedLocation.id) {
        this.snackBar.open(`There is no Product in selected loaction!`, "Close");
      }
    });



  }
  getOrder() {
    this._orderService.getOrder({ orderid: this.orderId }).subscribe((data) => {
      // console.log(data)
      this.order = data;
      this.shipment.order = JSON.parse(
        JSON.stringify(this.order)
      );
      this.getTotals();
      if (!this.selectedLocation.id) {
        _.each(this.shipment.order.groupBreakdown, group => {
          _.each(group.styleBreakdown, style => {
            _.each(style.breakdown, (innerValue, innerKey) => {
              innerValue['pickedQty'] = 0
            });
          });
        });
      } else if (this.selectedLocation.id) {
        _.each(this.shipment.order.groupBreakdown, group => {
          _.each(group.styleBreakdown, style => {
            _.each(style.breakdown, (innerValue, innerKey) => {
              let pickedObj = _.find(this.receivedData, {
                comboId: innerValue.comboId,
                sizeId: innerValue.sizeId,
                styleId: innerValue.styleId,
              });
              innerValue['pickedQty'] = pickedObj ? pickedObj.pickedQty : 0;
              // innerValue['fromSelectedLocation'] = style.fromSelectedLocation;
            });
          });


        });
      }

      if (this.editMode == 'packing') {
      } else if (this.editMode == 'create') {
        if (_.chain(this.order).get('locations').size().value() == 1) {
          this.selectedLocation.id = _.chain(this.order).get('locations').head().get('id').value();
        }
      } else if (this.editMode == 'edit') {
        this.getShipment();
      }

      this.clearShipmentProcessingUpdateObject();

    });
    // totalnumbers

  }



  getShipment() {
    this._shipmentService.getGroupBreakdown({ shipmentid: this.shipmentId }).subscribe(resp => {
      // this.selectedLocation.id = _.get(resp, 'inventoryLocation.id');
      let shipment = resp;
      console.log('shipmentDetail', shipment)
      this.shipment.data.comment = _.get(resp, 'comment', null);
      _.each(this.shipment.order.groupBreakdown, group => {
        _.each(group.styleBreakdown, style => {
          _.each(style.breakdown, qtyBreakdown => {

            let privShipmentQty = _.chain(shipment.shipmentDetail)
              .find(['sizeGroupId', _.get(style, 'sizeGroupId')])
              .get('detail')
              .find(['id', _.get(qtyBreakdown, 'styleId')])
              .get('detail')
              .filter(['combo', _.get(qtyBreakdown, 'combo')])
              .find(['sizeId', _.get(qtyBreakdown, 'sizeId')])
              .get('shipQty')
              .value();

            if (privShipmentQty) {
              let detailList = _.flatMap(_.flatMap(shipment.shipmentDetail, 'detail'), 'detail');
              _.each(detailList, detail => {
                _.each(detail.boxes, box => {
                  box['orderDetailId'] = detail.orderDetailId;
                })
                return detail;
              })
              // qtyBreakdown['picked'] = true;
              qtyBreakdown['newShipmentQuantity'] = _.add(0, privShipmentQty);
              qtyBreakdown['pickedQty'] = _.add(0, privShipmentQty);
              qtyBreakdown['balance'] = _.add(_.get(qtyBreakdown, 'balance', 0), privShipmentQty);
              qtyBreakdown['balanceOrderQty'] = (qtyBreakdown.orderedQty - qtyBreakdown.shipQty) || 0;
              qtyBreakdown['boxList'] = _.flatMap(detailList, 'boxes')
              qtyBreakdown['boxListForPayload'] = _.flatMap(detailList)

              this.disableAcceptBtn = true;
              this.selectedOrderedProductForPicking = _.merge({}, qtyBreakdown)
              this.getTotals()
              console.log('this.shipment', this.shipment)
            }
          });
        });
      });

    });

  }

  clearShipmentCreateObjcet() {
    this.shipment = {
      data: {
        orderId: this.orderId,
        comment: null,
        packingProcessing: true,
        shipmentDetails: [],
      },
      order: [],
    };
  }

  clearShipmentProcessingUpdateObject() {
    this.shipmentProcessingUpdate = {
      id: this.shipmentId,
      shipmentNo: null,
      shipmentDate: toDateString(new Date()),
      shipmentMode: _.get(this.order, 'shippingMode', null),
      courierCompany: _.get(this.order, 'courierCompany', null),
      courierAccountNo: _.get(this.order, 'courierAccountNo', null),
      shipmentModeInstruction: _.get(this.order, 'shipmentModeInstruction', null),
      trackingNumber: null,
      noOfBox: null,
      totalWeight: null,
      packingProcessing: false,
      comment: this.comment || null,
    }
  }

  createShipment(flag) {
    this.btnDisable = true;
    let error = [];

    var payload = _.merge({}, this.shipment.data);
    payload['inventoryLocationId'] = this.selectedLocation.id;
    payload.shipmentDetails = [];
    // let boxList = [];
    console.log('this.shipment.order.groupBreakdown', this.shipment.order.groupBreakdown)
    _.each(this.shipment.order.groupBreakdown, group => {
      _.each(group.styleBreakdown, (style) => {
        for (var property in style.breakdown) {
          if (style.breakdown[property].pickedQty) {
            let bq = null;
            let nsq = null;
            let wq = null;
            bq = style.breakdown[property].balance;
            nsq = parseInt(style.breakdown[property].pickedQty)
            wq = _.chain(style.breakdown[property].inventoryLocations).get('_' + this.selectedLocation.id).get('quantity').value();
            if (nsq > bq) {
              error.push(`${style.styleName}-${style.breakdown[property].size},${style.breakdown[property].combo} can not be more than order quantity!`);
            }
            if (nsq > wq) {
              error.push(`${style.styleName}-${style.breakdown[property].size},${style.breakdown[property].combo} can not be more than warehouse inventory balance quantity!`);
            }
            console.log('style.breakdown', style.breakdown)

            let boxList = []
            if (style.breakdown[property].boxList) {
              boxList = _.map(style.breakdown[property].boxList, br => {
                return { 'orderDetailId': _.get(br, 'orderDetailId'), "quantity": _.get(br, 'details') ? br.details[0].selectedQty : br.quantity, "factoryShipmentPackingDetailId": _.get(br, 'details') ? br.details[0].id : br.factoryShipmentPackingDetailId }
              })
            } else {
              boxList = _.get(_.find(style.breakdown[property].boxListForPayload, ['orderDetailId', _.chain(style.breakdown).get(property).get('details').head().get('id').value()]), 'boxes')
            }
            let isOrderDetailIdExist = _.get(_.head(boxList), 'orderDetailId')
            payload.shipmentDetails.push({
              inventoryLocationId: this.selectedLocation.id,
              orderDetailId: _.chain(style.breakdown).get(property).get('details').head().get('id').value(),
              shipmentDetailBoxes: isOrderDetailIdExist ? _.filter(boxList, ['orderDetailId', _.chain(style.breakdown).get(property).get('details').head().get('id').value()]) : boxList,
              quantity: nsq,
            });
          }
        }
      });
    });
    // return
    console.log('payload', payload)
    if (!payload.shipmentDetails.length) {
      this.snackBar.open(`Please! add minimum quantity!`, "Close");
      this.btnDisable = false;
      return;
    }
    // return
    if (error.length) {
      this.snackBar.open(_.join(error, ', '), "Close", true);
      this.btnDisable = false;
      return;
    }

    payload['packingProcessing'] = true;
    if (this.shipmentId) {
      payload['id'] = this.shipmentId
      this._shipmentService.updateFields(payload).subscribe((resp) => {
        this.clearShipmentProcessingUpdateObject();
        this.shipmentId = _.get(resp, 'id');
        this.shipmentProcessingUpdate['id'] = _.get(resp, 'id');
        this.shipmentProcessingUpdate['shipmentNo'] = _.get(resp, 'id');
        this.shipmentProcessingUpdate['comment'] = _.get(resp, 'comment');
        this.editMode = 'packing';

        this.btnDisable = false;
      }, err => {
        this.btnDisable = false;
      });
    } else {
      this._shipmentService.create(payload).subscribe((resp) => {
        this.clearShipmentProcessingUpdateObject();
        this.shipmentId = _.get(resp, 'id');
        this.shipmentProcessingUpdate['id'] = _.get(resp, 'id');
        this.shipmentProcessingUpdate['shipmentNo'] = _.get(resp, 'id');
        this.shipmentProcessingUpdate['comment'] = _.get(resp, 'comment');
        this.editMode = 'packing';
        this.snackBar.open(`Packing Process Start!`, "Close");
        this.btnDisable = false;
      }, err => {
        this.btnDisable = false;
      });
    }

  }

  shipmentPrefillByMaxQuantity() {
    _.each(this.shipment.order.groupBreakdown, group => {
      _.each(group.styleBreakdown, style => {
        _.each(style.breakdown, inner => {
          let warehouseQty = _.chain(inner.inventoryLocations).get(`_${this.selectedLocation.id}`).get('quantity').value();
          if (_.get(inner, 'balance') > 0 && warehouseQty) {
            inner['newShipmentQuantity'] = inner.balance < warehouseQty ? inner.balance : warehouseQty;
          } else {
            inner['newShipmentQuantity'] = null;
          }
        });
      });
    });
  }

  updateShipment() {
    this.btnDisable = true;
    if (!this.shipmentProcessingUpdate.id) {
      this.snackBar.open(`Please select a shipment id!`, "Close");
      this.btnDisable = false;
      return;
    }
    if (!this.shipmentProcessingUpdate.shipmentDate) {
      this.snackBar.open(`Please select a shipment date!`, "Close");
      this.btnDisable = false;
      return;
    }
    if (!this.shipmentProcessingUpdate.shipmentMode) {
      this.snackBar.open(`Please select a shipment mode!`, "Close");
      this.btnDisable = false;
      return;
    }

    if (!this.shipmentProcessingUpdate.courierCompany) {
      this.snackBar.open(`Please enter courier company!`, "Close");
      this.btnDisable = false;
      return;
    }

    if (this.shipmentProcessingUpdate.shipmentMode == 'CUSTOMER' && !this.shipmentProcessingUpdate.courierAccountNo) {
      this.snackBar.open(`Please enter courier account no!`, "Close");
      this.btnDisable = false;
      return;
    }

    if (!this.shipmentProcessingUpdate.trackingNumber) {
      this.snackBar.open(`Please enter tracking number!`, "Close");
      this.btnDisable = false;
      return;
    }
    if (!this.shipmentProcessingUpdate.noOfBox) {
      this.snackBar.open(`Please enter number of boxes!`, "Close");
      this.btnDisable = false;
      return;
    }

    if (this.shipmentProcessingUpdate.shipmentMode != 'CUSTOMER') {
      this.shipmentProcessingUpdate.courierAccountNo = null;
    }

    this._shipmentService.updateFields(this.shipmentProcessingUpdate).subscribe((resp) => {
      this.snackBar.open(`Shipped!`, "Close");
      this.dialogRef.close();
      this.btnDisable = false;
    }, err => {
      this.btnDisable = false;
    });
  }

  goEdit() {
    this.editMode = 'edit';
    this.getOrder();
  }

  goLink(id) {
    this._router.navigateByUrl(`orders/${id}`);
  }

  openPackingPDFModal(shipmentId) {
    this._shipmentService.getPackingPDF(shipmentId).subscribe(resp => {
      this.modalRef = this.dialog.open(PdfModalComponent, {
        // minWidth: "700px",
        width: "90%",
        // disableClose: true,
        data: {
          title: "Packing",
          resource: _.get(resp, 'path'),
          emailButton: true,
          emailObject: {
            type: 'packing',
            shipmentId: shipmentId,
          }
        }
      });
    })
  }

  // PICKING=====================// PICKING=====================// PICKING=====================

  // ******* qtyInCart use na kore , 'pick button click korar por initial cart er quantity koto silo seta diye maximum picking qty value handle korte hobe'

  onSelectOrderToPick(item, style) {
    this.emptyCarts();
    // this.qtyInCart = _.get(item, 'pickedQty')
    // this.orderDetailsId = item.details[0].id
    this.wearHouseBoxListView = true;
    this.selectedOrderedProductForPicking = _.merge({}, item);
    console.log('this.selectedOrderedProductForPicking', this.selectedOrderedProductForPicking)
    _.each(this.shipment.order.groupBreakdown, group => {
      _.each(group.styleBreakdown, style => {
        _.each(style.breakdown, (innerValue, innerKey) => {
          if ('_' + item.comboId + '_' + item.sizeId + '_' + item.combo == innerKey) {
            innerValue['picked'] = true;
            this.getWearHouseBoxList(item.sizeId, item.poId, item.comboId, item)
          } else {
            innerValue['picked'] = false;
          }
        });
      });
    });

  }



  getWearHouseBoxList(sizeid, poId, colorid, item) {

    this._factoryShipmentService.getWearHouseBoxList({ 'sizeid': sizeid, 'poid': poId, 'colorid': colorid, 'shipba': true, 'shipmentid': this.shipmentId }).subscribe((data) => {
      _.each(data, d => {
        d.details[0].actualQty = Math.max(0, _.get(d.details[0], 'quantity'))
      })
      this.allBoxexInWearHouseOfAnItem = data;
      this.wearHouseBoxList = this.selectedLocation.id ? _.filter(data, ['locationId', this.selectedLocation.id]) : data;
      this.chunkWearHouseBoxList();
      //total product qty by all boxes for selected order
      this.availableQty = _.sumBy(_.flatMap(this.wearHouseBoxList, 'details'), 'actualQty')

      if (_.get(item, 'boxList') || _.get(item, 'actualShipQty')) {
        _.each(this.wearHouseBoxList, wearHousBox => {

          wearHousBox.details[0].actualQty = _.get(wearHousBox.details[0], 'quantity')
          var existVlueOnCreateMode = _.find(_.get(item, 'boxList'), ['id', wearHousBox.id])
          var existVlueOnEditMode = _.find(_.get(item, 'boxList'), ['factoryShipmentPackingDetailId', wearHousBox.details[0].id])
          // var existVlueOnCreateMode = _.find(_.flatMap(_.get(item, 'boxList'), 'rows'), ['id', row.id])
          if (existVlueOnEditMode || existVlueOnCreateMode) {
            wearHousBox.selected = true;
            if (existVlueOnEditMode) { // in edit mode
              wearHousBox.details[0].actualQty = Math.max(0, _.subtract(_.get(wearHousBox.details[0], 'actualQty'), _.get(existVlueOnEditMode, 'quantity')));
              wearHousBox.details[0].selectedQty = _.get(existVlueOnEditMode, 'quantity', 0);
              this.cart.push(wearHousBox);
              this.chunkCartList();
            } else if (existVlueOnCreateMode) { // new create mode
              wearHousBox.details[0].actualQty = Math.max(0, _.get(existVlueOnCreateMode.details[0], 'selectedQty') ? _.subtract(_.get(wearHousBox.details[0], 'actualQty'), _.get(existVlueOnCreateMode.details[0], 'selectedQty')) : _.get(wearHousBox.details[0], 'actualQty'))
              this.cart.push(existVlueOnCreateMode);
              this.chunkCartList();
            }
          }
          // })
        })
        this.previousCartQuantity = _.sumBy(this.cart, 'details[0].selectedQty')

      }

    });
  }

  // click the wear house boxes =============
  onWearhouseBoxSelect(box, boxIndex, rowIndex) {
    if (!this.selectedLocation.id) {
      this.snackBar.open(`Select Location FIrst !`, "Close");
      return;
    }
    // WEARHOUSE BOX LIST LOOPING!!!!!!!!!!!!!!!!!!!!!
    _.each(this.wearHouseBoxList, wh => {
      if (_.get(box, 'id') == _.get(wh, 'id')) {
        wh['selected'] = true
      }
    })

    let balanceQty = _.chain(_.get(this.selectedOrderedProductForPicking, 'quantity'))
      .subtract(_.add(_.get(this.selectedOrderedProductForPicking, 'pickedQty'), _.get(this.selectedOrderedProductForPicking, 'actualShipQty')))
      .value();
    if (!_.find(this.cart, ['id', box.id])) {
      box.details[0]['selectedQty'] = balanceQty < _.get(box.details[0], 'actualQty') ? balanceQty : _.get(box.details[0], 'actualQty');// if selected box qty is bigger than balance qty then prefill with bal otherwise box qty 
      this.cart.push(box)
      // _.each(this.cart, item => {
      //   item.details[0].selectedQty = _.parseInt(item.details[0].selectedQty || 0)
      // })
      this.cartSelectedQtyParseInt();
      this.chunkCartList()
    } else {
      return
    }
    //
    console.log('this.cart', this.cart)
    this.qtyInCart = _.sumBy(this.cart, 'details[0].selectedQty');
    this.pickingValidation(); // Validation !!!!!!!!!!!!

  }

  onQtyChange(data, qty) {
    let quantity = _.parseInt(qty || 0);
    let wearHouseBoxQty = this.editMode == 'edit' ? data.details[0].quantity : data.details[0].actualQty;

    if (quantity > wearHouseBoxQty) {
      this.wearHouseBoxListView = true
      this.snackBar.open(`Picked quantity must not be greater than box quantity!`, "Close");
      this.disableAcceptBtn = true
      return;
    } else {
      this.disableAcceptBtn = false;
    }
    //input quantity values getting stringify. But it must be integer 
    this.cartSelectedQtyParseInt();
    this.qtyInCart = _.sumBy(this.cart, 'details[0].selectedQty');

    if (this.qtyInCart > _.get(this.selectedOrderedProductForPicking, 'quantity') || this.pickingValidation()) {
      this.snackBar.open(`You cant pick more than ordered quantity or balance  quantity!`, "Close");
      this.disableAcceptBtn = true
      return;
    } else {
      this.disableAcceptBtn = false
    }

  }

  onAccept() {
    _.each(this.shipment.order.groupBreakdown, group => {
      _.each(group.styleBreakdown, style => {
        _.each(style.breakdown, (innerValue, innerKey) => {
          if (innerValue.picked) {
            this.receivedData.push(innerValue)

            if (innerValue.boxList) {
              if (this.editMode == 'edit') {
                let mappedCart = _.map(this.cart, cartItem => {
                  return {
                    'id': _.get(cartItem, 'id'),
                    'quantity': _.get(cartItem, 'details[0].selectedQty'),
                    'factoryShipmentPackingDetailId': _.get(cartItem, 'details[0].id'),
                  }
                })
                //here box list consist of the all boxes of all order so need to filtered with cart boxes only

                innerValue.boxList = _.unionBy(mappedCart, innerValue.boxList, 'factoryShipmentPackingDetailId')
                var filteredBoxListWithCart = [];
                _.each(innerValue.boxList, box => {
                  let isExistInCart = _.find(this.allBoxexInWearHouseOfAnItem, ['details[0].id', _.get(box, 'factoryShipmentPackingDetailId')]);
                  if (isExistInCart) {
                    filteredBoxListWithCart.push(box);
                  }
                })
                innerValue.boxList = _.filter(innerValue.boxList, 'quantity')
                innerValue.pickedQty = _.sumBy(filteredBoxListWithCart, 'quantity');
              } else {
                let filteredBoxList = [];
                innerValue.boxList = _.unionBy(this.cart, innerValue.boxList, 'id');
                filteredBoxList = _.filter(innerValue.boxList, 'details[0].selectedQty')
                // filtered for empty boxes. 0 or empty input
                innerValue.pickedQty = _.sumBy(filteredBoxList, 'details[0].selectedQty');
              }


            } else {
              innerValue.boxList = this.cart;
              innerValue.boxList = _.filter(innerValue.boxList, 'details[0].selectedQty')
              innerValue.pickedQty = _.sumBy(innerValue.boxList, 'details[0].selectedQty');
            }
            //needed to parse int quantity, because from the inputs we are getting string 
            if (innerValue.boxList) {
              _.each(innerValue.boxList, box => {
                box.details[0].selectedQty = _.parseInt(box.details[0].selectedQty || 0)
              })
            }
          }
        });
      });
    })
    console.log('this.shipment.order', this.shipment.order)
    this.disablePickBtn = false;
    this.getTotals();

  }

  removeselectedBox(boxIndex, rowIndex, item) {
    this.disableAcceptBtn = false;
    _.remove(this.cart, ['id', item.id]);
    this.chunkCartList();
    this.pickingValidation();
    _.each(this.shipment.order.groupBreakdown, group => {
      _.each(group.styleBreakdown, style => {
        _.each(style.breakdown, (innerValue, innerKey) => {
          if (innerValue.picked) {
            if (innerValue.boxList) {
              _.remove(innerValue.boxList, ['id', item.id]);
            }
            // innerValue.pickedQty = _.sumBy(innerValue.boxList, 'details[0].selectedQty')

          }
        });
      });
    })

  }


  // AUTO CALCULATION FUNCTIONS  // AUTO CALCULATION FUNCTIONS  


  chunkWearHouseBoxList() {
    // in view needed to show 5 items in a table . so we chunked the list 
    this.wearHouseBoxListForView = _.chain(this.wearHouseBoxList)
      .sortBy('details[0].shipmentName')
      .chunk(5)
      .map(obj => { return { 'rows': obj } })
      .value();

    return this.wearHouseBoxListForView;
  }

  chunkCartList() {
    // in view needed to show 5 items in a table . so we chunked the list 
    if (!this.cart.length) {
      return;
    }
    this.cartForView = _.chain(this.cart)
      // .filter('details[0].selectedQty')
      .chunk(5)
      .map(obj => { return { 'rows': obj } })
      .value();
    console.log('this.cartForView', this.cartForView)
    return this.cartForView;
  }

  cartSelectedQtyParseInt() {
    _.each(this.cart, item => {
      item.details[0].selectedQty = _.parseInt(item.details[0].selectedQty || 0)
    })
  }

  pickingValidation() {
    let balanceQty = 0
    let shippedAndPickedQty = _.add(_.get(this.selectedOrderedProductForPicking, 'pickedQty'),
      _.get(this.selectedOrderedProductForPicking, 'actualShipQty'))

    balanceQty = _.chain(this.previousCartQuantity)
      .add(_.subtract(_.get(this.selectedOrderedProductForPicking, 'quantity'), shippedAndPickedQty))
      .value();
    this.maximumPickableQty = balanceQty;
    let errorFlag = false;
    if (this.qtyInCart > balanceQty) {
      errorFlag = true;
    } else {
      errorFlag = false
    }
    return errorFlag;
  }
  locationClear() {
    _.each(this.shipment.order.groupBreakdown, group => {
      if (group.styleBreakdown.length) {
        _.each(group.styleBreakdown, style => {
          _.each(style.breakdown, (innerValue, innerKey) => {
            innerValue['fromSelectedLocation'] = true;
          });
        });
      } else if (!group.styleBreakdown.length && this.selectedLocation.id) {
        this.snackBar.open(`There is no Product in selected loaction!`, "Close");
      }
    });
  }




  emptyCarts() {
    this.wearHouseBoxList = []
    this.wearHouseBoxListForView = []
    // this.pickedSelectedBoxCartList = []
    this.cart = []
    this.cartForView = []
    this.selectedOrderedProductForPicking = {}
    this.qtyInCart = 0
    this.errorMsgDisplay = false
    this.disableAcceptBtn = false

  }

  getTotals() {
    let styleList = _.chain(this.shipment.order)
      .get('groupBreakdown')
      .flatMap('styleBreakdown')
      .value();
    let comboList = _.flatMap(styleList, 'colorList')
    let breakdownList = _.flatMap(styleList, 'breakdown')
    let pickedList = []
    _.each(breakdownList, value => {
      _.each(value, (v, k) => {
        pickedList.push(v)
      })
    })
    //  comboActualShipTotal
    //  .sumBy('comboQuantityTotal')
    this.instantPicked = _.sumBy(pickedList, 'pickedQty')
    this.totalOrderedQty = _.sumBy(comboList, 'comboQuantityTotal');
    this.totalPickedQty = _.sumBy(comboList, 'comboPickedQuantityTotal')
    this.totalShippedQty = _.sumBy(comboList, 'comboActualShipTotal')
  }

}
//newShipmentQuantity
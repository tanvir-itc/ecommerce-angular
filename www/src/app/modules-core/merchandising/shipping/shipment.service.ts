import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from "rxjs/internal/operators/map";
import * as _ from "lodash";
import { makeParams } from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { SERVER_API_URL } from "app/app.constants";

@Injectable({
  providedIn: "root",
})
export class ShipmentService {
  urlBase = "shipments";
  urlReturn = "shipment-returns";

  httpOptions = {
    params: new HttpParams(),
  };
  constructor(private http: HttpClient) { }

  getAll(opts = {}) {
    return this.http
      .get(`${this.urlBase}/v2`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = JSON.parse(resp.data);
            resp.date = _.map(resp.data, (shipment) => {
              shipment.shipmentDetail = _.map(
                shipment.shipmentDetail,
                (style) => {
                  style["totalQuantity"] = _.sumBy(style.detail, "quatity");
                  style['price'] = _.chain(style)
                    .get("detail")
                    .head()
                    .get("price")
                    .value();
                  style["totalValue"] = _.multiply(style.price, style['totalQuantity']);
                  return style;
                }
              );
              shipment["totalQuantity"] = _.sumBy(
                shipment.shipmentDetail,
                "totalQuantity"
              );
              shipment["totalValue"] = _.sumBy(
                shipment.shipmentDetail,
                "totalValue"
              );
              shipment['netTotal'] = _.get(shipment, 'otherCharge', 0) + _.get(shipment, 'tax', 0) + _.get(shipment, 'freightCharge', 0) + _.get(shipment, 'handlingCharge', 0) + _.get(shipment, 'totalValue', 0) - _.get(shipment, 'discount', 0);
              shipment['dueAmount'] = _.subtract(_.get(shipment, 'netTotal', 0), _.get(shipment, 'totalPayment', 0));
              shipment['badDebt'] = _.subtract(_.get(shipment, 'otherCharge', 0) + _.get(shipment, 'tax', 0) + _.get(shipment, 'freightCharge', 0) + _.get(shipment, 'handlingCharge', 0) + _.get(shipment, 'totalValue', 0) - _.get(shipment, 'discount', 0), _.get(shipment, 'totalPayment', 0));
              return shipment;
            });

            return resp.data;
          }
          return [];
        })
      );
  }

  get(id) {
    return this.http.get(`${this.urlBase}/${id}/v2`).pipe(
      map((resp: any) => {
        if (resp.data) {
          resp.data = JSON.parse(resp.data);
          return _.head(resp.data);
        }
        return resp.data;
      })
    );
  }

  getInvoicePDF(id) {
    return this.http.get(`${this.urlBase}/${id}/invoice`).pipe(
      map((resp: any) => {
        resp.data.path = `${SERVER_API_URL}${resp.data.path}`
        return resp.data;
      })
    );
  }
  getInvoiceEmailBody(id) {
    return this.http.post(`${this.urlBase}/${id}/invoice-email-content`, {}).pipe(
      map((resp: any) => {
        return resp.data;
      })
    );
  }
  getPackingPDF(id) {
    return this.http.get(`${this.urlBase}/${id}/packing`).pipe(
      map((resp: any) => {
        resp.data.path = `${SERVER_API_URL}${resp.data.path}`
        return resp.data;
      })
    );
  }
  getPackingBoxPDF(id) {
    return this.http.get(`${this.urlBase}/${id}/packing-box`).pipe(
      map((resp: any) => {
        resp.data.path = `${SERVER_API_URL}${resp.data.path}`
        return resp.data;
      })
    );
  }
  getPackingEmailBody(id) {
    return this.http.post(`${this.urlBase}/${id}/packing-email-content`, {}).pipe(
      map((resp: any) => {
        return resp.data;
      })
    );
  }
  getBreakdown(id) {
    return this.http.get(`${this.urlBase}/${id}/v2`).pipe(
      map((resp: any) => {
        if (resp.data) {
          resp.data = JSON.parse(resp.data);
          var details = _.head(resp.data);
          details['invoiceRevision'] = _.maxBy(details['invoiceRevisions'], 'id');

          var styleBreakdown = [];
          _.each(_.get(details, "shipmentDetail"), (styleItem) => {
            let style = _.chain(styleItem)
              .get("detail")
              .head().value();

            styleItem["styleImage"] = _.get(style, "styleImage");
            styleItem["price"] = _.get(style, "price");
            styleItem["styleNote"] = _.get(style, "styleNote");

            var uniqueCombos = [];
            uniqueCombos = _.chain(styleItem)
              .get("detail")
              .uniqBy("comboId")
              .value();
            uniqueCombos = _.map(uniqueCombos, (combo) => {
              return {
                comboId: combo.comboId,
                comboName: combo.combo,
                comboNameForDisplay: combo.combo.substring(0, combo.combo.indexOf('Available')) || combo.combo.substring(0, combo.combo.indexOf('In Stock')),

              };
            });
            styleItem["combos"] = _.sortBy(uniqueCombos, "comboId");
            styleItem["breakdown"] = {};
            _.each(styleItem.detail, (item) => {
              var key = null;
              key = "_" + item.comboId + "_" + item.sizeId;
              styleItem["breakdown"][key] = item;
            });
            styleItem["totalQty"] = _.sumBy(styleItem.detail, "quatity");
            styleItem["totalPrice"] = _.multiply(styleItem["price"], styleItem["totalQty"]);
            styleBreakdown.push(styleItem);
          });
          details['totalQty'] = _.sumBy(styleBreakdown, 'totalQty');
          details['totalPrice'] = _.sumBy(styleBreakdown, 'totalPrice');
          details['netTotal'] = _.get(details, 'otherCharge', 0) + _.get(details, 'tax', 0) + _.get(details, 'freightCharge', 0) + _.get(details, 'handlingCharge', 0) + _.get(details, 'totalPrice', 0) - _.get(details, 'discount', 0);

          details['dueAmount'] = _.subtract(_.get(details, 'netTotal', 0), _.get(details, 'totalPayment', 0));
          return {
            shipment: details,
            breakdown: styleBreakdown,
          };
        }
        return [];
      })
    );
  }

  getGroupBreakdown(opts = {}) {
    return this.http
      .get(`${this.urlBase}/v3`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          resp.data = JSON.parse(resp.data);
          resp.data = _.head(resp.data);
          let groupItems = [];
          let breakdown = _.get(resp.data, 'shipmentDetail')
          _.each(breakdown, sizeGroup => {

            let styleGroup = {
              sizeList: _.get(sizeGroup, 'sizeList'),
              styleBreakdown: [],
            }

            _.each(sizeGroup.detail, style => {
              let uniqueCombos = [];
              uniqueCombos = _.chain(style)
                .get("detail")
                .uniqBy("comboId")
                .value();
              uniqueCombos = _.map(uniqueCombos, (combo) => {
                return {
                  comboId: combo.comboId,
                  comboName: combo.combo,
                  comboNameForDisplay: combo.combo.substring(0, combo.combo.indexOf('Available')) || combo.combo.substring(0, combo.combo.indexOf('In Stock')),

                };
              });

              let item = {
                breakdown: {},
                styleName: style.name,
                styleId: style.styleId,
                colorList: _.merge([], uniqueCombos),
                styleShippedQty: _.sumBy(style.detail, 'quatity'),
              }

              _.each(style.detail, quantityBreakdown => {
                let key = "_" + quantityBreakdown.comboId + "_" + quantityBreakdown.sizeId;
                quantityBreakdown['shipQty'] = _.get(quantityBreakdown, 'quatity');
                item.breakdown[key] = quantityBreakdown;
              });
              item['sizeGroupId'] = _.get(sizeGroup, 'sizeGroupId');
              styleGroup.styleBreakdown.push(item);
            });
            styleGroup['groupShippedQty'] = _.sumBy(styleGroup.styleBreakdown, 'styleShippedQty');

            groupItems.push(styleGroup)

          });

          resp.data['groupBreakdown'] = groupItems;
          resp.data['totalShippedQty'] = _.sumBy(resp.data['groupBreakdown'], 'groupShippedQty');
          return resp.data;
        })
      );
  }

  create(payload) {
    return this.http
      .post(this.urlBase, payload)
      .pipe(map((resp: any) => resp.data));
  }

  update(id, payload) {
    return this.http
      .put(`${this.urlBase} /${id}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFields(payload) {
    return this.http
      .patch(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  delete(id) {
    return this.http
      .delete(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  addStyle(id, payload) {
    return this.http
      .post(`${this.urlBase}/${id}/details`, payload)
      .pipe(map((resp: any) => resp.data));
  }


  getShipmentReturns(opts = {}) {
    return this.http
      .get(`${this.urlReturn}`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          resp.data = _.map(resp.data, parent => {
            parent['returnQuantity'] = _.sumBy(parent.shipmentReturnDetails, 'quantity');
            parent['returnValue'] = _.sumBy(parent.shipmentReturnDetails, 'value');
            return parent;
          });
          return resp.data;
        })
      );
  }

  createShipmentReturn(payload) {
    return this.http
      .post(this.urlReturn, payload)
      .pipe(map((resp: any) => resp.data));
  }

  acceptShipment(payload, id) {
    return this.http
      .patch(`${this.urlBase}/${id}/details`, payload)
      .pipe(map((resp: any) => resp.data));
  }
  sendToUnPaid(payload) {
    return this.http
      .patch(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }
}

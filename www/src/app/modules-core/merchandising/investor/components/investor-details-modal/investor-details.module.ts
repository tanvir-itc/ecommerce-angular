import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { RouterModule } from '@angular/router';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { NgxSummernoteModule } from 'ngx-summernote';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { InvestorDetailsModalComponent } from './investor-details-modal.component';
import { InvestorCommissionComponent } from './investor-commission/investor-commission.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { InvestorBasicComponent } from './investor-basic/investor-basic.component';

@NgModule({
  declarations: [
    InvestorDetailsModalComponent,
    InvestorBasicComponent,
    InvestorCommissionComponent,
  ],
  entryComponents: [
    InvestorDetailsModalComponent,
    InvestorBasicComponent,
    InvestorCommissionComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FuseSharedModule,
    RouterModule,
    ButtonModule,
    DateTimeModule,
    GeneralModule,
    NgxSummernoteModule,
    AttachmentModule,
    NgSelectModule,
  ],
  exports: [
    InvestorDetailsModalComponent
  ]
})

export class InvestorDetailsModule {
}
import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import * as _ from 'lodash';
import { AuthService } from "app/authentication/auth.service";


@Component({
  selector: 'investor-details-modal',
  templateUrl: 'investor-details-modal.component.html',
})
export class InvestorDetailsModalComponent {
  investorId = null;
  constructor(
    public dialogRef: MatDialogRef<InvestorDetailsModalComponent>,
    public _auth: AuthService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.investorId = data.investorId;
  }

}
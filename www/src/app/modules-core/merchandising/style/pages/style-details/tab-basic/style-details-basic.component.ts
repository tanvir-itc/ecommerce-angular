import { Component, OnInit, Input } from "@angular/core";
import * as _ from "lodash";
import { fuseAnimations } from "@fuse/animations";
import { AttachmentService } from "app/modules-core/common/attachment/attachment.service";
import { AuthService } from "app/authentication/auth.service";
import { MatDialog } from "@angular/material";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { SizeService } from "app/modules-core/common/size/size.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { StyleService } from "../../../style.service";
import { PriceTypeService } from "app/modules-core/common/price-type/price-type.service";
import { StylePriceService } from "app/modules-core/common/price-type/style-price.service";
import { StyleUpdateModalComponent } from "../../../components/style-update-modal/style-update-modal.component";

@Component({
  selector: "style-details-basic",
  templateUrl: "./style-details-basic.component.html",
  animations: fuseAnimations,
})
export class StyleDetailsBasicComponent implements OnInit {
  id;

  style: any = null;

  styleColorList: any[] = [];
  styleFeatureList: any[] = [];

  sizeList: any[] = [];

  images: any[] = [];
  bannerImage: any = null;

  modalRef;

  newColor = {};
  updateColor: any = {
    id: null,
    name: null,
    colorCode: null,
    primary: null,
  };

  newFeature = {};
  updateFeature: any = {
    id: null,
    reason: null,
    discountPercentage: null,
    fromDate: null,
    toDate: null
  };


  editStyle: any = null;


  sizes: any[] = [];

  colorId: any = null;

  priceTypeList: any[] = [];

  constructor(
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    public _auth: AuthService,
    private _styleService: StyleService,
    private _sizeService: SizeService,
    private _attachmentService: AttachmentService,
    private _priceTypeService: PriceTypeService,
    private _stylePriceService: StylePriceService,
    private _router: Router
  ) {
    this._quickPanelSubjectService.heading.next("Style Editor");
    this.id = +_route.parent.snapshot.paramMap.get("id");
  }

  ngOnInit() {
    this.getStyle();
    this.getStyleColors();
    this.getStyleFeatures();
    this.getAttachmentImages();
  }

  getStyle() {
    this._styleService.get(this.id).subscribe((style) => {
      this.style = style;
    });
  }
  getStyleColors() {
    this._styleService.getColors({ styleid: this.id }).subscribe((data) => {
      this.styleColorList = data;
      this.colorId = _.chain(this.styleColorList).find(['primary', true]).get('id').value();
    });
  }

  getStyleFeatures() {
    this._styleService.getFeatures({ styleid: this.id }).subscribe((data) => {
      this.styleFeatureList = data;
    });
  }

  getAttachmentImages() {
    this._attachmentService
      .getAll({ entitytype: "style_image", entityid: this.id })
      .subscribe((data) => {
        this.images = data;
        this.bannerImage = null;
        this.bannerImage = _.chain(this.images)
          .head()
          .get("fullpath", this.bannerImage)
          .value();
      });
  }

  openColorModal(templateRef) {
    this.clearColorObject();
    this.clearColorUpdate();
    this.modalRef = this.dialog.open(templateRef, {
      width: "600px",
      disableClose: true,
    });
    this.modalRef.afterClosed().subscribe((resp) => {
      this.getStyleColors();
    });
  }

  openFeatureModal(templateRef) {
    this.clearFeatureObject();
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "70%",
      disableClose: true,
    });
    this.modalRef.afterClosed().subscribe((resp) => {
      this.getStyleFeatures();
    });
  }

  openPriceModal(templateRef) {
    this.getStylePriceType();
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "500px",
      disableClose: true,
    });

  }

  getStylePriceType() {
    this._priceTypeService.getAll().subscribe((data) => {
      this.priceTypeList = _.map(data, item => {
        let itemPrice = _.chain(this.style.stylePriceList).find(['priceTypeId', item.id]).value();
        return {
          id: _.get(itemPrice, 'id'),
          price: _.get(itemPrice, 'price'),
          priceTypeId: item.id,
          priceTypeLabel: item.label,
          styleId: this.id,
        }
      });
    });
  }


  addStyleColor() {
    if (!_.get(this.newColor, "name")) {
      this.snackBar.open(`Please, enter color name!`, "Close");
      return;
    }
    var payload = {
      styleId: this.id,
      name: _.get(this.newColor, "name"),
      colorCode: _.get(this.newColor, "colorCode"),
      primary: _.get(this.newColor, "primary"),
    };
    this._styleService.addColor(payload).subscribe((resp) => {
      this.getStyleColors();
      this.clearColorObject();
      this.snackBar.open(`Style color added!`, "Close");
    });
  }

  addStyleFeature() {
    if (!_.get(this.newFeature, "label")) {
      this.snackBar.open(`Please, enter feature label!`, "Close");
      return;
    }
    var payload = {
      styleId: this.id,
      label: _.get(this.newFeature, "label"),
    };
    this._styleService.addFeature(payload).subscribe((resp) => {
      this.getStyleFeatures();
      this.clearFeatureObject();
      this.snackBar.open(`Style feature added!`, "Close");
    });
  }

  onColorSelect(color) {
    this.updateColor = _.merge({}, color);
  }

  onFeatureSelect(feature) {
    this.updateFeature = _.merge({}, feature);
  }

  updateStyleColor() {
    if (!_.get(this.updateColor, "name")) {
      this.snackBar.open(`Please, enter color name!`, "Close");
      return;
    }
    var payload = {
      id: this.updateColor.id,
      name: _.get(this.updateColor, "name"),
      colorCode: _.get(this.updateColor, "colorCode"),
      primary: _.get(this.updateColor, "primary"),
    };
    this._styleService.updateColorFields(payload).subscribe((resp) => {
      this.getStyleColors();
      this.updateColor = {
        id: null,
        name: null,
      };
      this.snackBar.open(`Style color updated!`, "Close");
    });
  }

  updateStylePrice() {

    var payload = this.priceTypeList;
    this._stylePriceService.updateFields(payload).subscribe((resp) => {
      this.getStyle();
      this.modalRef.close();
      this.snackBar.open(`Style price updated!`, "Close");
    });
  }

  updateStyleFeature() {
    if (!_.get(this.updateFeature, "label")) {
      this.snackBar.open(`Please, enter feature label!`, "Close");
      return;
    }
    var payload = {
      id: this.updateFeature.id,
      label: _.get(this.updateFeature, "label"),
    };
    this._styleService.updateFeatureFields(payload).subscribe((resp) => {
      this.getStyleFeatures();
      this.updateColor = {
        id: null,
        label: null,
      };
      this.clearFeatureObject();
      this.updateFeature = null;
      this.snackBar.open(`Style feature updated!`, "Close");
    });
  }


  deleteStyleColor(colorId) {
    if (!confirm("Are you sure?")) {
      return;
    }
    this._styleService.deleteColor(colorId).subscribe((resp) => {
      this.getStyleColors();
      this.updateColor = {
        id: null,
        name: null,
      };
      this.snackBar.open(`Style color deleted!`, "Close");
    });
  }

  deleteStyleFeature(featureId) {
    if (!confirm("Are you sure?")) {
      return;
    }
    this._styleService.deleteFeature(featureId).subscribe((resp) => {
      this.getStyleFeatures();
      this.updateFeature = {
        id: null,
        label: null,
      };
      this.clearFeatureObject();
      this.snackBar.open(`Style feature deleted!`, "Close");
    });
  }

  clearColorUpdate() {
    this.updateColor = {
      id: null,
      name: null,
      primary: null,
    };
  }

  clearFeatureUpdate() {
    this.updateFeature = {
      id: null,
      label: null,
    };
  }

  clearColorObject() {
    this.newColor = {
      name: null,
      colorCode: null,
      primary: null,
    };
  }

  clearFeatureObject() {
    this.newFeature = {
      label: null,
    };
  }

  getSizes() {
    this._sizeService.getAll().subscribe((data) => {
      this.sizeList = data;
    });
  }

  openEditModal() {
    const colorModal = this.dialog.open(StyleUpdateModalComponent, {
      minWidth: '600px',
      maxWidth: '960px',
      disableClose: true,
      data: {
        styleId: this.id,
        typeFlag: 'update',
      }
    });
    colorModal.afterClosed().subscribe(result => {
      result && this.getStyle();
    });
  }


  styleDelete() {
    if (confirm("Delete style confirm?")) {
      this._styleService.delete(this.id).subscribe((res) => {
        this.snackBar.open(`Style deleted!`, "Close");
        this._router.navigateByUrl(`styles`);
      });
    }
  }

  styleArchive(status) {
    if (confirm("Archive status confirm?")) {
      this._styleService
        .updateFields([{ archive: status, id: this.id }])
        .subscribe((res) => {
          this.snackBar.open(`Style archive status change!`, "Close");
          this._router.navigateByUrl(`styles`);
        });
    }
  }




  loadColorPhotos(colorId) {
    this.colorId = colorId;
  }





}

import { Component, OnInit, Input } from "@angular/core";
import * as _ from "lodash";
import { fuseAnimations } from "@fuse/animations";
import { AuthService } from "app/authentication/auth.service";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { StyleService } from "../../../style.service";
import { CostTypeService } from "app/modules-core/common/cost-type/cost-type.service";
import { StyleCostService } from "app/modules-core/common/style-cost/style-cost.service";
import { forkJoin } from "rxjs/internal/observable/forkJoin";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { MatDialog } from "@angular/material";
import { removeEmptyFields } from "app/modules-core/utility/helpers";
import { SeasonService } from "app/modules-core/common/season/season.service";
import { StyleTypeService } from "app/modules-core/common/style-type/style-type.service";

@Component({
  selector: "style-details-costing",
  templateUrl: "./style-details-costing.component.html",
  animations: fuseAnimations,
})
export class StyleDetailsCostingComponent implements OnInit {
  id;

  style: any = null;

  costHistory: any = [];

  styleCosts: any[] = [];
  styleCostsDetails: any = {
    styleId: null,
    comment: null,
    usdCurrencyRate: null,
    styleCostDetails: [],
  }
  editMode: any = false;

  cloneStyleId: any = null;

  modalRef: any = null;









  filterStyle: any = {};
  filterCounterStyle = 0;
  filterMoreStyle = false;

  styleList: any[] = [];

  styleTypeList: any[] = [];
  seasonList: any[] = [];

  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
    private _styleService: StyleService,
    private _styleCostService: StyleCostService,
    private _costTypeService: CostTypeService,
    private _seasonService: SeasonService,
    private _styleTypeService: StyleTypeService,
  ) {
    this._quickPanelSubjectService.heading.next("Style Editor");
    this.id = +_route.parent.snapshot.paramMap.get("id");
  }

  ngOnInit() {
    this.getStyle();
  }

  getStyle() {
    this._styleService.get(this.id).subscribe((style) => {
      this.style = style;
      this.getStyleCosting();
    });
  }

  getStyleCosting() {
    if (!_.get(this.style, 'costType')) {
      this.snackBar.open(`Cost type update first!`, "Close", true);
      return;
    }
    let payload = {}

    payload[_.get(this.style, 'costType')] = true;
    forkJoin(
      this._styleCostService.getAll({ 'style.id': this.id }),
      this._costTypeService.getAll(payload),
    ).subscribe(results => {
      this.costHistory = results[0];
      let costs = _.maxBy(results[0], 'id');
      this.styleCostsDetails = {
        styleId: this.id,
        comment: _.get(costs, 'comment'),
        id: _.get(costs, 'id'),
        usdCurrencyRate: _.get(costs, 'usdCurrencyRate'),
        confirm: _.get(costs, 'date') ? true : false,
        styleCostDetails: [],
      }
      this.styleCosts = _.map(results[1], (type, index) => {
        type['costItems'] = _.filter(_.get(costs, 'styleCostDetails', []), ['costTypeId', type.id]);
        return type;
      });
      this.calculationStyleCosts();
    });

  }

  addItem(typeIndex, commissionFlag) {
    let item = {
      description: null,
      supplierName: null,
      consumption: null,
      consumptionUnit: null,
      unitPrice: null,
      wastage: null,
      costUsdPerStyle: null,
      costPerStyle: null,
      costTypeId: null,
      commission: commissionFlag || null,

    }
    this.styleCosts[typeIndex].costItems.push(_.merge({}, item));
  }
  removeItem(typeIndex, costIndex) {
    this.styleCosts[typeIndex].costItems.splice(costIndex, 1);
  }

  onConsumptionInputChange() {
    this.calculationStyleCosts();
  }

  calculationStyleCosts() {
    let fobIndex = -1;
    fobIndex = _.subtract(_.findIndex(this.styleCosts, item => {
      if (item.manufacture == true && item.stockLot == true && item.fob == true) {
        return true;
      }
      return false;
    }), 1);

    let total = 0;
    let totalUsd = 0;
    _.each(this.styleCosts, (type, index) => {
      _.each(type.costItems, cost => {
        if (cost.unitPrice) {
          cost.consumption = Number(cost.consumption);
          cost.wastage = Number(cost.wastage);
          cost.unitPrice = Number(cost.unitPrice);
          if (_.get(type, 'usdCurrency')) {
            cost.costUsdPerStyle = _.round(_.multiply(_.multiply(cost.consumption || 0, cost.unitPrice), _.add(1, _.divide(cost.wastage, 100) || 0)), 2).toFixed(2);
            cost.costPerStyle = _.round(_.multiply(cost.costUsdPerStyle, _.get(this.styleCostsDetails, 'usdCurrencyRate', 1)), 2).toFixed(2);
          } else {
            cost.costUsdPerStyle = 0;
            cost.costPerStyle = _.round(_.multiply(_.multiply(cost.consumption || 0, cost.unitPrice), _.add(1, _.divide(cost.wastage, 100) || 0)), 2).toFixed(2);
          }

        } else {
          cost.costUsdPerStyle = 0;
          cost.costPerStyle = 0;
        }
        cost.costUsdPerStyle = Number(cost.costUsdPerStyle);
        cost.costPerStyle = Number(cost.costPerStyle);
      });
      type['typeUsdTotal'] = _.sumBy(type['costItems'], 'costUsdPerStyle');
      type['typeTotal'] = _.sumBy(type['costItems'], 'costPerStyle');
      totalUsd = _.add(totalUsd, type['typeUsdTotal']);
      total = _.add(total, type['typeTotal']);

      type['levelUsdTotal'] = _.add(0, totalUsd);
      type['levelTotal'] = _.add(0, total);
      if (index == fobIndex) {
        type['fobFlag'] = true;
      }
    });
    this.styleCostsDetails['styleUsdUnitPrice'] = _.sumBy(this.styleCosts, 'typeUsdTotal');
    this.styleCostsDetails['styleUnitPrice'] = _.sumBy(this.styleCosts, 'typeTotal');
  }

  saveCosting() {
    if (_.get(this.styleCostsDetails, 'confirm')) {
      this.snackBar.open(`Completed costing can not update!`, "Close");
      return;
    }
    this.styleCostsDetails.styleId = this.id
    let payload = _.merge({}, this.styleCostsDetails);
    payload.styleCostDetails = [];
    _.each(this.styleCosts, type => {
      _.each(type.costItems, cost => {
        cost['costTypeId'] = type.id
        payload.styleCostDetails.push(_.merge({}, cost));
      });
    });
    this._styleCostService.create(payload).subscribe(resp => {
      this.getStyleCosting();
      this.snackBar.open(`Costing saved!`, "Close");
    });
  }

  completeCosting() {
    if (_.get(this.styleCostsDetails, 'confirm')) {
      this.snackBar.open(`Completed costing can not complete!`, "Close");
      return;
    }
    if (!confirm("Are you want to complete it?")) {
      return
    }
    if (!_.get(this.styleCostsDetails, 'id')) {
      this.snackBar.open(`Please! Costing save first!`, "Close");
      return;
    }
    let payload = {
      id: _.get(this.styleCostsDetails, 'id'),
      confirm: true,
      styleId: this.id,
    }
    this._styleCostService.updateFields(payload).subscribe(resp => {
      this.editMode = false;
      this.getStyleCosting();
      this.snackBar.open(`Costing complete!`, "Close");
    });
  }



  getPriviousCosting(index) {
    this.editMode = false;
    let costs = this.costHistory[index];
    this.styleCostsDetails = {
      styleId: this.id,
      id: _.get(costs, 'id'),
      usdCurrencyRate: _.get(costs, 'usdCurrencyRate'),
      comment: _.get(costs, 'comment'),
      styleCostDetails: [],
      oldCosting: true,
      confirm: _.get(costs, 'date') ? true : false,
    }
    this.styleCosts = _.map(this.styleCosts, type => {
      type['costItems'] = [];
      type['costItems'] = _.filter(_.get(costs, 'styleCostDetails', []), ['costTypeId', type.id]);
      return type;
    });
    this.calculationStyleCosts();
  }


  openCloneModal(templateRef) {
    this.cloneStyleId = null;
    this.clearAllFilterStyle();
    this.getAllSeasons();
    this.getAllStyleTypes();
    this.applyStyleFilters(this.filterStyle);
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "700px",
      maxWidth: "90%",
    });
  }

  cloneCosting() {
    if (!this.cloneStyleId) {
      this.snackBar.open(`Enter style id!`, "Close");
      return;
    }
    if (!confirm("Are you want to clone costing from this style?")) {
      return
    }
    let payload = {
      id: this.id,
      styleId: this.cloneStyleId
    }
    this._styleCostService.clone(payload).subscribe(resp => {
      this.editMode = false;
      this.getStyleCosting();
      this.modalRef.close();
      this.snackBar.open(`Costing clone!`, "Close");
    });
  }


  clearAllFilterStyle() {
    this.filterStyle = {
      formdate: null,
      todate: null,
      styleid: null,
      stylename: null,
      styletypeid: [],
      seasonid: [],
    };
  }



  applyStyleFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    this._styleService.getAll(params).subscribe(data => {
      this.styleList = data;
    });
  }




  getAllStyleTypes() {
    this._styleTypeService.getAll().subscribe(data => {
      this.styleTypeList = data;
    });
  }

  getAllSeasons() {
    if (this._auth.hasPermission('PERMISSION_SEASON_VIEW')) {
      this._seasonService.getAll().subscribe(data => {
        this.seasonList = data;
      });
    }

  }































}

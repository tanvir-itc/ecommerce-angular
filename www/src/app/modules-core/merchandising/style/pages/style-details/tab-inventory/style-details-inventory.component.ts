import { Component, OnInit, Input } from "@angular/core";
import * as _ from "lodash";
import { fuseAnimations } from "@fuse/animations";
import { AuthService } from "app/authentication/auth.service";
import { MatDialog } from "@angular/material";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { SizeService } from "app/modules-core/common/size/size.service";
import { forkJoin } from "rxjs";
import { InventoryService } from "app/modules-core/merchandising/inventory/inventory.service";
import { StyleService } from "../../../style.service";
import { ActivatedRoute } from "@angular/router";
import { InventoryAddModalComponent } from "app/modules-core/merchandising/inventory/components/inventory-add-edit-modal/inventory-add-modal.component";
import { InventoryEditModalComponent } from "app/modules-core/merchandising/inventory/components/inventory-add-edit-modal/inventory-edit-modal.component";

@Component({
  selector: "style-details-inventory",
  templateUrl: "./style-details-inventory.component.html",
  animations: fuseAnimations,
})
export class StyleDetailsInventoryComponent implements OnInit {
  id;

  style: any = null;


  modalRef;


  sizes: any[] = [];

  inventories: any[] = [];

  inventoryId: any = null;

  styleColorList: any[] = [];

  summary: any = null;


  details: any = null;

  constructor(
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    public _auth: AuthService,
    private _route: ActivatedRoute,
    private _styleService: StyleService,
    private _sizeService: SizeService,
    private _inventoryService: InventoryService,
  ) {
    this._quickPanelSubjectService.heading.next("Style Editor");
    this.id = +_route.parent.snapshot.paramMap.get("id");
  }

  ngOnInit() {
    this.getStyle();
    this.getStyleColors();
    this.getStyleOrderSummary();
  }



  getStyle() {
    this._styleService.get(this.id).subscribe((style) => {
      this.style = style;
      this.getInventory();
    });
  }

  getStyleColors() {
    this._styleService.getColors({ styleid: this.id }).subscribe((data) => {
      this.styleColorList = data;
    });
  }


  getInventory() {
    let params = {
      styleid: this.id,
    }
    forkJoin([
      this._sizeService.getAll({ sizegroupid: _.get(this.style, 'sizeGroupId') }),
      this._inventoryService.getBreakdown(params)
    ]).subscribe(results => {
      this.sizes = results[0];
      this.inventories = results[1];
    });
  }



  openAddStyleInventoryModal() {
    const addModal = this.dialog.open(InventoryAddModalComponent, {
      minWidth: '400px',
      maxWidth: '95%',
      disableClose: true,
      data: {
        styleId: this.id,
        groupId: _.get(this.style, 'sizeGroupId'),
      }
    });
    addModal.afterClosed().subscribe(result => {
      if (result) {
        this.getInventory();
        this.getStyleOrderSummary();
      }
    });
  }

  openEditStyleInventoryEditModal() {
    const editModal = this.dialog.open(InventoryEditModalComponent, {
      minWidth: '400px',
      maxWidth: '95%',
      disableClose: true,
      data: {
        styleId: this.id,
        groupId: _.get(this.style, 'sizeGroupId'),
      }
    });
    editModal.afterClosed().subscribe(result => {
      if (result) {
        this.getInventory();
        this.getStyleOrderSummary();
      }
    });
  }


  getStyleOrderSummary() {
    this._styleService.getStyleOrderSummary(this.id, {}).subscribe((resp) => {
      this.summary = resp;
    });
  }


  openDetailsModal(templateRef, type, title, data) {
    this.details = null;
    let dataArray = _.groupBy(data, 'orderId');
    this.details = {
      type: type,
      title: title,
      data: [],
    }
    _.each(dataArray, item => {
      let headItem = _.head(item);
      let ord = {
        rimsPo: _.get(headItem, 'rimsPo'),
        poNo: _.get(headItem, 'poNo'),
        shipDate: _.get(headItem, 'shipDate'),
        customerName: _.get(headItem, 'customerName'),
        qty: _.sumBy(item, 'qty'),
      }
      this.details.data.push(ord);

    });
    this.modalRef = this.dialog.open(templateRef, {
      maxWidth: "95%",
      minWidth: "500px",
      disableClose: true,
    });
  }
}

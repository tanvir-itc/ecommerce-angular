import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/authentication/auth.service';

@Component({
  selector: 'app-style-details',
  templateUrl: './style-details.component.html',
  styleUrls: ['./style-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class StyleDetailsComponent implements OnInit {

  id;
  show = false;
  tabs;
  tabPathBase;
  tabFromUrl;

  constructor(
    private _route: ActivatedRoute,
    public _auth: AuthService
  ) {
    // this.id = + this._route.snapshot.paramMap.get('id');
    this.tabFromUrl = this._route.snapshot.firstChild.routeConfig.path; //find a better way to get child param
  }

  ngOnInit() {
    this._route.paramMap.subscribe(params => {
      this.id = params.get("id")
      this.show = true;
      this.tabPathBase = `styles/${this.id}`;

      this.tabs = [
        {
          path: `${this.tabPathBase}/basic`,
          label: 'Basic Info',
          display: this._auth.hasPermission('PERMISSION_MENU_STYLE_VIEW') ? true : false
        },
        {
          path: `${this.tabPathBase}/cost`,
          label: 'Costing',
          display: this._auth.hasPermission('PERMISSION_STYLE_COST_VIEW') ? true : false
        },
        {
          path: `${this.tabPathBase}/inventory`,
          label: 'Inventory',
          display: true,
        },
        {
          path: `${this.tabPathBase}/inventory-flow`,
          label: 'Inventory Flow',
          display: true,
        },
      ];
      this.show = true;
    });
  }


}

import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { AuthService } from "app/authentication/auth.service";
import * as moment from "moment";
import { InventoryService } from "app/modules-core/merchandising/inventory/inventory.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "style-details-inventory-flow",
  templateUrl: "./style-details-inventory-flow.component.html",
})
export class StyleDetailsInventoryFlowComponent implements OnInit {
  id;

  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  report: any[] = [];


  sorter: any = {
    field: null,
    sortType: 'asc',
    fieldType: 'text',
  }

  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    public _auth: AuthService,
    private _route: ActivatedRoute,
    private _inventoryService: InventoryService,
  ) {
    this._quickPanelSubjectService.heading.next("Style Editor");
    this.id = +_route.parent.snapshot.paramMap.get("id");
  }
  ngOnInit() {

    this.applyFilters(this.filter);

  }


  applyFilters(filterParams) {
    var params = _.merge({ styleid: this.id }, filterParams);

    this._inventoryService.getAllStyleHistory(params).subscribe((data) => {
      this.report = data;
      debugger
    });
  }





} //class

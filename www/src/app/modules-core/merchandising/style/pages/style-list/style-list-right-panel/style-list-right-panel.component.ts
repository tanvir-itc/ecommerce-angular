import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import * as _ from "lodash";
import { fuseAnimations } from "@fuse/animations";
import { StyleService } from "../../../style.service";
import { AttachmentService } from "app/modules-core/common/attachment/attachment.service";
import { AuthService } from "app/authentication/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "style-list-right-panel",
  templateUrl: "./style-list-right-panel.component.html",
  animations: fuseAnimations,
})
export class StyleListRightPanelComponent implements OnInit, OnChanges {
  @Input()
  styleId: any = null;

  style: any = null;

  styleColorList: any[] = [];

  images: any[] = [];

  bannerImage: any = null;

  constructor(
    private _styleService: StyleService,
    private _attachmentService: AttachmentService,
    public _auth: AuthService,
    private _router: Router
  ) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    this.getStyleDetails();
  }

  getStyleDetails() {
    this.style = null;
    this.styleColorList = [];

    this.images = [];
    this.bannerImage = null;

    if (this.styleId) {
      this.getStyle();
      this.getStyleColors();
      this.getAttachmentImages();
    }
  }

  getStyle() {
    this._styleService.get(this.styleId).subscribe((style) => {
      this.style = style;
    });
  }

  getStyleColors() {
    this._styleService
      .getColors({ styleid: this.styleId })
      .subscribe((data) => {
        this.styleColorList = data;
      });
  }

  getAttachmentImages() {
    this._attachmentService
      .getAll({ entitytype: "style_image", entityid: this.styleId })
      .subscribe((data) => {
        this.images = data;
        this.bannerImage = null;
        this.bannerImage = _.chain(this.images)
          .head()
          .get("fullpath", this.bannerImage)
          .value();
      });
  }

  goLink(styleId) {
    this._router.navigateByUrl(`styles/${styleId}`);
  }
}

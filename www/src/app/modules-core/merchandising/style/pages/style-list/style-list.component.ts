import { Component, OnInit } from "@angular/core";
import { fuseAnimations } from "@fuse/animations";
import * as _ from "lodash";
import { MatDialog } from "@angular/material";
import { StyleService } from "../../style.service";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  stringToIntegerArray,
  removeEmptyFields,
  integerArrayToString, customSortBy, isDateBefore
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { StyleTypeService } from "app/modules-core/common/style-type/style-type.service";
import { SeasonService } from "app/modules-core/common/season/season.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { OrderService } from "app/modules-core/merchandising/order/order.service";
import { CustomerService } from "app/modules-core/merchandising/customers/customer.service";
import { SizeGroupService } from "app/modules-core/common/size/size-group.service";
import { StyleBrandService } from "app/modules-core/common/style-brand/style-brand.service";
import { StyleUpdateModalComponent } from "../../components/style-update-modal/style-update-modal.component";
import { DragulaService } from "ng2-dragula";
import { Subscription } from "rxjs";

@Component({
  selector: "style-list",
  templateUrl: "./style-list.component.html",
  animations: fuseAnimations,
})
export class StyleListComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  styles: any[] = [];
  styleId: any = null;

  styleTypeList: any[] = [];
  styleBrandList: any[] = [];
  seasonList: any[] = [];
  sizeGroupList: any[] = [];

  modalRef = null;

  newStyle: any = null;

  styleGallery: any = null;

  costTypes: any[] = [
    { id: 'manufacture', label: 'Manufactured by Rawmanco' },
    { id: 'fob', label: 'Manufacture by Vendor' },
    { id: 'stockLot', label: 'Stock Lot' },
  ]

  ratingList: any[] = [];

  BAG = 'STYLE_ITEMS';
  subs = new Subscription();
  constructor(
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _styleService: StyleService,
    private _styleTypeService: StyleTypeService,
    private _styleBrandService: StyleBrandService,
    private _seasonService: SeasonService,
    public _orderService: OrderService,
    public _customerService: CustomerService,
    public _sizeGroupService: SizeGroupService,
    private _dragulaService: DragulaService,
  ) {
    _dragulaService.createGroup(this.BAG, {
      revertOnSpill: true
    });
    this.subs.add(_dragulaService.drop(this.BAG)
      .subscribe(v => {
        this.updateStyle();
      })
    );
    this._quickPanelSubjectService.heading.next("Style Editor");
  }
  ngOnInit() {
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));

    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
    this.getAllStyleTypes();
    this.getAllStyleBrands();
    this.getAllSeasons();
    this.getRatingList();
  }

  clearAllFilter() {
    this.filter = {
      //fromdate: toDateString(moment(this.filter.todate || new Date).subtract(3, 'months').startOf('month')),
      //todate: toDateString(moment(this.filter.todate || new Date).endOf('month')),
      fromdate: null,
      todate: null,
      styleid: null,
      stylename: null,
      styletypeid: [],
      seasonid: [],
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "styletypeid") {
        params[key] = stringToIntegerArray(params[key]);
      }
      if (key == "seasonid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "styletypeid") {
        params[key] = integerArrayToString(params[key]);
      }
      if (key == "seasonid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    params['sort'] = 's.sequence';
    this._styleService.getAll(params).subscribe((data) => {
      this.styles = data;
      console.log(this.styles)
      this.setFilterToURL(filterParams);
    });
  }

  getAllStyleTypes() {
    this._styleTypeService.getAll().subscribe((data) => {
      this.styleTypeList = data;
    });
  }
  getAllStyleBrands() {
    this._styleBrandService.getAll().subscribe((data) => {
      this.styleBrandList = data;
    });
  }

  getAllSizeGroups() {
    this._sizeGroupService.getAll().subscribe((data) => {
      this.sizeGroupList = data;
    });
  }

  getAllSeasons() {
    if (this._auth.hasPermission('PERMISSION_SEASON_VIEW')) {
      this._seasonService.getAll().subscribe((data) => {
        this.seasonList = data;
      });
    }

  }

  openCreateModal(templateRef) {
    this.clearCreateObject();
    this.getAllSizeGroups();
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "600px",
      maxWidth: "90%",
      disableClose: true,
    });
  }
  editStyle: any = {}
  openUpdateModal(templateRef, style) {
    this.editStyle = {
      id: style.id,
      sequence: style.sequence
    }
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "300px",
      maxWidth: "90%",
      disableClose: true,
    });
  }

  clearCreateObject() {
    this.newStyle = {
      description: null,
      name: null,
      note: null,
      heatmap: null,
      lowStockQty: null,
      styleTypeId: null,
      styleBrandId: null,
      seasonId: null,
      archive: null,
      sizeGroupId: null,
      costType: null,
      weight: null,
      composition: null,
      fobPrice: null,
      minimumRatingRequired: null,
      earliestShipmentDate: null,
      ccUpCharge: 3,
      paymentRequired: false,
    };
  }

  createStyle() {
    if (!_.get(this.newStyle, "styleTypeId")) {
      this.snackBar.open(`Please select a style type!`, "Close");
      return;
    }
    if (!_.get(this.newStyle, "styleBrandId")) {
      this.snackBar.open(`Please select a style brand!`, "Close");
      return;
    }
    if (!_.get(this.newStyle, "sizeGroupId")) {
      this.snackBar.open(`Please select a size group!`, "Close");
      return;
    }
    if (!_.get(this.newStyle, "seasonId")) {
      this.snackBar.open(`Please select a season!`, "Close");
      return;
    }
    if (!_.get(this.newStyle, "name")) {
      this.snackBar.open(`Please insert name!`, "Close");
      return;
    }
    if (!_.get(this.newStyle, "minimumRatingRequired")) {
      this.snackBar.open(`Please select minimum rating required!`, "Close");
      return;
    }
    if (!_.get(this.newStyle, "costType")) {
      this.snackBar.open(`Please insert cost type!`, "Close");
      return;
    }
    var payload = this.newStyle;
    this._styleService.create(payload).subscribe((resp) => {
      this._router.navigateByUrl(`styles/${resp.id}/basic`);
      this.snackBar.open(`Style created!`, "Close");
      this.modalRef.close();
    });
  }
  updateStyle() {
    let payload = [];
    _.each(this.styles, (v, i) => {
      payload.push({ id: v.id, sequence: i + 1 });
    });
    this._styleService.updateFields(payload).subscribe((resp) => {
      this.snackBar.open(`Style update!`, "Close");
      this.applyFilters(this.filter);
      this.modalRef.close();
    });
  }

  goLink(styleId) {
    this._router.navigateByUrl(`styles/${styleId}/basic`);
  }

  openGalleryModal(templateRef, style) {
    this.styleGallery = {
      title: `${_.get(style, 'name')}'s Gallery`,
      styleId: _.get(style, 'id'),
    }
    this.modalRef = this.dialog.open(templateRef, {
      width: "600px",
    });
  }

  getRatingList() {
    this._orderService.getAllRating().subscribe((resp) => {
      this.ratingList = resp;
    });
  }


  openEditModal(id) {
    const colorModal = this.dialog.open(StyleUpdateModalComponent, {
      minWidth: '600px',
      maxWidth: '960px',
      disableClose: true,
      data: {
        styleId: id,
      }
    });
    colorModal.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
    });
  }
  ngOnDestroy(): void {
    this._dragulaService.destroy(this.BAG);
    this.subs.unsubscribe();
  }


} //class

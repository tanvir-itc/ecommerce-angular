import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from "rxjs/internal/operators/map";
import * as _ from "lodash";
import { isDateAfter, isDateBefore, makeParams } from "app/modules-core/utility/helpers";
import * as moment from "moment";

@Injectable({
  providedIn: "root",
})
export class StyleService {
  urlBase = "styles";
  urlColor = "style-combos";
  urlFeature = "style-features";
  urlSales = "style-sales";

  httpOptions = {
    params: new HttpParams(),
  };
  constructor(private http: HttpClient) { }

  getAll(opts = {}) {
    return this.http
      .get(`${this.urlBase}/v2`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = _.map(JSON.parse(resp.data), style => {
              style['newTillDateFlag'] = style.newTillDate ? isDateAfter(style.newTillDate, new Date) : false;
              style['defaultPrice'] = _.chain(style.stylePriceList).find(['priceTypeName', 'default']).get('price').value();
              style['availableInventory'] = !!style.invQty;
              style['availableShipDate'] = true;
              style['addedToOrder'] = false;
              style['colorImages'] = _.filter(_.flatMap(style.combos, 'styleColorImages'), 'isDefault')
              if (_.get(style, 'earliestShipmentDate')) {
                style['availableShipDate'] = isDateAfter(new Date, style.earliestShipmentDate);
              }
              //sales
              if (_.get(style, 'saleDiscounts.id')) {
                if (
                  (isDateAfter(moment().format('YYYY-MM-DD'), moment(_.get(style, 'saleDiscounts.fromDate')).subtract(1, 'day').format('YYYY-MM-DD')) &&
                    isDateBefore(moment().format('YYYY-MM-DD'), moment(_.get(style, 'saleDiscounts.toDate')).add(1, 'day').format('YYYY-MM-DD'))) || !_.get(style, 'saleDiscounts.toDate')
                ) {
                  style.stylePriceList = _.map(style.stylePriceList, item => {
                    let discount = item.price ? _.divide(_.multiply(item.price, _.get(style, 'saleDiscounts.discountPercentage')), 100) : 0;
                    item.discountedPrice = _.subtract(_.get(item, 'price', 0), discount || 0);
                    return item;
                  })
                  style['defaultPriceDis'] = _.subtract(_.get(style, 'defaultPrice', 0), _.divide(_.multiply(style.defaultPrice, _.get(style, 'saleDiscounts.discountPercentage')), 100));
                  style.discountFlag = true;
                }
              }
              // 
              style['priceObject'] = {};
              style['priceObjectDis'] = {};
              _.each(style.stylePriceList, price => {
                style.priceObject[`price_${price.priceTypeId}`] = _.get(price, 'price');
                style.priceObjectDis[`price_${price.priceTypeId}`] = _.get(price, 'discountedPrice');
              });
              let styleImage = _.chain(style.combos).find(['primary', true]).get('styleColorImages').value() || _.chain(style.combos).head().get('styleColorImages').value();
              style.styleImage = styleImage


              return style;
            });
            return resp.data;
          }
          return [];
        })
      );
  }

  getAllLight(opts = {}) {
    return this.http
      .get(`${this.urlBase}/v2`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = JSON.parse(resp.data)
            return resp.data;
          }
          return [];
        })
      );
  }

  get(id) {
    return this.http.get(`${this.urlBase}/${id}/v2?showorderbalance=true`).pipe(
      map((resp: any) => {
        if (resp.data) {
          resp.data = _.map(JSON.parse(resp.data), style => {
            style['defaultPrice'] = _.chain(style.stylePriceList).find(['priceTypeName', 'default']).get('price').value();
            //sales
            if (_.get(style, 'saleDiscounts.id')) {

              if (
                (isDateAfter(moment().format('YYYY-MM-DD'), moment(_.get(style, 'saleDiscounts.fromDate')).subtract(1, 'day').format('YYYY-MM-DD')) &&
                  isDateBefore(moment().format('YYYY-MM-DD'), moment(_.get(style, 'saleDiscounts.toDate')).add(1, 'day').format('YYYY-MM-DD'))) || !_.get(style, 'saleDiscounts.fromDate')
              ) {

                style.stylePriceList = _.map(style.stylePriceList, item => {
                  let discount = item.price ? _.divide(_.multiply(item.price, _.get(style, 'saleDiscounts.discountPercentage')), 100) : 0;
                  item.discountedPrice = _.subtract(_.get(item, 'price', 0), discount || 0);
                  return item;
                })
                style['defaultPriceDis'] = _.subtract(_.get(style, 'defaultPrice', 0), _.divide(_.multiply(style.defaultPrice, _.get(style, 'saleDiscounts.discountPercentage')), 100));
                style.discountFlag = true;
              }
            }
            //
            style['priceObject'] = {};
            style['priceObjectDis'] = {};
            _.each(style.stylePriceList, price => {
              style.priceObject[`price_${price.priceTypeId}`] = _.get(price, 'price');
              style.priceObjectDis[`price_${price.priceTypeId}`] = _.get(price, 'discountedPrice');
            });
            let styleImage = _.chain(style.combos).find(['primary', true]).get('styleColorImages').value() || _.chain(style.combos).head().get('styleColorImages').value();
            style.styleImage = styleImage
            return style;
          });
          return _.head(resp.data);
        }
        return resp.data;
      })
    );
  }

  create(payload) {
    return this.http
      .post(this.urlBase, payload)
      .pipe(map((resp: any) => resp.data));
  }

  update(payload) {
    return this.http
      .put(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFields(payload) {
    return this.http
      .patch(`${this.urlBase}/v2`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  delete(id) {
    return this.http
      .delete(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  getColors(opts = {}) {
    return this.http
      .get(`${this.urlColor}`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          return _.sortBy(resp.data, "id");
        })
      );
  }

  addColor(payload) {
    return this.http.post(`${this.urlColor}`, payload).pipe(
      map((resp: any) => {
        return resp.data;
      })
    );
  }

  updateColor(payload) {
    return this.http.put(`${this.urlColor}`, payload).pipe(
      map((resp: any) => {
        return resp.data;
      })
    );
  }

  updateColorFields(payload) {
    return this.http.patch(`${this.urlColor}`, payload).pipe(
      map((resp: any) => {
        return resp.data;
      })
    );
  }

  deleteColor(id) {
    return this.http.delete(`${this.urlColor}/${id}`).pipe(
      map((resp: any) => {
        return resp.data;
      })
    );
  }

  getFeatures(opts = {}) {
    return this.http
      .get(`${this.urlFeature}`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          return _.sortBy(resp.data, "id");
        })
      );
  }

  addFeature(payload) {
    return this.http.post(`${this.urlFeature}`, payload).pipe(
      map((resp: any) => {
        return resp.data;
      })
    );
  }

  updateFeature(payload) {
    return this.http.put(`${this.urlFeature}`, payload).pipe(
      map((resp: any) => {
        return resp.data;
      })
    );
  }

  updateFeatureFields(payload) {
    return this.http.patch(`${this.urlFeature}`, payload).pipe(
      map((resp: any) => {
        return resp.data;
      })
    );
  }

  deleteFeature(id) {
    return this.http.delete(`${this.urlFeature}/${id}`).pipe(
      map((resp: any) => {
        return resp.data;
      })
    );
  }


  getStyleOrderSummary(styleId, opts = {}) {
    return this.http
      .get(`${this.urlBase}/${styleId}/order-summary`)
      .pipe(
        map((resp: any) => {
          resp.data['awaitingApprovalDetailList'] = _.groupBy(resp.data['awaitingApprovalDetail'], 'customerId');
          resp.data['awaitingApprovalDetailList'] = _.map(resp.data['awaitingApprovalDetailList'], parent => {
            let firstItem = _.head(parent);
            let parentObject = {
              customerName: _.get(firstItem, 'customerName'),
              qty: _.sumBy(parent, 'qty'),
            }
            return parentObject;
          });

          resp.data['draftBookDetailsList'] = _.groupBy(resp.data['draftBookDetails'], 'customerId');
          resp.data['draftBookDetailsList'] = _.map(resp.data['draftBookDetailsList'], parent => {
            let firstItem = _.head(parent);
            let parentObject = {
              customerName: _.get(firstItem, 'customerName'),
              qty: _.sumBy(parent, 'qty'),
            }
            return parentObject;
          });

          resp.data['processingDetailList'] = _.groupBy(resp.data['processingDetail'], 'customerId');
          resp.data['processingDetailList'] = _.map(resp.data['processingDetailList'], parent => {
            let firstItem = _.head(parent);
            let parentObject = {
              customerName: _.get(firstItem, 'customerName'),
              qty: _.sumBy(parent, 'qty'),
            }
            return parentObject;
          });

          return resp.data;
        })
      );
  }
}

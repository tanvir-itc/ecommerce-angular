import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import * as _ from 'lodash';
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { AuthService } from "app/authentication/auth.service";
import { StyleService } from "../../style.service";
import { StyleTypeService } from "app/modules-core/common/style-type/style-type.service";
import { StyleBrandService } from "app/modules-core/common/style-brand/style-brand.service";
import { SizeGroupService } from "app/modules-core/common/size/size-group.service";
import { SeasonService } from "app/modules-core/common/season/season.service";
import { OrderService } from "app/modules-core/merchandising/order/order.service";
import { SaleDiscountService } from "app/modules-core/common/sale-discounts/sale-discount.service";

@Component({
  selector: 'style-update-modal',
  templateUrl: 'style-update-modal.component.html',
})
export class StyleUpdateModalComponent {
  styleId = null;

  editStyle: any = null;

  styleTypeList: any[] = []
  saleDiscountList: any[] = []
  styleBrandList: any[] = []
  sizeGroupList: any[] = [];
  seasonList: any[] = [];
  ratingList: any[] = [];


  costTypes: any[] = [
    { id: 'manufacture', label: 'Manufactured by Rawmanco' },
    { id: 'fob', label: 'Manufacture by Vendor' },
    { id: 'stockLot', label: 'Stock Lot' },
  ];

  constructor(
    public dialogRef: MatDialogRef<StyleUpdateModalComponent>,
    private snackBar: SnackbarService,
    public _auth: AuthService,
    public _styleService: StyleService,
    public _styleTypeService: StyleTypeService,
    public _styleBrandService: StyleBrandService,
    public _sizeGroupService: SizeGroupService,
    public _orderService: OrderService,
    public _seasonService: SeasonService,
    public _saleDiscountService: SaleDiscountService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.styleId = data.styleId;
    this.getStyle();
    this.getAllStyleTypes();
    this.getSaleDiscountList();
    this.getAllStyleBrands();
    this.getAllSizeGroups();
    this.getAllSeasons();
    this.getRatingList();
  }

  getStyle() {
    this._styleService.get(this.styleId).subscribe((style) => {
      this.editStyle = _.merge({}, style);
      console.log('this.editStyle', this.editStyle)
      this.editStyle.saleDiscountId = _.get(style, 'saleDiscounts.id');
      this.editStyle.ccUpCharge = _.get(style, 'ccUpCharge') || 3;
    });
  }


  updateStyle() {
    if (!_.get(this.editStyle, "styleTypeId")) {
      this.snackBar.open(`Please! Select a style type!`, "Close");
      return;
    }
    if (!_.get(this.editStyle, "styleBrandId")) {
      this.snackBar.open(`Please! Select a style brand!`, "Close");
      return;
    }
    if (!_.get(this.editStyle, "sizeGroupId")) {
      this.snackBar.open(`Please! Select a size group!`, "Close");
      return;
    }
    if (!_.get(this.editStyle, "seasonId")) {
      this.snackBar.open(`Please! Select a season!`, "Close");
      return;
    }
    if (!_.get(this.editStyle, "name")) {
      this.snackBar.open(`Please! Insert name!`, "Close");
      return;
    }

    if (!_.get(this.editStyle, "costType")) {
      this.snackBar.open(`Please! Insert cost type!`, "Close");
      return;
    }
    var payload = [{
      id: _.get(this.editStyle, "id"),
      description: _.get(this.editStyle, "description"),
      name: _.get(this.editStyle, "name"),
      catalogName: _.get(this.editStyle, "catalogName"),
      note: _.get(this.editStyle, "note"),
      heatmap: _.get(this.editStyle, "heatmap"),
      price: _.get(this.editStyle, "price"),
      styleTypeId: _.get(this.editStyle, "styleTypeId"),
      styleBrandId: _.get(this.editStyle, "styleBrandId"),
      seasonId: _.get(this.editStyle, "seasonId"),
      sizeGroupId: _.get(this.editStyle, "sizeGroupId"),
      costType: _.get(this.editStyle, "costType"),
      weight: _.get(this.editStyle, "weight"),
      composition: _.get(this.editStyle, "composition"),
      lowStockQty: _.get(this.editStyle, "lowStockQty"),
      earliestShipmentDate: _.get(this.editStyle, "earliestShipmentDate", null),
      fobPrice: _.get(this.editStyle, "fobPrice"),
      minimumRatingRequired: _.get(this.editStyle, "minimumRatingRequired"),
      saleDiscountId: _.get(this.editStyle, "saleDiscountId", null),
      newTillDate: _.get(this.editStyle, "newTillDate"),
      ccUpCharge: _.get(this.editStyle, 'paymentRequired') ? _.get(this.editStyle, 'ccUpCharge') || 3 : 3,
      paymentRequired: _.get(this.editStyle, 'paymentRequired'),
    }]
    this._styleService.updateFields(payload).subscribe((resp) => {
      this.snackBar.open(`Style updated!`, "Close");
      this.dialogRef.close(true);
    });
  }

  getAllStyleTypes() {
    this._styleTypeService.getAll().subscribe((data) => {
      this.styleTypeList = data;
    });
  }

  getSaleDiscountList() {
    this._saleDiscountService.getAll().subscribe((data) => {
      this.saleDiscountList = data;
    });
  }

  getAllStyleBrands() {
    this._styleBrandService.getAll().subscribe((data) => {
      this.styleBrandList = data;
    });
  }

  getAllSizeGroups() {
    this._sizeGroupService.getAll().subscribe((data) => {
      this.sizeGroupList = data;
    });
  }

  getAllSeasons() {
    if (this._auth.hasPermission('PERMISSION_SEASON_VIEW')) {
      this._seasonService.getAll().subscribe((data) => {
        this.seasonList = data;
      });
    }
  }

  getRatingList() {
    this._orderService.getAllRating().subscribe((resp) => {
      this.ratingList = resp;
    });
  }


}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeatmapComponent } from './heatmap.component';

@NgModule({
  declarations: [
    HeatmapComponent,
  ],
  entryComponents: [
    HeatmapComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    HeatmapComponent,
  ]
})

export class HeatmapModule {
}

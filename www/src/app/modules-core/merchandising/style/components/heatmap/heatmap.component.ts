import { Component, ViewEncapsulation, Input } from "@angular/core";
import * as _ from 'lodash';

@Component({
  selector: 'heatmap',
  templateUrl: 'heatmap.component.html',
  styleUrls: ['./heatmap.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HeatmapComponent {

  @Input()
  value = null;

  colors: any[] = [
    { id: 1, color: '#fffc00' },
    { id: 2, color: '#ffde00' },
    { id: 3, color: '#ffcc00' },
    { id: 4, color: '#ffb400' },
    { id: 5, color: '#ff9600' },
    { id: 6, color: '#ff7800' },
    { id: 7, color: '#ff6000' },
    { id: 8, color: '#ff4e00' },
    { id: 9, color: '#ff4200' },
    { id: 10, color: '#ff1e00' },
    { id: 11, color: '#ff0c00' },
    { id: 12, color: '#ec0000' },
    { id: 13, color: '#dc0000' },
    { id: 14, color: '#cb0000' },
    { id: 15, color: '#a60000' },
  ];


  constructor() {

  }

  ngOnInit() {

  }

}
import { Component, OnInit, Input, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { StyleService } from 'app/modules-core/merchandising/style/style.service';
import * as _ from 'lodash';
import { AuthService } from 'app/authentication/auth.service';

@Component({
  selector: 'style-basic',
  templateUrl: './style-basic.component.html',
})
export class StyleBasicComponent implements OnInit {

  @Input()
  styleId = null;

  @Input()
  priceType = null;

  @Input()
  displayType = "details";

  @Input()
  api = false;

  @Input()
  style;

  @Input()
  editable = false;

  @Output()
  onEditClick: EventEmitter<any> = new EventEmitter();

  constructor(
    private _styleService: StyleService,
    public _auth: AuthService,
  ) {
  }
  ngOnInit() {
  }

  getStyle() {
    if (this.api && this.styleId) {
      this._styleService.get(this.styleId).subscribe(style => {
        this.style = style;
      });
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getStyle();
  }

  onEditClicked() {
    this.onEditClick.emit();
  }

}
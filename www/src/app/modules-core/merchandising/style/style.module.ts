import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from '../../utility/date-time-picker/date-time.module';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { UserAutocompleteModule } from 'app/modules-core/common/user/components/user-autocomplete.module';
import { StyleListComponent } from './pages/style-list/style-list.component';
import { StyleListRightPanelComponent } from './pages/style-list/style-list-right-panel/style-list-right-panel.component';
import { StyleDetailsComponent } from './pages/style-details/style-details.component';
import { StyleBasicModule } from './components/style-basic/style-basic.module';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { InventoryModule } from '../inventory/inventory.module';
import { HeatmapModule } from './components/heatmap/heatmap.module';
import { ColorPickerModule } from 'ngx-color-picker';
import { StyleDetailsCostingComponent } from './pages/style-details/tab-costing/style-details-costing.component';
import { StyleDetailsBasicComponent } from './pages/style-details/tab-basic/style-details-basic.component';
import { StyleDetailsInventoryComponent } from './pages/style-details/tab-inventory/style-details-inventory.component';
import { StyleDetailsInventoryFlowComponent } from './pages/style-details/tab-inventory-flow/style-details-inventory-flow.component';
import { InventoryAddEditModalModule } from '../inventory/components/inventory-add-edit-modal/inventory-add-edit-modal.module';
import { StyleUpdateModalComponent } from './components/style-update-modal/style-update-modal.component';
import { DragulaModule } from 'ng2-dragula';

const routes = [
  {
    path: 'styles',
    component: StyleListComponent,
    data: { permission: 'PERMISSION_MENU_STYLE_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  },
  {
    path: 'styles/:id',
    component: StyleDetailsComponent,
    data: { permission: 'PERMISSION_MENU_STYLE_VIEW' },
    canActivate: [LoginCheckRouteGuard],
    children: [
      {
        path: 'basic',
        component: StyleDetailsBasicComponent,
        canActivate: [LoginCheckRouteGuard],
      },
      {
        path: 'cost',
        data: { permission: 'PERMISSION_STYLE_COST_VIEW' },
        component: StyleDetailsCostingComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
      },
      {
        path: 'inventory',
        component: StyleDetailsInventoryComponent,
        canActivate: [LoginCheckRouteGuard],
      },
      {
        path: 'inventory-flow',
        component: StyleDetailsInventoryFlowComponent,
        canActivate: [LoginCheckRouteGuard],
      },
    ]
  }
];
@NgModule({
  declarations: [
    StyleListComponent,
    StyleListRightPanelComponent,
    StyleDetailsComponent,
    StyleDetailsCostingComponent,
    StyleDetailsBasicComponent,
    StyleDetailsInventoryComponent,
    StyleDetailsInventoryFlowComponent,
    StyleUpdateModalComponent,
  ],
  entryComponents: [
    StyleUpdateModalComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    DateTimeModule,
    AttachmentModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
    ColorPickerModule,
    UserAutocompleteModule,
    StyleBasicModule,
    InventoryModule,
    HeatmapModule,
    InventoryAddEditModalModule,
    DragulaModule.forRoot(),
  ],
  exports: [
  ]
})
export class StyleModule {
}
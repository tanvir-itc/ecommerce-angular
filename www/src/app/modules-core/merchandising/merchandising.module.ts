
import { NgModule } from "@angular/core";
import { StyleModule } from "./style/style.module";
import { OrderModule } from "./order/order.module";
import { CustomerModule } from "./customers/customer.module";
import { InventoryModule } from "./inventory/inventory.module";
import { SalesRepModule } from "./sales-rep/sales-rep.module";
import { WarehouseUserModule } from "./warehouse-user/warehouse-user.module";
import { NewOrderModule } from "./new-order/new-order.module";
import { PartnerModule } from "./partner/partner.module";
import { InvoiceModule } from "./invoice/invoice.module";
import { LoginHistoryModule } from "./login-history/login-history.module";
import { ShippingModule } from "./shipping/shipping.moudle";
import { InventoryTransferModule } from "./inventory-transfer/inventory-transfer.module";
import { InventoryHistoryModule } from "./inventory-history/inventory-history.module";
import { CommissionModule } from "./commission/commission.module";
import { InvestorModule } from "./investor/investor.module";
import { DigitalCatalogueModule } from "./digital-catalogue/digital-catalogue.module";
import { FactoryShipmentsModule } from "./factory-shipments/factory-shipment.module";
import { FactoryUserModule } from "./factory-user/factory-user.module";
import { SupplierModule } from "./supplier/supplier.module";
import { POModule } from "./po/po.module";
import { NgxSummernoteModule } from "ngx-summernote";

@NgModule({
  imports: [
    NewOrderModule,
    StyleModule,
    OrderModule,
    CustomerModule,
    InventoryModule,
    InvoiceModule,
    SalesRepModule,
    PartnerModule,
    InvestorModule,
    WarehouseUserModule,
    LoginHistoryModule,
    ShippingModule,
    InventoryTransferModule,
    InventoryHistoryModule,
    DigitalCatalogueModule,
    CommissionModule,
    FactoryShipmentsModule,
    FactoryUserModule,
    SupplierModule,
    POModule,
    NgxSummernoteModule
  ],
  exports: [

  ]
})
export class MerchandisingModule { }
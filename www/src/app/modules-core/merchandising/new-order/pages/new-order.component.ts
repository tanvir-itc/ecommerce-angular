import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { MatDialog, MatStepper } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import { fuseAnimations } from "@fuse/animations";
import { AuthService } from "app/authentication/auth.service";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { SizeService } from "app/modules-core/common/size/size.service";
import { StyleTypeService } from "app/modules-core/common/style-type/style-type.service";
import { customSortBy, integerArrayToString, integerStringToInteger, isDateBefore, removeEmptyFields, stringToIntegerArray, toDateString, toDateTimeString } from "app/modules-core/utility/helpers";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import * as _ from "lodash";
import { forkJoin } from "rxjs";
import { CustomerService } from "../../customers/customer.service";
import { InventoryService } from "../../inventory/inventory.service";
import { OrderService } from "../../order/order.service";
import { StyleService } from "../../style/style.service";
import * as moment from "moment";
import { ProvinceService } from "app/modules-core/common/province/province.service";
import { CustomerAddModalComponent } from "../../customers/components/customer-add-modal/customer-add-modal.component";
import { CustomerAddressService } from "../../customers/customer-address.service";
import { CustomerDetailsUpdateModalComponent } from "../../customers/components/customer-details-update-modal/customer-details-update-modal.component";
import { StripePaymentComponent } from "./stripe-payment.component";


@Component({
  selector: "new-order",
  templateUrl: "./new-order.component.html",
  styleUrls: ['./new-order.component.scss'],
  animations: fuseAnimations,
})
export class NewOrderComponent implements OnInit {
  @ViewChild('stepper') private cartStepper: MatStepper;

  @ViewChild('customer') customerModal: TemplateRef<any>;

  @ViewChild('cart') cartModal: TemplateRef<any>;

  @ViewChild('styleAddOnlyModal') styleAddOnlyModal: TemplateRef<any>;

  filterMore = false;
  filterCounter = 0;
  filter: any = {};
  btnDisable = false;

  styles: any[] = [];

  styleId: any = null;
  styleGroupId: any = null;
  style: any = null;

  styleTypeList: any[] = [];

  userRole: any = null;


  customerList: any[] = [];
  customerId: any = null;
  customerPriceType = null;
  minInvQtyShowOnOrder = null;

  modalRefDetails: any = null;
  modalRefCard: any = null;
  modalRefStyleOnly: any = null;

  newOrderObject: any = null;

  provinceList: any = [];

  modalRefCustomer: any = null;

  color: any = null;

  cartOpen: any = null;

  tempDetailsOpen: any = {
    type: null,
    style: null,
  }
  stripeKey = null;
  shippingAddressList: any[] = [];
  billingAddressList: any[] = [];
  selectedCustomer = null;
  quantityValidation: any = null;

  constructor(
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _styleService: StyleService,
    private _styleTypeService: StyleTypeService,
    public _orderService: OrderService,
    public _customerService: CustomerService,
    public _inventoryService: InventoryService,
    public _sizeService: SizeService,
    public _provinceService: ProvinceService,
    public _customerAddressService: CustomerAddressService,
  ) {
    this._quickPanelSubjectService.heading.next("Wholesale Catalogue");
  }
  ngOnInit() {

    this.clearAllFilter();
    console.log("24June23")
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));
    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.customerId = _.get(this.filter, 'customerId', null);
    this.cartOpen = _.get(this.filter, 'cartOpen', null);
    this.applyFilters(this.filter);
    this.getAllStyleTypes();
    this.getAllCustomers();
    this.getAllProvinces();
    this.getStripeKey();
    this.userRole = _.chain(this._auth.getCurrentUser()).get('roles').head().get('name').value();
  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null,
      styleid: null,
      stylename: null,
      styletypeid: [],
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "styletypeid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }
  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "styletypeid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    params['showorderbalance'] = true;
    params['archive'] = '0';
    params['sort'] = 's.sequence'
    this._styleService.getAll(params).subscribe((data) => {
      this.styles = data;
      this.setFilterToURL(filterParams);
    });
  }

  getAllStyleTypes() {
    this._styleTypeService.getAll().subscribe((data) => {
      this.styleTypeList = data;
    });
  }


  getAllCustomers() {
    this._customerService.getAll({ sort: 'name', all: false, requested: false, }).subscribe((data) => {
      this.customerList = data;
      if (this.userRole == 'ROLE_CUSTOMER') {
        let customer = _.chain(this.customerList).find(['id', this._auth.getCurrentUser().customerId]).value();
        this.customerId = _.get(customer, 'id');
        this.customerPriceType = `price_${_.get(customer, 'priceTypeId')}`;
        this.minInvQtyShowOnOrder = _.get(customer, 'minInvQtyShowOnOrder')
        if (this.cartOpen) {
          this.onCartOpen(this.cartModal);
        }
        this.getOrder();
      } else if (this.cartOpen) {
        this.onCartOpen(this.cartModal);
        if (this.customerId) {
          this.onCustomerChange();
        }
      }
    });
  }

  onCustomerChange() {
    let customer = _.chain(this.customerList).find(['id', this.customerId]).value();
    this.customerPriceType = `price_${_.get(customer, 'priceTypeId')}`;
    this.minInvQtyShowOnOrder = _.get(customer, 'minInvQtyShowOnOrder')
    debugger
    this.getOrder();
  }


  goLink(styleId) {
    this._router.navigateByUrl(`styles/${styleId}`);
  }



  /// order


  editFlag: any = false;
  sizes: any[] = [];

  styleInventory: any[] = [];
  showAddSection = false;
  order: any = null;

  getOrder(flag = false) {
    this.order = null;
    if (this.customerId) {
      this._orderService.getOrder({ customerid: this.customerId, statusname: 'draft' }).subscribe((data) => {
        this.order = data;
        let styleList = _.chain(this.order).get('groupBreakdown').map('styleBreakdown').flatten().map('styleId').value();
        _.each(this.styles, style => {
          style['addedToOrder'] = _.includes(styleList, style.id) ? true : false;
        });
        if (_.get(this.order, 'orderCreateTime')) {
          var stillUtc = moment.utc(this.order.orderCreateTime).toDate();
          this.order['localOrderCreateTime'] = moment(stillUtc).local().add(3, 'hours').format('YYYY-MM-DD HH:mm:ss');
        }
        // open modal
        if (flag) {
          if (this.tempDetailsOpen.type == 'cart') {
            this.onCartOpen(this.cartModal);
          }

          if (this.tempDetailsOpen.type == 'style') {
            this.openStyleAddOnlyModal(this.tempDetailsOpen.style, this.styleAddOnlyModal);
          }
          this.tempDetailsOpen = {
            type: null,
            style: null,
          }
        }
      });
    }

  }

  openStyleCardDetailsModal(styleId, templateRef) {
    this.color = null;
    this.showAddSection = false;
    this.styleId = styleId;
    this.getStyleDetails();
    this.modalRefDetails = this.dialog.open(templateRef, {
      minWidth: '60%',
      maxWidth: '960px',
      disableClose: true,
    });
  }

  getStyleDetails() {
    this.style = null;
    this._styleService.get(this.styleId).subscribe((style) => {
      this.style = style;
      this.color = _.chain(this.style).get('combos').find(['primary', true]).value();
    });
  }



  getStyleBreakdown() {
    this.sizes = [];
    this.styleInventory = [];


    let alreadyOrderedStyle = null;

    _.each(_.get(this.order, 'orderDetails'), group => {
      alreadyOrderedStyle = _.find(_.get(group, 'orderDetail'), ['styleId', this.styleId]);
      if (alreadyOrderedStyle) {
        return false;
      }
    });
    console.log('alreadyOrderedStyle', alreadyOrderedStyle)

    if (alreadyOrderedStyle) {
      this.editFlag = true;
    } else {
      this.editFlag = false;
    }
    let params = {
      styleid: this.styleId,
      showorderbalance: true,
      archive: false,
      orderid: _.get(this.order, 'id', 0)
    }

    let createBetweenTime = false;
    if (_.get(this.order, 'localOrderCreateTime')) {
      createBetweenTime = moment(_.get(this.order, 'localOrderCreateTime')).isAfter(toDateTimeString(new Date));
    }

    forkJoin([
      this._styleService.getColors({ styleid: this.styleId, order: true, orderid: _.get(this.order, 'id', 0) }),
      this._inventoryService.getAll(params),
      this._sizeService.getAll({ sizegroupid: this.styleGroupId }),
    ]).subscribe((results) => {
      console.log(results)
      this.sizes = results[2];
      var inventoryQuantity = _.head(results[1]);
      if (inventoryQuantity) {
        _.each(results[0], (color) => {
          if (color.name.includes("In Stock")) {
            let status = 'In Stock'
            color['displayName'] = color.name.slice(0, color.name.indexOf('In Stock'))
            color['colorStatus'] = color.name.slice(color.name.indexOf('In Stock'), color.name.indexOf('In Stock') + (status.length + 1))
            color['stockDate'] = color.name.slice((color.name.indexOf('In Stock') + status.length + 1))
          } else if (color.name.includes("Available")) {
            color['status'] = 'available'
            color['displayName'] = color.name.slice(0, color.name.indexOf('Available'))
            color['colorStatus'] = color.name.slice(color.name.indexOf('Available'), color.name.length)
          } else {
            color['displayName'] = color.name
          }
          // color['displayName'] = color.name.includes("In Stock") ? color.name.substring(0, color.name.indexOf('In Stock')) : color.name;
          let quantities = {
            color: color,
            sizes: [],
            stylePrice: _.chain(this.style).get('priceObject').get(this.customerPriceType, this.style.defaultPrice).value(),
          };
          _.each(this.sizes, (size) => {
            let item = _.find(_.get(inventoryQuantity, "inventories"), {
              sizeId: size.id,
              styleCombo: color.name,
            });

            let orderedQty = null;
            if (alreadyOrderedStyle) {
              orderedQty = _.chain(alreadyOrderedStyle).get('orderDetail').find({ 'sizeId': size.id, 'combo': color.name }).get('quantity').value();
            }
            if (_.get(item, 'quantity') > -1) {
              /*if (this.editFlag && createBetweenTime) {
                item['quantity'] = _.add(_.get(item, 'quantity', 0), orderedQty || 0);
              } else {
                */item['quantity'] = _.get(item, 'quantity', 0);

              //}
            }

            let sizeObj = {
              size: size,
              item: item ? item : orderedQty && this.editFlag /*&& createBetweenTime*/ ? _.chain(alreadyOrderedStyle).get('orderDetail').find({ 'sizeId': size.id, 'combo': color.name }).value() : { quantity: null },
              quantity: orderedQty ? orderedQty : null,
              po: item ? _.get(_.find(_.filter(_.get(inventoryQuantity, "comboList"), 'poId'), ['name', item.styleCombo]), 'poId', null) : null,
            };
            quantities.sizes.push(_.merge({}, sizeObj));
          });
          this.styleInventory.push(quantities);
        });
      }
      this.showAddSection = true;
      this.totalStyleCalculation();
      console.log('this.styleInventory', this.styleInventory)
      // console.log('this.order', this.order)
    });


  }

  totalStyleCalculation() {
    let quantityValidation = null;
    _.each(this.styleInventory, color => {
      let total = 0;
      _.each(color.sizes, item => {
        total = _.add(total, parseInt(item.quantity || 0));
        if (_.parseInt(item.quantity) > _.parseInt(item.item.quantity)) {
          quantityValidation = item;
        }
      });

      color['totalQuantity'] = total;
      color['totalPrice'] = this.style.discountFlag ? _.multiply(color.totalQuantity, _.get(this.style, 'defaultPriceDis', 0)) : _.multiply(color.totalQuantity, _.get(color, 'stylePrice', 0));
    });
    if (quantityValidation) {
      this.quantityValidation = quantityValidation;
    } else {
      this.quantityValidation = null
    }
    console.log('this.styleInventory', this.styleInventory)
  }



  addStyle() {
    if (!this.customerId) {
      this.snackBar.open(`Please, First select customer for item add!`, "Close");
      this.openCustomerModal();
      return;
    }
    this.btnDisable = true;
    var payload = [];
    var error = false;

    _.each(this.styleInventory, (color) => {
      _.each(color.sizes, (size) => {
        if (_.get(size, "item.sizeId") && (_.get(size, "item.styleComboId") || _.get(size, "item.comboId"))) {
          payload.push({
            styleId: _.get(color, "color.styleId"),
            quantity: _.get(size, "quantity") || 0,
            sizeId: _.get(size, "item.sizeId"),
            comboId: _.get(size, "item.styleComboId") || _.get(size, "item.comboId"),
            poId: _.get(size, "po") ? _.get(size, "po") : null
          });
        }
        if (_.get(size, "quantity", 0) > _.get(size, "item.quantity", 0)) {
          error = true;
        }
      });
    });
    if (!payload.length) {
      this.snackBar.open(`Please! Insert minimum quantity!`, "Close");
      this.btnDisable = false;;
      return;
    }
    if (error) {
      this.snackBar.open(`Order quantity can not be more than inventory quantity!`, "Close");
      this.btnDisable = false;
      return;
    }

    let finalPayload = {
      customerId: this.customerId,
      orderDetail: payload
    }
    this._orderService.addOrderItems(finalPayload).subscribe((resp) => {
      this.getOrder();
      this.snackBar.open(`Style quantity added!`, "Close");
      this.btnDisable = false;
      this.modalRefStyleOnly && this.modalRefStyleOnly.close();
    }, err => {
      this.btnDisable = false;
    });
  }


  onCartOpen(templateRef) {

    this.btnDisable = false;
    this.modalRefDetails && this.modalRefDetails.close();
    if (!this.customerId) {
      this.tempDetailsOpen = {
        type: 'cart',
        id: null,
      }
      this.snackBar.open(`Please, First select customer for item add!`, "Close");
      this.openCustomerModal();
      return;
    }

    this.orderValidation();

    this.clearNewOrder();
    this.getShippingAddressList()
    this.getBillingAddressList()
    this.selectedCustomer = _.chain(this.customerList).find(['id', this.customerId]).value();

    this.newOrderObject['name'] = _.get(this.selectedCustomer, 'name');
    this.newOrderObject['shipmentMode'] = _.get(this.order, 'shippingMode') || _.get(this.selectedCustomer, 'shipmentMode');
    this.newOrderObject['courierCompany'] = _.get(this.order, 'courierCompany') || _.get(this.selectedCustomer, 'courierCompany');
    this.newOrderObject['courierAccountNo'] = _.get(this.order, 'courierAccountNo') || _.get(this.selectedCustomer, 'courierAccountNo');
    this.newOrderObject['shipmentModeInstruction'] = _.get(this.order, 'shipmentModeInstruction') || null;
    this.newOrderObject['orderDate'] = _.get(this.order, 'orderDate') || null;
    this.newOrderObject['comment'] = _.get(this.order, 'comment') || null;
    this.newOrderObject['poNo'] = _.get(this.order, 'poNo') || null;
    this.newOrderObject['shipmentDate'] = _.get(this.order, 'shipmentDate') || null;
    this.newOrderObject['stepFlag'] = 'breakdown';
    this.newOrderObject['currentDate'] = toDateString(new Date);


    this.modalRefCard = this.dialog.open(templateRef, {
      minWidth: '50%',
      maxWidth: '960px',
      disableClose: true,
    });
  }

  clearNewOrder() {
    this.newOrderObject = {
      comment: null,
      shipmentModeInstruction: null,
      orderDate: toDateString(new Date()),
      poNo: null,
      shipmentDate: null,
      shipmentMode: null,

      courierAccountNo: null,
      courierCompany: null,

      street: null,
      poBoxNo: null,
      city: null,
      postCode: null,
      provinceId: null,
      shippingAddressId: null,
      billingAddressId: null,
    }
  }

  getShippingAddressList() {
    this._customerAddressService.getAll({ 'customer.id': this.customerId, addressType: "SHIPPING" }).subscribe((resp) => {
      this.shippingAddressList = resp;
      const address = _.find(this.shippingAddressList, ['default', true]);
      this.newOrderObject['shippingAddressId'] = _.get(address, 'id');
      this.newOrderObject['street'] = _.get(address, 'street');
      this.newOrderObject['poBoxNo'] = _.get(address, 'poBoxNo');
      this.newOrderObject['city'] = _.get(address, 'city');
      this.newOrderObject['postCode'] = _.get(address, 'postCode');
      this.newOrderObject['provinceId'] = _.get(address, 'provinceId');
    });
  }
  getBillingAddressList() {
    this._customerAddressService.getAll({ 'customer.id': this.customerId, addressType: "BILLING" }).subscribe((resp) => {
      this.billingAddressList = resp;
      const address = _.find(this.billingAddressList, ['default', true]);
      this.newOrderObject['billingAddressId'] = _.get(address, 'id');
      this.newOrderObject['billingStreet'] = _.get(address, 'street');
      this.newOrderObject['billingPoBoxNo'] = _.get(address, 'poBoxNo');
      this.newOrderObject['billingCity'] = _.get(address, 'city');
      this.newOrderObject['billingPostCode'] = _.get(address, 'postCode');
      this.newOrderObject['billingProvinceId'] = _.get(address, 'provinceId');
    });
  }

  onShippingAddressChange() {
    const address = _.find(this.shippingAddressList, ['id', this.newOrderObject.shippingAddressId]);
    this.newOrderObject['street'] = _.get(address, 'street');
    this.newOrderObject['poBoxNo'] = _.get(address, 'poBoxNo');
    this.newOrderObject['city'] = _.get(address, 'city');
    this.newOrderObject['postCode'] = _.get(address, 'postCode');
    this.newOrderObject['provinceId'] = _.get(address, 'provinceId');
  }
  onBillingAddressChange() {
    const address = _.find(this.billingAddressList, ['id', this.newOrderObject.billingAddressId]);
    this.newOrderObject['billingStreet'] = _.get(address, 'street');
    this.newOrderObject['billingPoBoxNo'] = _.get(address, 'poBoxNo');
    this.newOrderObject['billingCity'] = _.get(address, 'city');
    this.newOrderObject['billingPostCode'] = _.get(address, 'postCode');
    this.newOrderObject['billingProvinceId'] = _.get(address, 'provinceId');
  }

  openStyleAddOnlyModal(style, templateRef) {
    this.btnDisable = false;
    this.style = style;
    if (!this.customerId) {
      this.tempDetailsOpen['type'] = 'style';
      this.tempDetailsOpen['style'] = _.merge({}, style);
      this.snackBar.open(`Please, First select customer for item add!`, "Close");
      this.openCustomerModal();
      return;
    }
    this.styleId = _.get(style, 'id') || _.get(style, 'styleId');
    this.styleGroupId = _.get(style, 'sizeGroupId');
    this.getStyleBreakdown();
    this.modalRefStyleOnly = this.dialog.open(templateRef, {
      minWidth: '20%',
      maxWidth: '960px',
      disableClose: true,
    });
  }



  goBack() {
    this.cartStepper.previous();
    this.newOrderObject['stepFlag'] = 'breakdown';
  }

  goForward() {
    this.cartStepper.next();
    this.newOrderObject['stepFlag'] = 'shipment';
  }


  onOrderSubmit(flag) {
    if (!this.customerId) {
      this.snackBar.open(`Please, First select customer for item add/update!`, "Close");
      this.openCustomerModal();
      return;
    }
    this.btnDisable = true;
    let payload = _.merge({}, this.newOrderObject);

    if (!_.get(payload, "shipmentDate")) {
      this.snackBar.open(`Please select Requested Ship Date.`, "Close");
      this.btnDisable = false;
      return;
    }
    if (isDateBefore(_.get(payload, "shipmentDate"), moment())) {
      this.snackBar.open(`Please! Select future date!`, "Close");
      this.btnDisable = false;
      return;
    }

    if (!_.get(payload, "shipmentMode")) {
      this.snackBar.open(`Please! Select shipment mode!`, "Close");
      this.btnDisable = false;
      return;
    } else {
      if (_.get(payload, "shipmentMode") == 'CUSTOMER' && !_.get(payload, "courierCompany") && !_.get(payload, "courierAccountNo")) {
        this.snackBar.open(`Please! Enter courier company and courier account not properly!`, "Close");
        this.btnDisable = false;
        return;
      } else if (_.get(payload, "shipmentMode") == 'RAWMANCO') {
        payload['courierCompany'] = null;
        payload['courierAccountNo'] = null;
        payload['shipmentModeInstruction'] = null;
      } else if (_.get(payload, "shipmentMode") == 'OTHER') {
        payload['courierCompany'] = null;
        payload['courierAccountNo'] = null;
      }
    }

    if (
      !_.get(payload, 'street') ||
      !_.get(payload, 'city') ||
      !_.get(payload, 'postCode') ||
      !_.get(payload, 'provinceId')
    ) {
      this.snackBar.open(`Please! Enter all required shipping address fields!`, "Close");
      this.btnDisable = false;
      return;
    }

    if (_.get(payload, 'billingSameAsShipping')) {
      payload.billingStreet = _.get(payload.billingStreet, 'street');
      payload.billingPoBoxNo = _.get(payload.billingStreet, 'poBoxNo');
      payload.billingCity = _.get(payload.billingStreet, 'city');
      payload.billingPostCode = _.get(payload.billingStreet, 'postCode');
      payload.billingProvinceId = _.get(payload.billingStreet, 'provinceId');
    } else {
      if (
        !_.get(payload, 'billingStreet') ||
        !_.get(payload, 'billingCity') ||
        !_.get(payload, 'billingPostCode') ||
        !_.get(payload, 'billingProvinceId')
      ) {
        this.snackBar.open(`Please! Enter all required billing address fields!`, "Close");
        this.btnDisable = false;
        return;
      }
    }

    payload['statusName'] = 'new';
    payload['customerId'] = this.customerId;

    if (_.get(this.selectedCustomer, 'paymentRequired')) {
      //let taxValue = (_.get(this.order,'totalOrderedValue',0) * _.get(this.order,'provinceRaxRate',0) ) / 100;
      let taxValue = _.get(this.order, 'tax', 0);
      this.order['taxValue'] = taxValue;
      // console.log('this.order', _.get(this.order,'shippingCharge',0) )
      this.order['totalPaymentAmount'] = _.multiply(_.get(this.order, 'totalOrderedValue', 0) + taxValue + _.get(this.order, 'shippingCharge', 0) + _.get(this.order, 'ccUpCharge', 0), 100);
      this.order['totalPaymentAmountForDisplay'] = _.get(this.order, 'totalOrderedValue', 0) + taxValue + _.get(this.order, 'shippingCharge', 0) + _.get(this.order, 'ccUpCharge', 0)
      this.order['stripeKey'] = _.get(this.stripeKey, 'publicKey')
      // STRIPE
      const dialogRef = this.dialog.open(StripePaymentComponent, {
        // opening dialog here which will give us back stripeToken
        width: '24vw',
        disableClose: true,
        data: {
          // totalAmount: 1000,
          order: _.merge({}, this.order)
        },
      });
      dialogRef.afterClosed()
        // waiting for stripe token that will be given back
        .subscribe((result: any) => {
          this.btnDisable = false;
          if (result) {
            payload['stripeToken'] = result.token.id;
            // this.createOrder(result.token.id);

            this._orderService.orderPlace(payload).subscribe((resp) => {
              this._router.navigateByUrl(`orders`);
              this.snackBar.open(`Order placed!`, "Close");
              this.btnDisable = false;
              this.modalRefCard.close();
            }, err => {
              this.btnDisable = false;
            });
          }
        });
      // STRIPE
    } else {
      this._orderService.orderPlace(payload).subscribe((resp) => {
        this._router.navigateByUrl(`orders`);
        this.snackBar.open(`Order placed!`, "Close");
        this.btnDisable = false;
        this.modalRefCard.close();
      }, err => {
        this.btnDisable = false;
      });

    }
  }

  getAllProvinces() {
    this._provinceService.getAll().subscribe((data) => {
      this.provinceList = data;
    });
  }

  openCustomerModal() {
    this.modalRefCustomer = this.dialog.open(this.customerModal, {
      minWidth: '400px',
      disableClose: true,
    });
  }
  onModalCustomerChange() {
    let customer = _.chain(this.customerList).find(['id', this.customerId]).value();
    this.customerPriceType = `price_${_.get(customer, 'priceTypeId')}`;
    this.minInvQtyShowOnOrder = _.get(customer, 'minInvQtyShowOnOrder')
    this.getOrder(true);
    this.modalRefCustomer.close();

  }


  onOrderStyleDelete(orderId, styleId) {
    if (!confirm("Order style delete?")) {
      return;
    }

    this._orderService.orderStyleDelete(orderId, styleId).subscribe(res => {
      this.snackBar.open(`Order Style Deleted!`, "Close");
      this.getOrder();
    });
  }


  loadColorPhotos(color) {
    this.color = color;
  }


  openDiscountModal(templateRef) {
    _.each(this.order.groupBreakdown, group => {
      _.each(group.styleBreakdown, style => {
        style['newStyleOrderedValue'] = _.get(style, 'styleOrderedValue');
        style['newPrice'] = _.get(style, 'price') || _.get(style, 'actualPrice') || _.get(style, 'defaultPrice')
      });
    });
    this.modalRefStyleOnly = this.dialog.open(templateRef, {
      minWidth: '20%',
      maxWidth: '960px',
      disableClose: true,
    });
  }

  applyDiscount() {
    let error = false;

    let payload = {
      id: this.order.id,
      orderDetail: [],
    };
    _.each(this.order.groupBreakdown, group => {
      _.each(group.styleBreakdown, style => {
        if (style.newPrice < style.minPrice) {
          error = true;
        }
        if (style.newPrice) {
          let item = {
            styleId: _.get(style, 'styleId'),
            price: _.get(style, 'newPrice'),
            discount: _.subtract(_.get(style, 'actualPrice', 0), _.get(style, 'newPrice')),
          }
          payload.orderDetail.push(item);
        }
      });
    });

    if (error) {
      this.snackBar.open(`Style price can not be less than minimum style price!`, "Close");
      return;
    }
    this._orderService.updateDiscount(payload).subscribe(resp => {
      this.snackBar.open(`Order discount applied!`, "Close");
      this.modalRefStyleOnly.close();
      this.getOrder();
    });
  }





  onDiscountValueKeyup() {
    _.each(this.order.groupBreakdown, group => {
      _.each(group.styleBreakdown, style => {
        style['newStyleOrderedValue'] = _.multiply(_.get(style, 'newPrice'), _.get(style, 'styleOrderedQty'));
      });
    });
  }




  openCreateModal() {
    const colorModal = this.dialog.open(CustomerAddModalComponent, {
      minWidth: '50%',
      maxWidth: '960px',
      disableClose: true,
      data: {
      }
    });
    colorModal.afterClosed().subscribe(result => {
      result && this.getAllCustomers();
    });
  }


  orderValidation() {
    let payload = {
      customerId: this.customerId,
    }

    this._orderService.orderValidation(payload).subscribe(resp => {
    }, err => {
      this.getOrder();
    });;
  }

  openCustomerUpdateModal() {
    const colorModal = this.dialog.open(CustomerDetailsUpdateModalComponent, {
      minWidth: '60%',
      maxWidth: '960px',
      disableClose: true,
      data: {
        customerId: this.customerId,
        typeFlag: 'update',
      }
    });
    colorModal.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
      this.getShippingAddressList();
      this.getBillingAddressList();
    });
  }
  // stripe
  checkout() {
    const dialogRef = this.dialog.open(StripePaymentComponent, {
      // opening dialog here which will give us back stripeToken
      width: '40vw',
      disableClose: true,
      data: { totalAmount: 1000 },
    });
    dialogRef.afterClosed()
      // waiting for stripe token that will be given back
      .subscribe((result: any) => {
        if (result) {
          // this.createOrder(result.token.id);
        }
      });
  }

  getStripeKey() {
    this._orderService.getStripeKey().subscribe((data) => {
      this.stripeKey = data;
    });
  }
  // stripe

} //class

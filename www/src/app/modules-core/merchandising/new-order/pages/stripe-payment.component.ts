import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, Inject, OnDestroy, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import * as _ from "lodash";
@Component({
  selector: 'app-stripe-payment',
  templateUrl: './stripe-payment.component.html'
})
export class StripePaymentComponent implements OnDestroy, AfterViewInit {
  @ViewChild('cardInfo') cardInfo: ElementRef;
  _totalAmount: number;
  card: any;
  orderData: any = {};
  cardHandler = this.onChange.bind(this);
  cardError: string;
  stripe; 
  elements ;
 
  constructor(
    private cd: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<StripePaymentComponent>,
  ) {
    this._totalAmount = data['totalAmount'];
    this.orderData = data.order;
    this.stripe = Stripe(_.get(this.orderData,'stripeKey'))
    this.elements =this.stripe.elements();
  }
  ngOnDestroy() {
    if (this.card) {
      // We remove event listener here to keep memory clean
      this.card.removeEventListener('change', this.cardHandler);
      this.card.destroy();
    }
  }
  ngAfterViewInit() {
    
    this.initiateCardElement();
  }
  initiateCardElement() {
    // Stripe()
     
    // Giving a base style here, but most of the style is in scss file
    const cardStyle = {
      base: {
        color: '#303238',
        fontFamily: '"Open Sans"',
        fontSmoothing: 'antialiased',
        fontSize: '6em',
        lineHeight: '6em',
        '::placeholder': {
          color: '#aab7c4',
          fontSeze: '6em'
        },
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a',
      },
    };
    this.card = this.elements.create('card', { cardStyle, hidePostalCode: true });
    this.card.mount(this.cardInfo.nativeElement);
    this.card.addEventListener('change', this.cardHandler);

  }
  onChange({ error }) {
    if (error) {
      this.cardError = error.message;
    } else {
      this.cardError = null;
    }
    this.cd.detectChanges();
  }
  async createStripeToken() {
    const { token, error } = await this.stripe.createToken(this.card);
    if (token) {
      this.onSuccess(token);
    } else {
      this.onError(error);
    }
  }
  onSuccess(token) {
    this.dialogRef.close({ token });
  }
  onError(error) {
    if (error.message) {
      this.cardError = error.message;
    }
  }

  modalClose(){
    this.dialogRef.close();
  }
}
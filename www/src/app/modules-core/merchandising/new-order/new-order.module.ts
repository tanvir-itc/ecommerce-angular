import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from '../../utility/date-time-picker/date-time.module';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { NewOrderComponent } from './pages/new-order.component';
import { StyleBasicModule } from '../style/components/style-basic/style-basic.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { CustomerAddModalModule } from '../customers/components/customer-add-modal/customer-add-modal.module';
import { StripePaymentComponent } from './pages/stripe-payment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


const routes = [
  {
    path: 'catalogue',
    component: NewOrderComponent,
    data: { permission: 'PERMISSION_MENU_CATALOGUE_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  },
];
@NgModule({
  declarations: [
    NewOrderComponent,
    StripePaymentComponent
  ],
  entryComponents: [
    StripePaymentComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    AttachmentModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
    AttachmentModule,
    StyleBasicModule,
    CustomerAddModalModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
  ]
})
export class NewOrderModule {
}
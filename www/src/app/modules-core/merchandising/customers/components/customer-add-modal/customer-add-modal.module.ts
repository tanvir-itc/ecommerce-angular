import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { RouterModule } from '@angular/router';
import { CustomerAddModalComponent } from './customer-add-modal.component';
import { GeneralModule } from 'app/modules-core/general/general.module';

@NgModule({
  declarations: [
    CustomerAddModalComponent,
  ],
  entryComponents: [
    CustomerAddModalComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FuseSharedModule,
    RouterModule,
    ButtonModule,
    DateTimeModule,
    GeneralModule,
  ],
  exports: [
    CustomerAddModalComponent,
  ]
})

export class CustomerAddModalModule {
}
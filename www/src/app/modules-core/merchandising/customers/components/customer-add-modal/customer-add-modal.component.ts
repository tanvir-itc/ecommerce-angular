import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import * as _ from 'lodash';
import { AuthService } from "app/authentication/auth.service";
import { CustomerService } from "../../customer.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { ProvinceService } from "app/modules-core/common/province/province.service";
import { UserService } from "app/modules-core/common/user/user.service";
import { APP_URL } from 'app/app.constants';
import { PriceTypeService } from "app/modules-core/common/price-type/price-type.service";

@Component({
  selector: 'customer-add-modal',
  templateUrl: 'customer-add-modal.component.html',
})
export class CustomerAddModalComponent {

  newCustomer: any = null;

  newBillingAddress: any = {};

  newUser: any = {};

  newUserFlag = true;

  newBillingAddressFlag = true;

  priceTypeList: any[] = [];
  provinceList: any[] = [];

  salesRepList: any[] = [];

  userRole = null;
  userRoleUser = null;

  constructor(
    public dialogRef: MatDialogRef<CustomerAddModalComponent>,
    public _auth: AuthService,
    private _customerService: CustomerService,
    private snackBar: SnackbarService,
    private _provinceService: ProvinceService,
    private _priceTypeService: PriceTypeService,
    private _userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.userRole = _.chain(this._auth.getCurrentUser()).get('roles').head().get('name').value();
    this.userRoleUser = this._auth.getCurrentUser();
    this.clearCreateObject();
    this.getStylePriceTypes();
    this.getAllProvinces();
    this.getSalesReps();
  }





  clearCreateObject() {
    this.newUserFlag = true;
    this.newBillingAddressFlag = true;

    this.newCustomer = {
      name: null,
      email: null,
      phoneNo: null,
      contactFirstName: null,
      contactLastName: null,
      priceTypeId: null,
      shipmentMode: null,
      preShipmentApproval: false,
      sendEmail: true,
      courierAccountNo: null,
      courierCompany: null,
      description: null,
      shippingCustomerAddress: {
        street: null,
        poBoxNo: null,
        city: null,
        postCode: null,
        provinceId: null,
      },

      saleRepCustomers: [{ userId: null }],
      ccUpCharge: 3,
      paymentRequired: false,
    };
    this.newBillingAddress = {
      street: null,
      poBoxNo: null,
      city: null,
      postCode: null,
      provinceId: null,
    }
  }

  createCustomer() {
    let payload = this.getPayload();
    if (this.validate(payload)) {
      return true;
    }
    payload['loginUserId'] = this._auth.getCurrentUser().id;
    this._customerService.create(payload).subscribe((resp) => {

      this.snackBar.open(`Customer created!`, "Close");
      this.dialogRef.close(true);
    });
  }

  validate(payload) {
    var error = false;
    if (
      !_.get(payload, 'name') ||
      !_.get(payload, 'email') ||
      !_.get(payload, 'contactFirstName') ||
      !_.get(payload, 'contactLastName')
    ) {
      error = true;
    }
    if (error) {
      this.snackBar.open(`Please, enter required fields!`, "Close");
      return true;
    }
    return false;
  }

  getPayload() {
    let payload = _.merge({}, this.newCustomer);
    if (!this.newBillingAddressFlag) {
      payload = _.merge(payload, { billingCustomerAddress: { ...this.newBillingAddress } });
    } else {
      payload = _.merge(payload, {
        billingCustomerAddress: {
          street: _.get(payload, 'shippingCustomerAddress.street'),
          poBoxNo: _.get(payload, 'shippingCustomerAddress.poBoxNo'),
          city: _.get(payload, 'shippingCustomerAddress.city'),
          postCode: _.get(payload, 'shippingCustomerAddress.postCode'),
          provinceId: _.get(payload, 'shippingCustomerAddress.provinceId'),
          isBillingSameAsShipping: true,
        }
      });
      payload['isBillingSameAsShipping'] = true;
    }

    if (this.newUserFlag) {
      let tempProvince = _.chain(this.provinceList).find(['id', _.get(payload, 'provinceId')]).get('name').value();
      let tempAddress = `${_.get(payload, 'street')}, ${_.get(payload, 'city')}, ${tempProvince}, ${_.get(payload, 'postCode')}.`
      payload['user'] = {
        email: _.get(payload, 'email'),
        firstName: _.get(payload, 'contactFirstName'),
        lastName: _.get(payload, 'contactLastName'),
        phoneNo: _.get(payload, 'phoneNo'),
        company: _.get(payload, 'name'),
        address: tempAddress,
        roleName: 'ROLE_CUSTOMER',
      }
    }

    if (payload.saleRepCustomers[0].userId == 0) {
      payload.saleRepCustomers[0].userId = null;
    }
    payload['appUrl'] = APP_URL;
    return payload;
  }


  getStylePriceTypes() {
    this._priceTypeService.getAll().subscribe((resp) => {
      this.priceTypeList = resp;
    });
  }

  getAllProvinces() {
    this._provinceService.getAll().subscribe((data) => {
      this.provinceList = data;
    });
  }



  getSalesReps() {
    if (this.userRole != 'ROLE_SALES') {
      let params = {};
      params['rolename'] = 'ROLE_SALES,ROLE_PARTNER';
      this._userService.getAll(params).subscribe(data => {
        this.salesRepList = [];
        if (this.userRole == 'ROLE_PARTNER') {
          this.salesRepList.push({
            id: _.get(this.userRoleUser, 'id'),
            fullname: `${_.get(this.userRoleUser, 'firstName')} ${_.get(this.userRoleUser, 'lastName')}`,
          });
          this.newCustomer.saleRepCustomers[0].userId = _.get(this.userRoleUser, 'id');
        }
        if (this.userRole == 'ROLE_ADMIN') {
          this.salesRepList.push({
            id: 0,
            fullname: `--Rawman & Co.--`,
          });
          this.newCustomer.saleRepCustomers[0].userId = 0;
        }
        _.each(data, item => {
          this.salesRepList.push(item);
        });
      });
    } else {
      this.newCustomer.saleRepCustomers[0].userId = _.get(this.userRoleUser, 'id');
    }

  }

  onRequirementChange() {

  }



}
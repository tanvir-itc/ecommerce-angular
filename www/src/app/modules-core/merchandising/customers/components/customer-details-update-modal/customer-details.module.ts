import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { RouterModule } from '@angular/router';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { CustomerDetailsUpdateModalComponent } from './customer-details-update-modal.component';
import { CustomerBasicComponent } from './customer-basic/customer-basic.component';
import { CustomerAddressComponent } from './customer-address/customer-address.component';
import { CustomerUserComponent } from './customer-user/customer-user.component';
import { NgxSummernoteModule } from 'ngx-summernote';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';

@NgModule({
  declarations: [
    CustomerDetailsUpdateModalComponent,
    CustomerBasicComponent,
    CustomerAddressComponent,
    CustomerUserComponent,
  ],
  entryComponents: [
    CustomerDetailsUpdateModalComponent,
    CustomerBasicComponent,
    CustomerAddressComponent,
    CustomerUserComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FuseSharedModule,
    RouterModule,
    ButtonModule,
    DateTimeModule,
    GeneralModule,
    NgxSummernoteModule,
    AttachmentModule,
  ],
  exports: [
    CustomerDetailsUpdateModalComponent,
  ]
})

export class CustomerDetailsModule {
}
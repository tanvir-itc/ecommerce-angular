import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AuthService } from 'app/authentication/auth.service';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { CustomerUserService } from '../../../customer-user.service';
import { CustomerService } from '../../../customer.service';

@Component({
  selector: 'customer-user',
  templateUrl: './customer-user.component.html',
})
export class CustomerUserComponent implements OnInit {
  @Input()
  customerId = null;

  userList: any[] = [];

  emailObject: any = null;
  sendInvitationMoadal = null;

  config: any = {
    placeholder: '',
    lineheight: 1,
    tabsize: 1,
    height: 450,
    uploadImagePath: '',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo']],
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table']],
    ],
    fontNames: ['Helvetica', 'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Roboto', 'Times']
  }
  constructor(
    public _auth: AuthService,
    public _customerUserService: CustomerUserService,
    private snackBar: SnackbarService,
    private _customerService: CustomerService,
    private dialog: MatDialog,
  ) {
  }
  ngOnInit() {
    this.getUserList();
  }

  getUserList() {
    this._customerUserService.getAll({ 'customer.id': this.customerId }).subscribe((customer) => {
      this.userList = customer.map(item => {
        const user = {
          ...item,
          firstName: item.user.firstName,
          lastName: item.user.lastName,
          email: item.user.email,
          phoneNo: item.user.phoneNo,
          password: item.user.password,
          enable: item.user.enable,
          userId: item.user.id,
        }
        return user;
      });
    });
  }

  updateUser(flag, user) {
    if (!user.firstName) {
      this.snackBar.open(`Please enter first name.`, "Close");
      return true;
    }
    
    if (!user.email) {
      this.snackBar.open(`Please enter email.`, "Close");
      return true;
    }

    const payload =
    {
      customerId: this.customerId,
      admin: user.admin,
      primary: user.primary,
      jobTitle: user.jobTitle,
      user: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        phoneNo: user.phoneNo,
        enable: user.enable,
        customerId: this.customerId
      }
    }
    if (flag) {
      payload['id'] = user.id
      payload.user['id'] = user.userId
      this._customerUserService.updateFields(user.id, payload).subscribe(resp => {
        this.getUserList()
      })
    } else {
      this._customerUserService.create(payload).subscribe(resp => {
        this.getUserList()
      })
    }
  }

  addRow() {
    this.userList.push(
      {
        firstName: null,
        lastName: null,
        email: null,
        phoneNo: null,
        designation: null,
        enable: true,
        admin: false,
        primary: false,
        new: true,
        editMode: true,
      }
    )
  }

  delete(id) {
    if (confirm("Delete confirm?")) {
      this._customerUserService.delete(id).subscribe(res => {
        this.getUserList();
        this.snackBar.open('Deleted!', "close");
      })
    }
  }










  openInvitationModal(user, templateRef: TemplateRef<any>) {
    this.emailObject = {
      emailBody: null,
      user: user,
    }
    this.getReinvitationEmail();
    this.sendInvitationMoadal = this.dialog.open(templateRef, {
      minWidth: "1200px"
    });
  }

  closeModal() {
    this.sendInvitationMoadal.close();
  }


  sendEmail() {
    let payload = {
      id: this.emailObject.user.id,
      email: this.emailObject.user.email,
      userId: this.emailObject.user.userId,
      body: this.emailObject.emailBody,
    }
    this._customerUserService.reinvitation(payload).subscribe(resp => {
      this.getUserList();
      this.closeModal();
      this.snackBar.open(`Reinvite mail sended to customer user email!`, 'Close');
    });
  }

  getReinvitationEmail() {
    let payload = {
      id: this.emailObject.user.id,
      userId: this.emailObject.user.userId,
      email: this.emailObject.user.email,
    }
    this._customerUserService.getReinvitationEmailBody(payload).subscribe(resp => {
      this.emailObject.emailBody = resp
    });
  }

}
import { Component, OnInit, Input, Inject } from '@angular/core';
import { AuthService } from 'app/authentication/auth.service';
import { PriceTypeService } from 'app/modules-core/common/price-type/price-type.service';
import { UserService } from 'app/modules-core/common/user/user.service';
import { CustomerService } from '../../../customer.service';
import * as _ from 'lodash';
import { APP_URL } from 'app/app.constants';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';

@Component({
  selector: 'customer-basic',
  templateUrl: './customer-basic.component.html',
})
export class CustomerBasicComponent implements OnInit {
  @Input()
  customerId = null;

  @Input()
  typeFlag = null;

  customer: any = null;
  newCustomer: any = null;
  priceTypeList: any[] = [];
  userRole: any = null;
  userRoleUser: any = null;

  salesRepList: any[] = null;
  constructor(
    public _auth: AuthService,
    public _customerService: CustomerService,
    public _priceTypeService: PriceTypeService,
    public _userService: UserService,
    private snackBar: SnackbarService,
  ) {
  }
  ngOnInit() {
    this.getCustomer();
    this.getStylePriceTypes();
    this.getSalesReps();
  }

  getCustomer() {
    this._customerService.get(this.customerId).subscribe((customer) => {
      this.newCustomer = customer;


      this.newCustomer = null;
      this.newCustomer = {
        editMode: false,
        id: _.get(customer, 'id'),
        name: _.get(customer, 'name'),
        accountant: _.get(customer, 'accountant'),
        accountantEmail: _.get(customer, 'accountantEmail'),
        email: _.get(customer, 'email'),
        shipmentMode: _.get(customer, 'shipmentMode'),
        priceTypeId: _.get(customer, 'priceTypeId'),
        courierCompany: _.get(customer, 'courierCompany'),
        courierAccountNo: _.get(customer, 'courierAccountNo'),
        description: _.get(customer, 'description'),
        preShipmentApproval: _.get(customer, 'preShipmentApproval'),
        minInvQtyShowOnOrder: _.get(customer, 'minInvQtyShowOnOrder'),
        sendEmail: _.get(customer, 'sendEmail'),
        saleRepCustomers: [{ id: null, userId: null }],
        ccUpCharge: _.get(customer, 'paymentRequired') ? _.get(customer, 'ccUpCharge') || 3 : 3,
        paymentRequired: _.get(customer, 'paymentRequired'),
      };
      this.newCustomer.saleRepCustomers[0].userId = _.chain(customer.saleRepCustomers).head().get('userId').value() || 0;
      this.newCustomer.saleRepCustomers[0].id = _.chain(customer.saleRepCustomers).head().get('id').value() || 0;
    });
  }

  updateCustomer() {
    var payload = this.getPayload();
    if (this.validate(payload)) {
      return true;
    }
    payload['requested'] = null;
    if (this.typeFlag == 'accept') {
      payload['requested'] = false;
    }
    payload['appUrl'] = APP_URL;
    payload['redirectUrl'] = `${APP_URL}reset-password`;
    this._customerService.updateFields(payload).subscribe((resp) => {
      this.snackBar.open(`Customer updated!`, "Close");
      this.getCustomer();
    });
  }


  validate(payload) {
    var error = false;
    if (
      !_.get(payload, 'name') ||
      !_.get(payload, 'email')
    ) {
      error = true;
    }
    if (error) {
      this.snackBar.open(`Please, enter required fields!`, "Close");
      return true;
    }
    return false;
  }

  getPayload() {
    let payload = _.merge({}, this.newCustomer);
    delete payload.user;
    if (payload.saleRepCustomers[0].userId == 0) {
      payload.saleRepCustomers[0].userId = null;
    }
    return payload;
  }

  getStylePriceTypes() {
    this._priceTypeService.getAll().subscribe((resp) => {
      this.priceTypeList = resp;
    });
  }

  getSalesReps() {
    if (this.userRole != 'ROLE_SALES') {
      let params = {};
      params['rolename'] = 'ROLE_SALES,ROLE_PARTNER';
      this._userService.getAll(params).subscribe(data => {
        this.salesRepList = [];
        if (this.userRole == 'ROLE_PARTNER') {
          this.salesRepList.push({
            id: _.get(this.userRoleUser, 'id'),
            fullname: `${_.get(this.userRoleUser, 'firstName')} ${_.get(this.userRoleUser, 'lastName')}`,
          });
        }
        if (this.userRole == 'ROLE_ADMIN') {
          this.salesRepList.push({
            id: 0,
            fullname: `--Rawman & Co.--`,
          });
        }
        _.each(data, item => {
          this.salesRepList.push(item);
        });
      });
    }
  }

  onRequirementChange() {

  }
}
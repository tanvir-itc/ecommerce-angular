import { Component, Inject, Input } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import * as _ from 'lodash';
import { AuthService } from "app/authentication/auth.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { APP_URL } from "app/app.constants";
import { CustomerService } from "../../customer.service";


@Component({
  selector: 'customer-details-update-modal',
  templateUrl: 'customer-details-update-modal.component.html',
})
export class CustomerDetailsUpdateModalComponent {
  customerId = null;
  newCustomer: any = null;
  typeFlag = null;
  constructor(
    public dialogRef: MatDialogRef<CustomerDetailsUpdateModalComponent>,
    public _auth: AuthService,
    @Inject(MAT_DIALOG_DATA) public data,
    private snackBar: SnackbarService,
    public _customerService: CustomerService,
  ) {
    this.customerId = data.customerId;
    this.typeFlag = data.typeFlag;
  }
  

  updateCustomer() {
    var payload = {};
    
    payload['requested'] = null;
    payload['id'] = this.customerId;
    if (this.typeFlag == 'accept') {
      payload['requested'] = false;
    }
    payload['appUrl'] = APP_URL;
    payload['redirectUrl'] = `${APP_URL}reset-password`;
    this._customerService.updateFields(payload).subscribe((resp) => {
      this.snackBar.open(`Customer updated!`, "Close");
      // Close the modal
      this.dialogRef.close('updated'); 
    });
  }
}
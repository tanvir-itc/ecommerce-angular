import { Component, OnInit, Input, Inject } from '@angular/core';
import { AuthService } from 'app/authentication/auth.service';
import { ProvinceService } from 'app/modules-core/common/province/province.service';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { CustomerAddressService } from '../../../customer-address.service';

@Component({
  selector: 'customer-address',
  templateUrl: './customer-address.component.html',
})
export class CustomerAddressComponent implements OnInit {
  @Input()
  customerId = null;

  @Input()
  addressType = null;

  addressList: any[] = [];
  provinceList: any[] = [];
  constructor(
    public _auth: AuthService,
    public _customerAddressService: CustomerAddressService,
    public _provinceService: ProvinceService,
    private snackBar: SnackbarService,
  ) {
  }
  ngOnInit() {
    this.getAddressList();
    this.getAllProvinces();
  }

  getAddressList() {
    this._customerAddressService.getAll({ 'customer.id': this.customerId, addressType: this.addressType }).subscribe((customer) => {
      this.addressList = customer.map(item => {
        item.editMode = false;
        return item;
      });
    });
  }

  updateAddress(flag, address) {
    if (!address.street) {
      this.snackBar.open(`Please enter street.`, "Close");
      return true;
    }
    if (!address.city) {
      this.snackBar.open(`Please enter city.`, "Close");
      return true;
    }
    if (!address.postCode) {
      this.snackBar.open(`Please enter post code.`, "Close");
      return true;
    }
    if (!address.provinceId) {
      this.snackBar.open(`Please select province.`, "Close");
      return true;
    }
    const payload = {
      addressType: this.addressType,
      shortCode: address.shortCode,
      city: address.city,
      customerId: this.customerId,
      default: address.default,
      poBoxNo: address.poBoxNo,
      postCode: address.postCode,
      provinceId: address.provinceId,
      street: address.street,
    }
    if (flag) {
      payload['id'] = address.id
      this._customerAddressService.updateFields(address.id, payload).subscribe(resp => {
        this.getAddressList()
      })
    } else {
      this._customerAddressService.create(payload).subscribe(resp => {
        this.getAddressList()
      })
    }
  }
  getAllProvinces() {
    this._provinceService.getAll().subscribe((data) => {
      this.provinceList = data;
    });
  }

  addRow() {
    this.addressList.push(
      {
        addressType: this.addressType,
        shortCode: null,
        city: null,
        default: false,
        poBoxNo: null,
        postCode: null,
        provinceId: null,
        street: null,
        new: true,
        editMode: true,
      }
    )
  }

  delete(id) {
    if (confirm("Delete confirm?")) {
      this._customerAddressService.delete(id).subscribe(res => {
        this.getAddressList();
        this.snackBar.open('Deleted!', "close");
      })
    }
  }

}
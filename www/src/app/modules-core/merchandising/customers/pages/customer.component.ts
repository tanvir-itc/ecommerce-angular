import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { fuseAnimations } from "@fuse/animations";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";

@Component({
  selector: "app-customer",
  templateUrl: "./customer.component.html",
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class CustomerComponent implements OnInit {
  show = false;
  tabs;
  tabFromUrl;

  basePath;

  constructor(
    private _route: ActivatedRoute,
    public _auth: AuthService,
  ) {
    // this.id = +this._route.snapshot.paramMap.get("id");
    this.tabFromUrl = this._route.snapshot.firstChild.routeConfig.path; //find a better way to get child param
    this.basePath = this._route.snapshot.url[0].path; //find a better way to get child param
  }

  ngOnInit() {
    this._route.paramMap.subscribe(params => {
      this.show = false;
      this.tabs = [
        {
          path: `${this.basePath}/registered`,
          label: "Registered",
          color: 'tab-green',
          display: this._auth.hasPermission('PERMISSION_MENU_CUSTOMER_VIEW') ? true : false
        },
        {
          path: `${this.basePath}/requested`,
          label: "Requested",
          color: 'tab-red',
          display: this._auth.hasPermission('PERMISSION_CUSTOMER_REQUEST_LIST_VIEW') ? true : false
        },
      ];
      this.show = true;
    });
  }

}

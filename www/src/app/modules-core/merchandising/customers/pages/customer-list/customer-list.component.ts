import { Component, OnInit } from "@angular/core";
import { fuseAnimations } from "@fuse/animations";
import * as _ from "lodash";
import { MatDialog } from "@angular/material";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  removeEmptyFields,
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { CustomerService } from "../../customer.service";
import { ProvinceService } from "app/modules-core/common/province/province.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { CustomerDetailsUpdateModalComponent } from "../../components/customer-details-update-modal/customer-details-update-modal.component";
import { CustomerAddModalComponent } from "../../components/customer-add-modal/customer-add-modal.component";
import { UserService } from "app/modules-core/common/user/user.service";

@Component({
  selector: "customer-list",
  templateUrl: "./customer-list.component.html",
  animations: fuseAnimations,
})
export class CustomerListComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  customers: any[] = [];
  customerId: any = null;

  provinceList: any[] = [];

  modalRef = null;




  priceTypeList: any[] = [];
  saleRepList: any[] = [];

  userRole = null;

  constructor(
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _customerService: CustomerService,
    private _provinceService: ProvinceService,
    private _userService: UserService
  ) {
    this._quickPanelSubjectService.heading.next("Customers");
  }
  ngOnInit() {
    this.userRole = _.chain(this._auth.getCurrentUser()).get('roles').head().get('name').value();
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));

    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
    this.getAllProvinces();
    this.getAllSaleRep();
  }

  clearAllFilter() {
    this.filter = {
      name: null,
      email: null,
      phoneno: null,
      provinceid: null,
      billingprovinceid: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    params['all'] = true;
    params['requested'] = false;
    params['sort'] = 'name';
    this._customerService.getAll(params).subscribe((data) => {
      this.customers = data;
      this.setFilterToURL(filterParams);
    });
  }

  getAllProvinces() {
    this._provinceService.getAll().subscribe((data) => {
      this.provinceList = data;
    });
  }

  openCreateModal() {
    const colorModal = this.dialog.open(CustomerAddModalComponent, {
      minWidth: '50%',
      maxWidth: '960px',
      disableClose: true,
      data: {
      }
    });
    colorModal.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
    });
  }


  goLink(customerId) {
    this._router.navigateByUrl(`customers/${customerId}`);
  }

  deleteCustomer(id) {
    if (!confirm("Customer delete confirm?")) {
      return;
    }
    this._customerService.delete(id).subscribe(res => {
      this.snackBar.open(`Customer deleted!`, "Close");
      this.applyFilters(this.filter)
    });
  }

  disableCustomer(id, flag) {
    if (!confirm("Customer enable/disable confirm?")) {
      return;
    }
    let payload = {
      id: id,
      enable: !flag
    }
    this._customerService.updateFields(payload).subscribe(res => {
      this.snackBar.open(`Customer updated!`, "Close");
      this.applyFilters(this.filter)
    });
  }


  openCustomerUpdateModal(customerId) {
    const colorModal = this.dialog.open(CustomerDetailsUpdateModalComponent, {
      minWidth: '60%',
      maxWidth: '960px',
      disableClose: true,
      data: {
        customerId: customerId,
        typeFlag: 'update',
      }
    });
    colorModal.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
    });
  }

  getAllSaleRep() {
    if (this._auth.hasPermission("PERMISSION_ADMIN_USER")) {
      var params = {};
      params['rolename'] = 'ROLE_SALES';
      this._userService.getAll(params).subscribe(data => {
        this.saleRepList = data;
      });
    }

  }




} //class

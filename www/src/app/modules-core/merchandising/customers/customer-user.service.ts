import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import { makeParams } from 'app/modules-core/utility/helpers';
import * as _ from 'lodash';
import { APP_URL } from "app/app.constants";

@Injectable({
  providedIn: 'root'
})
export class CustomerUserService {
  urlBase = 'customer-users';

  httpOptions = {
    params: new HttpParams()
  }
  constructor(
    private http: HttpClient,
  ) { }

  getAll(opts = {}) {
    return this.http.get(`${this.urlBase}`, {
      params: makeParams(_.merge({
        httpParams: this.httpOptions.params
      }, { queryParams: opts }))
    }).pipe(map((resp: any) => {
      resp.data = _.map(resp.data, item => {
        item.user.fullname = `${item.user.firstName} ${item.user.lastName}`
        return item;
      })
      return resp.data
    }));
  }

  get(id) {
    return this.http.get(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  create(payload) {
    return this.http.post(this.urlBase, payload)
      .pipe(map((resp: any) => resp.data));
  }

  update(id, payload) {
    return this.http.put(`${this.urlBase}/${id}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFields(id, payload) {
    return this.http.patch(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  delete(id) {
    return this.http.delete(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }


  reinvitation(customerUser) {
    let payload = {
      email: _.get(customerUser, 'email'),
      body: customerUser.body,
      redirectUrl: `${APP_URL}reset-password`,
    }
    return this.http.post(`${this.urlBase}/${_.get(customerUser, 'id')}/email`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  getReinvitationEmailBody(customerUser) {
    let payload = {
      email: _.get(customerUser, 'email'),
      redirectUrl: `${APP_URL}reset-password`,
    }
    return this.http.post(`${this.urlBase}/${_.get(customerUser, 'id')}/email-content`, payload)
      .pipe(map((resp: any) => resp.data));
  }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from '../../utility/date-time-picker/date-time.module';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { UserAutocompleteModule } from 'app/modules-core/common/user/components/user-autocomplete.module';
import { CustomerListComponent } from './pages/customer-list/customer-list.component';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { CustomerAddModalModule } from './components/customer-add-modal/customer-add-modal.module';
import { CustomerComponent } from './pages/customer.component';
import { RequestedListComponent } from './pages/requested-list/requested-list.component';
import { CustomerDetailsModule } from './components/customer-details-update-modal/customer-details.module';

const routes = [

  {
    path: "customers",
    component: CustomerComponent,
    data: { permission: 'PERMISSION_MENU_CUSTOMER_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
    children: [
      {
        path: "",
        redirectTo: "registered",
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_CUSTOMER_VIEW' },
      },
      {
        path: "registered",
        component: CustomerListComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_CUSTOMER_VIEW' },
      },
      {
        path: "requested",
        component: RequestedListComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_CUSTOMER_REQUEST_LIST_VIEW' },
      },
    ],
  },
];
@NgModule({
  declarations: [
    CustomerComponent,
    CustomerListComponent,
    RequestedListComponent,

  ],
  entryComponents: [
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    DateTimeModule,
    AttachmentModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
    UserAutocompleteModule,
    CustomerAddModalModule,
    CustomerDetailsModule,
  ],
  exports: [
  ]
})
export class CustomerModule {
}
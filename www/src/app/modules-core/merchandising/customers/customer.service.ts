import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from "rxjs/internal/operators/map";
import * as _ from "lodash";
import { makeParams } from "app/modules-core/utility/helpers";
import { APP_URL } from "app/app.constants";

@Injectable({
  providedIn: "root",
})
export class CustomerService {
  urlBase = "customers";

  httpOptions = {
    params: new HttpParams(),
  };
  constructor(private http: HttpClient) { }

  getAll(opts = {}) {
    return this.http
      .get(`${this.urlBase}`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          resp.data = _.map(resp.data, item => {
            item['salesRep'] = _.head(item.saleRepCustomers);
            return item;
          })
          return resp.data;
        })
      );
  }

  get(id) {
    return this.http
      .get(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => {
        resp.data['salesRep'] = _.head(resp.data.saleRepCustomers);
        return resp.data
      }));
  }

  create(payload) {
    return this.http
      .post(this.urlBase, payload)
      .pipe(map((resp: any) => resp.data));
  }

  update(payload) {
    return this.http
      .put(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFields(payload) {
    return this.http
      .patch(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  delete(id) {
    return this.http
      .delete(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }


  passwordReset(user) {
    let payload = {
      email: _.get(user, 'email'),
      redirectUrl: `${APP_URL}reset-password`,
    }
    return this.http.post(`${this.urlBase}/${_.get(user, 'id')}/email`, payload)
      .pipe(map((resp: any) => resp.data));
  }





}

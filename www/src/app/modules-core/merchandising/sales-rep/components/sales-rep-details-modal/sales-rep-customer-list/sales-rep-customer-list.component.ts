import { Component, OnInit, Input, Inject } from '@angular/core';
import { AuthService } from 'app/authentication/auth.service';
import { UserService } from 'app/modules-core/common/user/user.service';
import * as _ from 'lodash';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { SalesRepCustomerService } from 'app/modules-core/common/sales-rep-customer/sales-rep-customer.service';
import { MatDialog } from '@angular/material';
import { CustomerService } from 'app/modules-core/merchandising/customers/customer.service';

@Component({
  selector: 'sales-rep-customer-list',
  templateUrl: './sales-rep-customer-list.component.html',
})
export class SalesRepCustomerListComponent implements OnInit {
  @Input()
  salesRepId = null;
  userCustomerList: any[] = [];
  user: any = null;

  customerList: any[] = [];

  customerAddModalRef: any = null;
  constructor(
    public _auth: AuthService,
    public _userService: UserService,
    public _salesRepCustomer: SalesRepCustomerService,
    public _customerService: CustomerService,
    private snackBar: SnackbarService,
    private dialog: MatDialog,
  ) {
  }
  ngOnInit() {
    this.getSaleRepCustomers();
  }

  getUser() {
    this._userService.getUser(this.salesRepId)
      .subscribe(user => {
        this.user = null;
      });
  }
  getSaleRepCustomers() {
    this._salesRepCustomer.getAll({ salerepid: this.salesRepId }).subscribe(resp => {
      this.userCustomerList = resp;
    });
  }


  openAddCustomerModal(templateRef) {
    this.getCustomers();
    this.customerAddModalRef = this.dialog.open(templateRef, {
      width: "90%",
      disableClose: true,
    });
  }
  getCustomers() {
    this._customerService.getAll({ requested: false }).subscribe((data) => {
      let userCustomerListIds = _.map(this.userCustomerList, 'customer.id');
      this.customerList = [];
      _.each(data, item => {
        if (!userCustomerListIds.includes(item.id)) {
          this.customerList.push(item);
        }
      });
    });
  }

  addCustomer() {
    let payload = [];
    _.each(this.customerList, item => {
      if (_.get(item, 'select')) {
        payload.push({
          customerId: item.id,
          userId: this.salesRepId,
        });
      }
    });

    if (!payload.length) {
      this.snackBar.open(`Please, select minimum one customer!`, "Close");
      return;
    }
    this._salesRepCustomer.create(payload).subscribe(resp => {
      this.getSaleRepCustomers();
      this.modalClose();
    });
  }
  modalClose() {
    this.customerAddModalRef.close();
  }

  deleteCustomer(id, index) {
    if (!confirm("Are you sure?")) {
      return;
    }
    this._salesRepCustomer.delete(id).subscribe(resp => {
      this.userCustomerList.splice(index, 1);
      this.getCustomers();
    });
  }

  removeSalesRepFromCustomer(customerSalesRepId, itemIndex) {
    if (!confirm("Are you sure?")) {
      return;
    }
    this._salesRepCustomer.delete(customerSalesRepId).subscribe(resp => {
      this.customerList[itemIndex].salesRep = null;
    });
  }

}
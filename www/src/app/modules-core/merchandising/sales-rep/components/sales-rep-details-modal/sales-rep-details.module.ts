import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { RouterModule } from '@angular/router';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { NgxSummernoteModule } from 'ngx-summernote';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { SalesRepDetailsModalComponent } from './sales-rep-details-modal.component';
import { SalesRepBasicComponent } from './sales-rep-basic/sales-rep-basic.component';
import { SalesRepCustomerListComponent } from './sales-rep-customer-list/sales-rep-customer-list.component';
import { SalesRepCommissionComponent } from './sales-rep-commission/sales-rep-commission.component';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
    SalesRepDetailsModalComponent,
    SalesRepBasicComponent,
    SalesRepCustomerListComponent,
    SalesRepCommissionComponent,
  ],
  entryComponents: [
    SalesRepDetailsModalComponent,
    SalesRepBasicComponent,
    SalesRepCustomerListComponent,
    SalesRepCommissionComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FuseSharedModule,
    RouterModule,
    ButtonModule,
    DateTimeModule,
    GeneralModule,
    NgxSummernoteModule,
    AttachmentModule,
    NgSelectModule,
  ],
  exports: [
    SalesRepDetailsModalComponent
  ]
})

export class SalesRepDetailsModule {
}
import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import * as _ from 'lodash';
import { AuthService } from "app/authentication/auth.service";


@Component({
  selector: 'sales-rep-details-modal',
  templateUrl: 'sales-rep-details-modal.component.html',
})
export class SalesRepDetailsModalComponent {
  salesRepId = null;
  salesRepname = null;
  constructor(
    public dialogRef: MatDialogRef<SalesRepDetailsModalComponent>,
    public _auth: AuthService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.salesRepId = data.salesRepId;
    this.salesRepname = _.get(data, 'salesRepname');
  }

}
import { Component, OnInit, Input, Inject } from '@angular/core';
import { AuthService } from 'app/authentication/auth.service';
import { UserService } from 'app/modules-core/common/user/user.service';
import * as _ from 'lodash';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { CommissionService } from 'app/modules-core/merchandising/commission/commission.service';
import { GlobalService } from 'app/modules-core/common/global/global.service';
import { toDateString } from 'app/modules-core/utility/helpers';
import { StyleService } from 'app/modules-core/merchandising/style/style.service';
import { MatDialog } from '@angular/material';
import { StyleCostService } from 'app/modules-core/common/style-cost/style-cost.service';

@Component({
  selector: 'sales-rep-commission',
  templateUrl: './sales-rep-commission.component.html',
})
export class SalesRepCommissionComponent implements OnInit {
  @Input()
  salesRepId = null;

  commission: any = null;
  partnerStyleList: any[] = [];
  styleList: any[] = [];
  styleInvestModal
  newStyle: any = null;
  styleDetails: any = null;
  styleCostList = [];

  constructor(
    public _auth: AuthService,
    public _userService: UserService,
    public _commissionService: CommissionService,
    public _globalService: GlobalService,
    public _styleService: StyleService,
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    private _styleCostService: StyleCostService,
  ) {
  }
  ngOnInit() {
    this.getSalesRepCommission();
    this.getSalesRepCommissionStyle();
  }

  getSalesRepCommission() {
    this._commissionService.getAllSalesCommission({ 'commissionUser.id': this.salesRepId }).subscribe(resp => {
      let commission = _.head(resp);

      this.commission = {
        editMode: false,
        id: _.get(commission, 'id'),
        commissionPercentage: _.get(commission, 'commissionPercentage'),
        commissionUserId: this.salesRepId,
      }
    })
  }

  getSalesRepCommissionStyle() {
    this._commissionService.getAllSalesCommissionStyle({ 'commissionUser.id': this.salesRepId }).subscribe(resp => {
      this.partnerStyleList = resp
    })
  }

  updateCommission() {
    if (_.get(this.commission, 'id')) {
      this._commissionService.updateFieldsSalesCommission(this.commission).subscribe(resp => {
        this.snackBar.open(`Commission updated!`, 'Close');
        this.getSalesRepCommission();
      });
    } else {
      this._commissionService.createSalesCommission(this.commission).subscribe(resp => {
        this.snackBar.open(`Commission updated!`, 'Close');
        this.getSalesRepCommission();
      });
    }

  }

  addModalOpen(templateRef) {
    this.getStyleList();
    this.newStyle = {
      commissionUserId: this.salesRepId,
      styleId: null,
      commissionSetupStyleDetails: [],
    }
    this.styleDetails = {
      commissionPercentage: null,
      partnerQty: null,
      startDate: null,
      styleCostId: null
    }
    this.styleInvestModal = this.dialog.open(templateRef, {
      width: "500px",
      disableClose: true,
    });
  }

  editUpdateModalOpen(templateRef, style, item) {
    this.getStyleList();
    this.newStyle = {
      ...style,
      commissionSetupStyleId: style.id,
    }
    this.styleDetails = {
      commissionPercentage: item.commissionPercentage,
      partnerQty: item.partnerQty,
      startDate: item.startDate,
      styleCostId: item.styleCostId,
      commissionSetupStyleId: style.id,
      id: item.id
    }
    this.getCostList();
    this.styleInvestModal = this.dialog.open(templateRef, {
      width: "500px",
      disableClose: true,
    });
  }

  addStyle() {

    if (!this.newStyle.styleId) {
      this.snackBar.open(`Please select style.`, "Close");
      return true;
    }
    if (!this.styleDetails.commissionPercentage) {
      this.snackBar.open(`Please enter commission percentage.`, "Close");
      return true;
    }
    if (!this.styleDetails.partnerQty) {
      this.snackBar.open(`Please enter investor qty.`, "Close");
      return true;
    }
    if (!this.styleDetails.startDate) {
      this.snackBar.open(`Please select start date.`, "Close");
      return true;
    }
    if (!this.styleDetails.styleCostId) {
      this.snackBar.open(`Please select costing date.`, "Close");
      return true;
    }

    let payload = {
      ...this.newStyle,
      commissionSetupStyleDetails: [{
        ...this.styleDetails
      }]
    }
    this._commissionService.createSalesCommissionStyle(payload).subscribe(resp => {
      this.getSalesRepCommissionStyle()
      this.snackBar.open('Added!', "close");
      this.closeStyleInvestModal();
    })
  }
  addUpdateStyleItem() {
    if (!this.styleDetails.commissionPercentage) {
      this.snackBar.open(`Please enter commission percentage.`, "Close");
      return true;
    }
    if (!this.styleDetails.partnerQty) {
      this.snackBar.open(`Please enter investor qty.`, "Close");
      return true;
    }
    if (!this.styleDetails.startDate) {
      this.snackBar.open(`Please select start date.`, "Close");
      return true;
    }
    if (!this.styleDetails.styleCostId) {
      this.snackBar.open(`Please select costing date.`, "Close");
      return true;
    }
    let payload = {
      ...this.styleDetails
    }

    this._commissionService.createSalesCommissionStyleItem(this.newStyle.id, payload).subscribe(resp => {
      this.getSalesRepCommissionStyle()
      this.snackBar.open('Added!', "close");
      this.closeStyleInvestModal();
    })
  }

  deleteSalesCommissionStyleItem(id, detailsId) {
    if (confirm("Delete confirm?")) {
      this._commissionService.deleteSalesCommissionStyleItem(id, detailsId).subscribe(resp => {
        this.getSalesRepCommissionStyle()
        this.snackBar.open('Delete!', "close");
      })
    }
  }

  getStyleList() {
    this._styleService.getAll().subscribe(resp => {
      this.styleList = resp;
    })
  }

  onStyleChange() {
    this.getCostList();
  }

  getCostList() {
    if (!this.newStyle.styleId) {
      this.styleCostList = [];
      return;
    }
    this._styleCostService.getAll({ 'style.id': this.newStyle.styleId }).subscribe(resp => {
      this.styleCostList = _.map(resp, item => {
        item.costDate = `${item.createdDate} - ${item.createdDate}`
        item.date = item.date || 'Not Completed'
        return item;
      });
    })
  }

  closeStyleInvestModal() {
    this.styleInvestModal.close();
  }

}
import { Component, OnInit, Input } from "@angular/core";
import * as _ from "lodash";
import { fuseAnimations } from "@fuse/animations";
import { AuthService } from "app/authentication/auth.service";
import { MatDialog } from "@angular/material";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "app/modules-core/common/user/user.service";
import { InventoryLocationService } from "app/modules-core/common/inventory-location/inventory-location.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { FactoryShipmentService } from "app/modules-core/common/factory-shipment/factory-shipment.service";
import { SizeService } from "app/modules-core/common/size/size.service";
import { StyleService } from "app/modules-core/merchandising/style/style.service";
import { CustomerService } from "app/modules-core/merchandising/customers/customer.service";
import { POService } from "app/modules-core/common/po/po.service";

@Component({
  selector: "factory-shipment-details",
  templateUrl: "./factory-shipment-details.component.html",
  animations: fuseAnimations,
})
export class FactoryShipmentDetailsComponent implements OnInit {
  id: any = null;
  shipment: any = {};
  shipmentUpdateObj: any = {};
  sizeList: any = [];
  styleSizeList: any = [];
  styleList: any = [];
  colorList: any = [];
  boxItemList: any = [{}];
  customerList: any = [];
  poList: any = [];
  alreadyAddedBoxNumberList: any = [];
  modalRef: any = null;
  newLocation: any = null;
  customerId: any = null;

  newUser: any = null;
  boxId: any = null;
  boxForm = 0;
  boxTo = 0;
  newBoxList;
  editModeEnable = false;
  disablesubmitBtn = false;
  selectAllBoxes = false;
  alreadyAddeditems: any = [];
  locationList: any = [];
  selectedItems = [];
  location = {};
  locationGivenBoxesNumber = 0;
  totalReceivedQuantity = 0;
  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _userService: UserService,
    private _locationService: InventoryLocationService,
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    public _auth: AuthService,
    private _FactoryShipmentService: FactoryShipmentService,
    private _sizeService: SizeService,
    private _styleService: StyleService,
    private _customerService: CustomerService,
    private _poservice: POService,
    private _router: Router,
  ) {
    this._quickPanelSubjectService.heading.next("Factory Shipment Details");
  }

  ngOnInit() {
    this.id = +this._route.snapshot.paramMap.get("id");
    this.getShipmentDetail();

    this.getAllCustomers();
    // this.getStyleList();
    this.newBoxList = Array.apply(null, Array(this.boxTo - this.boxForm));
    this.location = {
      locationId: null,
      remark: null,
    },
      this.selectedItems = _.filter(this.shipment.factoryShipmentPackings, 'selectd')

  }

  getShipmentDetail() {
    this._FactoryShipmentService.getById(this.id)
      .subscribe(shipment => {
        this.styleSizeList = _.orderBy(_.uniqBy(_.flatMap(shipment.factoryShipmentPackings, 'factoryShipmentPackingDetails'), 'sizeId'), 'sizeId');
        console.log('style sizelist', this.styleSizeList)
        _.each(shipment.factoryShipmentPackings, p => {
          p['boxNumber'] = parseInt(p.boxNumber)
          _.each(p.factoryShipmentPackingDetails, detail => {
            detail['styleSizeList'] = _.map(this.styleSizeList, s => {
              if (detail.sizeId == s.sizeId) {
                return {
                  'sizeId': _.get(detail, 'sizeId'),
                  'sizeName': _.get(detail, 'sizeName'),
                  'quantity': _.get(detail, 'quantity')
                }
              } else {
                return null;
              }

            })

          })
          p['totalQty'] = _.sumBy(p.factoryShipmentPackingDetails, 'quantity')
        })
        this.shipment = shipment;
        this.shipment.factoryShipmentPackings = _.orderBy(shipment.factoryShipmentPackings, ['boxNumber'], ['asc']);
        this.locationGivenBoxesNumber = _.filter(shipment.factoryShipmentPackings, pack => { return pack.inventoryLocationId || pack.receiveComment }).length;
        this.totalReceivedQuantity = _.sumBy(_.flatMap(_.filter(shipment.factoryShipmentPackings, 'inventoryLocationId'), 'factoryShipmentPackingDetails'), 'quantity')
        // console.log(this.locationGivenBoxesNumber)
        this.boxForm = _.add(_.get(_.last(this.shipment.factoryShipmentPackings), 'boxNumber'), 1)
        this.boxTo = _.add(_.get(_.last(this.shipment.factoryShipmentPackings), 'boxNumber'), 1);
        console.log(this.shipment)
        this.getStyleList(this.shipment.poId)
        this.getSizes(this.shipment.poId);
      });
  }

  getStyleList(poid) {
    this._styleService.getAll({ 'poid': poid }).subscribe((data) => {
      this.styleList = data;
      console.log(this.styleList)
    });
  }

  getSizes(poid) {
    this._sizeService.getAll({
      'poid': poid
    }).subscribe((data) => {
      this.sizeList = data;
      console.log(this.sizeList)
    });
  }

  getAllCustomers() {
    this._customerService.getAll().subscribe((data) => {
      this.customerList = data;
    });
  }

  onChangeStyle(data, item) {
    this.colorList = [];
    item.styleComboId = null;
    item.colorList = _.merge([], _.get(data, 'combos'))

  }

  onAddRow() {
    this.boxItemList.push({})
  }

  rowRemove(index) {
    this.boxItemList.splice(index, 1);
  }

  onChangeItem(id, type, obj) {
    if (this.boxItemList.length < 2) {
      return;
    }
    let sameStyleIdObject = _.find(_.initial(this.boxItemList), [type, id])
    let sameObj = false;
    if (sameStyleIdObject) {
      if (_.get(sameStyleIdObject, 'styleId') == obj.styleId && _.get(sameStyleIdObject, 'sizeId') == obj.sizeId && _.get(sameStyleIdObject, 'styleComboId') == obj.styleComboId) {
        sameObj = true;
      } else {
        sameObj = false;
      }
    }
    if (sameObj) {
      this.boxItemList.splice(-1)
    }
    // _.each(this.boxItemList, boxitem => {
    //   delete boxitem.quantity
    //   isEqual = _.isEqual(boxitem, obj)
    //   debugger
    //   if (isEqual) {
    //     this.boxItemList.splice(-1)
    //   }
    // })


  }

  isAllReadyAddedItems() {
    this.alreadyAddedBoxNumberList = _.map(this.shipment.factoryShipmentPackings, m => { return _.parseInt(m.boxNumber) })
    this.newBoxList = _.range(this.boxForm, this.boxTo + 1);
    this.alreadyAddeditems = _.intersectionWith(this.alreadyAddedBoxNumberList, this.newBoxList, _.isEqual);
    if (this.alreadyAddeditems.length) {
      // this.boxForm = _.add(_.get(_.last(this.shipment.factoryShipmentPackings), 'boxNumber'), 1)
      // this.boxTo = _.add(_.get(_.last(this.shipment.factoryShipmentPackings), 'boxNumber'), 1);
      return this.snackBar.open(`Box number ${_.join(this.alreadyAddeditems, ',')} are already added!`, "Close");
    }
  }

  onStylesSubmit() {
    this.disablesubmitBtn = true;
    this.isAllReadyAddedItems();
    if (this.alreadyAddeditems.length) {
      this.disablesubmitBtn = false;
      return;
    }
    // if (!this.customerId) {
    //   this.snackBar.open(`Select customer!`, "Close");
    //   this.disablesubmitBtn = false;
    //   return
    // }
    if (this.boxForm > this.boxTo) {
      this.snackBar.open(`Starting box must not greater than ending box!`, "Close");
      this.disablesubmitBtn = false;
      return
    }
    if (!this.boxItemList[0].quantity || !this.boxItemList[0].sizeId || !this.boxItemList[0].styleComboId) {
      this.snackBar.open(`Select style, size and quantity first!`, "Close");
      this.disablesubmitBtn = false;

      return
    }

    let payload = {
      "boxFrom": this.boxForm,
      "boxTo": this.boxTo,
      "customerId": this.customerId,
      "factoryShipmentPackingDetails": _.map(_.filter(this.boxItemList, f => { return f.quantity > 0 }), box => {
        return {
          "quantity": box.quantity,
          "sizeId": box.sizeId,
          "styleComboId": box.styleComboId
        }
      })

    };
    this._FactoryShipmentService.detailCreate(payload, this.id).subscribe(resp => {
      this.disablesubmitBtn = false;
      // this.getUserLocations();
      this.getShipmentDetail();
      this.clearObject();
      this.snackBar.open(`Shipment created!`, "Close");
    }, err => {
      this.disablesubmitBtn = false;
    });


  }

  onStylesUpdate() {
    let payload = [{
      "id": this.boxId,
      "boxFrom": this.boxForm,
      "boxTo": this.boxTo,
      "customerId": this.customerId,
      "factoryShipmentPackingDetails": _.map(this.boxItemList, box => {
        return {
          "id": box.id,
          "quantity": box.quantity,
          "sizeId": box.sizeId,
          "styleComboId": box.styleComboId
        }
      })

    }];

    this._FactoryShipmentService.detailUpdate(payload, this.id).subscribe(resp => {
      // this.getUserLocations();
      this.clearObject();
      this.editModeEnable = false
      this.getShipmentDetail();
      this.snackBar.open(`Shipment created!`, "Close");
    });


  }

  onEditBtnClick(item) {
    this.getStyleList(this.shipment.poId)
    this.boxId = _.get(item, 'id')
    // this.getStyleList()
    this.boxForm = parseInt(_.get(item, 'boxNumber'))
    this.boxTo = parseInt(_.get(item, 'boxNumber'));
    this.customerId = parseInt(_.get(item, 'customerId'));
    this.editModeEnable = true;
    console.log(item)
    this.boxItemList = _.merge([], _.get(item, 'factoryShipmentPackingDetails'))

    _.each(this.boxItemList, box => {
      box.colorList = _.get(_.find(this.styleList, ['id', box.styleId]), 'combos')
    })
  }

  onStatusChange(flag, shipmentId) {
    if (!confirm("You wont be able to make any change after completing !<br> Are you sure to complete this?")) {
      return;
    }
    this._FactoryShipmentService.updateFields(shipmentId, { 'complete': flag, 'id': shipmentId }).subscribe((data) => {
      this.snackBar.open(`Status Updated!`, "Close");
      this.getShipmentDetail();
    });
  }

  editShipment(templateRef) {
    this.getPOList();
    this.shipmentUpdateObj = _.merge({}, this.shipment);
    this.modalRef = this.dialog.open(templateRef, {
      width: "600px",
      disableClose: true,
    });
  }

  getPOList() {
    this._poservice.getAll().subscribe(data => {
      this.poList = data;
    });
  }
  updateShipment() {

    if (!_.get(this.shipmentUpdateObj, "name")) {
      this.snackBar.open(`Please! Insert  name!`, "Close");
      return;
    }


    if (!_.get(this.shipmentUpdateObj, "shipmentDate")) {
      this.snackBar.open(`Please! Insert date!`, "Close");
      return;
    }
    if (!_.get(this.shipmentUpdateObj, "poId")) {
      this.snackBar.open(`Please! Select PO!`, "Close");
      return;
    }
    this._FactoryShipmentService.updateFields(this.shipmentUpdateObj.id, this.shipmentUpdateObj).subscribe(resp => {
      this.snackBar.open(`Shipment updated!`, 'Close');
      this.getShipmentDetail();
      this.clearObject();
      this.modalRef.close();
    });

  }

  clearObject() {
    this.boxItemList = [{}];
    this.customerId = null;
    this.boxForm = _.add(_.get(_.last(this.shipment.factoryShipmentPackings), 'boxNumber'), 1)
    this.boxTo = _.add(_.get(_.last(this.shipment.factoryShipmentPackings), 'boxNumber'), 1);
  }

  removeBox(id) {
    if (!confirm("Box delete confirm?")) {
      return;
    }
    this._FactoryShipmentService.deleteBox(id, this.id).subscribe(res => {
      this.snackBar.open(`Customer deleted!`, "Close");
      this.getShipmentDetail();
    });
  }

  // receive
  onSelectAllBoxes(flag) {
    _.each(this.shipment.factoryShipmentPackings, packing => {
      packing.selectd = flag;
    })
    this.selectedItems = _.filter(this.shipment.factoryShipmentPackings, 'selectd')
    console.log(this.selectedItems)
  }

  onSelectBox() {
    this.selectedItems = _.filter(this.shipment.factoryShipmentPackings, 'selectd')
  }

  getLocations() {
    this._locationService.getAll().subscribe((data) => {
      data.push({ "id": 'null', "name": 'No-location' })
      this.locationList = data;
      console.log(this.locationList)
    });
  }

  openLocationModal(templateRef) {
    this.getLocations();
    this.location = {
      locationId: null,
      remark: null,
    }
    // this.shipmentUpdateObj = _.merge({}, this.shipment);
    this.modalRef = this.dialog.open(templateRef, {
      width: "400px",
      disableClose: true,
    });
  }

  addLocation() {
    if (_.get(this.location, "locationId") == 'null') {
      if (!_.get(this.location, "remark")) {
        this.snackBar.open(`Please! Give a remark!`, "Close");
        return;
      }
    }
    let boxList = _.map(_.filter(this.shipment.factoryShipmentPackings, 'selectd'), v => {
      return {
        "id": v.id,
        "inventoryLocationId": _.get(this.location, 'locationId'),
        "receiveComment": _.get(this.location, 'remark')
      }
    })

    this._FactoryShipmentService.detailUpdate(boxList, this.id).subscribe(resp => {
      // this.getUserLocations();
      this.selectedItems = []
      this.selectAllBoxes = false
      this.clearObject();
      this.editModeEnable = false
      this.modalRef.close();
      this.getShipmentDetail();
      this.snackBar.open(`Location Added!`, "Close");

    })
  }

  onLocationSelect() {

  }

  poComplete() {
    if (!confirm("Are you sure to complete this !?")) {
      return;
    }
    this._FactoryShipmentService.completePO({ 'isPoShipmentComplete': true, 'id': this.id }).subscribe(resp => {
      this.getShipmentDetail();
      // this._router.navigateByUrl(`po`);
      this.snackBar.open(`PO Completed!`, "Close");
    })
  }

  onLocationClickWithoutBox() {
    this.snackBar.open(`Please select box first !`, "Close");
    return;
  }
  // receive

}

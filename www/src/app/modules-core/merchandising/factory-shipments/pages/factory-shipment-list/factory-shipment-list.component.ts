import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/authentication/auth.service';
import { integerStringToInteger, toDateString, removeEmptyFields, integerArrayToString, stringToIntegerArray } from 'app/modules-core/utility/helpers';
import * as _ from 'lodash';
import { UserService } from 'app/modules-core/common/user/user.service';
import { APP_URL } from 'app/app.constants';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { InventoryService } from 'app/modules-core/merchandising/inventory/inventory.service';
import * as moment from "moment";
import { SeasonService } from 'app/modules-core/common/season/season.service';
import { FactoryShipmentService } from 'app/modules-core/common/factory-shipment/factory-shipment.service';
import { POService } from 'app/modules-core/common/po/po.service';

@Component({
  selector: 'factory-shipment-list',
  templateUrl: './factory-shipment-list.component.html',
  animations: fuseAnimations,
})
export class FactoryShipmentListComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};
  shipmentsAndPackingList: any[] = [];
  seasonList: any[] = [];
  saleRepList: any[] = [];
  statusList: any[] = [];
  poList: any[] = [];
  newShipment: any = {};
  sorter: any = {
    field: null,
    sortType: 'asc',
    fieldType: 'text',
  };
  modalRef: any = null;
  selectedPO: any = {};


  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private dialog: MatDialog,
    private _inventoryService: InventoryService,
    private seasonService: SeasonService,
    private _FactoryShipmentService: FactoryShipmentService,
    private _poservice: POService,
    private snackBar: SnackbarService,
  ) {
    this._quickPanelSubjectService.heading.next("Shipments And Packing Lists");
  }
  ngOnInit() {
    this.statusList = [
      { 'name': 'Complete', 'value': true },
      { 'name': 'Draft', 'value': false }
    ]
    // this.getSeasonList();
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));
    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  // onDateNextPrivious(priv) {
  //   if (priv) {
  //     this.filter.todate = toDateString(
  //       moment(this.filter.todate || new Date())
  //         .add(1, "months")
  //         .endOf("month")
  //     );
  //     this.filter.fromdate = toDateString(
  //       moment(this.filter.todate || new Date()).startOf("month")
  //     );
  //   } else {
  //     this.filter.fromdate = toDateString(
  //       moment(this.filter.fromdate || new Date())
  //         .subtract(1, "months")
  //         .startOf("month")
  //     );
  //     this.filter.todate = toDateString(
  //       moment(this.filter.fromdate || new Date()).endOf("month")
  //     );
  //   }
  //   this.applyFilters(this.filter);
  // }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {

    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    this.shipmentsAndPackingList = [];
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    this._FactoryShipmentService.getAll(params).subscribe((data) => {
      this.shipmentsAndPackingList = data;
      this.setFilterToURL(filterParams);
    });
  }

  getSeasonList() {
    this.seasonService.getAll().subscribe((data) => {
      this.seasonList = data;
    });
  }

  getPOList() {
    this._poservice.getAll({ 'notship': true }).subscribe(data => {
      this.poList = data;
    });
  }

  openCreateModal(templateRef) {
    this.getPOList();
    this.clearCreateObject();
    this.modalRef = this.dialog.open(templateRef, {
      width: "600px",
      disableClose: true,
    });
  }

  onPOSelect(poid) {
    this.selectedPO = {}
    this.selectedPO = _.find(this.poList, ['id', poid]);
    console.log(this.selectedPO)
  }

  clearCreateObject() {
    this.newShipment = {
      name: null,
      shipmentDate: null,
    }
  }

  createShipment() {
    var payload = this.newShipment;
    if (!_.get(this.newShipment, "name")) {
      this.snackBar.open(`Please! Insert  name!`, "Close");
      return;
    }


    if (!_.get(this.newShipment, "shipmentDate")) {
      this.snackBar.open(`Please! Insert date!`, "Close");
      return;
    }
    if (!_.get(this.newShipment, "poId")) {
      this.snackBar.open(`Please! Select PO!`, "Close");
      return;
    }
    this._FactoryShipmentService.create(payload).subscribe(resp => {
      this._router.navigateByUrl(`factory-shipments/${resp.id}`);
      this.applyFilters(this.filter)
      this.snackBar.open(`Shipment created!`, 'Close');
      this.modalRef.close();
    });
  }

  onStatusSelect(event, shipmentId) {
    this._FactoryShipmentService.updateFields(shipmentId, { 'complete': _.get(event, 'value'), 'id': shipmentId }).subscribe((data) => {
      this.applyFilters(this.filter)
      this.snackBar.open(`Status Updated!`, "Close");
    });
  }

  deleteShipment(id) {
    if (!confirm("Delete confirm?")) {
      return;
    }
    this._FactoryShipmentService.delete(id).subscribe(res => {
      this.snackBar.open(`Shipment  deleted!`, "Close");
      this.applyFilters(this.filter)
    });
  }

  onDetailClick(id) {
    this._router.navigateByUrl(`factory-shipments/${id}`);
  }
} //class


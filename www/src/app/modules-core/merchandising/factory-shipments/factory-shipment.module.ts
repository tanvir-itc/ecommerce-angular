import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { FactoryShipmentListComponent } from './pages/factory-shipment-list/factory-shipment-list.component';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { FactoryShipmentDetailsComponent } from './pages/factory-shipment-details/factory-shipment-details.component';


const routes = [
  {
    path: 'factory-shipments',
    component: FactoryShipmentListComponent,
    // data: { permission: 'PERMISSION_MENU_WAREHOUSE_USER' },
    // canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  },
  {
    path: 'factory-shipments/:id',
    component: FactoryShipmentDetailsComponent,
    // data: { permission: 'PERMISSION_MENU_WAREHOUSE_USER' },
    // canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  }
];
@NgModule({
  declarations: [
    FactoryShipmentListComponent,
    FactoryShipmentDetailsComponent,
  ],
  entryComponents: [
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    DateTimeModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
    AttachmentModule,
  ],
  exports: [
  ]
})

export class FactoryShipmentsModule {
}

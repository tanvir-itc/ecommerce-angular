import { Component, Inject, OnInit, TemplateRef } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { AuthService } from "app/authentication/auth.service";
import { AttachmentService } from "app/modules-core/common/attachment/attachment.service";
import { StyleService } from "app/modules-core/merchandising/style/style.service";
import { StylePriceService } from "app/modules-core/common/price-type/style-price.service";
import * as _ from "lodash";
import { DigitalCatalogueService } from "../../digital-catalogue.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { getSummarnoteConfig } from "app/modules-core/utility/helpers";

@Component({
  selector: 'digital-catalogue-modal',
  templateUrl: 'digital-catalogue-modal.component.html',
})
export class DigitalCatalogueModalComponent implements OnInit {
  id = null;

  digiCatalouge: any = {};

  digitalCatalogueModal: any = null;
  styles: any[] = [];
  priceTiarList: any[] = [];
  priceId = null;
  termsCondition: any;
  modalRef: MatDialogRef<unknown, any>;
  config = getSummarnoteConfig();
  termsModalRef: any = null;

  constructor(
    public dialogRef: MatDialogRef<DigitalCatalogueModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _digitalCatalogueService: DigitalCatalogueService,
    private dialog: MatDialog,
    private _styleService: StyleService,
    public _auth: AuthService,
    public _attachmentService: AttachmentService,
    public _stylePriceService: StylePriceService,
    private snackBar: SnackbarService,
  ) {
    this.id = this.data.id
  }
  ngOnInit() {
    // this.getStylePrice()
    this.getDetails()
  }
  getDetails() {
    this._digitalCatalogueService.get(this.id).subscribe(resp => {
      _.each(resp.catalogPdfStyles, pdfStyleObj => {
        pdfStyleObj['priceId'] = _.get(_.find(pdfStyleObj.styleMultiPrices, ['price', _.get(pdfStyleObj, 'stylePrice')]), 'id') || null;
        let obj = { 'price': 'No Price', 'id': null }
        pdfStyleObj.styleMultiPrices.push(obj)
      })
      this.digiCatalouge = resp;
      console.log('this.digiCatalouge', this.digiCatalouge)
    })
  }

  update(flag = false) {
    let payload = {
      id: this.id,
      name: this.digiCatalouge.name,
      title: this.digiCatalouge.title,
      subTitle: this.digiCatalouge.subTitle,
      catalogPdfStyles: [],
      paymentTerm: this.digiCatalouge.paymentTerm,
      shippingCharge: this.digiCatalouge.shippingCharge,
    }
    _.each(this.digiCatalouge.catalogPdfStyles, item => {
      payload.catalogPdfStyles.push({
        id: item.id,
        styleId: item.styleId,
      })
    });
    this._digitalCatalogueService.update(this.id, payload).subscribe((data) => {
      this.getDetails();
      if (flag) {
        this.dialogRef.close(true);
      }
    });
  }
  openStyleModal(templateRef) {
    this._styleService.getAll().subscribe((data) => {
      this.styles = data;
      console.log(' this.styles', this.styles)
      this.digitalCatalogueModal = this.dialog.open(templateRef, {
        minWidth: "400px",
      });

    });
  }


  addStyle(style) {
    this.digiCatalouge.catalogPdfStyles.push({
      styleId: style.id,
      styleName: style.name,
    })
    this.update();
    this.closeModal();
    // this.priceTiarList = _.get(style, 'stylePriceList')
  }

  closeModal() {
    this.digitalCatalogueModal.close();
  }

  onDelete(id) {
    if (confirm("Delete confirm?")) {
      this._digitalCatalogueService.styleDelete(this.id, id).subscribe((data) => {
        this.getDetails();
      });
    }
  }

  updateStylePrice(item, priceId) {
    let price = _.find(item.styleMultiPrices, ['id', priceId])
    this._digitalCatalogueService.updateStylePriceData(_.get(item, 'catalogPdfId'), { 'id': item.id, 'stylePrice': price.price == 'No Price' ? null : price.price }).subscribe((data) => {
      this.snackBar.open(`Price updated!`, "Close");
      this.getDetails();
    });
  }
  openCreateModal(templateRef: TemplateRef<any>) {
    this.digiCatalouge.paymentTerm = null;
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "60%"
    });
  }

  openTermsModal(templateRef: TemplateRef<any>) {
    this.termsModalRef = this.dialog.open(templateRef, {
      minWidth: "60%"
    });
  }


  termsModalClose() {
    this.termsModalRef.close(true);
  }

}
import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  stringToIntegerArray,
  removeEmptyFields,
  integerArrayToString,
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { DigitalCatalogueService } from "../../digital-catalogue.service";
import { MatDialog } from "@angular/material";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { DigitalCatalogueModalComponent } from "../../components/digital-catalogue-modal/digital-catalogue-modal.component";
import { PdfModalComponent } from "app/modules-core/general/pdf-modal/pdf-modal.component";

@Component({
  selector: "digital-catalogue",
  templateUrl: "./digital-catalogue.component.html",
})
export class DigitalCatalogueComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  pdfList: any[] = [];

  newItem = {};

  customerList: any[] = [];
  saleRepList: any[] = [];

  sorter: any = {
    field: null,
    sortType: 'asc',
    fieldType: 'text',
  }

  modalRef: any = null;


  constructor(
    private snackBar: SnackbarService,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private dialog: MatDialog,
    private _digitalCatalogueService: DigitalCatalogueService,
  ) {
    this._quickPanelSubjectService.heading.next("Catalogue");
  }
  ngOnInit() {
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));
    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "statusname") {
        params[key] = stringToIntegerArray(params[key]);
      }
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "statusname") {
        params[key] = integerArrayToString(params[key]);
      }
      if (key == "customerid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    this.pdfList = [];
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    this._digitalCatalogueService.getAll(params).subscribe((data) => {
      this.pdfList = data;
      this.setFilterToURL(filterParams);
    });

  }

  openAddModal(templateRef) {
    this.newItem = {
      name: null
    }
    this.modalRef = this.dialog.open(templateRef, {
      width: "400px",
    });
  }

  create() {
    if (!_.get(this.newItem, "name")) {
      this.snackBar.open(`Please insert cost type!`, "Close");
      return;
    }
    this._digitalCatalogueService.create(this.newItem).subscribe((data) => {
      this.applyFilters(this.filter)
      this.modalRef.close();
    });
  }

  catalogueDetailsModal(id) {
    let modalRef = this.dialog.open(DigitalCatalogueModalComponent, {
      width: "480px",
      disableClose: true,
      data: {
        id: id
      }
    });
    modalRef.afterClosed().subscribe(result => {
      result && this.applyFilters(this.filter);
    });
  }

  openPdfModal(id, name) {
    this._digitalCatalogueService.getPdf(id).subscribe(data => {
      this.modalRef = this.dialog.open(PdfModalComponent, {
        // minWidth: "700px",
        width: "90%",
        // disableClose: true,
        data: {
          title: name,
          resource: _.get(data, 'path'),
          emailButton: true,
          emailObject: {
            type: 'digital-catalogue',
            digitalCatalogueId: id,
          }
        }
      });
    });
  }

  delete(id) {
    if (confirm("Delete confirm?")) {
      this._digitalCatalogueService.delete(id).subscribe((data) => {
        this.applyFilters(this.filter);
      });
    }
  }

} //class

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from '../../utility/date-time-picker/date-time.module';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DigitalCatalogueComponent } from './pages/digital-catalogue/digital-catalogue.component';
import { DigitalCatalogueModalComponent } from './components/digital-catalogue-modal/digital-catalogue-modal.component';
import { PdfViewModule } from 'app/modules-core/general/pdf-modal/pdf-view.module';
import { NgxSummernoteModule } from 'ngx-summernote';

const routes = [
  {
    path: 'digital-catalogue',
    component: DigitalCatalogueComponent,
    canActivate: [LoginCheckRouteGuard],
  },

];
@NgModule({
  declarations: [
    DigitalCatalogueComponent,
    DigitalCatalogueModalComponent,
  ],
  entryComponents: [
    DigitalCatalogueModalComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    AttachmentModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
    PdfViewModule,
    NgxSummernoteModule
  ],
  exports: [
  ]
})
export class DigitalCatalogueModule {
}
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/authentication/auth.service';
import { integerStringToInteger, toDateString, removeEmptyFields } from 'app/modules-core/utility/helpers';
import * as _ from 'lodash';
import { UserService } from 'app/modules-core/common/user/user.service';
import { APP_URL } from 'app/app.constants';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';


@Component({
  selector: 'warehouse-user-list',
  templateUrl: './warehouse-user-list.component.html',
  animations: fuseAnimations,
})
export class WarehouseUserListComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  users = [];


  modalRef = null;

  newUser: any = null;


  constructor(
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _userService: UserService,
  ) {
    this._quickPanelSubjectService.heading.next('Warehouse Users');
  }

  ngOnInit() {
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, 'snapshot.queryParams'));

    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
  }

  clearAllFilter() {
    this.filter = {
      firstname: null,
      lastname: null,
      email: null,
      phoneno: null,
      company: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
    }
    return params;
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {

    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    params['rolename'] = 'ROLE_WAREHOUSE';
    this._userService.getAll(params).subscribe(data => {
      this.users = data;
      this.setFilterToURL(filterParams);
    });
  }


  openCreateModal(templateRef) {
    this.clearCreateObject();
    this.modalRef = this.dialog.open(templateRef, {
      width: "600px",
      disableClose: true,
    });
  }

  clearCreateObject() {
    this.newUser = {
      email: null,
      firstName: null,
      lastName: null,
      phoneNo: null,
      company: null,
      address: null
    }
  }

  createUser() {
    var payload = this.newUser;
    if (!_.get(this.newUser, "firstName")) {
      this.snackBar.open(`Please! Insert first name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "lastName")) {
      this.snackBar.open(`Please! Insert last name!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "email")) {
      this.snackBar.open(`Please! Insert email!`, "Close");
      return;
    }
    if (!_.get(this.newUser, "phoneNo")) {
      this.snackBar.open(`Please! Insert phone no!`, "Close");
      return;
    }
    payload['roleName'] = 'ROLE_WAREHOUSE';
    payload['appUrl'] = `${APP_URL}login`;
    this._userService.create(payload).subscribe(resp => {
      this._router.navigateByUrl(`warehouse-user/${resp.id}`);
      this.snackBar.open(`User created!`, 'Close');
      this.modalRef.close();
    });
  }

  goLink(userId) {
    this._router.navigateByUrl(`warehouse-user/${userId}`);
  }

  deleteUser(id) {
    if (!confirm("Warehouse User delete confirm?")) {
      return;
    }
    this._userService.delete(id).subscribe(res => {
      this.snackBar.open(`Warehouse User deleted!`, "Close");
      this.applyFilters(this.filter)
    });
  }

} //class


import { Component, OnInit, TemplateRef } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/authentication/auth.service';
import { integerStringToInteger, toDateString, removeEmptyFields } from 'app/modules-core/utility/helpers';
import * as _ from 'lodash';
import { UserService } from 'app/modules-core/common/user/user.service';
import { APP_URL } from 'app/app.constants';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { SupplierService } from 'app/modules-core/common/suppliers/supplier.service';


@Component({
  selector: 'supplier-list',
  templateUrl: './supplier-list.component.html',
  animations: fuseAnimations,
})
export class SupplierListComponent implements OnInit {
  filterMore = false;
  editFlag = false;
  disableActionButton = false;
  filterCounter = 0;
  filter: any = {};

  suppliers = [];


  modalRef = null;

  newSupplier: any = null;


  constructor(
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _supplierService: SupplierService,
  ) {
    this._quickPanelSubjectService.heading.next('Suppliers');
  }

  ngOnInit() {
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, 'snapshot.queryParams'));

    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
  }

  clearAllFilter() {
    this.filter = {
      firstname: null,
      lastname: null,
      email: null,
      phoneno: null,
      company: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
    }
    return params;
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {

    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    // params['rolename'] = 'ROLE_SUPPLIER';
    this._supplierService.getAll(params).subscribe(data => {
      this.suppliers = data;
      this.setFilterToURL(filterParams);
    });
  }


  openCreateModal(templateRef) {
    this.disableActionButton = false;
    this.editFlag = false;
    this.clearCreateObject();
    this.modalRef = this.dialog.open(templateRef, {
      width: "600px",
      disableClose: true,
    });
  }

  openEditModal(templateRef: TemplateRef<any>, item) {
    this.disableActionButton = false;
    this.editFlag = true;
    this.clearCreateObject();
    this.newSupplier = _.merge({}, item);
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "35%"
    });
  }

  clearCreateObject() {
    this.newSupplier = {
      email: null,
      name: null,
      phone: null,
      address: null
    }
  }

  createSupplier() {

    var payload = this.newSupplier;
    if (!_.get(this.newSupplier, "name")) {
      this.snackBar.open(`Please! Insert  name!`, "Close");
      this.disableActionButton = false;
      return;
    }


    // payload['roleName'] = 'ROLE_SUPPLIER';
    // payload['appUrl'] = `${APP_URL}login`;

    if (!this.editFlag) {
      this._supplierService.create(payload).subscribe(resp => {
        this.snackBar.open(`Supplier created!`, 'Close');
        this.applyFilters(this.filter);
        this.modalRef.close();
      });
    } else {
      this._supplierService.update(this.newSupplier.id, payload).subscribe(resp => {
        this.snackBar.open(`Supplier updated!`, 'Close');
        this.applyFilters(this.filter);
        this.modalRef.close();
      });
    }


  }

  goLink(userId) {
    this._router.navigateByUrl(`factory-user/${userId}`);
  }

  deleteSupplier(id) {
    if (!confirm("Supplier delete confirm?")) {
      return;
    }
    this._supplierService.delete(id).subscribe(res => {
      this.snackBar.open(`Supplier deleted!`, "Close");
      this.applyFilters(this.filter)
    });
  }

} //class


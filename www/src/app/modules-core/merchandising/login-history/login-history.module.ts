import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from '../../utility/date-time-picker/date-time.module';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { LoginHistoryComponent } from './pages/login-history/login-history.component';

const routes = [
  {
    path: 'login-history',
    component: LoginHistoryComponent,
    data: { permission: 'PERMISSION_ADMIN_USER' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  },

];
@NgModule({
  declarations: [
    LoginHistoryComponent,
  ],
  entryComponents: [
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    AttachmentModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
  ],
  exports: [
  ]
})
export class LoginHistoryModule {
}
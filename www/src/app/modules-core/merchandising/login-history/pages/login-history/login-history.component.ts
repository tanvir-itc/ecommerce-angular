import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  stringToIntegerArray,
  removeEmptyFields,
  integerArrayToString,
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { CustomerService } from "app/modules-core/merchandising/customers/customer.service";

import { UserService } from "app/modules-core/common/user/user.service";

@Component({
  selector: "login-history",
  templateUrl: "./login-history.component.html",
})
export class LoginHistoryComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  orders: any[] = [];



  customerList: any[] = [];
  saleRepList: any[] = [];

  sorter: any = {
    field: null,
    sortType: 'asc',
    fieldType: 'text',
  }


  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _customerService: CustomerService,
    private _userService: UserService,
  ) {
    this._quickPanelSubjectService.heading.next("Login History");
  }
  ngOnInit() {
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));
    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
    this.getAllCustomers();
    this.getAllSaleRep();




  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "statusname") {
        params[key] = stringToIntegerArray(params[key]);
      }
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "statusname") {
        params[key] = integerArrayToString(params[key]);
      }
      if (key == "customerid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    this.orders = [];
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    this._userService.getAllLoginHistory(params).subscribe((data) => {
      this.orders = data;
      this.setFilterToURL(filterParams);
    });

  }

  getAllSaleRep() {
    if (this._auth.hasPermission("PERMISSION_ADMIN_USER")) {
      var params = {};
      params['rolename'] = 'ROLE_SALES';
      this._userService.getAll(params).subscribe(data => {
        this.saleRepList = data;
      });
    }

  }

  getAllCustomers() {
    this._customerService.getAll({ requested: false }).subscribe((data) => {
      this.customerList = data;
    });
  }


} //class

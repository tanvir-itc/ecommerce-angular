import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/authentication/auth.service';
import { integerStringToInteger, toDateString, removeEmptyFields } from 'app/modules-core/utility/helpers';
import * as _ from 'lodash';
import { UserService } from 'app/modules-core/common/user/user.service';
import { APP_URL } from 'app/app.constants';
import { SnackbarService } from 'app/modules-core/utility/snackbar.service';
import { SupplierService } from 'app/modules-core/common/suppliers/supplier.service';
import { POService } from 'app/modules-core/common/po/po.service';
import { StyleService } from 'app/modules-core/merchandising/style/style.service';
import { SizeService } from 'app/modules-core/common/size/size.service';
import { SeasonService } from 'app/modules-core/common/season/season.service';


@Component({
  selector: 'po-list',
  templateUrl: './po-list.component.html',
  animations: fuseAnimations,
})
export class POListComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};
  style: any = {};
  selectedPo: any = {};
  supplierList = [];
  poList = [];
  styleList = [];
  styleListForPoEdit = [];
  styleBreakdownList = [];
  seasonList = [];
  poDetailList = [];
  data = [];
  modalRef = null;
  styleModalodalRef = null;
  styleEditModalodalRef = null;
  newPO: any = null;
  poId = null;
  po = null;
  totalQty = null;
  minValue = toDateString(new Date());
  colorQtyOnAddMode = 0;
  StyleQtyOnAddMode = 0;
  size = {
    items: []
  };
  tabs: any = {};
  items = [];
  editMode = false;


  constructor(
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _supplierService: SupplierService,
    private _poservice: POService,
    private _styleService: StyleService,
    private sizeService: SizeService,
    private seasonService: SeasonService,
  ) {
    this._quickPanelSubjectService.heading.next('POs');
  }

  ngOnInit() {
    this.getAllSupplier();
    this.getSeasonList();
    this.clearAllFilter();
    this.clearTabsFlag('PENDING');
    var queryParams = _.merge({}, _.get(this._route, 'snapshot.queryParams'));

    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
  }
  clearTabsFlag(flag) {
    this.poList = []
    this.tabs = {
      PENDING: false,
      RECEIVED: false,
      PARTIAL: false,
    }
    if (flag) {
      this.tabs[flag] = true;
    }
    this.applyFilters(this.filter);
  }
  clearAllFilter() {
    this.filter = {
      firstname: null,
      lastname: null,
      email: null,
      phoneno: null,
      company: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
    }
    return params;
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {

    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    var params = _.merge({}, filterParams);
    if (this.tabs.PENDING) {
      params['shipStatus'] = 'PENDING';
    }
    if (this.tabs.RECEIVED) {
      params['shipStatus'] = 'SHIPPED';
    }
    if (this.tabs.PARTIAL) {
      params['shipStatus'] = 'PARTIAL';
    }
    params = removeEmptyFields(params);
    // params['rolename'] = 'ROLE_SUPPLIER';
    this._poservice.getAll(params).subscribe(data => {
      this.poList = data;
      this.setFilterToURL(filterParams);
    });
  }


  openCreateModal(templateRef) {
    this.editMode = false
    this.clearCreateObject();
    this.modalRef = this.dialog.open(templateRef, {
      width: "600px",
      disableClose: true,
    });
  }

  onChange() {
    this.minValue = this.newPO.shipmentDate
  }

  clearCreateObject() {
    this.newPO = {
      supplierId: null,
      shipmentDate: toDateString(new Date()),
      poNumber: null,
      receiveDate: toDateString(new Date()),
      termAndCondition: null
    }
  }

  createPO(templateRef) {
    var payload = this.newPO;
    if (!_.get(this.newPO, "supplierId")) {
      this.snackBar.open(`Please! Select supplier!`, "Close");
      return;
    }

    if (!_.get(this.newPO, "number")) {
      this.snackBar.open(`Please! Insert PO Number!`, "Close");
      return;
    }
    if (!_.get(this.newPO, "receiveDate")) {
      this.snackBar.open(`Please! Insert receive date!`, "Close");
      return;
    }
    if (!_.get(this.newPO, "shipmentDate")) {
      this.snackBar.open(`Please! Insert shipment date!`, "Close");
      return;
    }
    this._poservice.create(payload).subscribe(resp => {
      this.po = resp;
      this.getAllStyle();
      this.snackBar.open(`PO created!`, 'Close');
      this.modalRef.close();
      this.applyFilters(this.filter);
      this.styleBreakdownList = []
      this.styleModalodalRef = this.dialog.open(templateRef, {
        width: "80%",
        disableClose: true,
      });
    })
  }

  goLink(userId) {
    this._router.navigateByUrl(`factory-user/${userId}`);
  }

  deletePO(id) {
    if (!confirm("PO delete confirm?")) {
      return;
    }
    this._poservice.delete(id).subscribe(res => {
      this.snackBar.open(`PO deleted!`, "Close");
      this.applyFilters(this.filter)
    });
  }

  getAllSupplier() {
    this._supplierService.getAll({ requested: false }).subscribe((data) => {
      this.supplierList = data;
    });
  }

  getSeasonList() {
    this.seasonService.getAll().subscribe((data) => {
      this.seasonList = data;
    });
  }

  getAllStyle() {
    this._styleService.getAll().subscribe(data => {
      this.styleList = data;
      console.log('this.styleList', this.styleList)
    });
  }

  onStyleSelect(data, dataFromStyleDropDown) {
    if (data.styleList) {
      this.items = []
      _.each(data.combos, combo => {
        combo['comboSizeList'] = _.map(data.sizeGroupSizes, size => {
          return {
            styleComboId: combo.id,
            styleId: data.id,
            styleName: data.name,
            colorName: combo.name,
            sizeId: size.sizeId,
            qtyObj: {}
          }
        })
      })

      this.items.push(data)
      let obj = {
        'sizeGroupId': data.sizeGroupId,
        'slyleDetailList': this.items,
        'sizeList': data.sizeGroupSizes
      };

      if (!this.styleBreakdownList.length) {
        this.styleBreakdownList.push(obj)
      } else if (this.styleBreakdownList.length) {
        let isSizeGroupExist = _.find(this.styleBreakdownList, ['sizeGroupId', obj.sizeGroupId])
        _.each(this.styleBreakdownList, style => {
          if (style.sizeGroupId == obj.sizeGroupId) {
            style.slyleDetailList.push(_.head(obj.slyleDetailList))
          } else if (!isSizeGroupExist) {
            this.styleBreakdownList.push(obj)
          }
        })
      }

    } else if (!data.styleList) {
      let styleList = _.chain(this.styleBreakdownList)
        .find(['sizeGroupId', data.sizeGroupId])
        .get('slyleDetailList')
        .value()
      if (styleList.length) {
        _.remove(styleList, ['id', data.id])
      }
      if (styleList.length == 0) {
        _.remove(this.styleBreakdownList, ['sizeGroupId', data.sizeGroupId])
      }

    }
    console.log('this.styleBreakdownList', this.styleBreakdownList)
  }
  onQtyChange(groupIndex, styleIndex, comboIndex, color, style) {
    if (!this.editMode) {
      _.each(color.comboSizeList, size => {
        size['quantity'] = _.parseInt(_.toString(Object.values(size.qtyObj)))
      })
      this.colorQtyOnAddMode = _.sumBy(_.filter(color.comboSizeList, f => { return f.quantity > 0 }), 'quantity')
      this.styleBreakdownList[groupIndex].slyleDetailList[styleIndex].combos[comboIndex]['colorQty'] = this.colorQtyOnAddMode;
      this.StyleQtyOnAddMode = _.sumBy(style.combos, 'colorQty')
      this.styleBreakdownList[groupIndex].slyleDetailList[styleIndex]['styleQty'] = this.StyleQtyOnAddMode;
    } else if (this.editMode) {
      _.each(color.comboSizeList, size => {
        size['quantity'] = _.parseInt(size.quantity)
      })
      _.each(this.poDetailList, group => {
        _.each(group.slyleDetailList, style => {
          style['styleQty'] = _.sumBy(_.flatMap(style.combos, 'comboSizeList'), 'quantity')
          _.each(style.combos, combo => {
            combo['colorQty'] = _.sumBy(combo.comboSizeList, 'quantity')
          })
        })
      })

    }
  }


  copletePO() {
    let payload = _.chain(this.styleBreakdownList)
      .flatMap('slyleDetailList')
      .flatMap('combos')
      .flatMap('comboSizeList')
      .map(v => {
        return {
          "poId": _.get(this.po, 'id'),
          "quantity": _.parseInt(_.toString(Object.values(v.qtyObj))) ? _.parseInt(_.toString(Object.values(v.qtyObj))) : 0,
          "sizeId": v.sizeId,
          "styleComboId": v.styleComboId,
          "styleId": v.styleId
        }
      })
      .value()

    this._poservice.addPODetail(_.get(this.po, 'id'), _.filter(payload, 'quantity')).subscribe(resp => {
      this.snackBar.open(`PO updated!`, "Close");
      this.styleModalodalRef.close();
    })
  }

  poDetail(podetail, templateRef) {
    this.editMode = true;
    this.selectedPo = podetail
    this.getAllStyle();
    this._poservice.getPoDetail(_.get(this.selectedPo, 'id')).subscribe(resp => {
      this.totalQty = _.sumBy(resp.data, 'quantity')
      _.each(resp.data, data => {
        data['sizeGroupId'] = data.sizeGroup.id
      })
      if (resp.status == 200) {
        this.onEditMode(resp.data, templateRef);
      }

    })
  }

  onEditMode(dataList, templateRef) {
    console.log('edit data', dataList)
    let poDetailStyleList = _.map(this.styleList, style => {
      let isExist = _.find(dataList, { styleId: style.id, sizeGroupId: style.sizeGroupId })
      if (isExist) {
        style.styleList = true;
        return style
      } else {
        return null
      }
    })

    this.poDetailList = _.map(_.groupBy(_.filter(poDetailStyleList), 'sizeGroupId'), styleGroup => {
      _.each(styleGroup, style => {
        _.each(style.combos, combo => {
          combo['comboSizeList'] = _.map(style.sizeGroupSizes, size => {
            let similarItem = _.find(dataList, {
              styleId: style.id, sizeGroupId: style.sizeGroupId, sizeId: size.sizeId, styleComboId: combo.id
            })
            return {
              styleComboId: combo.id,
              styleId: style.id,
              size: size.name,
              styleName: style.name,
              colorName: combo.name,
              sizeId: size.sizeId,
              quantity: similarItem ? _.get(similarItem, 'quantity') : 0,
              id: similarItem ? _.get(similarItem, 'id') : null,
            }
          })
        })
      })
      return {
        'sizeGroupId': styleGroup[0].sizeGroupId,
        'slyleDetailList': styleGroup,
        'sizeList': styleGroup[0].sizeGroupSizes
      }
    })

    _.each(this.poDetailList, group => {
      _.each(group.slyleDetailList, style => {
        style['styleQty'] = _.sumBy(_.flatMap(style.combos, 'comboSizeList'), 'quantity')
        _.each(style.combos, combo => {
          combo['colorQty'] = _.sumBy(combo.comboSizeList, 'quantity')
        })
      })
    })
    this.styleEditModalodalRef = this.dialog.open(templateRef, {
      width: "80%",
      disableClose: true,
    });
  }

  updatePoStyleQty() {
    let payload = _.chain(this.poDetailList)
      .flatMap('slyleDetailList')
      .flatMap('combos')
      .flatMap('comboSizeList')
      .map(v => {
        return {
          "poId": _.get(this.selectedPo, 'id'),
          "quantity": v.quantity,
          "sizeId": v.sizeId,
          "styleComboId": v.styleComboId,
          "styleId": v.styleId,
          'id': v.id
        }
      })
      .filter('quantity')
      .value()
    this._poservice.updatePODetail(_.get(this.selectedPo, 'id'), payload).subscribe(resp => {
      this.snackBar.open(`PO quantity updated !`, "Close");
      this.styleModalodalRef.close();
    })
  }


}


//class

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from '../../utility/date-time-picker/date-time.module';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { InventoryHistoryComponent } from './pages/inventory-history.component';
import { InventoryDetailsModalComponent } from './components/inventory-details-modal/inventory-details-modal.component';

const routes = [
  {
    path: 'inventory-history',
    component: InventoryHistoryComponent,
    data: { permission: 'PERMISSION_MENU_INVENTORY_HISTORY_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  },
];
@NgModule({
  declarations: [
    InventoryHistoryComponent,
    InventoryDetailsModalComponent,
  ],
  entryComponents: [
    InventoryDetailsModalComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    DateTimeModule,
    AttachmentModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
  ],
  exports: [
  ]
})
export class InventoryHistoryModule {
}
import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  stringToIntegerArray,
  removeEmptyFields,
  integerArrayToString,
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { InventoryService } from "../../inventory/inventory.service";
import { MatDialog } from "@angular/material";
import { InventoryDetailsModalComponent } from "../components/inventory-details-modal/inventory-details-modal.component";

@Component({
  selector: 'inventory-history',
  templateUrl: './inventory-history.component.html',
})
export class InventoryHistoryComponent implements OnInit {

  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  historyList: any[] = [];



  customerList: any[] = [];
  saleRepList: any[] = [];

  sorter: any = {
    field: null,
    sortType: 'asc',
    fieldType: 'text',
  }


  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private dialog: MatDialog,
    private _inventoryService: InventoryService,
  ) {
    this._quickPanelSubjectService.heading.next("Inventory Change History");
  }
  ngOnInit() {
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));
    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "statusname") {
        params[key] = stringToIntegerArray(params[key]);
      }
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "statusname") {
        params[key] = integerArrayToString(params[key]);
      }
      if (key == "customerid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    this.historyList = [];
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    this._inventoryService.getAllHistory(params).subscribe((data) => {
      this.historyList = data;
      this.setFilterToURL(filterParams);
    });

  }

  openDetailsModal(item) {
    const returnModal = this.dialog.open(InventoryDetailsModalComponent, {
      maxWidth: '95%',
      minWidth: '300px',
      disableClose: true,
      data: {
        inventory: item,
      }
    });
    returnModal.afterClosed().subscribe(result => {

    });
  }
}
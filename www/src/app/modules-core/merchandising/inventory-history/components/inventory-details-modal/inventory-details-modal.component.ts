import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { AuthService } from "app/authentication/auth.service";
import * as _ from "lodash";


@Component({
  selector: 'inventory-details-modal',
  templateUrl: 'inventory-details-modal.component.html',
})
export class InventoryDetailsModalComponent implements OnInit {

  object: any = null;

  inventory: any[] = [];

  constructor(
    public _auth: AuthService,
    public dialogRef: MatDialogRef<InventoryDetailsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.object = this.data.inventory
  }

  ngOnInit() {
    this.getBreakdown();
  }

  getBreakdown() {
    this.object

    this.object.details = _.map(this.object.details, group => {
      group.inventories = _.map(group.inventories, style => {
        let uniqueCombos = _.uniqBy(style.inventoryHistories, 'styleComboId');
        style['colorList'] = _.map(uniqueCombos, item => {
          return {
            id: _.get(item, 'styleComboId'),
            name: _.get(item, 'styleCombo'),
          }
        });
        style['breakdown'] = {};
        let qty = 0;
        _.each(style.inventoryHistories, qtyBreakdown => {
          qtyBreakdown['adjustQuantity'] = _.sumBy(qtyBreakdown.details, 'quantity');
          qty = _.add(qtyBreakdown['adjustQuantity'] || 0, qty)
          let key = null;
          key = "_" + qtyBreakdown.styleComboId + "_" + qtyBreakdown.sizeId;
          style.breakdown[key] = qtyBreakdown;
        });
        style['totalAdjustQuantity'] = qty;
        return style;
      });
      return group;
    });
  }


}
import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { MatDialog } from "@angular/material";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  stringToIntegerArray,
  removeEmptyFields,
  integerArrayToString,
  isDateBefore,
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { CustomerService } from "app/modules-core/merchandising/customers/customer.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { ShipmentService } from "app/modules-core/merchandising/shipping/shipment.service";
import { InvoiceService } from "../../invoice.service";
import { forkJoin } from "rxjs";
import { InvoiceCreateUpdateModalComponent } from "../../components/invoice-create-update/invoice-create-update-modal.component";
import { PdfModalComponent } from "app/modules-core/general/pdf-modal/pdf-modal.component";

@Component({
  selector: "invoice-list",
  templateUrl: "./invoice-list.component.html",
})
export class InvoiceListComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  shipments: any[] = [];
  total: any = {};

  customerList: any[] = [];


  shipment: any = null;
  shipmentId: any = null;
  styleBreakdown: any[] = [];


  paymentList: any[] = [];

  newPayment: any = null;

  modalRef: any = null;
  userRole: any = null;

  constructor(
    private snackBar: SnackbarService,
    private dialog: MatDialog,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _customerService: CustomerService,
    private _shipmentService: ShipmentService,
    private _invoiceService: InvoiceService,
  ) {
    this._quickPanelSubjectService.heading.next("Invoices");
  }
  ngOnInit() {
    this.userRole = _.chain(this._auth.getCurrentUser()).get('roles').head().get('name').value();
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));

    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
    this.getAllCustomers();
  }

  clearAllFilter() {
    this.filter = {
      // fromdate: toDateString(
      //   moment(this.filter.todate || new Date())
      //     .subtract(3, "months")
      //     .startOf("month")
      // ),
      // todate: toDateString(
      //   moment(this.filter.todate || new Date()).endOf("month")
      // ),
      fromdate: null,
      todate: null,
      styleid: null,
      orderid: null,
      customerid: [],
      shipmentid: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "customerid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    params['invoice'] = true;
    params['payment'] = false;
    params['sort'] = 's.invoice_date,desc';
    this._shipmentService.getAll(params).subscribe((data) => {
      this.shipments = data;
      _.each(this.shipments, parent => {
        parent['due'] = false;
        if (_.get(parent, 'paymentDueDate')) {
          parent['due'] = isDateBefore(_.get(parent, 'paymentDueDate'), new Date);
          parent['filteredinvoIceSendDates'] = _.slice(parent.invoiceSendDates, -3);
        }
      });
      console.log(this.shipments)
      this.total = {
        totalValue: _.sumBy(this.shipments, 'totalValue'),
        tax: _.sumBy(this.shipments, 'tax'),
        freightCharge: _.sumBy(this.shipments, 'freightCharge'),
        otherCharge: _.sumBy(this.shipments, 'otherCharge'),
        handlingCharge: _.sumBy(this.shipments, 'handlingCharge'),
        discount: _.sumBy(this.shipments, 'discount'),
        ccUpCharge: _.sumBy(this.shipments, 'ccUpCharge'),
        netTotal: _.sumBy(this.shipments, 'netTotal'),
        totalPayment: _.sumBy(this.shipments, 'totalPayment'),
        dueAmount: _.sumBy(this.shipments, 'dueAmount'),
      }

      this.setFilterToURL(filterParams);
    });
  }

  getAllCustomers() {
    this._customerService.getAll({ requested: false }).subscribe((data) => {
      this.customerList = data;
    });
  }

  goLink(id) {
    this._router.navigateByUrl(`invoice/${id}/shipment`);
  }

  goLinkInvoice(id) {
    this._router.navigateByUrl(`invoice/${id}/invoice`);
  }



  openPaymentCreateModal(shipmentId, templateRef) {
    this.clearPaymentDetails();
    this.shipmentId = shipmentId;
    this.getShipment();

    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "700px",
      maxWidth: "90%",
      disableClose: true,
    });
  }

  clearPaymentDetails() {
    this.newPayment = {
      paymentDate: toDateString(new Date),
      //creditNoteAdjustment: null,
      //discount: null,
      //finalValue: null,
      paymentValue: null,
      shipmentId: null,
      comment: null
    }
  }


  getShipment() {
    this.shipment = null;
    this.styleBreakdown = [];
    this.paymentList = [];
    forkJoin(
      this._shipmentService.getBreakdown(this.shipmentId),
      this._invoiceService.getAllPayment({ 'shipment.id': this.shipmentId })
    ).subscribe(resp => {
      this.shipment = _.get(resp[0], 'shipment')
      this.styleBreakdown = _.get(resp[0], 'breakdown');
      this.paymentList = resp[1];
      this.newPayment['paymentValue'] = _.round(_.get(this.shipment, 'dueAmount', 0), 2);
    });
  }


  addPayments() {
    if (!_.get(this.newPayment, "paymentDate")) {
      this.snackBar.open(`Please! Select payment date!`, "Close");
      return;
    }
    if (!_.get(this.newPayment, "paymentValue")) {
      this.snackBar.open(`Please! Enter payment amount!`, "Close");
      return;
    }
    let payload = _.merge({}, this.newPayment);
    payload['shipmentId'] = this.shipmentId;
    this._invoiceService.createPayment(payload).subscribe((data) => {
      this.snackBar.open(`Invoice payment completed!`, "Close");
      this.modalRef.close();
      this.applyFilters(this.filter);
      //this._router.navigateByUrl(`invoice/payments`);
    });
  }

  openInvoiceCreateModal(shipmentId) {
    this.modalRef = this.dialog.open(InvoiceCreateUpdateModalComponent, {
      minWidth: "700px",
      maxWidth: "90%",
      disableClose: true,
      data: {
        shipmentId: shipmentId,
        isUpdate: true
      }
    });
  }

  openInvoicePDFModal(shipmentId) {
    this._shipmentService.getInvoicePDF(shipmentId).subscribe(resp => {
      this.modalRef = this.dialog.open(PdfModalComponent, {
        // minWidth: "700px",
        width: "90%",
        // disableClose: true,
        data: {
          title: "Invoice",
          resource: _.get(resp, 'path'),
          emailButton: true,
          emailObject: {
            type: 'invoice',
            shipmentId: shipmentId,
          }
        }
      });
    })
  }

  sendToBadDebt() {
    if (!confirm('Are you sure to do this!?')) {
      return;
    }
    let payload = { 'id': this.shipmentId, 'badDebt': true }
    this._shipmentService.sendToUnPaid(payload).subscribe(resp => {
      // this.snackBar.open(`Saved as unpaid!`, "Close");
      this.modalRef.close();
      this._router.navigateByUrl(`invoice/baddebt`);
    });

  }

} //class

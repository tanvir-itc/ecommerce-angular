import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { fuseAnimations } from "@fuse/animations";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";

@Component({
  selector: "app-invoice",
  templateUrl: "./invoice.component.html",
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class InvoiceComponent implements OnInit {
  show = false;
  tabs;
  tabFromUrl;

  basePath;

  constructor(
    private _route: ActivatedRoute,
    private router: Router,
    private _auth: AuthService,
  ) {
    // this.id = +this._route.snapshot.paramMap.get("id");
    this.tabFromUrl = this._route.snapshot.firstChild.routeConfig.path; //find a better way to get child param
    this.basePath = this._route.snapshot.url[0].path; //find a better way to get child param
  }

  ngOnInit() {
    this._route.paramMap.subscribe(params => {
      this.show = false;
      this.tabs = [
        {
          path: `${this.basePath}/list`,
          label: "Unpaid Invoices",
          display: this._auth.hasPermission('PERMISSION_INVOICE_LIST_VIEW') ? true : false
        },
        {
          path: `${this.basePath}/payments`,
          label: "Paid Invoices",
          display: this._auth.hasPermission('PERMISSION_INVOICE_PAYMENT_VIEW') ? true : false
        },
        {
          path: `${this.basePath}/pending`,
          label: "Generate Invoices",
          display: this._auth.hasPermission('PERMISSION_INVOICE_PENDING_VIEW') ? true : false
        },
        {
          path: `${this.basePath}/baddebt`,
          label: "Bad Debt Invoices",
          display: this._auth.hasPermission('PERMISSION_INVOICE_PAYMENT_VIEW') ? true : false
        },
      ];
      this.show = true;
    });
  }

}

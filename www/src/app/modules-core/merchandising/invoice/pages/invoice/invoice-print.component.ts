import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuickPanelSubjectService } from 'app/layout/components/quick-panel/quick-panel-subject.service';
import { SizeService } from 'app/modules-core/common/size/size.service';
import { ShipmentService } from 'app/modules-core/merchandising/shipping/shipment.service';
import * as _ from "lodash";
import { forkJoin } from 'rxjs';
import { InvoiceService } from '../../invoice.service';

@Component({
  selector: 'app-invoice-print',
  templateUrl: './invoice-print.component.html',
  styleUrls: ['./invoice-print.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoicePrintComponent implements OnInit {
  id: any = null;
  shipment: any = null;

  styleBreakdown: any[] = [];
  sizes: any[] = [];
  paymentList: any[] = [];

  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _sizeService: SizeService,
    private _shipmentService: ShipmentService,
    private _invoiceService: InvoiceService,
    private _quickPanelSubjectService: QuickPanelSubjectService
  ) {
    this.id = + this.route.snapshot.paramMap.get('id');
    this._quickPanelSubjectService.heading.next('Invoice Print');
  }

  ngOnInit() {
    this.getOrder();
  }

  getOrder() {
    forkJoin([
      this._sizeService.getAll(),
      this._shipmentService.getBreakdown(this.id),
      this._invoiceService.getAllPayment({ 'shipment.id': this.id })
    ]).subscribe(results => {
      this.sizes = results[0];
      this.shipment = _.get(results[1], 'shipment');
      this.styleBreakdown = _.get(results[1], 'breakdown');
      this.paymentList = results[2];
    });
  }


  goLink(id) {
    this._router.navigateByUrl(`styles/${id}/`);
  }
}

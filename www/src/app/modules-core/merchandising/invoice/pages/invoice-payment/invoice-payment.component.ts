import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  stringToIntegerArray,
  removeEmptyFields,
  integerArrayToString,
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { CustomerService } from "app/modules-core/merchandising/customers/customer.service";
import { ShipmentService } from "app/modules-core/merchandising/shipping/shipment.service";
import { MatDialog } from "@angular/material";
import { PdfModalComponent } from "app/modules-core/general/pdf-modal/pdf-modal.component";
import { InvoiceService } from "../../invoice.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";

@Component({
  selector: "invoice-payment",
  templateUrl: "./invoice-payment.component.html",
})
export class InvoicePaymentComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  shipments: any[] = [];
  total: any = {};

  customerList: any[] = [];
  modalRef = null;

  userRole: any = null;
  shipmentId: any = null;
  paymentList: any[] = [];

  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _customerService: CustomerService,
    private _shipmentService: ShipmentService,
    private _invoiceService: InvoiceService,
    private dialog: MatDialog,
    private snackBar: SnackbarService,
  ) {
    this._quickPanelSubjectService.heading.next("Invoices");
  }
  ngOnInit() {
    this.userRole = _.chain(this._auth.getCurrentUser()).get('roles').head().get('name').value();
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));

    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
    this.getAllCustomers();
  }

  clearAllFilter() {
    this.filter = {
      // fromdate: toDateString(
      //   moment(this.filter.todate || new Date())
      //     .subtract(3, "months")
      //     .startOf("month")
      // ),
      // todate: toDateString(
      //   moment(this.filter.todate || new Date()).endOf("month")
      // ),
      fromdate: null,
      todate: null,
      styleid: null,
      orderid: null,
      customerid: [],
      shipmentid: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "customerid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    params['payment'] = true;
    params['sort'] = 's.payment_date,desc';
    this._shipmentService.getAll(params).subscribe((data) => {
      this.shipments = data;
      this.total = {
        totalQuantity: _.sumBy(this.shipments, 'totalQuantity'),
        totalValue: _.sumBy(this.shipments, 'totalValue'),
        tax: _.sumBy(this.shipments, 'tax'),
        freightCharge: _.sumBy(this.shipments, 'freightCharge'),
        otherCharge: _.sumBy(this.shipments, 'otherCharge'),
        handlingCharge: _.sumBy(this.shipments, 'handlingCharge'),
        discount: _.sumBy(this.shipments, 'discount'),
        ccUpCharge: _.sumBy(this.shipments, 'ccUpCharge'),
        netTotal: _.sumBy(this.shipments, 'netTotal'),
        totalPayment: _.sumBy(this.shipments, 'totalPayment'),
      }
      this.setFilterToURL(filterParams);
    });
  }

  getAllCustomers() {
    this._customerService.getAll({ requested: false }).subscribe((data) => {
      this.customerList = data;
    });
  }




  goLink(id) {
    this._router.navigateByUrl(`invoice/${id}/shipment`);
  }

  goLinkInvoice(id) {
    this._router.navigateByUrl(`invoice/${id}/invoice`);
  }


  openInvoicePDFModal(shipmentId) {
    this._shipmentService.getInvoicePDF(shipmentId).subscribe(resp => {
      this.modalRef = this.dialog.open(PdfModalComponent, {
        // minWidth: "700px",
        width: "90%",
        // disableClose: true,
        data: {
          title: "Invoice",
          resource: _.get(resp, 'path'),
          emailButton: true,
          emailObject: {
            type: 'invoice',
            shipmentId: shipmentId,
          }
        }
      });
    })
  }


  openPaymentModal(shipmentId, templateRef) {
    this.shipmentId = shipmentId;
    this.getPayments();
    this.modalRef = this.dialog.open(templateRef, {
      minWidth: "700px",
      maxWidth: "90%",
      disableClose: true,
    });
  }

  getPayments() {
    this.paymentList = [];
    this._invoiceService.getAllPayment({ 'shipment.id': this.shipmentId }).subscribe(resp => {
      this.paymentList = resp;
    });
  }

  unPaid() {
    if (!confirm('Are you sure to do this!?')) {
      return;
    }
    let payload = { 'id': this.shipmentId, 'payment': false }
    this._shipmentService.sendToUnPaid(payload).subscribe(resp => {
      this.snackBar.open(`Saved as unpaid!`, "Close");
      this.modalRef.close();
      this._router.navigateByUrl(`invoice/list`);
    });

  }

} //class

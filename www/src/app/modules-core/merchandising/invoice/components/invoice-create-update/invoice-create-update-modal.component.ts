import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import { ShipmentService } from "app/modules-core/merchandising/shipping/shipment.service";
import { toDateString } from "app/modules-core/utility/helpers";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import * as _ from "lodash";
import * as moment from "moment";

@Component({
  selector: 'invoice-create-update-modal',
  templateUrl: 'invoice-create-update-modal.component.html',
})
export class InvoiceCreateUpdateModalComponent implements OnInit {
  shipmentId = null;
  isUpdate = false;

  invoiceDetails: any = null;
  shipment: any = null;
  styleBreakdown: any[] = [];

  constructor(
    public dialogRef: MatDialogRef<InvoiceCreateUpdateModalComponent>,
    public _auth: AuthService,
    @Inject(MAT_DIALOG_DATA) public data,
    private _shipmentService: ShipmentService,
    private snackBar: SnackbarService,
    private _router: Router,
  ) {
    this.shipmentId = _.get(data, 'shipmentId');
    this.isUpdate = _.get(data, 'isUpdate', false);
  }
  ngOnInit() {
    this.clearInvoiceDetails();
    this.getShipment();
  }

  clearInvoiceDetails() {
    this.invoiceDetails = {
      invoiceDate: toDateString(new Date),
      dueDateFlag: null,
      tax: null,
      discount: null,
      freightCharge: null,
      otherCharge: null,
      otherChargeCause: null,
      handlingCharge: null,
      invoiceTaxRate: null,
      netTotal: null,
      paymentDueDate: toDateString(new Date),
      comment: null,
    }
  }


  invoiceCalculate() {
    let amount = _.get(this.invoiceDetails, 'tax', 0) + _.get(this.invoiceDetails, 'freightCharge', 0) + _.get(this.invoiceDetails, 'otherCharge', 0) + _.get(this.invoiceDetails, 'handlingCharge', 0) - _.get(this.invoiceDetails, 'discount', 0);
    this.invoiceDetails['netTotal'] = _.add(_.get(this.shipment, 'totalPrice', 0), amount);
  }


  getShipment() {
    this.shipment = null;
    this.styleBreakdown = [];
    this._shipmentService.getBreakdown(this.shipmentId).subscribe((data) => {
      this.shipment = _.get(data, 'shipment');

      this.invoiceDetails['invoiceTaxRate'] = _.get(this.shipment, 'provinceRaxRate');
      this.invoiceDetails['tax'] = _.divide(_.multiply(_.get(this.shipment, 'totalPrice', 0), _.get(this.invoiceDetails, 'invoiceTaxRate')), 100);
      this.styleBreakdown = _.get(data, 'breakdown');
      if (this.isUpdate) {
        this.invoiceDetails = {
          ...this.invoiceDetails,
          invoiceDate: _.get(this.shipment, 'invoiceDate'),
          tax: _.get(this.shipment, 'tax'),
          discount: _.get(this.shipment, 'discount'),
          freightCharge: _.get(this.shipment, 'freightCharge'),
          otherCharge: _.get(this.shipment, 'otherCharge'),
          otherChargeCause: _.get(this.shipment, 'otherChargeCause'),
          handlingCharge: _.get(this.shipment, 'handlingCharge'),
          invoiceTaxRate: _.get(this.shipment, 'provinceRaxRate'),
          paymentDueDate: _.get(this.shipment, 'paymentDueDate'),
          comment: _.get(this.shipment, 'comment'),
          invoiceRevision: { revisionCause: null }
        }
      }
      this.invoiceCalculate();
    });
  }

  createInvoice(flag) {
    if (!_.get(this.invoiceDetails, "invoiceDate")) {
      this.snackBar.open(`Please select invoice date.`, "Close");
      return;
    }
    if (!this.isUpdate && !_.get(this.invoiceDetails, "dueDateFlag")) {
      this.snackBar.open(`Please select last due time.`, "Close");
      return;
    }


    let payload = _.merge({}, this.invoiceDetails);
    if (!this.isUpdate && _.get(this.invoiceDetails, "dueDateFlag") != 'CUSTOM') {
      payload['paymentDueDate'] = toDateString(moment(this.invoiceDetails.invoiceDate, "YYYY-MM-DD").add(parseInt(_.get(this.invoiceDetails, "dueDateFlag")), 'days'));
    } else if (!this.isUpdate && _.get(this.invoiceDetails, "dueDateFlag") == 'CUSTOM') {
      payload['paymentDueDate'] = this.invoiceDetails.paymentDueDate ? (this.invoiceDetails.paymentDueDate) : null
    } else {
      payload['paymentDueDate'] = null;
    }
    if (!this.isUpdate && _.get(payload, "dueDateFlag") && !_.get(payload, "paymentDueDate")) {
      this.snackBar.open(`Please select payment due date.`, "Close");
      return;
    }

    payload['discount'] = payload['discount'] > 0 ? payload['discount'] : 0
    payload['id'] = this.shipmentId;
    this._shipmentService.updateFields(payload).subscribe((data) => {
      this.snackBar.open(`Invoice created!`, "Close");
      this.dialogRef.close(true);
    });
  }
  closeDatePicker() {
    this.invoiceDetails.dueDateFlag = '!CUSTOM'
  }
}
import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from "rxjs/internal/operators/map";
import { makeParams } from "app/modules-core/utility/helpers";
import * as _ from "lodash";

@Injectable({
  providedIn: "root",
})
export class InvoiceService {
  urlPayments = "invoice-payments";

  httpOptions = {
    params: new HttpParams(),
  };
  constructor(private http: HttpClient) { }

  getAllPayment(opts = {}) {
    return this.http
      .get(`${this.urlPayments}`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          return _.sortBy(resp.data, "sequence");
        })
      );
  }

  getPayment(id) {
    return this.http
      .get(`${this.urlPayments}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  createPayment(payload) {
    return this.http
      .post(this.urlPayments, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updatePayment(payload) {
    return this.http
      .put(`${this.urlPayments}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updatePaymentFields(id, payload) {
    return this.http
      .patch(`${this.urlPayments}/${id}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  deletePayment(id) {
    return this.http
      .delete(`${this.urlPayments}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { InvoiceComponent } from './pages/invoice.component';
import { InvoiceListComponent } from './pages/invoice-list/invoice-list.component';
import { InvoicePaymentComponent } from './pages/invoice-payment/invoice-payment.component';
import { InvoicePendingComponent } from './pages/invoice-pending/invoice-pending.component';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { InvoicePrintComponent } from './pages/invoice/invoice-print.component';
import { OrderBasicModule } from '../order/components/order-basic/order-basic.module';
import { InvoiceCreateUpdateModalComponent } from './components/invoice-create-update/invoice-create-update-modal.component';
import { PdfViewModule } from 'app/modules-core/general/pdf-modal/pdf-view.module';
import { InvoiceBadDebtComponent } from './pages/invoice-backdated/invoice-backdated.component';


const routes = [
  {
    path: "invoice",
    component: InvoiceComponent,
    data: { permission: 'PERMISSION_MENU_INVOICE_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
    children: [
      {
        path: "",
        redirectTo: "list",
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_INVOICE_LIST_VIEW' },
      },
      {
        path: "list",
        component: InvoiceListComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_INVOICE_LIST_VIEW' },
      },
      {
        path: "payments",
        component: InvoicePaymentComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_INVOICE_PAYMENT_VIEW' },
      },
      {
        path: "pending",
        component: InvoicePendingComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_INVOICE_PENDING_VIEW' },
      },
      {
        path: "baddebt",
        component: InvoiceBadDebtComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_INVOICE_PENDING_VIEW' },
      },
    ]
  },
  {
    path: 'invoice/:id/invoice',
    component: InvoicePrintComponent,
    data: { permission: 'PERMISSION_ORDER_INVOICE_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  },

];

@NgModule({
  declarations: [
    InvoiceComponent,
    InvoicePendingComponent,
    InvoiceListComponent,
    InvoicePaymentComponent,
    InvoicePrintComponent,
    InvoiceBadDebtComponent,
    InvoiceCreateUpdateModalComponent,
  ],
  entryComponents: [
    InvoiceCreateUpdateModalComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    ButtonModule,
    DateTimeModule,
    GeneralModule,
    OrderBasicModule,
    PdfViewModule,
  ],
  exports: [
  ]
})

export class InvoiceModule {
}

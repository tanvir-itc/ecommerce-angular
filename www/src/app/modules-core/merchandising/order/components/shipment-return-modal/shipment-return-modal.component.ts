import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { AuthService } from "app/authentication/auth.service";
import { InventoryLocationService } from "app/modules-core/common/inventory-location/inventory-location.service";
import { ShipmentService } from "app/modules-core/merchandising/shipping/shipment.service";
import { toDateString } from "app/modules-core/utility/helpers";
import { OrderService } from "../../order.service";
import * as _ from "lodash";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";

@Component({
  selector: 'shipment-return-modal',
  templateUrl: 'shipment-return-modal.component.html',
})
export class ShipmentReturnModalComponent implements OnInit {

  orderId: any = null;
  order: any = null;

  newReturn: any = null;

  inventoryLocationList: any[] = [];

  constructor(
    private _orderService: OrderService,
    private _shipmentService: ShipmentService,
    private _inventoryLocationService: InventoryLocationService,
    public _auth: AuthService,
    private snackBar: SnackbarService,
    public dialogRef: MatDialogRef<ShipmentReturnModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.orderId = this.data.orderId
  }

  ngOnInit() {
    this.clearNewReturn();
    this.getOrder();
    this.getAllInventoryLocation();
  }

  getOrder() {
    this._orderService.getOrder({ orderid: this.orderId }).subscribe((data) => {
      this.order = data;
    });
  }




  clearNewReturn() {
    this.newReturn = {
      orderId: this.orderId,
      returnDate: toDateString(new Date),


      inventoryReturn: false,
      receiveDate: toDateString(new Date),
      inventoryLocationId: null,

      agreeReturnValue: null,
      adjustmentType: null,
      freightCost: null,
      restockingCost: null,

      quantity: 0,
      actualValue: 0,

      // chequeDate: null,
      // chequeNo: null,
      // discountValue: null,

      remark: null,

      shipmentReturnDetails: []
    }
  }


  getAllInventoryLocation() {
    this._inventoryLocationService.getAll().subscribe(data => {
      this.inventoryLocationList = data;
    });
  }

  createReturn() {
    let payload = _.merge({}, this.newReturn);


    if (!_.get(payload, "returnDate")) {
      this.snackBar.open(`Please select return Date.`, "Close");
      return;
    }


    let error = [];
    _.each(this.order.groupBreakdown, group => {
      _.each(group.styleBreakdown, (style) => {
        for (var property in style.breakdown) {
          if (style.breakdown[property].returnQuantity) {
            //mr = maxReturn, nrq = new return quantity
            let mr = null;
            let nrq = null;
            mr = style.breakdown[property].maxReturnQuantity;
            nrq = parseInt(style.breakdown[property].returnQuantity)
            if (nrq > mr) {
              error.push(`${style.styleName}-${style.breakdown[property].size},${style.breakdown[property].combo} can not be more than balance shipment quantity!`);
            }
            payload.shipmentReturnDetails.push({
              backToInv: _.get(payload, 'inventoryReturn', false),
              orderDetailId: _.chain(style.breakdown).get(property).get('details').head().get('id').value(),
              quantity: nrq,
            });
          }
        }
      });
    });

    if (!payload.shipmentReturnDetails.length) {
      this.snackBar.open(`Please! add minimum quantity!`, "Close");
      return;
    }
    if (error.length) {
      this.snackBar.open(_.join(error, ', '), "Close", true);
      return;
    }


    if (_.get(payload, "inventoryReturn")) {
      if (!_.get(payload, "inventoryLocationId")) {
        this.snackBar.open(`Please select inventory location.`, "Close");
        return;
      }
      if (!_.get(payload, "receiveDate")) {
        this.snackBar.open(`Please select receive Date.`, "Close");
        return;
      }
    } else {
      payload['inventoryLocationId'] = null;
      payload['receiveDate'] = null;
    }

    if (!_.get(payload, "agreeReturnValue")) {
      this.snackBar.open(`Please enter agree return value`, "Close");
      return;
    }
    if (!_.get(payload, "adjustmentType")) {
      this.snackBar.open(`Please select adjustment type`, "Close");
      return;
    }




    this._shipmentService.createShipmentReturn(payload).subscribe((data) => {
      this.snackBar.open(`Return submitted!`, "Close");
      this.dialogRef.close(true);
    });


  }


  calculation() {
    let totalQuantity = 0;
    let totalPrice = 0;
    _.each(this.order.groupBreakdown, group => {
      _.each(group.styleBreakdown, (style) => {
        for (var property in style.breakdown) {
          if (style.breakdown[property].returnQuantity) {
            let item = style.breakdown[property];

            totalQuantity = _.add(totalQuantity, parseInt(_.get(item, 'returnQuantity')));
            totalPrice = _.add(totalPrice, _.multiply(parseInt(_.get(item, 'returnQuantity')), _.get(item, 'price', 0)));
          }
        }
      });
    });

    this.newReturn['quantity'] = _.add(0, totalQuantity);
    this.newReturn['actualValue'] = _.add(0, totalPrice);
  }

}
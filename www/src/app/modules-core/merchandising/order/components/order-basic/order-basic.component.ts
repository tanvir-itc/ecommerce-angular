import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { OrderService } from 'app/modules-core/merchandising/order/order.service';
import * as _ from 'lodash';
import { AuthService } from 'app/authentication/auth.service';

@Component({
  selector: 'order-basic',
  templateUrl: './order-basic.component.html',
})
export class OrderBasicComponent implements OnInit {

  @Input()
  orderId = null;

  @Input()
  displayType = "details";

  @Input()
  api = false;

  @Input()
  order;

  constructor(
    private _orderService: OrderService,
    public _auth: AuthService,
  ) {
  }
  ngOnInit() {
  }

  getOrder() {
    if (this.api && this.orderId) {
      this._orderService.get(this.orderId).subscribe(resp => {
        this.order = resp;
      });
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getOrder();
  }

}
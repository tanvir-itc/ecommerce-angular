import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { DateTimeModule } from 'app/modules-core/utility/date-time-picker/date-time.module';
import { OrderBasicComponent } from './order-basic.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    OrderBasicComponent,
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FuseSharedModule,
    RouterModule,
    ButtonModule,
    DateTimeModule
  ],
  exports: [
    OrderBasicComponent,
  ]
})

export class OrderBasicModule {
}
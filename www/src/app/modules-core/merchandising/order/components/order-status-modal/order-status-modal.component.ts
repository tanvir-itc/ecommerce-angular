import { Component, Inject, OnInit } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { AuthService } from "app/authentication/auth.service";
import { PdfModalComponent } from "app/modules-core/general/pdf-modal/pdf-modal.component";
import { ShipmentService } from "app/modules-core/merchandising/shipping/shipment.service";
import * as _ from "lodash";


@Component({
  selector: 'order-status-modal',
  templateUrl: 'order-status-modal.component.html',
})
export class OrderStatusModalComponent implements OnInit {
  order: any = null;
  total: any = {};

  shipments: any[] = [];
  constructor(
    public _auth: AuthService,
    public _shipmentService: ShipmentService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<OrderStatusModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.order = this.data.order
  }

  ngOnInit() {
    this.getShipments();
  }

  getShipments() {
    this.total = {};
    let params = {};
    params['orderid'] = this.order.id
    params['sort'] = 's.invoice_date,desc';
    this._shipmentService.getAll(params).subscribe((data) => {
      this.shipments = data;

      this.total = {
        totalQuantity: _.sumBy(this.shipments, 'totalQuantity'),
        totalValue: _.sumBy(this.shipments, 'totalValue'),
        tax: _.sumBy(_.filter(this.shipments, 'invoiceId'), 'tax'),
        freightCharge: _.sumBy(_.filter(this.shipments, 'invoiceId'), 'freightCharge'),
        handlingCharge: _.sumBy(_.filter(this.shipments, 'invoiceId'), 'handlingCharge'),
        discount: _.sumBy(_.filter(this.shipments, 'invoiceId'), 'discount'),
        netTotal: _.sumBy(_.filter(this.shipments, 'invoiceId'), 'netTotal'),
        totalPayment: _.sumBy(_.filter(this.shipments, 'invoiceId'), 'totalPayment'),
        dueAmount: _.sumBy(_.filter(this.shipments, 'invoiceId'), 'dueAmount'),
      }

    });
  }

  modalRef: any = null;

  openInvoicePDFModal(shipmentId) {
    this._shipmentService.getInvoicePDF(shipmentId).subscribe(resp => {
      this.modalRef = this.dialog.open(PdfModalComponent, {
        // minWidth: "700px",
        width: "90%",
        // disableClose: true,
        data: {
          title: "Invoice",
          resource: _.get(resp, 'path'),
          emailButton: true,
          emailObject: {
            type: 'invoice',
            shipmentId: shipmentId,
          }
        }
      });
    })
  }

}
import { Component, Inject, OnInit } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { AuthService } from "app/authentication/auth.service";
import { UserService } from "app/modules-core/common/user/user.service";
import { ShipmentService } from "app/modules-core/merchandising/shipping/shipment.service";
import { integerStringToInteger, removeEmptyFields, stringToIntegerArray } from "app/modules-core/utility/helpers";
import * as _ from "lodash";
import { OrderService } from "../../order.service";
import { StatusService } from "app/modules-core/common/status/status.service";
import { Router } from "@angular/router";
import { OrderStatusModalComponent } from "../order-status-modal/order-status-modal.component";


@Component({
  selector: 'customer-order-history',
  templateUrl: 'customer-order-history.component.html',
})
export class CustomerOrderHistoryComponent implements OnInit {
  customerId: any = null;


  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  orders: any[] = [];
  orderId: any = null;

  statusList: any[] = [];

  modalRef = null;

  customerList: any[] = [];
  saleRepList: any[] = [];

  sorter: any = {
    field: null,
    sortType: 'asc',
    fieldType: 'text',
  }

  total: any = null;

  userRole: any = null;


  constructor(
    public _auth: AuthService,
    public _shipmentService: ShipmentService,
    public _orderService: OrderService,
    public _userService: UserService,
    public _statusService: StatusService,
    public dialog: MatDialog,
    private _router: Router,
    public dialogRef: MatDialogRef<CustomerOrderHistoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.customerId = this.data.customerId
  }
  ngOnInit() {
    this.userRole = _.chain(this._auth.getCurrentUser()).get('roles').head().get('name').value();
    this.clearAllFilter();
    this.applyFilters(this.filter);
    this.getAllStatus();
    this.getAllSaleRep();
  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "statusname") {
        params[key] = stringToIntegerArray(params[key]);
      }
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }



  onFilter() {
    this.applyFilters(this.filter);
  }


  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    this.orders = [];
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    params['exceptstatus'] = 'cancel,draft';
    params['sort'] = 'order_date,desc';
    params['customerid'] = this.customerId;
    this._orderService.getAll(params).subscribe((data) => {
      this.orders = data;
      this.totalCalcuation();
    });

  }

  totalCalcuation() {
    this.total = null;
    this.total = {
      order: _.sumBy(this.orders, 'orderQty'),
      shipped: _.sumBy(this.orders, 'shipQty'),
      orderValue: _.sumBy(this.orders, 'totalValue'),
      shippedValue: _.sumBy(this.orders, 'totalShipValue'),
    }
  }

  getAllStatus() {
    this._statusService.getAll().subscribe((data) => {
      this.statusList = data;
    });
  }

  getAllSaleRep() {
    if (this._auth.hasPermission("PERMISSION_ADMIN_USER")) {
      var params = {};
      params['rolename'] = 'ROLE_SALES';
      this._userService.getAll(params).subscribe(data => {
        this.saleRepList = data;
      });
    }

  }

  goLink(id) {
    this._router.navigateByUrl(`orders/${id}`);
  }


  openDetailsModal(order) {
    const statusModal = this.dialog.open(OrderStatusModalComponent, {
      maxWidth: '95%',
      minWidth: '300px',
      disableClose: true,
      data: {
        order: order,
      }
    });
    statusModal.afterClosed().subscribe(result => {

    });
  }



}
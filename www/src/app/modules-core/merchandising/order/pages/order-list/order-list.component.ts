import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { fuseAnimations } from "@fuse/animations";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-order-list",
  templateUrl: "./order-list.component.html",
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class OrderListComponent implements OnInit {
  show = false;
  tabs;
  tabFromUrl;

  basePath;

  constructor(
    private _route: ActivatedRoute,
  ) {
    // this.id = +this._route.snapshot.paramMap.get("id");
    this.tabFromUrl = this._route.snapshot.firstChild.routeConfig.path; //find a better way to get child param
    this.basePath = this._route.snapshot.url[0].path; //find a better way to get child param
  }

  ngOnInit() {
    this._route.paramMap.subscribe(params => {
      this.show = false;
      this.tabs = [
        {
          path: `${this.basePath}/orders`,
          label: "Orders",
          color: 'tab-green',
        },
        {
          path: `${this.basePath}/draft`,
          label: "Draft",
          color: 'tab-yellow',
        },
        {
          path: `${this.basePath}/canceled`,
          label: "Canceled",
          color: 'tab-red',
        }
      ];
      this.show = true;
    });
  }

}

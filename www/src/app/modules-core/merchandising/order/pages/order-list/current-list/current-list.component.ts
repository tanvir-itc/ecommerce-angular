import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "app/authentication/auth.service";
import {
  toDateString,
  integerStringToInteger,
  stringToIntegerArray,
  removeEmptyFields,
  integerArrayToString,
  customSortBy
} from "app/modules-core/utility/helpers";
import * as moment from "moment";
import { CustomerService } from "app/modules-core/merchandising/customers/customer.service";
import { StatusService } from "app/modules-core/common/status/status.service";
import { UserService } from "app/modules-core/common/user/user.service";
import { OrderService } from "../../../order.service";
import { MatDialog } from "@angular/material";
import { OrderStatusModalComponent } from "../../../components/order-status-modal/order-status-modal.component";
import { PdfModalComponent } from "app/modules-core/general/pdf-modal/pdf-modal.component";
import { ShipmentService } from "app/modules-core/merchandising/shipping/shipment.service";

@Component({
  selector: "current-list",
  templateUrl: "./current-list.component.html",
})
export class OrderCurrentListComponent implements OnInit {
  filterMore = false;
  filterCounter = 0;
  filter: any = {};

  orders: any[] = [];
  orderId: any = null;

  statusList: any[] = [];

  modalRef = null;

  customerList: any[] = [];
  saleRepList: any[] = [];

  sorter: any = {
    field: null,
    sortType: 'asc',
    fieldType: 'text',
  }

  total: any = null;

  userRole: any = null;




  constructor(
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _orderService: OrderService,
    private _customerService: CustomerService,
    private _statusService: StatusService,
    private _userService: UserService,
    private _shipmentService: ShipmentService,
    private dialog: MatDialog,
  ) {
    this._quickPanelSubjectService.heading.next("Order History");
  }
  ngOnInit() {
    this.userRole = _.chain(this._auth.getCurrentUser()).get('roles').head().get('name').value();
    this.clearAllFilter();
    var queryParams = _.merge({}, _.get(this._route, "snapshot.queryParams"));

    this.filter = _.merge(this.filter, this.filterParamsGenerator(queryParams));
    this.applyFilters(this.filter);
    this.getAllStatus();
    this.getAllCustomers();
    this.getAllSaleRep();




  }

  clearAllFilter() {
    this.filter = {
      fromdate: null,
      todate: null,
      styleid: null,
      stylename: null,
      statusname: [],
      customerid: null,
      salerepid: null,
    };
  }

  filterParamsGenerator(params) {
    for (var key in params) {
      params[key] = integerStringToInteger(params[key]);
      if (key == "statusname") {
        params[key] = stringToIntegerArray(params[key]);
      }
      if (key == "customerid") {
        params[key] = stringToIntegerArray(params[key]);
      }
    }
    return params;
  }

  onDateNextPrivious(priv) {
    if (priv) {
      this.filter.todate = toDateString(
        moment(this.filter.todate || new Date())
          .add(1, "months")
          .endOf("month")
      );
      this.filter.fromdate = toDateString(
        moment(this.filter.todate || new Date()).startOf("month")
      );
    } else {
      this.filter.fromdate = toDateString(
        moment(this.filter.fromdate || new Date())
          .subtract(1, "months")
          .startOf("month")
      );
      this.filter.todate = toDateString(
        moment(this.filter.fromdate || new Date()).endOf("month")
      );
    }
    this.applyFilters(this.filter);
  }

  onFilter() {
    this.applyFilters(this.filter);
  }

  setFilterToURL(filterParams) {
    var params = _.merge({}, filterParams);
    for (var key in params) {
      if (key == "statusname") {
        params[key] = integerArrayToString(params[key]);
      }
      if (key == "customerid") {
        params[key] = integerArrayToString(params[key]);
      }
    }
    params = removeEmptyFields(params);
    this.filterCounter = _.size(params);
    this._router.navigate([], {
      queryParams: params,
      relativeTo: this._route,
    });
  }

  onClear() {
    this.clearAllFilter();
    this.applyFilters(this.filter);
  }

  applyFilters(filterParams) {
    this.orders = [];
    var params = _.merge({}, filterParams);
    params = removeEmptyFields(params);
    params['exceptstatus'] = 'cancel,draft';
    params['sort'] = 'order_date,desc';
    this._orderService.getAll(params).subscribe((data) => {
      this.orders = data;
      this.totalCalcuation();
      this.setFilterToURL(filterParams);
    });

  }

  totalCalcuation() {
    this.total = null;
    this.total = {
      order: _.sumBy(this.orders, 'orderQty'),
      shipped: _.sumBy(this.orders, 'shipQty'),
      orderValue: _.sumBy(this.orders, 'totalValue'),
      shippedValue: _.sumBy(this.orders, 'totalShipValue'),
    }
  }

  getAllStatus() {
    this._statusService.getAll().subscribe((data) => {
      this.statusList = data;
    });
  }

  getAllSaleRep() {
    if (this._auth.hasPermission("PERMISSION_ADMIN_USER")) {
      var params = {};
      params['rolename'] = 'ROLE_SALES';
      this._userService.getAll(params).subscribe(data => {
        this.saleRepList = data;
      });
    }

  }

  getAllCustomers() {
    this._customerService.getAll({ requested: false }).subscribe((data) => {
      this.customerList = data;
    });
  }


  goLink(id) {
    this._router.navigateByUrl(`orders/${id}`);
  }
  onSort(field, fieldType = 'text') {
    this.sorter = {
      field: field,
      fieldType: fieldType,
      sortType: this.sorter.sortType == 'asc' ? 'desc' : 'asc',
    }
    this.orders = customSortBy(this.orders, this.sorter);
  }


  onDelete(id) {
    if (!confirm("Confirm! Order delete?")) {
      return;
    }
    this._orderService.delete(id).subscribe(resp => {
      this.applyFilters(this.filter);
    });
  }


  openShipmentNewTab(id) {
    const url = this._router.serializeUrl(
      this._router.createUrlTree([`/shipment/${id}/shipment-print`])
    );
    window.open(url, '_blank');
  }


  openDetailsModal(order) {
    const returnModal = this.dialog.open(OrderStatusModalComponent, {
      maxWidth: '95%',
      minWidth: '300px',
      disableClose: true,
      data: {
        order: order,
      }
    });
    returnModal.afterClosed().subscribe(result => {

    });
  }


  openPackingPDFModal(shipmentId) {
    this._shipmentService.getPackingPDF(shipmentId).subscribe(resp => {
      this.modalRef = this.dialog.open(PdfModalComponent, {
        // minWidth: "700px",
        width: "90%",
        // disableClose: true,
        data: {
          title: "Packing",
          resource: _.get(resp, 'path'),
          emailButton: true,
          emailObject: {
            type: 'packing',
            shipmentId: shipmentId,
          }
        }
      });
    })
  }

} //class

import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { AuthService } from "app/authentication/auth.service";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { ActivatedRoute, Router } from "@angular/router";
import { OrderService } from "../../order.service";

import { UserService } from "app/modules-core/common/user/user.service";
import { SnackbarService } from "app/modules-core/utility/snackbar.service";
import { ShipmentService } from "app/modules-core/merchandising/shipping/shipment.service";
import { ShipmentReturnModalComponent } from "../../components/shipment-return-modal/shipment-return-modal.component";
import { MatDialog } from "@angular/material";
import { PdfModalComponent } from "app/modules-core/general/pdf-modal/pdf-modal.component";
import { CustomerUserService } from "app/modules-core/merchandising/customers/customer-user.service";
import { CustomerOrderHistoryComponent } from "../../components/customer-order-history/customer-order-history.component";

@Component({
  selector: "order-details",
  templateUrl: "./order-details.component.html",
})
export class OrderDetailsComponent implements OnInit {
  id;
  order: any = null;
  defaultCustomerUser = null;
  styleBreakdown: any = [];

  status: any[] = [
    {
      id: 1,
      name: "Test",
    },
  ];

  orderInfo: any = {
    saleRepId: null,
    id: null,
    orderRating: null,
  }
  salesRepList: any[] = [];
  ratingList: any[] = [];

  shipmentList: any[] = [];
  returnList: any[] = [];

  displayFlag = {
    shipment: false,
    return: false,
  };


  returnDetails: any = null;
  modalRef: any = null;





  constructor(
    private snackBar: SnackbarService,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _auth: AuthService,
    private _orderService: OrderService,
    private _shipmentService: ShipmentService,
    private _customerUserService: CustomerUserService,
    private _userService: UserService,
    private dialog: MatDialog,
  ) {
    this._quickPanelSubjectService.heading.next("Order Details");
  }

  ngOnInit() {
    this.id = +this._route.snapshot.paramMap.get("id");
    this.getOrder();
    this.getShipmentsByOrder();
    this.getSalesRepUsers();
    this.getOrderRatingList();
    this.getReturns();
  }

  getOrder() {
    this._orderService.getOrder({ orderid: this.id }).subscribe((data) => {
      this.order = data;
      if (_.get(this.order, 'saleRepId')) {
        this.orderInfo.saleRepId = _.get(this.order, 'saleRepId')
      } else {
        this.orderInfo.saleRepId = 0;
      }
      this.orderInfo.orderRating = _.get(this.order, 'orderRating', null);
      this.getDefaultCustomerUser();
      console.log(' this.order', this.order)
    });
  }

  getDefaultCustomerUser() {
    this._customerUserService.getAll({ 'customer.id': this.order.customer.id }).subscribe((customer) => {
      this.defaultCustomerUser = _.find(customer, ['primary', true])
    });
  }

  orderApproved() {
    if (!confirm(`Confirm shipment approval?`)) {
      return;
    }
    let payload = {
      id: this.id,
      approved: true,
    }
    this._orderService
      .statusUpdate(payload)
      .subscribe((resp) => {
        this.getOrder();
        this.snackBar.open(`Order shipment approved!`, "Close");
      });
  }

  orderStatusUpdate(statusName) {
    let title = null;
    if (statusName == 'new') {
      title = "Placed"
    } else if (statusName == 'cancel') {
      title = "Cancel"
    } else if (statusName == 'shipped') {
      title = "Shipped"
    } else if (statusName == 'approved') {
      title = "Approve"
    } else if (statusName == 'draft') {
      title = "Draft"
    }
    if (!confirm(`Confirm ${title}?`)) {
      return;
    }
    let payload = {
      id: this.id,
      statusName: statusName,
    }
    this._orderService
      .statusUpdate(payload)
      .subscribe((resp) => {
        this.getOrder();
        this.snackBar.open(`Order ${title} updated!`, "Close");
      });
  }

  orderPaymentStatusUpdate(statusName) {
    if (!confirm(`Confirm payment complete!`)) {
      return;
    }
    let payload = {
      id: this.id,
      paymentStatusName: statusName,
    }
    this._orderService
      .statusUpdate(payload)
      .subscribe((resp) => {
        this.getOrder();
        this.snackBar.open(`Order payment completed!`, "Close");
      });
  }

  getShipmentsByOrder() {
    this._shipmentService.getAll({ orderid: this.id, packingprocessing: false }).subscribe((resp) => {
      this.shipmentList = resp;
    });
  }

  getSalesRepUsers() {
    if (this._auth.hasPermission('PERMISSION_ADMIN_USER')) {
      this._userService.getAll({ rolename: 'ROLE_SALES,ROLE_PARTNER' }).subscribe((resp) => {
        this.salesRepList = resp;
      });
    }
  }

  getOrderRatingList() {
    this._orderService.getAllRating().subscribe((resp) => {
      this.ratingList = resp;
    });
  }

  onPermissionChange(event) {
    this.orderInfo['id'] = this.id;
    let payload = {
      id: this.id,
      saleRepId: this.orderInfo.saleRepId,
    }
    if (this.orderInfo.saleRepId == 0) {
      payload['saleRepUserEmpty'] = true;
    }
    this._orderService.updateFields(payload).subscribe(res => {
      this.snackBar.open(`Permission Updated!`, "Close");
      this.getOrder();
    });
  }

  onRatingChange(event) {
    let payload = {
      id: this.id,
      orderRating: _.chain(this.ratingList).find(['id', this.orderInfo.orderRating]).get('name').value(),
    }
    this._orderService.updateFields(payload).subscribe(res => {
      this.snackBar.open(`Customer Rating Updated!`, "Close");
      this.getOrder();
    });
  }

  deleteOrder() {
    if (!confirm("Order cancel confirm?")) {
      return;
    }
    this._orderService.delete(this.id).subscribe(res => {
      this.snackBar.open(`Order Deleted!`, "Close");
      this._router.navigateByUrl(`orders`);
    });
  }

  goLinkInvoice(id) {
    this._router.navigateByUrl(`invoice/${id}/invoice`);
  }

  goToCart() {
    this._router.navigateByUrl(`catalogue?customerId=${this.order.customer.id}&cartOpen=${true}`);
  }




  openNewTab(id) {
    const url = this._router.serializeUrl(
      this._router.createUrlTree([`/shipment/${id}/shipment-print`])
    );
    window.open(url, '_blank');
  }


  returnModal() {
    const returnModal = this.dialog.open(ShipmentReturnModalComponent, {
      minWidth: '95%',
      disableClose: true,
      data: {
        orderId: this.id,
      }
    });
    returnModal.afterClosed().subscribe(result => {
      result && this.getReturns();
    });
  }

  getReturns() {
    this._shipmentService.getShipmentReturns({ orderid: this.id }).subscribe((resp) => {
      this.returnList = resp;
    });
  }




  openDetailsModal(templateRef, data) {
    this.returnDetails = null;
    this.returnDetails = _.merge({}, data);

    let list = _.groupBy(this.returnDetails.shipmentReturnDetails, 'styleId');
    this.returnDetails['styleDetails'] = _.map(list, parent => {
      let firstItem = _.head(parent);
      let parentObject = {
        styleName: _.get(firstItem, 'styleName'),
        styleId: _.get(firstItem, 'styleId'),
        list: parent,
      };
      return parentObject;
    });
    this.modalRef = this.dialog.open(templateRef, {
      maxWidth: "95%",
      minWidth: "500px",
      disableClose: true,
    });
  }


  openInvoicePDFModal(shipmentId) {
    this._shipmentService.getInvoicePDF(shipmentId).subscribe(resp => {
      this.modalRef = this.dialog.open(PdfModalComponent, {
        // minWidth: "700px",
        width: "90%",
        // disableClose: true,
        data: {
          title: "Invoice",
          resource: _.get(resp, 'path'),
          emailButton: true,
          emailObject: {
            type: 'invoice',
            shipmentId: shipmentId,
          }
        }
      });
    })
  }

  openPackingPDFModal(shipmentId) {
    this._shipmentService.getPackingPDF(shipmentId).subscribe(resp => {
      this.modalRef = this.dialog.open(PdfModalComponent, {
        // minWidth: "700px",
        width: "90%",
        // disableClose: true,
        data: {
          title: "Packing",
          resource: _.get(resp, 'path'),
          emailButton: true,
          emailObject: {
            type: 'packing',
            shipmentId: shipmentId,
          }
        }
      });
    })
  }


  openOrderHistory() {
    const historyModal = this.dialog.open(CustomerOrderHistoryComponent, {
      width: '95%',
      data: {
        customerId: this.order.customer.id
      }
    });
    historyModal.afterClosed().subscribe(result => {

    });
  }

}

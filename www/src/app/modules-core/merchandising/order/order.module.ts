import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/modules-core/utility/material.module';
import { LoginCheckRouteGuard } from 'app/authentication/check-login-route-guard';
import { NgSelectModule } from '@ng-select/ng-select';
import { DateTimeModule } from '../../utility/date-time-picker/date-time.module';
import { AttachmentModule } from 'app/modules-core/common/attachment/attachment.module';
import { GeneralModule } from 'app/modules-core/general/general.module';
import { ButtonModule } from 'app/modules-core/utility/button/button.module';
import { OrderListComponent } from './pages/order-list/order-list.component';
import { OrderDetailsComponent } from './pages/order-details/order-details.component';
import { OrderBasicModule } from './components/order-basic/order-basic.module';
import { StyleBasicModule } from '../style/components/style-basic/style-basic.module';
import { PermissionCheckRouteGuard } from 'app/authentication/permission-route-guard';
import { ShipmentReturnModalComponent } from './components/shipment-return-modal/shipment-return-modal.component';
import { OrderCurrentListComponent } from './pages/order-list/current-list/current-list.component';
import { OrderDraftListComponent } from './pages/order-list/draft-list/draft-list.component';
import { OrderCanceledListComponent } from './pages/order-list/canceled-list/canceled-list.component';
import { OrderStatusModalComponent } from './components/order-status-modal/order-status-modal.component';
import { CustomerOrderHistoryComponent } from './components/customer-order-history/customer-order-history.component';

const routes = [

  {
    path: "orders",
    component: OrderListComponent,
    data: { permission: 'PERMISSION_MENU_ORDER_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
    children: [
      {
        path: "",
        redirectTo: "orders",
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_ORDER_VIEW' },
      },
      {
        path: "orders",
        component: OrderCurrentListComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_ORDER_VIEW' },
      },
      {
        path: "draft",
        component: OrderDraftListComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_ORDER_VIEW' },
      },
      {
        path: "canceled",
        component: OrderCanceledListComponent,
        canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
        data: { permission: 'PERMISSION_MENU_ORDER_VIEW' },
      },
    ],
  },





  // {
  //   path: 'orders',
  //   component: OrderListComponent,
  //   data: { permission: 'PERMISSION_MENU_ORDER_VIEW' },
  //   canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  // },
  {
    path: 'orders/:id',
    component: OrderDetailsComponent,
    data: { permission: 'PERMISSION_MENU_ORDER_VIEW' },
    canActivate: [LoginCheckRouteGuard, PermissionCheckRouteGuard],
  },

];
@NgModule({
  declarations: [
    OrderListComponent,
    OrderCurrentListComponent,
    OrderDraftListComponent,
    OrderCanceledListComponent,
    OrderDetailsComponent,
    ShipmentReturnModalComponent,
    OrderStatusModalComponent,
    CustomerOrderHistoryComponent,
  ],
  entryComponents: [
    ShipmentReturnModalComponent,
    OrderStatusModalComponent,
    CustomerOrderHistoryComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    NgSelectModule,
    AttachmentModule,
    GeneralModule,
    DateTimeModule,
    ButtonModule,
    OrderBasicModule,
    StyleBasicModule,
  ],
  exports: [
  ]
})
export class OrderModule {
}
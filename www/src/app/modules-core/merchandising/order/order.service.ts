import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from "rxjs/internal/operators/map";
import * as _ from "lodash";
import { makeParams, toDateString } from "app/modules-core/utility/helpers";
import * as moment from "moment";

@Injectable({
  providedIn: "root",
})
export class OrderService {
  urlBase = "orders";

  httpOptions = {
    params: new HttpParams(),
  };
  constructor(private http: HttpClient) { }

  getAll(opts = {}) {
    return this.http
      .get(`${this.urlBase}/v2`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = JSON.parse(resp.data);
            resp.data = _.map(resp.data, order => {
              order.orderDetails = _.map(order.orderDetails, (style) => {
                let price = _.chain(style.orderDetail).head().get('price', null).value();
                style["styleOrderQty"] = _.sumBy(style.orderDetail, "quantity");
                style["styleShipQty"] = _.sumBy(style.orderDetail, "actualShipQty");
                style["totalPrice"] = _.multiply(style["styleOrderQty"], price);
                style["totalShipPrice"] = _.multiply(style["styleShipQty"], price);
                return style;
              });
              order["orderQty"] = _.sumBy(order.orderDetails, "styleOrderQty");
              order["shipQty"] = _.sumBy(order.orderDetails, "styleShipQty");
              order['totalValue'] = _.sumBy(order.orderDetails, "totalPrice");
              order['totalShipValue'] = _.sumBy(order.orderDetails, "totalShipPrice");
              order['shipmentDateList'] = JSON.parse(order.shipmentDateList);
              order['unfinishedShipment'] = _.find(order['shipmentDateList'], item => !item.shipmentDate);
              return order;
            });
            return resp.data;
          }
          return [];
        })
      );
  }

  get(id) {
    return this.http.get(`${this.urlBase}/${id}/v2`).pipe(
      map((resp: any) => {
        if (resp.data) {
          resp.data = JSON.parse(resp.data);
          resp.data['po'] = `P${moment(resp.data.orderDate || new Date).format('MMDD')}${resp.data.id}`;
          return _.head(resp.data);
        }
        return resp.data;
      })
    );
  }

  getBreakdown(id, opts = {}) {
    return this.http
      .get(`${this.urlBase}/${id}/v2`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = JSON.parse(resp.data);
            var details = _.head(resp.data);
            var styleBreakdown = [];
            _.each(_.get(details, "orderDetails"), (styleItem) => {
              styleItem["styleImage"] = _.get(styleItem, "styleImage");
              var uniqueCombos = [];
              uniqueCombos = _.chain(styleItem)
                .get("orderDetail")
                .uniqBy("combo")
                .value();
              uniqueCombos = _.map(uniqueCombos, (combo) => {
                return {
                  comboId: combo.comboId,
                  comboName: combo.combo,
                };
              });
              styleItem["combos"] = uniqueCombos;
              styleItem["breakdown"] = {};
              _.each(styleItem.orderDetail, (item) => {
                item["newShipmentQuantity"] = null;
                item["balance"] = _.subtract(
                  _.get(item, "quantity"),
                  _.get(item, "shipQty", 0)
                );
                let key = null;
                key = "_" + item.comboId + "_" + item.sizeId + "_" + item.combo;
                let locations = _.chain(item).get('details').head().get('locations').value();
                item['inventoryLocations'] = {};
                _.each(locations, location => {
                  item.inventoryLocations["_" + location.id] = location;
                })

                styleItem["breakdown"][key] = item;
              });

              styleItem["price"] = _.chain(styleItem.orderDetail)
                .head()
                .get('price')
                .value();
              styleItem["total"] = _.sumBy(styleItem.orderDetail, "quantity");

              styleItem["totalShipped"] = _.sumBy(
                styleItem.orderDetail,
                "shipQty"
              );
              styleItem["totalPrice"] = _.multiply(styleItem["price"], styleItem["total"]);
              styleItem["totalShippedPrice"] = _.multiply(styleItem["price"], styleItem["totalShipped"]);
              styleBreakdown.push(styleItem);
            });
            details['totalPrice'] = _.sumBy(styleBreakdown, 'totalPrice');
            details['totalShippedPrice'] = _.sumBy(styleBreakdown, 'totalShippedPrice');
            return {
              order: details,
              breakdown: styleBreakdown,
            };
          }
          return [];
        })
      );
  }

  create(payload) {
    return this.http
      .post(this.urlBase, payload)
      .pipe(map((resp: any) => resp.data));
  }

  update(id, payload) {
    return this.http
      .put(`${this.urlBase}/${id}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  updateFields(payload) {
    return this.http
      .patch(`${this.urlBase}`, payload)

      .pipe(map((resp: any) => resp.data));
  }
  updateDiscount(payload) {
    return this.http
      .patch(`${this.urlBase}/discount`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  delete(id) {
    return this.http
      .delete(`${this.urlBase}/${id}`)
      .pipe(map((resp: any) => resp.data));
  }

  addStyle(id, payload) {
    return this.http
      .post(`${this.urlBase}/${id}/details`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  statusUpdate(payload) {
    return this.http
      .patch(`${this.urlBase}`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  getAllRating(opts = {}) {
    return this.http
      .get(`${this.urlBase}/order-ranting`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          return resp.data;
        })
      );
  }

  orderStyleDelete(id, styleId) {
    return this.http
      .delete(`${this.urlBase}/${id}/style/${styleId}`)
      .pipe(map((resp: any) => resp.data));
  }

  addOrderItems(payload) {
    return this.http
      .post(`${this.urlBase}/v2`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  getOrder(opts = {}) {
    return this.http
      .get(`${this.urlBase}/v3`, {
        params: makeParams(
          _.merge(
            {
              httpParams: this.httpOptions.params,
            },
            { queryParams: opts }
          )
        ),
      })
      .pipe(
        map((resp: any) => {
          if (resp.data) {
            resp.data = JSON.parse(resp.data);
            let details = _.head(resp.data);
            details['customerFullname'] = `${_.get(details, 'customer.contactFirstName')} ${_.get(details, 'customer.contactLastName')}`
            details['po'] = `P${moment(_.get(details, 'orderDate') || new Date).format('MMDD')}${_.get(details, 'id')}`;
            let breakdown = _.get(details, 'orderDetails');
            let groupItems = [];
            breakdown = _.each(breakdown, sizeGroup => {
              let styleGroup = {
                sizeList: _.get(sizeGroup, 'sizeList'),
                styleBreakdown: [],
              }
              _.each(sizeGroup.orderDetail, product => {
                var uniqueCombos = [];
                uniqueCombos = _.chain(product)
                  .get("orderDetail")
                  .uniqBy("combo")
                  .value();
                uniqueCombos = _.map(uniqueCombos, (combo) => {
                  let list = _.chain(product.orderDetail).filter(['combo', combo.combo]).value()
                  if (combo.combo.includes("In Stock")) {
                    let status = 'In Stock'
                    combo['displayName'] = combo.combo.slice(0, combo.combo.indexOf('In Stock'))
                    combo['colorStatus'] = combo.combo.slice(combo.combo.indexOf('In Stock'), combo.combo.indexOf('In Stock') + (status.length + 1))
                    combo['stockDate'] = combo.combo.slice((combo.combo.indexOf('In Stock') + status.length + 1))
                  } else if (combo.combo.includes("Available")) {
                    combo['status'] = 'available'
                    combo['displayName'] = combo.combo.slice(0, combo.combo.indexOf('Available'))
                    combo['colorStatus'] = combo.combo.slice(combo.combo.indexOf('Available'), combo.combo.length)
                  } else {
                    combo['displayName'] = combo.combo
                  }
                  return {
                    comboId: combo.comboId,
                    comboName: combo.combo,
                    image: combo.styleColorImage.find(img => img.isDefault === true),
                    comboNameForOrderDetail: combo.displayName,
                    statusNameForOrderDetail: combo.colorStatus,
                    comboNameForDisplay: combo.displayName,
                    comboStatusForDisplay: combo.colorStatus,
                    comboStockDateForDisplay: combo.stockDate,
                    comboQuantityTotal: _.sumBy(list, 'quantity'),
                    comboPickedQuantityTotal: _.sumBy(list, 'shipQty'),
                    comboReturnTotal: _.sumBy(list, 'returnQty'),
                    comboActualShipTotal: _.sumBy(list, 'actualShipQty'),
                    styleColorImages: _.filter(_.get(combo, 'styleColorImage'), 'isDefault'),
                  };
                })
                let earliestShipmentDate = _.chain(product.orderDetail).maxBy('earliestShipmentDate').get('earliestShipmentDate').value();
                let availableDate = earliestShipmentDate ? moment(earliestShipmentDate).isBefore(new Date) : true;
                let item = {
                  breakdown: {},
                  styleName: product.name,
                  styleId: product.styleId,
                  colorList: _.merge([], uniqueCombos),
                  styleImages: _.get(product, 'styleImage'),
                  price: _.chain(product.orderDetail).head().get('price').value(),
                  earliestShipmentDate: earliestShipmentDate,
                  availableDate: availableDate,
                  actualPrice: _.chain(product.orderDetail).head().get('actualPrice').value(),
                  minPrice: _.chain(product.orderDetail).head().get('minimumPrice').value(),
                  locations: _.chain(product.orderDetail).flatMap('details').flatMap('locations').filter().value(),
                  newPrice: null,
                  styleOrderedQty: _.sumBy(product.orderDetail, 'quantity'),
                  styleReturnQty: _.sumBy(product.orderDetail, 'returnQty'),
                  styleShippedQty: _.sumBy(product.orderDetail, 'actualShipQty'),
                };
                item['newPrice'] = _.get(item, 'price') || _.get(item, 'actualPrice') || _.get(item, 'defaultPrice');

                item['styleOrderedValue'] = _.multiply(item.price, item.styleOrderedQty);
                item['newStyleOrderedValue'] = _.get(item, 'styleOrderedValue');
                item['styleShippedValue'] = _.multiply(item.price, item.styleShippedQty);

                _.each(product.orderDetail, quantityBreakdown => {
                  let key = "_" + quantityBreakdown.comboId + "_" + quantityBreakdown.sizeId + "_" + quantityBreakdown.combo;
                  quantityBreakdown['orderedQty'] = _.get(quantityBreakdown, 'quantity');
                  quantityBreakdown['returnQty'] = _.get(quantityBreakdown, 'returnQty');
                  quantityBreakdown['balance'] = _.subtract(_.get(quantityBreakdown, 'quantity'), _.get(quantityBreakdown, 'shipQty'));
                  quantityBreakdown["newShipmentQuantity"] = null;
                  quantityBreakdown["returnQuantity"] = null;
                  quantityBreakdown["maxReturnQuantity"] = _.subtract(_.get(quantityBreakdown, 'actualShipQty'), _.get(quantityBreakdown, 'returnQty'));


                  let locations = _.chain(quantityBreakdown).get('details').head().get('locations').value();
                  quantityBreakdown['inventoryLocations'] = {};
                  _.each(locations, location => {
                    quantityBreakdown.inventoryLocations["_" + location.id] = location;
                  })

                  item.breakdown[key] = quantityBreakdown;
                });
                item['sizeGroupId'] = _.get(sizeGroup, 'sizeGroupId');

                styleGroup.styleBreakdown.push(item);
              });
              styleGroup['earliestDates'] = [];
              _.each(styleGroup.styleBreakdown, item => {
                if (item.earliestShipmentDate) {
                  styleGroup['earliestDates'].push(item.earliestShipmentDate);
                }
              });
              styleGroup['groupOrderedQty'] = _.sumBy(styleGroup.styleBreakdown, 'styleOrderedQty');
              styleGroup['groupReturnQty'] = _.sumBy(styleGroup.styleBreakdown, 'styleReturnQty');
              styleGroup['groupShippedQty'] = _.sumBy(styleGroup.styleBreakdown, 'styleShippedQty');
              styleGroup['groupOrderedValue'] = _.sumBy(styleGroup.styleBreakdown, 'styleOrderedValue');
              styleGroup['groupShippedValue'] = _.sumBy(styleGroup.styleBreakdown, 'styleShippedValue');
              groupItems.push(styleGroup)
            });

            details['groupBreakdown'] = groupItems;
            let dateArray = [];
            _.each(details['groupBreakdown'], item => {
              _.each(item.earliestDates, date => {
                dateArray.push(moment(date))
              });
            })
            let maxDate = _.max(dateArray);
            details['maxDate'] = maxDate ? toDateString(maxDate) : null;

            details['maxDateFuture'] = moment(details['maxDate']).isAfter(new Date);
            if (!details['maxDateFuture']) {
              details['maxDate'] = toDateString(new Date);
            }
            details['totalOrderedQty'] = _.sumBy(details['groupBreakdown'], 'groupOrderedQty');
            details['totalShippedQty'] = _.sumBy(details['groupBreakdown'], 'groupShippedQty');
            details['totalOrderedValue'] = _.sumBy(details['groupBreakdown'], 'groupOrderedValue');
            details['totalShippedValue'] = _.sumBy(details['groupBreakdown'], 'groupShippedValue');
            return details;
          } else {
            return null;
          }

        })
      );
  }

  orderPlace(payload) {
    return this.http
      .patch(`${this.urlBase}/v2`, payload)
      .pipe(map((resp: any) => resp.data));
  }


  orderValidation(payload) {
    return this.http
      .patch(`${this.urlBase}/v2/validation`, payload)
      .pipe(map((resp: any) => resp.data));
  }

  getStripeKey() {
    return this.http.get(`stripes/public-key`).pipe(
      map((resp: any) => {
        return resp.data;
      })
    );
  }



}

import { NgModule } from "@angular/core";
import { CommonSharedModule } from "./common/common.module";
import { UtilityModule } from "./utility/utility.module";
import { ApiModule } from "./api/api.module";
import { AuthModule } from "app/authentication/auth.module";
import {
  Routes,
  RouterModule
} from "@angular/router";
import { GeneralModule } from "./general/general.module";

const routes: Routes = [
  {
    path: "",
    loadChildren: "app/modules-core/dashboard/dashboard.module#DashboardModule"
  },
  {
    path: "",
    loadChildren:
      "app/modules-core/merchandising/merchandising.module#MerchandisingModule"
  },
  {
    path: "",
    loadChildren: "app/modules-core/settings/settings.module#SettingsModule"
  },
  {
    path: "",
    loadChildren: "app/modules-core/profile/profile.module#ProfileModule"
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { anchorScrolling: 'enabled' }),
    CommonSharedModule,
    UtilityModule,
    GeneralModule,
    ApiModule,
    AuthModule,

    //Tmp - Sidebar filters - entryComponents in lazy loaded modules not working
    //create a separate module and add here
  ],
  exports: [
  ]
})
export class CoreModules { }

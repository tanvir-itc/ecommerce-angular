import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { LoginCheckRouteGuard } from "app/authentication/check-login-route-guard";
import { MaterialModule } from "app/modules-core/utility/material.module";
import { TranslateModule } from "@ngx-translate/core";
import { FuseSharedModule } from "@fuse/shared.module";
import { DateTimeModule } from "app/modules-core/utility/date-time-picker/date-time.module";
import { CommonSharedModule } from "app/modules-core/common/common.module";
import { AttachmentModule } from "app/modules-core/common/attachment/attachment.module";
import { DashboardComponent } from "./pages/dashboard.component";
import { FuseHighlightModule } from "@fuse/components";


const routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  // {
  //   path: 'dashboard',
  //   redirectTo: 'styles',
  //   pathMatch: 'full'
  // },
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [LoginCheckRouteGuard]
  }
];

@NgModule({
  declarations: [
    DashboardComponent
  ],
  exports: [

  ],
  entryComponents: [
  ],
  imports: [
    MaterialModule,
    CommonSharedModule,
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MaterialModule,
    DateTimeModule,
    AttachmentModule,
    FuseHighlightModule,
  ]
})
export class DashboardModule { }

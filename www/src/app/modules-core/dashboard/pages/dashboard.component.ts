import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { fuseAnimations } from "@fuse/animations";
import * as _ from "lodash";
import * as moment from 'moment';
import { ListPageBaseClass } from "app/modules-core/utility/list-page-base-class";
import { QuickPanelSubjectService } from "app/layout/components/quick-panel/quick-panel-subject.service";
import { AuthService } from "app/authentication/auth.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  animations: fuseAnimations
})
export class DashboardComponent extends ListPageBaseClass implements OnInit {

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private _router: Router,
    private _quickPanelSubjectService: QuickPanelSubjectService,
    public _auth: AuthService,
  ) {
    super();
    this._quickPanelSubjectService.heading.next('Dashboard');
  }

  cardInfo = <any>{};

  ngOnInit() {
    let userRole = _.chain(this._auth.getCurrentUser()).get('roles').head().get('name').value();
    if (userRole == 'ROLE_WAREHOUSE') {
      this._router.navigate(['/shipping/pending']);
    } else if (userRole == 'ROLE_SUPPLIER') {
      this._router.navigate(['/factory-shipments']);
    } else {
      this._router.navigate(['/catalogue']);
    }
  }

  getCardInfo() {

  }


}

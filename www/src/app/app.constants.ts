export const DEBUG_INFO_ENABLED: boolean = true;

//TODO - this has already been moved to api.constants.ts
//after all services get rid of usage of SERVER_API_URL, delete the following from this file
export const APP_URL = `//${window.location.hostname}${getFrontendPort(window.location.port)}/`;
export const SERVER_API_URL = `//${window.location.hostname}:${getBackendPort(window.location.port)}/api/`;
function getBackendPort(port = '') {
  const portLocal = 9395;

  const portMap = {
    80: 9395,
    8003: 9003,
    8002: 9002,
    '': 9395
  };

  //TODO - check why this is returning undefined and not portLocal in local env on forhads machine
  // return portMap[port] || portLocal;
  return portMap[port] ? portMap[port] : portLocal;
}

function getFrontendPort(port) {
  const portLocal = port ? `:${port}` : '';
  return portLocal;
}

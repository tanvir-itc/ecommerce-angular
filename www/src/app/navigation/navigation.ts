import { FuseNavigation } from "@fuse/types";

export const navigation: FuseNavigation[] = [
  {
    id: "general",
    title: "App",
    translate: "NAV.APPLICATIONS",
    type: "group",
    children: [
      {
        id: "dashboard",
        title: "Dashboard",
        translate: "NAV.DASHBOARD",
        type: "item",
        icon: "dashboard",
        image: "dashboard.png",
        url: "dashboard",
        permission: "PERMISSION_MENU_DASHBOARD"
      },
      {
        id: "catalogue",
        title: "Catalogue",
        translate: "NAV.STYLES.TITLE",
        type: "item",
        icon: "vpn_lock",
        image: "Catalogue.png",
        url: "/catalogue",
        permission: "PERMISSION_MENU_CATALOGUE_VIEW"
      },
      {
        id: "digital-catalogue",
        title: "eCatalogue",
        translate: "NAV.ORDERS.TITLE",
        type: "item",
        icon: "vpn_lock",
        image: "digital-pdf.png",
        url: "/digital-catalogue",
        permission: "PERMISSION_MENU_DIGITAL_CATALOG"
      },
      {
        id: "styles",
        title: "Style Editor",
        translate: "NAV.STYLES.TITLE",
        type: "item",
        icon: "vpn_lock",
        image: "Style_Edititor_2.png",
        url: "/styles",
        permission: "PERMISSION_MENU_STYLE_VIEW"
      },


      {
        id: "stock",
        title: "Inventory",
        translate: "NAV.USERS.TITLE",
        type: "collapsable",
        icon: "vpn_lock",
        image: "Stock.png",
        permission: "PERMISSION_MENU_STOCK",
        children: [

          {
            id: "inventories",
            title: "Quick View",
            translate: "NAV.STYLES.TITLE",
            type: "item",
            icon: "vpn_lock",
            image: "Inventory.png",
            url: "/inventories",
            permission: "PERMISSION_MENU_INVENTORY_VIEW"
          },
          {
            id: "inventory-edit",
            title: "Management",
            translate: "NAV.STYLES.TITLE",
            type: "item",
            icon: "vpn_lock",
            image: "Inventory.png",
            url: "/inventory-edit",
            permission: "PERMISSION_MENU_INVENTORY_MANAGEMENT_VIEW"
          },

          // {
          //   id: "inventory-history",
          //   title: "History",
          //   translate: "NAV.STYLES.TITLE",
          //   type: "item",
          //   icon: "vpn_lock",
          //   image: "Inventory.png",
          //   url: "/inventory-history",
          //   permission: "PERMISSION_MENU_INVENTORY_HISTORY_VIEW"
          // },
          // {
          //   id: "inventory-transfer",
          //   title: "Transfer",
          //   translate: "NAV.ORDERS.TITLE",
          //   type: "item",
          //   icon: "vpn_lock",
          //   image: "Inventory_Transfer.png",
          //   url: "/inventory-transfer",
          //   permission: "PERMISSION_MENU_STOCK_TRANSFER"
          // },
        ]
      },
      {
        id: "supplier",
        title: "Supplier",
        translate: "NAV.USERS.TITLE",
        type: "collapsable",
        icon: "vpn_lock",
        image: "Stock.png",
        permission: "PERMISSION_MENU_SUPPLIER",
        children: [

          {
            id: "suppliers",
            title: "List",
            translate: "NAV.STYLES.TITLE",
            type: "item",
            icon: "vpn_lock",
            image: "Inventory.png",
            url: "/suppliers",
            permission: "PERMISSION_MENU_SUPPLIER_LIST"
          },

          {
            id: "po",
            title: "PO",
            translate: "NAV.STYLES.TITLE",
            type: "item",
            icon: "vpn_lock",
            image: "Inventory.png",
            url: "/po",
            permission: "PERMISSION_PO_VIEW"
          },
          {
            id: "factory-shipments",
            title: "Shipment",
            translate: "NAV.ORDERS.TITLE",
            type: "item",
            icon: "vpn_lock",
            image: "Warehouse_User.png",
            url: "/factory-shipments",
            permission: "PERMISSION_MENU_SUPPLIER_SHIPMENT_VIEW"
          },
        ]
      },
      {
        id: "orders",
        title: "Order History",
        translate: "NAV.ORDERS.TITLE",
        type: "item",
        icon: "vpn_lock",
        image: "new_order_history.png",
        url: "/orders",
        permission: "PERMISSION_MENU_ORDER_VIEW"
      },
      {
        id: "shipping",
        title: "Shipping",
        translate: "NAV.ORDERS.TITLE",
        type: "item",
        icon: "vpn_lock",
        image: "Shipping.png",
        url: "/shipping",
        permission: "PERMISSION_MENU_SHIPMENT_VIEW"
      },

      {
        id: "invoice",
        title: "Invoices",
        translate: "NAV.ORDERS.TITLE",
        type: "item",
        icon: "vpn_lock",
        image: "new_invoice.png",
        url: "/invoice",
        permission: "PERMISSION_MENU_INVOICE_VIEW"
      },
      {
        id: "customers",
        title: "Customers",
        translate: "NAV.ORDERS.TITLE",
        type: "item",
        icon: "vpn_lock",
        image: "Customers.png",
        url: "/customers",
        permission: "PERMISSION_MENU_CUSTOMER_VIEW"
      },
      {
        id: "commission",
        title: "Earnings",
        translate: "NAV.ORDERS.TITLE",
        type: "item",
        icon: "vpn_lock",
        image: "earning.png",
        url: "/commission-list",
        permission: "PERMISSION_MENU_COMMISSION_VIEW"
      },

      {
        id: "alluser",
        title: "Users",
        translate: "NAV.USERS.TITLE",
        type: "collapsable",
        icon: "vpn_lock",
        image: "All_Users.png",
        permission: "PERMISSION_MENU_ALL_USER_VIEW",
        children: [
          {
            id: "investor",
            title: "Investor",
            translate: "NAV.ORDERS.TITLE",
            type: "item",
            icon: "vpn_lock",
            image: "Partner.png",
            url: "/investors",
            permission: "PERMISSION_MENU_INVESTOR"
          },
          {
            id: "partner",
            title: "Sales manager",
            translate: "NAV.ORDERS.TITLE",
            type: "item",
            icon: "vpn_lock",
            image: "Partner.png",
            url: "/sales-managers",
            permission: "PERMISSION_MENU_PARTNER"
          },
          {
            id: "sales-rep",
            title: "Sales rep",
            translate: "NAV.ORDERS.TITLE",
            type: "item",
            icon: "vpn_lock",
            image: "Sales_Reps.png",
            url: "/sales-rep",
            permission: "PERMISSION_MENU_SALE_REPRESENTATIVE"
          },
          {
            id: "warehouse-user",
            title: "Warehouse",
            translate: "NAV.ORDERS.TITLE",
            type: "item",
            icon: "vpn_lock",
            image: "Warehouse_User.png",
            url: "/warehouse-user",
            permission: "PERMISSION_MENU_WAREHOUSE_USER"
          },
          {
            id: "factory-user",
            title: "Supplier",
            translate: "NAV.ORDERS.TITLE",
            type: "item",
            icon: "vpn_lock",
            image: "Warehouse_User.png",
            url: "/factory-user",
            permission: "PERMISSION_MENU_WAREHOUSE_USER"
          },

          {
            id: "users",
            title: "All",
            translate: "NAV.USERS.TITLE",
            type: "item",
            icon: "email",
            image: "Users.png",
            url: "/users",
            permission: "PERMISSION_MENU_USER_VIEW"
          },
        ]

      },
      {
        id: "login-history",
        title: "Login History",
        translate: "NAV.ORDERS.TITLE",
        type: "item",
        icon: "vpn_lock",
        image: "Login_History.png",
        url: "/login-history",
        permission: "PERMISSION_ADMIN_USER"
      },



    ]
  },
];
